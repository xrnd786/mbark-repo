//
//  DefaultDataModel.h
//  mBark
//
//  Created by Xiphi Tech on 16/05/2015.
//
//

#import <Foundation/Foundation.h>

@interface DefaultDataModel : NSObject

@property (nonatomic, retain) NSString * defaultImage;
@property (nonatomic, retain) NSString * applicationVersion;


@end
