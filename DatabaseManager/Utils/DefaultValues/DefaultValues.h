//
//  DefaultValues.h
//  DataHelper
//
//  Created by Xiphi Tech on 16/10/2014.
//
//

#import <Foundation/Foundation.h>

#import "DefaultValues.h"
#import "DefaultDataModel.h"

@interface DefaultValues : NSObject


+(DefaultDataModel*)getDefaultDataModel;

@end
