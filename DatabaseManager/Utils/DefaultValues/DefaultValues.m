//
//  DefaultValues.m
//  DataHelper
//
//  Created by Xiphi Tech on 16/10/2014.
//
//

#import "DefaultValues.h"

#define USER_IMAGE @"defaultProfile.png"
#define APPLICATION_VERSION @"1.0.1"

@implementation DefaultValues


+(DefaultDataModel*)getDefaultDataModel{
    
    DefaultDataModel *defaultModel=[[DefaultDataModel alloc]init];
    
    defaultModel.defaultImage=USER_IMAGE;
    defaultModel.applicationVersion=APPLICATION_VERSION;
    
    return defaultModel;

}

@end

