//
//  DeviceUtils.h
//  DataHelper
//
//  Created by Xiphi Tech on 16/10/2014.
//
//

#import <Foundation/Foundation.h>

#import "DeviceUtilsModel.h"

#define DEVICEISIPAD1 @"Ipad1"
#define DEVICEISIPAD2 @"Ipad2"
#define DEVICEISIPAD3 @"Ipad3"
#define DEVICEISIPHONE4S @"Iphone4S"
#define DEVICEISIPHONE5 @"Iphone5"
#define DEVICEISIPHONE5C @"Iphone5C"
#define DEVICEISIPHONE5S @"Iphone5S"
#define DEVICEISIPHONE6 @"Iphone6"
#define DEVICEISIPHONE6Plus @"Iphone6Plus"


@interface DeviceUtils : NSObject


+(DeviceUtilsModel*)getDeviceUtilsModel;

@end
