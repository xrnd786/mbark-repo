//
//  DeviceUtilsModel.h
//  mBark
//
//  Created by Xiphi Tech on 16/05/2015.
//
//

#import <Foundation/Foundation.h>

@interface DeviceUtilsModel : NSObject



@property (strong, nonatomic) NSString * deviceModel;
@property (strong, nonatomic) NSString * deviceSerialNumber;
@property (strong, nonatomic) NSString * deviceToken;
@property (strong, nonatomic) NSString * deviceOS;
@property (strong, nonatomic) NSString * deviceOSVersion;
@property (strong, nonatomic) NSString * appVersion;


@end
