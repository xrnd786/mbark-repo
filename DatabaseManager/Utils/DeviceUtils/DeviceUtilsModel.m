//
//  DeviceUtilsModel.m
//  mBark
//
//  Created by Xiphi Tech on 16/05/2015.
//
//

#import "DeviceUtilsModel.h"

@implementation DeviceUtilsModel

@synthesize  deviceModel;
@synthesize  deviceSerialNumber;
@synthesize  deviceToken;
@synthesize  deviceOS;
@synthesize  deviceOSVersion;
@synthesize  appVersion;


@end
