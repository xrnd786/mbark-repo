//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//


#import "AppAttributesDefaults.h"
#import "Attribute.h"

#define JSON_RES_AppAttributes_Name @"Name"
#define JSON_RES_AppAttributes_Type @"Type"
#define JSON_RES_AppAttributes_Group @"Group"
#define JSON_RES_AppAttributes_Groupable @"Groupable"
#define JSON_RES_AppAttributes_Id @"Id"
#define JSON_RES_AppAttributes_IsDeleted @"IsDeleted"
#define JSON_RES_AppAttributes_TenantId @"TenantId"
#define JSON_RES_AppAttributes_CreatedBy @"CreatedBy"
#define JSON_RES_AppAttributes_ModifiedBy @"ModifiedBy"
#define JSON_RES_AppAttributes_ModifiedOn @"ModifiedOn"


@implementation AppAttributesDefaults

+(void)saveTotalCount:(int)count{
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",count] forKey:[NSString stringWithFormat:@"attributes_count"]];
    [[NSUserDefaults standardUserDefaults]synchronize];
 
}

+(void)saveAppAttributes:(NSDictionary*)appAttributesDictinary withAttributeNumber:(int)number{
    
    DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass: [Attribute class]];
    Attribute *attribute = [parser parseDictionary:appAttributesDictinary];

   
    NSData *data=[NSKeyedArchiver archivedDataWithRootObject:attribute];
   [[NSUserDefaults standardUserDefaults]setObject:data forKey:[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_attributesValue"]];
   [[NSUserDefaults standardUserDefaults]synchronize];

}


+(NSString*)getName:(int)number{
    
    
    NSData *data=[[NSUserDefaults standardUserDefaults]objectForKey:[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_attributesValue"]];
    
    Attribute *attribute=(Attribute*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return  attribute.Name;
    
}

+(NSString*)getType:(int)number{
    
    NSData *data=[[NSUserDefaults standardUserDefaults]objectForKey:[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_attributesValue"]];
    
    Attribute *attribute=(Attribute*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return  attribute.Type;

}

+(NSString*)getGroup:(int)number{
    
    NSData *data=[[NSUserDefaults standardUserDefaults]objectForKey:[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_attributesValue"]];
    
    Attribute *attribute=(Attribute*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return  attribute.Group;
    
}

/*
+(BOOL)getIsGroupable:(int)number{
    
    NSData *data=[[NSUserDefaults standardUserDefaults]objectForKey:[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_attributesValue"]];
    
    Attribute *attribute=(Attribute*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return  [attribute.isGroupable boolValue];
    
}*/


@end
