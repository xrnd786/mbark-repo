//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

@interface AppAttributesDefaults : NSObject

//Set Data
+(void)saveTotalCount:(int)count;
+(void)saveAppAttributes:(NSDictionary*)appAttributesDictinary withAttributeNumber:(int)number;


//Get data
+(NSString*)getName:(int)number;
+(NSString*)getType:(int)number;
+(NSString*)getGroup:(int)number;
//+(BOOL)getIsGroupable:(int)number;

@end
