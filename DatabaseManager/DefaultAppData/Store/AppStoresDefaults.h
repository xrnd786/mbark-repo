//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>


#define JSON_RES_Store_Id @"Id"
#define JSON_RES_Store_Name @"Name"
#define JSON_RES_Store_Address @"Address"
#define JSON_RES_Store_City @"City"
#define JSON_RES_Store_Pincode @"Pincode"
#define JSON_RES_Store_Country @"Country"
#define JSON_RES_Store_Location @"Location"
#define JSON_RES_Store_Latitude @"Latitude"
#define JSON_RES_Store_Longitude @"Longitude"

#define JSON_RES_Store_Contact @"Contact"
#define JSON_RES_Store_Email @"Email"



@interface AppStoresDefaults : NSObject


+(void)saveStoreDetails:(NSArray*)stores;

+(NSArray*)getAllStores;


@end
