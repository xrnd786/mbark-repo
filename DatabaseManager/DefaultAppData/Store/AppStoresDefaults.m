//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import "AppStoresDefaults.h"

#import "Store.h"
#import "StoreMapper.h"

@implementation AppStoresDefaults


+(void)saveStoreDetails:(NSArray*)stores{
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:(int)[stores count]]  forKey:@"stores_count"];
    
    for (NSDictionary *sliderData in stores) {
   
        
        Store *store=[[StoreMapper new]mapThisServerDictionaryToStore:sliderData];
        
        NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:store];
        
        [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:[NSString stringWithFormat:@"store_%d",[stores indexOfObject:sliderData]]];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
   
    
}

+(NSArray*)getAllStores{
    
    int count=[[[NSUserDefaults standardUserDefaults] objectForKey:@"stores_count"] integerValue];

    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    for (int i=0;i<count;i++) {
        
        NSData *data = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"store_%d",i]];
        Store *store=[NSKeyedUnarchiver unarchiveObjectWithData:data];

        [array addObject:store];
    }
    
    return (NSArray*)array;
    
}


@end
