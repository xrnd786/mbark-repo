//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

@interface AppSettingDefaults : NSObject

//SET VALUES
+(void)saveAppSettings:(NSDictionary*)appSettingsDictinary;



//GET VALUES

+(NSString*)getProductDisplayName;
+(NSString*)getCategoryDisplayName;
+(NSString*)getApplicationDisplayName;
+(NSString*)getAppModel;
+(NSString*)getRegistrationType;

//FIRST NAME

+(BOOL)isFNameCaptured;
+(BOOL)isFNameRequired;

//LAST NAME
+(BOOL)isLNameCaptured;
+(BOOL)isLNameRequired;


//ORGANIZATION
+(BOOL)isOrganizationCaptured;
+(BOOL)isOrganizationRequired;


//MOBILE
+(BOOL)isMobileCaptured;
+(BOOL)isMobileRequired;


//EMAIL
+(BOOL)isEmailCaptured;
+(BOOL)isEmailRequired;


//GENDER
+(BOOL)isGenderCaptured;
+(BOOL)isGenderRequired;

//ADDRESS
+(BOOL)isAddressCaptured;
+(BOOL)isAddressRequired;


//PINCODE
+(BOOL)isPinCodeCaptured;
+(BOOL)isPinCodeRequired;

//COUNTRY
+(BOOL)isCountryCaptured;
+(BOOL)isCountryRequired;

//LOCATION
+(BOOL)isLocationCaptured;
+(BOOL)isLocationRequired;

@end
