//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//


#import "AppSettingDefaults.h"

#define JSON_RES_AppSettings_ProductsDisplayName @"ProductsDisplayName"
#define JSON_RES_AppSettings_CategoriesDisplayName @"CategoriesDisplayName"
#define JSON_RES_AppSettings_ApplicationsDisplayName @"ApplicationsDisplayName"
#define JSON_RES_AppSettings_AppModel @"AppModel"
#define JSON_RES_AppSettings_RegistrationType @"RegistrationType"
#define JSON_RES_AppSettings_RegistrationRules @"RegistrationRules"
#define JSON_RES_AppSettings_Parameter @"Parameter"
#define JSON_RES_AppSettings_Capture @"Capture"
#define JSON_RES_AppSettings_Required @"Required"
#define JSON_RES_AppSettings_Value @"Value"
#define JSON_RES_AppSettings_ProductRules @"ProductRules"
#define JSON_RES_AppSettings_UserRules @"UserRules"

#define PARAMETER_FNAME @"FirstName"
#define PARAMETER_LNAME @"LastName"
#define PARAMETER_Organization @"Organization"
#define PARAMETER_Mobile @"Mobile"
#define PARAMETER_Email @"Email"
#define PARAMETER_Gender @"Gender"
#define PARAMETER_Address @"Address"
#define PARAMETER_Pincode @"Pincode"
#define PARAMETER_Country @"Country"
#define PARAMETER_Location @"Location"



@implementation AppSettingDefaults


+(void)saveAppSettings:(NSDictionary*)appSettingsDictinary{
    
    for (NSString *sting in [self getArrayOfConstants]) {
        
        id value=[appSettingsDictinary objectForKey:sting];
        [[NSUserDefaults standardUserDefaults]setObject:(value!=[NSNull null])?value:@"" forKey:sting];
        
    }
    
    for (NSDictionary *registration in [appSettingsDictinary objectForKey:JSON_RES_AppSettings_RegistrationRules]) {
        
        NSString *parameter=[registration objectForKey:JSON_RES_AppSettings_Parameter];
        
        [[NSUserDefaults standardUserDefaults]setObject:([registration objectForKey:JSON_RES_AppSettings_Capture]!=[NSNull null])?[registration objectForKey:JSON_RES_AppSettings_Capture]:@"" forKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_AppSettings_Capture]];
      
        [[NSUserDefaults standardUserDefaults]setObject:([registration objectForKey:JSON_RES_AppSettings_Required]!=[NSNull null])?[registration objectForKey:JSON_RES_AppSettings_Required]:@"" forKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_AppSettings_Required]];
        
        
    }
    
    
    for (NSDictionary *products in [appSettingsDictinary objectForKey:JSON_RES_AppSettings_ProductRules])
    {
        
        NSString *parameter=[products objectForKey:JSON_RES_AppSettings_Parameter];
        
        [[NSUserDefaults standardUserDefaults]setObject:([products objectForKey:JSON_RES_AppSettings_Value]!=[NSNull null])?[products objectForKey:JSON_RES_AppSettings_Value]:@"" forKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_AppSettings_Value]];
        
    }
    
    
    for (NSDictionary *users in [appSettingsDictinary objectForKey:JSON_RES_AppSettings_UserRules]) {
        
        NSString *parameter=[users objectForKey:JSON_RES_AppSettings_Parameter];
        [[NSUserDefaults standardUserDefaults]setObject:([users objectForKey:JSON_RES_AppSettings_Value]!=[NSNull null])?[users objectForKey:JSON_RES_AppSettings_Value]:@"" forKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_AppSettings_Value]];
   
    }

    [[NSUserDefaults standardUserDefaults]synchronize];

}


+(NSString*)getProductDisplayName{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_AppSettings_ProductsDisplayName];
    
}

+(NSString*)getCategoryDisplayName{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_AppSettings_CategoriesDisplayName];
    
}

+(NSString*)getApplicationDisplayName{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_AppSettings_ApplicationsDisplayName];
    
}

+(NSString*)getAppModel{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_AppSettings_AppModel];
    
}

+(NSString*)getRegistrationType{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_AppSettings_RegistrationType];
    
}


//FIRST NAME
+(BOOL)isFNameCaptured{
    
    return [self isCaptured:PARAMETER_FNAME];
}

+(BOOL)isFNameRequired{
    
    return  [self isRequired:PARAMETER_FNAME];
    
}


//LAST NAME

+(BOOL)isLNameCaptured{
    
    return [self isCaptured:PARAMETER_LNAME];
    
}

+(BOOL)isLNameRequired{
    
    return  [self isRequired:PARAMETER_LNAME];
    
}

//ORGANIZATION

+(BOOL)isOrganizationCaptured{
    
    return [self isCaptured:PARAMETER_Organization];
    
}

+(BOOL)isOrganizationRequired{
    
    return  [self isRequired:PARAMETER_Organization];
    
}

//MOBILE


+(BOOL)isMobileCaptured{
    
    return [self isCaptured:PARAMETER_Mobile];
    
}

+(BOOL)isMobileRequired{
    
    return  [self isRequired:PARAMETER_Mobile];
    
}

//EMAIL

+(BOOL)isEmailCaptured{
    
    return [self isCaptured:PARAMETER_Email];
    
}

+(BOOL)isEmailRequired{
    
    return  [self isRequired:PARAMETER_Email];
    
}


//GENDER
+(BOOL)isGenderCaptured{
    
    return [self isCaptured:PARAMETER_Gender];
    
}

+(BOOL)isGenderRequired{
    
    return  [self isRequired:PARAMETER_Gender];
    
}


//ADDRESS
+(BOOL)isAddressCaptured{
    
    return [self isCaptured:PARAMETER_Address];
    
}

+(BOOL)isAddressRequired{
    
    return  [self isRequired:PARAMETER_Address];
    
}



//PINCODE
+(BOOL)isPinCodeCaptured{
    
    return [self isCaptured:PARAMETER_Pincode];
    
}

+(BOOL)isPinCodeRequired{
    
    return  [self isRequired:PARAMETER_Pincode];
    
}


//COUNTRY
+(BOOL)isCountryCaptured{
    
    return [self isCaptured:PARAMETER_Country];
    
}

+(BOOL)isCountryRequired{
    
    return  [self isRequired:PARAMETER_Country];
    
}

//LOCATION
+(BOOL)isLocationCaptured{
    
    return [self isCaptured:PARAMETER_Location];
    
}

+(BOOL)isLocationRequired{
    
    return  [self isRequired:PARAMETER_Location];
    
}


+(NSMutableArray*)getArrayOfConstants{
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    [array addObject:JSON_RES_AppSettings_ProductsDisplayName];
    [array addObject:JSON_RES_AppSettings_CategoriesDisplayName];
    [array addObject:JSON_RES_AppSettings_ApplicationsDisplayName];
    [array addObject:JSON_RES_AppSettings_RegistrationType];
    [array addObject:JSON_RES_AppSettings_AppModel];
    
    return array;
    
}

+(BOOL)isCaptured:(NSString*)parameter{
    
    return  [[[NSUserDefaults standardUserDefaults]objectForKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_AppSettings_Capture]] boolValue];
}

+(BOOL)isRequired:(NSString*)parameter{
    
    return  [[[NSUserDefaults standardUserDefaults]objectForKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_AppSettings_Required]] boolValue];

}




@end
