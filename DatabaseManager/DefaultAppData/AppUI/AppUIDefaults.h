//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

@interface AppUIDefaults : NSObject

//Set data
+(void)saveAppUIDetails:(NSDictionary*)appUIDictinary;


//get data
+(NSString*)getPrimaryColor;
+(NSString*)getSecondaryColor;
+(NSString*)getNotificationColor;

@end
