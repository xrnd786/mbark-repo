//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import "AppUIDefaults.h"


#define JSON_RES_AppUI_PrimaryColor @"PrimaryColor"
#define JSON_RES_AppUI_SecondaryColor @"SecondaryColor"
#define JSON_RES_AppUI_GeneralFont @"GeneralFont"
#define JSON_RES_AppUI_SmallFont @"SmallFont"
#define JSON_RES_AppUI_Font_family @"Family"
#define JSON_RES_AppUI_Font_size @"Size"
#define JSON_RES_AppUI_HeaderFont @"HeaderFont"
#define JSON_RES_AppUI_PrimaryTextColor @"PrimaryTextColor"
#define JSON_RES_AppUI_SecondaryTextColor @"SecondaryTextColor"
#define JSON_RES_AppUI_BackgroundColor @"BackgroundColor"
#define JSON_RES_AppUI_MenuColor @"MenuColor"

#define PLIST_KEY_PrimaryColor @"primaryColor"
#define PLIST_KEY_SecondaryColor @"secondaryColor"
#define PLIST_KEY_GeneralFont @"generalFont"
#define PLIST_KEY_GeneralFontSize @"generalFontSize"
#define PLIST_KEY_HeaderFont @"headerFont"
#define PLIST_KEY_HeaderFontSize @"headerFontSize"
#define PLIST_KEY_SmallFont @"smallFont"
#define PLIST_KEY_SmallFontSize @"smallFontSize"
#define PLIST_KEY_PrimaryTextColor @"primaryTextColor"
#define PLIST_KEY_SecondaryTextColor @"secondaryTextColor"
#define PLIST_KEY_BackroundColor @"backgroundColor"
#define PLIST_KEY_MenuColor @"menuColor"
#define PLIST_KEY_lightColor @"lightColor"


#define PLIST_KEY_FavouriteColor @"favouriteColor"
#define PLIST_KEY_CheckColor @"checkColor"
#define PLIST_KEY_lightColor @"lightColor"
#define PLIST_KEY_notification @"notification"
#define PLIST_KEY_menu @"menu"
#define PLIST_KEY_check @"check"
#define PLIST_KEY_back @"back"
#define PLIST_KEY_enquire @"enquire"
#define PLIST_KEY_home @"home"
#define PLIST_KEY_search @"search"
#define PLIST_KEY_store @"store"
#define PLIST_KEY_toTop @"toTop"
#define PLIST_KEY_clearHistory @"clearHistory"
#define PLIST_KEY_favourite @"favourite"
#define PLIST_KEY_browseBy @"browseBy"
#define PLIST_KEY_filter @"filter"
#define PLIST_KEY_filterapplied @"filterapplied"
#define PLIST_KEY_close @"close"
#define PLIST_KEY_myBag @"myBag"
#define PLIST_KEY_usernameimage @"usernameimage"
#define PLIST_KEY_passwordimage @"passwordimage"
#define PLIST_KEY_passwordrepeatimage @"passwordrepeatimage"
#define PLIST_KEY_addtocart @"addtocart"
#define PLIST_KEY_defaultProfile @"defaultProfile"
#define PLIST_KEY_gridView @"gridView"
#define PLIST_KEY_listView @"listView"
#define PLIST_KEY_edit @"edit"
#define PLIST_KEY_add @"add"
#define PLIST_KEY_send @"send"
#define PLIST_KEY_share @"share"

#define PLIST_KEY_email @"email"
#define PLIST_KEY_mobile @"mobile"
#define PLIST_KEY_organization @"organization"

#define PLIST_KEY_MessageTitleColor @"messagetitleColor"

#define default_primaryColor @"#c4414c"
#define default_secondaryColor @"#302b2c"
#define default_notificationColor @"#F7D54D"
#define default_messageTitleColor @"#0F0F0F"

#define default_primaryTextColor @"#c4414c"
#define default_secondaryTextColor @"#302b2c"

#define default_generalFont @"Arial"
#define default_generalFontSize @"14"

#define default_headerFont @"Arial"
#define default_headerFontSize @"17"

#define default_smallFont @"Arial"
#define default_smallFontSize @"12"

#define default_menuColor @"#EDF0F1"
#define default_backgroundColor @"#1A2531"


@implementation AppUIDefaults

static NSDictionary *myUIDictionary;


+(NSDictionary*)getDictionary{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"Default.plist"];
    NSMutableDictionary *dictplist;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        
        dictplist = [[[NSMutableDictionary alloc]initWithContentsOfFile:plistPath] mutableCopy];
        return dictplist;
    
    }
    
    return nil;
}

+(void)saveAppUIDetails:(NSDictionary*)appUIDictinary{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"Default.plist"];
    NSMutableDictionary *dictplist;
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        
        NSLog(@"EXISTS");
        dictplist = [[[NSMutableDictionary alloc]initWithContentsOfFile:plistPath] mutableCopy];
        
    }else
    {
        dictplist = [[NSMutableDictionary alloc ] init];
        [self saveImagesValues:dictplist];
        
    }
    
    [dictplist setObject:([appUIDictinary objectForKey:JSON_RES_AppUI_PrimaryColor]!=nil)?[appUIDictinary objectForKey:JSON_RES_AppUI_PrimaryColor]:default_primaryColor forKey:PLIST_KEY_PrimaryColor];
    
    
    [dictplist setObject:([appUIDictinary objectForKey:JSON_RES_AppUI_SecondaryTextColor]!=nil)?[appUIDictinary objectForKey:JSON_RES_AppUI_SecondaryTextColor]:default_secondaryColor forKey:PLIST_KEY_SecondaryColor];
    
    [dictplist setObject:([appUIDictinary objectForKey:JSON_RES_AppUI_SecondaryColor]!=nil)?[appUIDictinary objectForKey:JSON_RES_AppUI_SecondaryColor]:default_notificationColor forKey:PLIST_KEY_notification];
    
    
    [dictplist setObject:([[appUIDictinary objectForKey:JSON_RES_AppUI_GeneralFont] objectForKey:JSON_RES_AppUI_Font_family]!=nil)?[[appUIDictinary objectForKey:JSON_RES_AppUI_GeneralFont] objectForKey:JSON_RES_AppUI_Font_family]:default_generalFont forKey:PLIST_KEY_GeneralFont];
    
    [dictplist setObject:([[appUIDictinary objectForKey:JSON_RES_AppUI_GeneralFont] objectForKey:JSON_RES_AppUI_Font_size]!=nil)?[[appUIDictinary objectForKey:JSON_RES_AppUI_GeneralFont] objectForKey:JSON_RES_AppUI_Font_size]:default_generalFontSize forKey:PLIST_KEY_GeneralFontSize];
    
    [dictplist setObject:([[appUIDictinary objectForKey:JSON_RES_AppUI_HeaderFont] objectForKey:JSON_RES_AppUI_Font_family]!=nil)?[[appUIDictinary objectForKey:JSON_RES_AppUI_HeaderFont] objectForKey:JSON_RES_AppUI_Font_family]:default_headerFont forKey:PLIST_KEY_HeaderFont];
    
    [dictplist setObject:([[appUIDictinary objectForKey:JSON_RES_AppUI_HeaderFont] objectForKey:JSON_RES_AppUI_Font_size]!=nil)?[[appUIDictinary objectForKey:JSON_RES_AppUI_HeaderFont] objectForKey:JSON_RES_AppUI_Font_size]:default_headerFontSize forKey:PLIST_KEY_HeaderFontSize];
   
    [dictplist setObject:([[appUIDictinary objectForKey:JSON_RES_AppUI_SmallFont] objectForKey:JSON_RES_AppUI_Font_family]!=nil)?[[appUIDictinary objectForKey:JSON_RES_AppUI_SmallFont] objectForKey:JSON_RES_AppUI_Font_family]:default_smallFont forKey:PLIST_KEY_SmallFont];
    
    [dictplist setObject:([[appUIDictinary objectForKey:JSON_RES_AppUI_SmallFont] objectForKey:JSON_RES_AppUI_Font_size]!=nil)?[[appUIDictinary objectForKey:JSON_RES_AppUI_SmallFont] objectForKey:JSON_RES_AppUI_Font_size]:default_smallFontSize forKey:PLIST_KEY_SmallFontSize];
    
    [dictplist setObject:([appUIDictinary objectForKey:JSON_RES_AppUI_PrimaryTextColor]!=nil)?[appUIDictinary objectForKey:JSON_RES_AppUI_PrimaryTextColor]:default_primaryTextColor forKey:PLIST_KEY_PrimaryTextColor];
   
    [dictplist setObject:([appUIDictinary objectForKey:JSON_RES_AppUI_SecondaryTextColor]!=nil)?[appUIDictinary objectForKey:JSON_RES_AppUI_SecondaryTextColor]:default_secondaryTextColor forKey:PLIST_KEY_SecondaryTextColor];
    
    [dictplist setObject:([appUIDictinary objectForKey:JSON_RES_AppUI_BackgroundColor]!=nil)?[appUIDictinary objectForKey:JSON_RES_AppUI_BackgroundColor]:default_backgroundColor forKey:PLIST_KEY_BackroundColor];
    
    [dictplist setObject:([appUIDictinary objectForKey:JSON_RES_AppUI_MenuColor]!=nil)?[appUIDictinary objectForKey:JSON_RES_AppUI_MenuColor]:default_menuColor forKey:PLIST_KEY_MenuColor];
   
    NSDictionary *newDictionary=[[NSDictionary alloc]initWithDictionary:dictplist];
    
    myUIDictionary=[[NSDictionary alloc]initWithDictionary:dictplist];
    
    [newDictionary writeToFile:plistPath atomically:YES];

    NSLog(@"Keys Before : \r%@",[dictplist allKeys]);
    NSLog(@"\rVlaues Before : \r%@",[dictplist allValues]);
    
}


+(void)saveImagesValues:(NSMutableDictionary*)dictPlist{
    
    [dictPlist setObject:@"#C13F2E" forKey:PLIST_KEY_FavouriteColor];
    
    [dictPlist setObject:@"#EFEFEF" forKey:PLIST_KEY_lightColor];
    [dictPlist setObject:@"#c4414c" forKey:PLIST_KEY_FavouriteColor];
    [dictPlist setObject:@"#62EBA3" forKey:PLIST_KEY_CheckColor];
    [dictPlist setObject:default_messageTitleColor forKey:PLIST_KEY_MessageTitleColor];
    
    [dictPlist setObject:@"notification.png" forKey:PLIST_KEY_notification];
    [dictPlist setObject:@"menu.png" forKey:PLIST_KEY_menu];
    [dictPlist setObject:@"check.png" forKey:PLIST_KEY_check];
    [dictPlist setObject:@"back.png" forKey:PLIST_KEY_back];
    
    [dictPlist setObject:@"enquire.png" forKey:PLIST_KEY_enquire];
    [dictPlist setObject:@"home.png" forKey:PLIST_KEY_home];
    [dictPlist setObject:@"search.png" forKey:PLIST_KEY_search];
    [dictPlist setObject:@"store.png" forKey:PLIST_KEY_store];
    
    [dictPlist setObject:@"toTop.png" forKey:PLIST_KEY_toTop];
    [dictPlist setObject:@"clearHistory.png" forKey:PLIST_KEY_clearHistory];
    [dictPlist setObject:@"favourite.png" forKey:PLIST_KEY_favourite];
    [dictPlist setObject:@"browseByCategory.png" forKey:PLIST_KEY_browseBy];
    
    [dictPlist setObject:@"filter.png" forKey:PLIST_KEY_filter];
    [dictPlist setObject:@"filterapplied.png" forKey:PLIST_KEY_filterapplied];
    
    [dictPlist setObject:@"close.png" forKey:PLIST_KEY_close];
    [dictPlist setObject:@"myBag.png" forKey:PLIST_KEY_myBag];
    [dictPlist setObject:@"addtocart.png" forKey:PLIST_KEY_addtocart];
    [dictPlist setObject:@"username.png" forKey:PLIST_KEY_usernameimage];

    [dictPlist setObject:@"email.png" forKey:PLIST_KEY_email];
    [dictPlist setObject:@"mobile.png" forKey:PLIST_KEY_mobile];
    [dictPlist setObject:@"organization.png" forKey:PLIST_KEY_organization];
    
    [dictPlist setObject:@"password.png" forKey:PLIST_KEY_passwordimage];
    [dictPlist setObject:@"passwordrepeat.png" forKey:PLIST_KEY_passwordrepeatimage];
    [dictPlist setObject:@"defaultProfile.png" forKey:PLIST_KEY_defaultProfile];
    [dictPlist setObject:@"grid.png" forKey:PLIST_KEY_gridView];
    [dictPlist setObject:@"list.png" forKey:PLIST_KEY_listView];
    [dictPlist setObject:@"add.png" forKey:PLIST_KEY_add];
    [dictPlist setObject:@"edit.png" forKey:PLIST_KEY_edit];
    [dictPlist setObject:@"send.png" forKey:PLIST_KEY_send];
    [dictPlist setObject:@"share.png" forKey:PLIST_KEY_share];

}


+(NSString*)getPrimaryColor{
    
   return  [myUIDictionary valueForKey:PLIST_KEY_PrimaryColor];

}

+(NSString*)getSecondaryColor{
    
    return  [myUIDictionary valueForKey:PLIST_KEY_SecondaryTextColor];

}

+(NSString*)getNotificationColor{
    
    return  [myUIDictionary valueForKey:PLIST_KEY_notification];
    
}

@end
