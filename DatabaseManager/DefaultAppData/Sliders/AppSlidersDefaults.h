//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

@interface AppSlidersDefaults : NSObject

//Set data
+(void)saveSliderDetails:(NSArray*)tenantDictinary;
+(void)saveSliderImageFor:(NSString*)indexNumber withData:(NSString*)raw_data;

//get data
+(NSMutableArray*)getSlidersImage;
+(NSMutableArray*)getSlidersImagesTypes;
+(NSMutableArray*)getSlidersImagesNavigateTo;
    

@end
