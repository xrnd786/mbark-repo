//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import "AppSlidersDefaults.h"


#define JSON_RES_Sliders_Image @"Image"
#define JSON_RES_Sliders_Type @"Type"
#define JSON_RES_Sliders_NavigateTo @"NavigateTo"
#define JSON_RES_Sliders_Image_Url @"Url"



@implementation AppSlidersDefaults


+(void)saveSliderDetails:(NSArray*)sliderDictinary{
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:(int)[sliderDictinary count]]  forKey:@"slider_count"];
    
    
    for (NSDictionary *sliderData in sliderDictinary) {
   
        NSString *parameter=[NSString stringWithFormat:@"%lu",(unsigned long)[sliderDictinary indexOfObject:sliderData]+1];
        
        [[NSUserDefaults standardUserDefaults]setObject:[[sliderData objectForKey:JSON_RES_Sliders_Image] objectForKey:JSON_RES_Sliders_Image] forKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_Sliders_Image_Url]];
        
        [[NSUserDefaults standardUserDefaults]setObject:[sliderData objectForKey:JSON_RES_Sliders_Type] forKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_Sliders_Type]];
        
        [[NSUserDefaults standardUserDefaults]setObject:([sliderData objectForKey:JSON_RES_Sliders_NavigateTo]!=[NSNull null])?[sliderData objectForKey:JSON_RES_Sliders_NavigateTo]:@""  forKey:[[parameter stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_Sliders_NavigateTo]];
        
    }
   
   
    [[NSUserDefaults standardUserDefaults]synchronize];
    // NSLog(@"DIC REPRE %@",[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]);

}

+(void)saveSliderImageFor:(NSString*)indexNumber withData:(NSString*)raw_data{

    [[NSUserDefaults standardUserDefaults]setObject:raw_data  forKey:[[indexNumber stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_Sliders_Image_Url]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(NSMutableArray*)getSlidersImage{
    
    int numberOfSliders=(int)[[[NSUserDefaults standardUserDefaults]objectForKey:@"slider_count"] integerValue];
    
    NSMutableArray *images=[[NSMutableArray alloc]init];
    
    
    for (int i=1; i<=numberOfSliders; i++) {
        
      [images addObject:[[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",i] stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_Sliders_Image_Url]]];
        
    }

    return images;
}


+(NSMutableArray*)getSlidersImagesTypes{
    
    int numberOfSliders=(int)[[[NSUserDefaults standardUserDefaults]objectForKey:@"slider_count"] integerValue];
    NSMutableArray *images=[[NSMutableArray alloc]init];
    
    for (int i=1; i<=numberOfSliders; i++) {
        
        [images addObject:[[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",i] stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_Sliders_Type]]];
        
    }
    
    return images;
}


+(NSMutableArray*)getSlidersImagesNavigateTo{
    
    int numberOfSliders=(int)[[[NSUserDefaults standardUserDefaults]objectForKey:@"slider_count"] integerValue];
    NSMutableArray *images=[[NSMutableArray alloc]init];
    
    for (int i=1; i<=numberOfSliders; i++) {
        
        NSString *value=[[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",i] stringByAppendingString:@"_"] stringByAppendingString:JSON_RES_Sliders_NavigateTo]];
        
        [images addObject:value];
        
    }
    
    return images;
}


@end
