//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

@interface AppSearchDefaults : NSObject

+(void)saveSearchText:(NSString*)searched;

+(NSArray*)getSearhcedTexts;
+(void)deleteSearchTexts;


@end
