//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//


#import "AppSearchDefaults.h"

#define USERDEFAULT_SEARCH @"USERDEFAULT_SEARCH"

@interface AppSearchDefaults()

@end

@implementation AppSearchDefaults

+(void)saveSearchText:(NSString*)searched{
    
    
    NSMutableArray *searchedArray=[[NSMutableArray alloc]initWithArray:[self getSearhcedTexts]];
    
    if ([searchedArray containsObject:searched]) {
        
        [searchedArray removeObject:searched];
        
    }
    
    [searchedArray addObject:searched];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:searchedArray forKey:USERDEFAULT_SEARCH];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(NSArray*)getSearhcedTexts{
    
   return [[NSUserDefaults standardUserDefaults]objectForKey:USERDEFAULT_SEARCH];
    
}

+(void)deleteSearchTexts{
    
    
    NSMutableArray *searchedArray=[[NSMutableArray alloc]init];
    
    [[NSUserDefaults standardUserDefaults]setObject:searchedArray forKey:USERDEFAULT_SEARCH];
    [[NSUserDefaults standardUserDefaults]synchronize];
    

    
}

@end