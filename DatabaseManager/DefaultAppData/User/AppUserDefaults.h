//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>



@interface AppUserDefaults : NSObject

//Set data
+(void)saveThisUserIDToDefaults:(NSString*)userID;


+(void)saveThisUserNameToDefaults:(NSString*)name;
+(void)saveThisUserAddressLine1ToDefaults:(NSString*)addressLine1;
+(void)saveThisUserAddressLine2ToDefaults:(NSString*)addressLine2;
+(void)saveThisUserPincodeToDefaults:(NSString*)country;
+(void)saveThisUserStateToDefaults:(NSString*)state;
+(void)saveThisUserCountryToDefaults:(NSString*)country;

+(void)saveThisUserOrganizationToDefaults:(NSString*)organization;
+(void)saveThisUserContactToDefaults:(NSString*)contact;
+(void)saveThisUserLocationLatitudeToDefaults:(NSString*)latitude;
+(void)saveThisUserLongitudeToDefaults:(NSString*)longitude;
+(void)saveThisUserFacebookToDefaults:(NSString*)facebookId;
+(void)saveThisUserTwitterToDefaults:(NSString*)twitter;
+(void)saveThisUserEmailToDefaults:(NSString*)email;
+(void)saveThisUserPasswordToDefaults:(NSString*)password;
+(void)saveThisUserGenderToDefaults:(NSString*)gender;
+(void)saveThisUserusernameToDefaults:(NSString*)username;


//Get Data
+(NSString*)getUserID;
+(NSString*)getUserName;
+(NSString*)getUserAddressLine1;
+(NSString*)getUserAddressLine2;
+(NSString*)getUserCountry;
+(NSString*)getUserState;

+(NSString*)getUserOrganization;
+(NSString*)getUserContact;
+(NSString*)getUserLocation_Latitude;
+(NSString*)getUserLocation_Longitude;
+(NSString*)getUserFacebook;
+(NSString*)getUserTwitter;
+(NSString*)getUserEmail;
+(NSString*)getUserPassword;
+(NSString*)getUserGender;
+(NSString*)getUserPincode;
+(NSString*)getUserUsername;

    
+(void)logoutNow;
@end
