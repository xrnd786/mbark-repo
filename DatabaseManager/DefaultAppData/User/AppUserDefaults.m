//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import "AppUserDefaults.h"

#define UserLogin_Id @"userid"
#define UserLogin_Name @"Name"
#define UserLogin_AddressLine1 @"AddressLine1"
#define UserLogin_AddressLine2 @"AddressLine2"
#define UserLogin_Country @"Country"
#define UserLogin_State @"State"

#define UserLogin_Organization @"Organization"
#define UserLogin_Contact @"Contact"
#define UserLogin_Location @"Location"
#define UserLogin_Latitude @"Latitude"
#define UserLogin_Longitude @"Longitude"
#define UserLogin_Facebook @"Facebook"
#define UserLogin_Twitter @"Twitter"
#define UserLogin_Email @"Email"
#define UserLogin_Password @"Password"
#define UserLogin_Gender @"Gender"
#define UserLogin_Pincode @"Pincode"
#define UserLogin_Username @"Username"


@implementation AppUserDefaults

+(void)saveThisUserIDToDefaults:(NSString*)userID{
    
    [[NSUserDefaults standardUserDefaults]setObject:userID forKey:UserLogin_Id];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveThisUserNameToDefaults:(NSString*)name{
    
    [[NSUserDefaults standardUserDefaults]setObject:name forKey:[self getKeyStringForUser:UserLogin_Name]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserusernameToDefaults:(NSString*)username{
    
    [[NSUserDefaults standardUserDefaults]setObject:username forKey:[self getKeyStringForUser:UserLogin_Username]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveThisUserAddressLine1ToDefaults:(NSString*)addressLine1{
    
    [[NSUserDefaults standardUserDefaults]setObject:addressLine1 forKey:[self getKeyStringForUser:UserLogin_AddressLine1]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveThisUserAddressLine2ToDefaults:(NSString*)addressLine2{
    
    [[NSUserDefaults standardUserDefaults]setObject:addressLine2 forKey:[self getKeyStringForUser:UserLogin_AddressLine2]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserPincodeToDefaults:(NSString*)country{
    
    [[NSUserDefaults standardUserDefaults]setObject:country forKey:[self getKeyStringForUser:UserLogin_Pincode]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}



+(void)saveThisUserStateToDefaults:(NSString*)state{
    
    [[NSUserDefaults standardUserDefaults]setObject:state forKey:[self getKeyStringForUser:UserLogin_State]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserCountryToDefaults:(NSString*)country{
    
    [[NSUserDefaults standardUserDefaults]setObject:country forKey:[self getKeyStringForUser:UserLogin_Country]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}



+(void)saveThisUserOrganizationToDefaults:(NSString*)organization{
    
    [[NSUserDefaults standardUserDefaults]setObject:organization forKey:[self getKeyStringForUser:UserLogin_Organization]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserContactToDefaults:(NSString*)contact{
    
    [[NSUserDefaults standardUserDefaults]setObject:contact forKey:[self getKeyStringForUser:UserLogin_Contact]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserLocationLatitudeToDefaults:(NSString*)latitude{
    
    [[NSUserDefaults standardUserDefaults]setObject:latitude forKey:[self getKeyStringForUser:UserLogin_Latitude]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserLongitudeToDefaults:(NSString*)longitude{
    
    [[NSUserDefaults standardUserDefaults]setObject:longitude forKey:[self getKeyStringForUser:UserLogin_Longitude]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserFacebookToDefaults:(NSString*)facebookId{
    
    [[NSUserDefaults standardUserDefaults]setObject:facebookId forKey:[self getKeyStringForUser:UserLogin_Facebook]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserTwitterToDefaults:(NSString*)twitter{
    
    [[NSUserDefaults standardUserDefaults]setObject:twitter forKey:[self getKeyStringForUser:UserLogin_Twitter]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserPasswordToDefaults:(NSString*)password{
    
    [[NSUserDefaults standardUserDefaults]setObject:password forKey:[self getKeyStringForUser:UserLogin_Password]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveThisUserEmailToDefaults:(NSString*)email{
    
    [[NSUserDefaults standardUserDefaults]setObject:email forKey:[self getKeyStringForUser:UserLogin_Email]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveThisUserGenderToDefaults:(NSString*)gender{
    
    [[NSUserDefaults standardUserDefaults]setObject:gender forKey:[self getKeyStringForUser:UserLogin_Gender]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(NSString*)getUserID{
    
    NSString *ud=[[NSUserDefaults standardUserDefaults]objectForKey:UserLogin_Id];
    return (ud!=nil)?ud:@"";

}


+(NSString*)getUserName{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Name]];
    
}

+(NSString*)getUserGender{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Gender]];
    
}

+(NSString*)getUserAddressLine1{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_AddressLine1]];
    
}


+(NSString*)getUserAddressLine2{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_AddressLine2]];
    
}


+(NSString*)getUserCountry{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Country]];
    
}



+(NSString*)getUserState{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_State]];
    
}


+(NSString*)getUserPincode{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Pincode]];
    
}

+(NSString*)getUserPassword{
    
    NSString *pass=[[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Password]];
    
    return pass;
}


+(NSString*)getUserOrganization{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Organization]];
    
}


+(NSString*)getUserContact{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Contact]];
    
}


+(NSString*)getUserLocation_Latitude{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Latitude]];
    
}


+(NSString*)getUserLocation_Longitude{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Longitude]];
    
}


+(NSString*)getUserFacebook{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Facebook]];
    
}


+(NSString*)getUserTwitter{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Twitter]];
    
}


+(NSString*)getUserEmail{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Email]];
    
}

+(NSString*)getUserUsername{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[self getKeyStringForUser:UserLogin_Username]];
    
}

+(NSString*)getKeyStringForUser:(NSString*)value{
    
    return [[[self getUserID] stringByAppendingString:@"_"] stringByAppendingString:value];
    
}


+(void)logoutNow{
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:[self getKeyStringForUser:UserLogin_Password]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   //NSLog(@"User Dictionary %@",[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]);
    
}

@end
