//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import "AppTenantDefaults.h"


#define JSON_RES_TenantDetails_organization @"Organization"
#define JSON_RES_TenantDetails_banner @"Banner"
#define JSON_RES_TenantDetails_Image @"Image"
#define JSON_RES_TenantDetails_logo @"Logo"
#define JSON_RES_TenantDetails_Email @"Email"
#define JSON_RES_TenantDetails_helpline @"Helpline"
#define JSON_RES_TenantDetails_address @"Address"
#define JSON_RES_TenantDetails_location @"Location"
#define JSON_RES_TenantDetails_facebook @"Facebook"
#define JSON_RES_TenantDetails_twitter @"Twitter"
#define JSON_RES_TenantDetails_googleplus @"GooglePlus"
#define JSON_RES_TenantDetails_about @"About"
#define JSON_RES_TenantDetails_appstorelink @"AppStoreLink"
#define JSON_RES_TenantDetails_playstorelink @"PlayStoreLink"
#define JSON_RES_TenantDetails_cname @"CNAME"

//***Tenant ID response keys****//
#define JSON_RES_TenantId @"TenantId"


@implementation AppTenantDefaults


+(void)saveTenantDetails:(NSDictionary*)tenantDictinary{
    
    NSArray *list=[self getArrayOfConstants];
    
    for (NSString *sting in list) {
   
        id value=[tenantDictinary objectForKey:sting];
        NSString *defaultValue=@"";
        
        if (value!=[NSNull null]) {
            
            [[NSUserDefaults standardUserDefaults]setObject:value forKey:sting];
            
        } else {
       
            [[NSUserDefaults standardUserDefaults]setObject:defaultValue forKey:sting];
            
        }
        
        
    }
   
   // id banner_urlString=[[tenantDictinary objectForKey:JSON_RES_TenantDetails_banner] objectForKey:JSON_RES_TenantDetails_Image];
   // id logo_urlString=[[tenantDictinary objectForKey:JSON_RES_TenantDetails_logo] objectForKey:JSON_RES_TenantDetails_Image];
    
    //[[NSUserDefaults standardUserDefaults]setObject:(banner_urlString!=[NSNull null])?banner_urlString:@"" forKey:@"banner_url"];
    //[[NSUserDefaults standardUserDefaults]setObject:(logo_urlString!=[NSNull null])?logo_urlString:@"" forKey:@"logo_url"];
    
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveTenantID:(NSString*)tenantID{
    
    [[NSUserDefaults standardUserDefaults]setObject:tenantID?tenantID:@"" forKey:JSON_RES_TenantId];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}

+(void)saveTenantLogo:(NSString*)rawData{
    
    [[NSUserDefaults standardUserDefaults]setObject:rawData forKey:@"logo_url"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveTenantBanner:(NSString*)rawData{
    
    [[NSUserDefaults standardUserDefaults]setObject:rawData forKey:@"banner_url"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveDeviceToken:(NSString*)token{
    
    [[NSUserDefaults standardUserDefaults]setObject:token forKey:@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(NSString*)getOrganization{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_organization];

}

+(NSString*)getEmail{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_Email];
    
}
+(NSString*)getAddress{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_address];
    
}

+(NSString*)getLocation{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_location];
    
}

+(NSString*)getFacebook{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_facebook];
    
}
+(NSString*)getTwitter{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_twitter];
    
}
+(NSString*)getGooglePlus{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_googleplus];
    
}
+(NSString*)getAbout{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_about];
    
}
+(NSString*)getAppStore{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_appstorelink];
    
}
+(NSString*)getPlayStore{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:JSON_RES_TenantDetails_playstorelink];
    
}

+(NSString*)getBannerURL{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:@"banner_url"];
    
}

+(NSString*)getLogoURL{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:@"logo_url"];
    
}

+(NSString*)getToken{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceToken"];
    
}

+(NSMutableArray*)getArrayOfConstants{
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    [array addObject:JSON_RES_TenantDetails_about];
    [array addObject:JSON_RES_TenantDetails_address];
    [array addObject:JSON_RES_TenantDetails_appstorelink];
    [array addObject:JSON_RES_TenantDetails_cname];
    [array addObject:JSON_RES_TenantDetails_Email];
    [array addObject:JSON_RES_TenantDetails_facebook];
    [array addObject:JSON_RES_TenantDetails_googleplus];
    [array addObject:JSON_RES_TenantDetails_helpline];
    [array addObject:JSON_RES_TenantDetails_location];
    [array addObject:JSON_RES_TenantDetails_organization];
    [array addObject:JSON_RES_TenantDetails_playstorelink];
    [array addObject:JSON_RES_TenantDetails_twitter];
   
    return array;
    
}



@end
