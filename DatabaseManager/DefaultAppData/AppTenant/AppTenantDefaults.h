//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

@interface AppTenantDefaults : NSObject

//Set data
+(void)saveTenantDetails:(NSDictionary*)tenantDictinary;
+(void)saveTenantID:(NSString*)tenantID;
+(void)saveTenantLogo:(NSString*)rawData;
+(void)saveTenantBanner:(NSString*)rawData;
+(void)saveDeviceToken:(NSString*)token;


//get data
+(NSString*)getOrganization;
+(NSString*)getEmail;
+(NSString*)getAddress;
+(NSString*)getLocation;
+(NSString*)getFacebook;
+(NSString*)getTwitter;
+(NSString*)getGooglePlus;
+(NSString*)getAbout;
+(NSString*)getAppStore;
+(NSString*)getPlayStore;
+(NSString*)getBannerURL;
+(NSString*)getLogoURL;
+(NSString*)getToken;

@end
