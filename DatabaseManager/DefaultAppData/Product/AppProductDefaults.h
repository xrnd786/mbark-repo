//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>


#define JSON_RES_AppProducts_SKU @"SKU"
#define JSON_RES_AppProducts_Name @"Name"
#define JSON_RES_AppProducts_Type @"Type"
#define JSON_RES_AppProducts_Description @"Description"
#define JSON_RES_AppProducts_Category @"Category"
#define JSON_RES_AppProducts_AvailableUnits @"AvailableUnits"
#define JSON_RES_AppProducts_MeasurementUnit @"MeasurementUnit"
#define JSON_RES_AppProducts_Rate @"Rate"
#define JSON_RES_AppProducts_Discount @"Discount"
#define JSON_RES_AppProducts_Discontinued @"Discontinued"
#define JSON_RES_AppProducts_OutOfStock @"OutOfStock"
#define JSON_RES_AppProducts_ShippedInDays @"ShippedInDays"
#define JSON_RES_AppProducts_Featured @"Featured"
#define JSON_RES_AppProducts_Tags @"Tags"
#define JSON_RES_AppProducts_Applications @"Applications"
#define JSON_RES_AppProducts_Images @"Images"
#define JSON_RES_AppProducts_ImageUrls @"ImageUrls"
#define JSON_RES_AppProducts_Thumbnail @"Thumbnail"
#define JSON_RES_AppProducts_Thumbnail_Image @"Image"
#define JSON_RES_AppProducts_Documents @"Documents"
#define JSON_RES_AppProducts_RelatedProducts @"RelatedProducts"
#define JSON_RES_AppProducts_RelatedProducts_Thumbnail @"RelatedThumbnails"

#define JSON_RES_AppProducts_Attributes @"Attributes"
#define JSON_RES_AppProducts_AttributeValue @"AttributeValue"

#define JSON_RES_AppProducts_Value @"Value"
#define JSON_RES_AppProducts_Id @"Id"
#define JSON_RES_AppProducts_Group @"Group"
#define JSON_RES_AppProducts_GroupedProducts @"GroupedProducts"

#define JSON_RES_AppAttribute_Name @"AttributeName"
#define JSON_RES_AppAttribute_Values @"Values"





@interface AppProductDefaults : NSObject

+(void)saveTotalCount:(int)number forCategoryName:(NSString*)categoryName;
+(void)saveAppProductsForCategory:(NSString*)categoryName :(NSArray*)products;
+(void)saveFeaturedTotalCount:(int)number;
+(void)saveAppFeaturedProducts:(NSArray*)products;


+(NSArray*)getProductsArrayForCategory:(NSString*)categoryName;
+(NSArray*)getFeaturedProductsArray;

+(void)saveWishList:(Products*)product;
+(void)removeFromWishList:(NSString*)productID;

+(NSArray*)getProductsArrayForWishList;
+(BOOL)isThisFavourite:(NSString*)productID;


+(void)saveThisProduct:(NSDictionary*)product;
+(NSDictionary*)getThisProduct:(NSString*)productID;

+(void)saveCartList:(NSDictionary*)productMain withQuantity:(NSString*)qty;
+(void)editCartList:(NSString*)productID withQuantity:(NSArray*)qtyArray;
+(void)removeFromCartList:(NSString*)productID;
+(void)saveProductsCart:(NSDictionary*)cartProductObject;

+(NSArray*)getProductsArrayForCartList;
+(BOOL)isThisInCart:(NSString*)productID;
+(NSMutableArray*)getProductsCart;
+(Products*)getProductsCartForId:(NSString*)productID;
+(NSMutableArray*)getProductsQtyArrayForCartList;

+(void)removeAllCartList;
+(void)removeAllWishList;


@end
