//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//


#import "AppProductDefaults.h"


@implementation AppProductDefaults


+(void)saveTotalCount:(int)number forCategoryName:(NSString*)categoryName{

    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d", number] forKey:[categoryName stringByAppendingString:@"_Products_Count"]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveAppProductsForCategory:(NSString*)categoryName :(NSArray*)products
{
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:products];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:[categoryName stringByAppendingString:@"_Products_Array"]];
    [[NSUserDefaults standardUserDefaults]synchronize];

}



+(void)saveFeaturedTotalCount:(int)number{
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d", number] forKey:@"FeaturedProducts_Count"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)saveAppFeaturedProducts:(NSArray*)products
{
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:products];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"FeaturedProducts_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   // NSLog(@"ALL the Default Data %@",[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]);
    
}


+(NSArray*)getProductsArrayForCategory:(NSString*)categoryName{
    
    NSData *data = [[NSUserDefaults standardUserDefaults]valueForKey:[categoryName stringByAppendingString:@"_Products_Array"]];
   
    //NSLog(@"ALL the Default Data %@",[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]);
    
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    
    
}

+(NSArray*)getFeaturedProductsArray{
    
    //For Reteriving
    NSData *data = [[NSUserDefaults standardUserDefaults]valueForKey:@"FeaturedProducts_Array"];
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}

+(void)saveThisProduct:(Products*)product{
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:product];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:[NSString stringWithFormat:@"%@_Product",[product valueForKey:JSON_RES_AppProducts_Id]]];
    [[NSUserDefaults standardUserDefaults]synchronize];
     //NSLog(@"Product Data %@",[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]);
}

+(Products*)getThisProduct:(NSString*)productID{
    
    //For Reteriving
    NSData *data = [[NSUserDefaults standardUserDefaults]valueForKey:[NSString stringWithFormat:@"%@_Product",productID]];
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}


+(void)saveWishList:(Products*)product{
    
    
    /*
    NSMutableArray *newArray=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)[AppProductDefaults getProductsArrayForWishList]];
    
    newArray=[newArray mutableCopy];
    
    NSArray *mutateThis=[[NSArray alloc]initWithArray:newArray];
    
    
    if ([newArray count]>0) {
        
        for (Product *productObj in mutateThis) {
            
            if (![productObj.Id isEqualToString:product.Id]) {
               
                [newArray addObject:product];
                break;
            }
            
        }
        
    } else {
        
        [newArray addObject:product];
    
    }
    
   

    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:newArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"WishList_Products_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    //NSLog(@"Wishlist Data %@",[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]);

     */
    
    
}



+(void)removeFromWishList:(NSString*)productID{
    
    
    
    /*
    NSMutableArray *newArray=(NSMutableArray*)[AppProductDefaults getProductsArrayForWishList];
    newArray=[newArray mutableCopy];
    
    NSArray *mutateThis=[[NSArray alloc]initWithArray:newArray];
    
    if ([newArray count]>0) {
        
        for (Products *product in mutateThis) {
            
            if ([product.Id isEqualToString:productID]) {
                [newArray removeObject:product];
                break;
            }
            
        }
        
    }else{
        
         //[newArray removeObject:productID];
        
    }
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:newArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"WishList_Products_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
*/
}




+(void)removeAllCartList{
    
    NSMutableArray *newArray=(NSMutableArray*)[AppProductDefaults getProductsArrayForCartList];
    newArray=[newArray mutableCopy];
    [newArray removeAllObjects];
    
    NSMutableArray *qArray=(NSMutableArray*)[AppProductDefaults getProductsQtyArrayForCartList];
    qArray=[qArray mutableCopy];
    [qArray removeAllObjects];
    
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:newArray];
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"CartList_Products_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    NSData *dataSaveNew = [NSKeyedArchiver archivedDataWithRootObject:qArray];
    [[NSUserDefaults standardUserDefaults]setObject:dataSaveNew forKey:@"CartList_Products_Array_Quantity"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(void)removeAllWishList{
    
    NSMutableArray *newArray=(NSMutableArray*)[AppProductDefaults getProductsArrayForWishList];
    newArray=[newArray mutableCopy];
    
    [newArray removeAllObjects];
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:newArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"WishList_Products_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}




+(NSMutableArray*)getProductsArrayForWishList{
    
    NSData *data = [[NSUserDefaults standardUserDefaults]valueForKey:@"WishList_Products_Array"];
   return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}

+(BOOL)isThisFavourite:(NSString*)productID{
    
    
    /*
    NSArray *array=[AppProductDefaults getProductsArrayForWishList];
    
    for (Product *product in array) {
        
        //NSLog(@"ID In Array %@ and Id given %@",product,productID);
        
        if ([product.Id isEqualToString:productID]) {
            
            return TRUE;
        }
        
    }
    
   
    */
     return FALSE;
}


+(void)saveCartList:(NSDictionary*)productMain withQuantity:(NSString*)qty{
    
    NSLog(@"Quantity %@",qty);
    
    NSMutableArray *newArray=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)[AppProductDefaults getProductsArrayForCartList]];
    newArray=[newArray mutableCopy];
    
    
    NSMutableArray *qArray=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)[AppProductDefaults getProductsQtyArrayForCartList]];
    qArray=[qArray mutableCopy];
    
    
    BOOL contains=FALSE;
    NSMutableArray *mutateThis=[[NSMutableArray alloc]initWithArray:newArray];
    
    NSMutableArray *array=[[NSMutableArray alloc]initWithArray:qArray];
    
    
    if ([newArray count]>0) {
        
        for (NSDictionary *product in mutateThis) {
            
            if ([[product valueForKey:JSON_RES_AppProducts_Id] isEqualToString:[productMain valueForKey:JSON_RES_AppProducts_Id]]) {
                contains=TRUE;
                break;
            }
        
        }
        
        
        if (!contains) {
            
            [newArray addObject:productMain];
            
            if ([qArray count]>0) {
                
                [array addObject:qty];
                
            }else{
                
                [array addObject:qty];
                
            }
            
        }
        
    }else{
        
        [newArray addObject:productMain];
        
        [array addObject:qty];
        
        NSLog(@"new array last object %@",((NSDictionary*)[newArray lastObject]));
        
    }
    
    
    NSLog(@"New array values %@",[newArray description]);
    NSLog(@"New quantity values %@",array);
   
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:newArray];
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"CartList_Products_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    NSData *dataSaveNew = [NSKeyedArchiver archivedDataWithRootObject:array];
    [[NSUserDefaults standardUserDefaults]setObject:dataSaveNew forKey:@"CartList_Products_Array_Quantity"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];

}


+(void)editCartList:(NSString*)productID withQuantity:(NSArray*)qtyArray{
    
    NSMutableArray *newArray=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)[AppProductDefaults getProductsArrayForCartList]];
    
    
    NSMutableArray *qArray=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)[AppProductDefaults getProductsQtyArrayForCartList]];
    qArray=[qArray mutableCopy];
    
    
    NSLog(@"New array values %@",[newArray description]);
    NSLog(@"New quantity values %@",[qArray description]);
    
    for (NSDictionary *product in newArray) {
    
        if ([[product valueForKey:JSON_RES_AppProducts_Id] isEqualToString:productID]) {
            
            int index=(int)[newArray indexOfObject:product];
            [qArray replaceObjectAtIndex:index withObject:[qtyArray objectAtIndex:0]];
            break;
            
        }
        
    }
    
    
    NSData *dataSaveNew = [NSKeyedArchiver archivedDataWithRootObject:qArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSaveNew forKey:@"CartList_Products_Array_Quantity"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(NSMutableArray*)getProductsQtyArrayForCartList{
 
    NSData *data = [[NSUserDefaults standardUserDefaults]objectForKey:@"CartList_Products_Array_Quantity"];
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}

+(void)removeFromCartList:(NSString*)productID{
    
    NSMutableArray *newArray=(NSMutableArray*)[AppProductDefaults getProductsArrayForCartList];
    newArray=[newArray mutableCopy];
    
    NSMutableArray *qArray=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)[AppProductDefaults getProductsQtyArrayForCartList]];
    qArray=[qArray mutableCopy];
    
    NSArray *mutateThis=[[NSArray alloc]initWithArray:newArray];
    
    if ([newArray count]>0) {
        
        for (NSDictionary *product in mutateThis) {
            
            if ([[product valueForKey:JSON_RES_AppProducts_Id] isEqualToString:productID]) {
                [newArray removeObject:product];
                [qArray removeObjectAtIndex:[mutateThis indexOfObject:product]];
                break;
            }
            
        }
        
    }else{
        
        //[newArray removeObject:product];
        
    }
    
    
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:newArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"CartList_Products_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSData *dataSaveNew = [NSKeyedArchiver archivedDataWithRootObject:qArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSaveNew forKey:@"CartList_Products_Array_Quantity"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}



+(NSArray*)getProductsArrayForCartList{
    
    NSData *data = [[NSUserDefaults standardUserDefaults]objectForKey:@"CartList_Products_Array"];
   // NSLog(@"unarchieved data %@", [NSKeyedUnarchiver unarchiveObjectWithData:data]);
    
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}


+(BOOL)isThisInCart:(NSString*)productID{
    
    NSArray *array=[AppProductDefaults getProductsArrayForCartList];
    
    for (NSDictionary *product in array) {
        
        if ([[product valueForKey:JSON_RES_AppProducts_Id] isEqualToString:productID])
        {
            return TRUE;
        }
    
    }
    
    return FALSE;
    
}


+(void)saveProductsCart:(NSDictionary*)cartProductObject{
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CartList_Products_Array"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CartList_Products_Array_Quantity"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSMutableArray *newArray=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)[AppProductDefaults getProductsCart]];
    
    newArray=[newArray mutableCopy];
    [newArray addObject:cartProductObject];
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:newArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:@"CartList_Array"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(NSMutableArray*)getProductsCart{
    
    NSData *data = [[NSUserDefaults standardUserDefaults]valueForKey:@"CartList_Array"];
    
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}


+(Products*)getProductsCartForId:(NSString*)productID{
    
    
    /*
    NSArray *array=[self getProductsCart];
    
    for (Product *product in array) {
        
        if ([product.Id isEqualToString:productID]) {
            
            return product;
            
        }
        
    }
    
   
     
     */
     return nil;
}

+(NSMutableArray*)getArrayOfConstants{
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    [array addObject:JSON_RES_AppProducts_Applications];
    [array addObject:JSON_RES_AppProducts_Attributes];
    [array addObject:JSON_RES_AppProducts_AttributeValue];
    [array addObject:JSON_RES_AppProducts_AvailableUnits];
    [array addObject:JSON_RES_AppProducts_Category];
    [array addObject:JSON_RES_AppProducts_Description];
    [array addObject:JSON_RES_AppProducts_Discontinued];
    [array addObject:JSON_RES_AppProducts_Discount];
    [array addObject:JSON_RES_AppProducts_Documents];
    [array addObject:JSON_RES_AppProducts_Featured];
    [array addObject:JSON_RES_AppProducts_Id];
    [array addObject:JSON_RES_AppProducts_Images];
    [array addObject:JSON_RES_AppProducts_MeasurementUnit];
    [array addObject:JSON_RES_AppProducts_Name];
    [array addObject:JSON_RES_AppProducts_OutOfStock];
    [array addObject:JSON_RES_AppProducts_Rate];
    [array addObject:JSON_RES_AppProducts_RelatedProducts];
    [array addObject:JSON_RES_AppProducts_ShippedInDays];
    [array addObject:JSON_RES_AppProducts_SKU];
    [array addObject:JSON_RES_AppProducts_Tags];
    [array addObject:JSON_RES_AppProducts_Type];
    
    return array;
    
}



@end
