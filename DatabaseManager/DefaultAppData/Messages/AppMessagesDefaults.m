//
//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//


#import "AppMessagesDefaults.h"
#define Message_Prefix @"MessageThread"

@implementation AppMessagesDefaults


/*
+(void)saveMessagesThread:(MessageThread*)messageThread{
    
    NSMutableArray *arrayOfmessages=[[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    
    NSMutableArray *copyArray=[[NSMutableArray alloc]initWithArray:arrayOfmessages];
    
    for (MessageThread *thread in arrayOfmessages) {
        
        if ([thread.threadID isEqualToString:messageThread.threadID]) {
            
            [copyArray removeObject:thread];
            break;
        }
    }
    
    [copyArray addObject:messageThread];
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:copyArray];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:messages_list];
    [[NSUserDefaults standardUserDefaults]synchronize];

}


+(void)removeAllMessages{
    
    NSMutableArray *arrayOfmessages=[[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    arrayOfmessages=[arrayOfmessages mutableCopy];
    
    [arrayOfmessages removeAllObjects];
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:arrayOfmessages];
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:messages_list];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(BOOL)doesThreadExists:(NSString*)threadType withReference:(NSString*)ref{
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    
    for (MessageThread *threadObj in array) {
    
        if ([threadObj.linkedTo isEqualToString:threadType])
        {
            if ([threadObj.linkReference isEqualToString:ref]) {
                
                return TRUE;
                
            }
        }
        
    }
    
    return FALSE;
    
}

+(MessageThread*)returnThreadID:(NSString*)threadType withReference:(NSString*)ref{
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    
    for (MessageThread *threadObj in array) {
        
        
        if ([threadObj.linkedTo isEqualToString:threadType])
        {
            if ([threadObj.linkReference isEqualToString:ref]) {
                
                return  threadObj;
                
            }
        }
        
    }
    
    
    return [MessageThread new];
    
}


+(void)MarkMessagesReadOFThreadID:(NSString*)threadID{
    
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    
    NSMutableArray *arrayModify=[[NSMutableArray alloc]initWithArray:array];
    
    int j=0;
    for (MessageThread *threadObj in array) {
        
        if ([threadObj.threadID isEqualToString:threadID])
        {
            
            NSMutableArray *messagesModify=[[NSMutableArray alloc]initWithArray:threadObj.messages];
            int i=0;
            
            for (MessageEntity *entity in threadObj.messages) {
                
                entity.status=Message_STATUS_Read;
                [messagesModify replaceObjectAtIndex:i withObject:entity];
                NSLog(@"Messages list after marking read %@",((MessageEntity*)[messagesModify objectAtIndex:i]).status);
                
                i++;
                
            }
            
           
            MessageThread *threadModify=[MessageThread new];
            threadModify=threadObj;
            threadModify.messages=messagesModify;
            
            [arrayModify replaceObjectAtIndex:j withObject:threadModify];
            break;
        }
        
        j++;
    }
    
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:arrayModify];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:messages_list];
    [[NSUserDefaults standardUserDefaults]synchronize];

    
}



+(MessageThread*)returnThreadSubject:(NSString*)threadType withReference:(NSString*)ref{
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    
    for (MessageThread *threadObj in array) {
        
        if ([threadObj.linkedTo isEqualToString:threadType])
        {
            if ([threadObj.linkReference isEqualToString:ref]) {
                
                return threadObj;
                
            }
        }
        
    }
    
    return [MessageThread new];
    
}

+(void)saveInPreviousMessagesThreadWithChatArray:(NSArray*)chat andThreadID:(NSString*)threadID{
 
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    MessageThread *thread=[MessageThread new];
    
    array=[array mutableCopy];
   
    
    int i=0;
    for (MessageThread *threadObj in array) {
        
        if ([threadObj.threadID isEqualToString:threadID])
        {
            thread=threadObj;
            thread.messages=chat;
            thread.lastMessage=(MessageEntity*)[chat lastObject];
    
            break;
        }
        
        i++;
    }
   
    if (!(i>=[array count])) {
        
        [array replaceObjectAtIndex:i withObject:thread];
        
    }else{
        
        return;
    }
    
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:array];
    
    [[NSUserDefaults standardUserDefaults]setObject:dataSave forKey:messages_list];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
        
}

+(NSArray*)getChatArrayOfMessagesWithThreadID:(NSString*)threadID{

    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self getArrayOfMessages]];
    MessageThread *thread;
    
    NSArray *arrayValue;
    
    int i=0;
    for (MessageThread *threadObj in array) {
        
        if ([threadObj.threadID isEqualToString:threadID])
        {
            thread=threadObj;
            arrayValue = [[NSArray alloc]initWithArray:thread.messages];
            
        }
        
        i++;
    }

    return arrayValue;
    
}

+(NSArray*)getArrayOfMessages{
    
    NSData *data =[[NSUserDefaults standardUserDefaults]valueForKey:messages_list];
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
   
    
}
*/


@end
