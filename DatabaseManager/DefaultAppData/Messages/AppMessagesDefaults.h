//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

#import "MessageEntity.h"
#import "MessageThread.h"

#import "MessageMapper.h"




@interface AppMessagesDefaults : NSObject

/*
+(void)saveMessagesThread:(MessageThread*)messageThread;

+(void)saveInPreviousMessagesThreadWithChatArray:(NSArray*)chat andThreadID:(NSString*)threadID;

    
+(BOOL)doesThreadExists:(NSString*)threadType withReference:(NSString*)ref;

+(MessageThread*)returnThreadID:(NSString*)threadType withReference:(NSString*)ref;
+(MessageThread*)returnThreadSubject:(NSString*)threadType withReference:(NSString*)ref;

+(void)MarkMessagesReadOFThreadID:(NSString*)threadID;
+(NSArray*)getArrayOfMessages;
+(NSArray*)getChatArrayOfMessagesWithThreadID:(NSString*)threadID;

+(void)removeAllMessages;
*/

@end
