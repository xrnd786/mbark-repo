//

//  AppUserDefaults.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//


#import "AppCategoryDefaults.h"
#import "FilterObject.h"

#define JSON_RES_AppCategory_Id @"Id"
#define JSON_RES_AppCategory_Name @"Name"
#define JSON_RES_AppCategory_Description @"Description"
#define JSON_RES_AppCategory_Image @"Image"
#define JSON_RES_AppCategory_Image_Url @"Url"
#define JSON_RES_AppCategory_Sequence @"Sequence"
#define JSON_RES_AppCategory_Children @"Children"

@interface AppCategoryDefaults()

@end

@implementation AppCategoryDefaults

+(void)saveAppCategories:(NSDictionary*)appCategoryDictinary withNumber:(int)number {
    
    for (NSString *sting in [self getArrayOfConstants]) {
        
        id value=[appCategoryDictinary objectForKey:sting];
        [[NSUserDefaults standardUserDefaults]setObject:(value!=[NSNull null])?value:@"" forKey:[[[NSString stringWithFormat:@"%d",number]stringByAppendingString:@"_Category_" ]stringByAppendingString:sting]];
        
    }
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[[appCategoryDictinary valueForKey:JSON_RES_AppCategory_Image] valueForKey:JSON_RES_AppCategory_Image]] forKey:[[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_Category_"]stringByAppendingString:JSON_RES_AppCategory_Image] ];
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[appCategoryDictinary valueForKey:JSON_RES_AppCategory_Children] count]] forKey:[[[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_Category_"]stringByAppendingString:JSON_RES_AppCategory_Children] stringByAppendingString:@"_Count"]];
    
    int i=0;
    for (NSDictionary *categoryDictionary in [appCategoryDictinary valueForKey:JSON_RES_AppCategory_Children]) {
        
        for (NSString *sting in [self getArrayOfConstantsForChild]) {
     
            id value=[categoryDictionary objectForKey:sting];
           
            [[NSUserDefaults standardUserDefaults]setObject:(value!=[NSNull null])?value:@"" forKey:[[[[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_Category_"]stringByAppendingString:[NSString stringWithFormat:@"%d",i]] stringByAppendingString:@"_Child_"] stringByAppendingString:sting]];
            
            if ([sting isEqual:JSON_RES_AppCategory_Image]) {
                
                 [[NSUserDefaults standardUserDefaults]setObject:([value objectForKey:JSON_RES_AppCategory_Image]!=[NSNull null])?[value objectForKey:JSON_RES_AppCategory_Image]:@"" forKey:[[[[[NSString stringWithFormat:@"%d",number] stringByAppendingString:@"_Category_"]stringByAppendingString:[NSString stringWithFormat:@"%d",i]] stringByAppendingString:@"_Child_"] stringByAppendingString:sting]];
            }
            
        }
        
        i++;
     }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveTotalCount:(int)count{
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",count] forKey:[NSString stringWithFormat:@"Categories_count"]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


+(NSString*)getCategoryIDFromName:(NSString*)name{
    
    NSArray *keyName= [[[NSUserDefaults standardUserDefaults]dictionaryRepresentation] allKeysForObject:name];
    
    int keyFirstNumber=[[[[keyName objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:0] integerValue];
    
    return [AppCategoryDefaults getID:keyFirstNumber];
    
    
}


+(NSArray*)getCategoryChildrenFromName:(NSString*)name{
    
    NSArray *keyName= [[[NSUserDefaults standardUserDefaults]dictionaryRepresentation] allKeysForObject:name];
    int keyFirstNumber=[[[[keyName objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:0] integerValue];
    
    if ([[[keyName objectAtIndex:0] componentsSeparatedByString:@"_"] count]==3) {
        
        return [AppCategoryDefaults getChildren:keyFirstNumber];

        
    } else {
        
        return [[NSArray alloc]init];

    }
    
    
    
}


+(void)saveCategoriesFilter:(NSArray*)filters forCategory:(NSString*)categoryID
{
    NSMutableArray *filterObjArray=[NSMutableArray new];
    
    for (NSDictionary *dictionary in filters) {
        
        DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass: [FilterObject class]];
        FilterObject *filter =[FilterObject new];
        filter=[parser parseDictionary:dictionary];
        NSLog(@"Object values %@, %@, %@, %@ %@",filter.AttributeName,filter.AttributeId,filter.Name,filter.AttributeType,filter.Values);
        [filterObjArray addObject:filter];
    
    }
    
    NSString *keyValue=[categoryID stringByAppendingString:@"_filter_List"];
    
    NSData *dataSaveNew = [NSKeyedArchiver archivedDataWithRootObject:filterObjArray];
    [[NSUserDefaults standardUserDefaults]setObject:dataSaveNew forKey:keyValue];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}




+(NSArray*)getCategoriesFilterForCategory:(NSString*)categoryID
{
    
    NSString *keyValue=[categoryID stringByAppendingString:@"_filter_List"];
    
    NSData *data = [[NSUserDefaults standardUserDefaults]valueForKey:keyValue];
    return  [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}



+(NSString*)getCount{
    
   return  [[NSUserDefaults standardUserDefaults]valueForKey:[NSString stringWithFormat:@"Categories_count"]];
    
}

+(NSString*)getID:(int)number{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",number]stringByAppendingString:@"_Category_" ]stringByAppendingString:JSON_RES_AppCategory_Id]];
    
}


+(NSString*)getName:(int)number{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",number]stringByAppendingString:@"_Category_" ]stringByAppendingString:JSON_RES_AppCategory_Name]];
    
}

+(NSString*)getImage:(int)number{
    
   return  [[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",number]stringByAppendingString:@"_Category_" ]stringByAppendingString:JSON_RES_AppCategory_Image]];
}

+(NSString*)getSequence:(int)number{
    
     return  [[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",number]stringByAppendingString:@"_Category_" ]stringByAppendingString:JSON_RES_AppCategory_Sequence]];
    
}

+(NSString*)getDescription:(int)number{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:[[[NSString stringWithFormat:@"%d",number]stringByAppendingString:@"_Category_"]stringByAppendingString:JSON_RES_AppCategory_Description]];
    
}

+(NSArray*)getChildren:(int)number{
    
   int childrenCount=(int)[[[NSUserDefaults standardUserDefaults]valueForKey:[[[[NSString stringWithFormat:@"%d",number]stringByAppendingString:@"_Category_"]
       stringByAppendingString: JSON_RES_AppCategory_Children]
       stringByAppendingString:@"_Count"]] integerValue];
    
    NSMutableArray *mainChildrenArray=[[NSMutableArray alloc]init];
    
    for (int i=0;i<childrenCount;i++) {
        
        NSMutableArray *innerArray=[[NSMutableArray alloc]init];
       
        for (NSString *sting in [self getArrayOfConstantsForChild]) {
            
            NSLog(@"KEY VALUE : %@",[[[[[NSString stringWithFormat:@"%d",number]
                stringByAppendingString:@"_Category_"]
                stringByAppendingString:[NSString stringWithFormat:@"%d",i]]
                stringByAppendingString:@"_Child_"]
                stringByAppendingString:sting]);
            
            
            [innerArray addObject:[[NSUserDefaults standardUserDefaults] valueForKey:[[[[[NSString stringWithFormat:@"%d",number]
                stringByAppendingString:@"_Category_"]
                stringByAppendingString:[NSString stringWithFormat:@"%d",i]]                       
                stringByAppendingString:@"_Child_"]
                stringByAppendingString:sting]]];
            
        }
        
        [mainChildrenArray addObject:innerArray];
        
    }
    
    return mainChildrenArray;
    
}

+(NSMutableArray*)getArrayOfConstants{
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    [array addObject:JSON_RES_AppCategory_Id];
    [array addObject:JSON_RES_AppCategory_Name];
    [array addObject:JSON_RES_AppCategory_Description];
    [array addObject:JSON_RES_AppCategory_Sequence];
    
    return array;
    
}


+(NSMutableArray*)getArrayOfConstantsForChild{
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    [array addObject:JSON_RES_AppCategory_Id];
    [array addObject:JSON_RES_AppCategory_Name];
    [array addObject:JSON_RES_AppCategory_Description];
    [array addObject:JSON_RES_AppCategory_Sequence];
    [array addObject:JSON_RES_AppCategory_Image];
    
    return array;
    
}

@end