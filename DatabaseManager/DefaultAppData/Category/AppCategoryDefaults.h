//
//  AppUserDefaults.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>

@interface AppCategoryDefaults : NSObject


//Set Data
+(void)saveAppCategories:(NSDictionary*)appCategoryDictinary withNumber:(int)i;
+(void)saveTotalCount:(int)count;
+(void)saveCategoriesFilter:(NSArray*)filters forCategory:(NSString*)categoryID;

//Get data

+(NSString*)getCount;
+(NSString*)getID:(int)number;
+(NSString*)getName:(int)number;
+(NSString*)getImage:(int)number;
+(NSString*)getSequence:(int)number;
+(NSArray*)getChildren:(int)number;
+(NSString*)getDescription:(int)number;
+(NSArray*)getCategoryChildrenFromName:(NSString*)name;
+(NSString*)getCategoryIDFromName:(NSString*)name;
+(NSArray*)getCategoriesFilterForCategory:(NSString*)categoryID;

@end
