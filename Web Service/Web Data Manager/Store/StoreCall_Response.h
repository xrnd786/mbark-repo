//
//  StoreCall_Response.h
//  mBark
//
//  Created by Xiphi Tech on 16/04/2015.
//
//

#import <Foundation/Foundation.h>

@interface StoreCall_Response : NSObject


+(NSArray*)callApiForStoreDetails;

    
@end
