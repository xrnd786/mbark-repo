//
//  StoreCall_Response.m
//  mBark
//
//  Created by Xiphi Tech on 16/04/2015.
//
//

#define contr @"Stores"
#define action_get @"Get"

#import "StoreCall_Response.h"
#import "ServerInteraction.h"

#import "AppStoresDefaults.h"

@implementation StoreCall_Response

static ServerInteraction *calling;


+(NSArray*)callApiForStoreDetails{
    
    __block NSArray *res;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_get andRequestBody:nil notifier:APINotification_Store_Get methodType:GET headerKeyArray:nil headerValueArray:nil];
       
    NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            res= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            [AppStoresDefaults saveStoreDetails:res];
            
            
        } else {
            
            res=[[NSMutableArray alloc]init];
            
        }
        
    });
    
    
    return [AppStoresDefaults getAllStores]!=nil?[AppStoresDefaults getAllStores]:[[NSMutableArray alloc]init];
    
}



@end
