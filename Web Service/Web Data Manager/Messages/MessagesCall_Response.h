//
//  TenantCall_Response.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>



#define message_thread_cart @"Cart"
#define message_thread_product @"Product"
#define message_thread_general @"General"

#define message_chatbody_Body @"Body"
#define message_chatbody_Date @"Date"
#define message_chatbody_Direction @"Direction"
#define message_chatbody_Status @"Status"

#define thread_type @"ThreadType"
#define thread_type_reference @"ThreadTypeRef"
#define thread_id @"ThreadID"

#define messages_list @"Messages_List"
#define messages_list_subject @"Messages_List_Subject"
#define messages_list_lastobject @"Messages_List_LastObject"
#define messages_list_chat @"Messages_List_Chat"
#define messages_list_date @"Messages_List_Date"

#define FROM_ADMIN_PLACEHOLDER @"Admin"
#define FROM_ME_PLACEHOLDER @"Me"

#define Message_STATUS_Pending @"Pending"
#define Message_STATUS_Read @"Read"

#define Message_Direction_ToAppUser @"ToAppUser"
#define Message_Direction_FromAppUser @"FromAppUser"



//***User response keys****//
#define JSON_RES_Messages_LinkedTo @"LinkedTo"
#define JSON_RES_Messages_LinkedToRef @"LinkedToRef"
#define JSON_RES_Messages_Chat @"Messages"
#define JSON_RES_Messages_ID @"Id"


#define JSON_Body_Message @"Message"
#define JSON_Body_LinkedTo @"LinkedTo"
#define JSON_Body_LinkedToRef @"LinkedToRef"

#define  JSON_RES_MESSAGE_Status @"Status"
#define  JSON_RES_MESSAGE_Body @"Body"
#define  JSON_RES_MESSAGE_Direction @"Direction"
#define  JSON_RES_MESSAGE_Date @"Date"

#import "Messages.h"
#import "MessagesDetails.h"


@interface MessagesCall_Response : NSObject


+(void)callApiForMessageCreation:(Messages*)messageThread;

+(void)callApiForMessageAddition:(MessagesDetails*)messageDetails threadID:(NSString*)threadID;
+(void)callApiToMarkRead:(NSString*)threadId;


+(Messages*)callApiForGetMessageThread:(NSString*)threadID;

@end
