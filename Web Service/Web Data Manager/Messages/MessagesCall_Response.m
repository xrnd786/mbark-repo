
//
//  TenantCall_Response.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//


#define contr @"Messages"

#define action_createthread @"CreateThread"
#define action_get @"Get"
#define action_post @"Post"
#define action_threads @"Threads"
#define action_markread @"MarkRead"



#import "Global_Server_Constants.h"
#import "MessagesCall_Response.h"
#import "ServerInteraction.h"

#import "Messages.h"
#import "MessagesDetails.h"

@implementation MessagesCall_Response

static MessagesDetails *messageDetails;
static ServerInteraction *calling;

NSString *threadIDCurrent;
NSString *subjectValue;

+(void)callApiForMessageCreation:(Messages*)messageThread{
    
    __block Messages *message=messageThread;
    
   
    subjectValue=messageThread.subject;
    
    __block NSDictionary *response;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
      
    calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:action_createthread andRequestBody:[self createDictionaryToSendThread:messageThread] notifier:APINotification_Messages_CreateThread methodType:POST headerKeyArray:nil headerValueArray:nil];
        
        NSError *error;
        
        if ([calling.responseHeader integerValue] == HEADER_ENUM_CREATED)
        {
            response= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            dispatch_async(dispatch_get_main_queue(),^{
                
                SCLAlertView *view=[[SCLAlertView alloc]init];
                [view showInfo:[[UIApplication sharedApplication]keyWindow].rootViewController title:@"Sent" subTitle:@"Your message has been sent" closeButtonTitle:@"Ok" duration:0.0f];
                
                
                message=[self returnMessageObject:response withObject:messageThread andIsNew:YES];
                message.subject=subjectValue;
                
                NSLog(@"New message object %@",messageThread);
                
                [GlobalCall saveContext];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"ReloadViews" object:nil];
                
            });
            
            
            
            
            
        } else {
            
            response=[[NSMutableDictionary alloc]init];
            
        }
        
        
    });
    
    
}



+(void)callApiForMessageAddition:(MessagesDetails*)message threadID:(NSString*)threadID{
    
    [GlobalCall removeOberverWithNotifier:APINotification_Messages_Post handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Messages_Post andSelector:@selector(messageAddResponse:) handlerClass:self withObject:nil];
    
    messageDetails=message;
    threadIDCurrent=threadID;
    
    NSArray *keyArray=[[NSArray alloc]initWithObjects:JSON_Body_Message,@"Thread", nil];
    NSArray *valueArray=[[NSArray alloc]initWithObjects:message.body ,threadID, nil];
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_post andRequestBody:[[NSDictionary alloc]initWithObjects:valueArray forKeys:keyArray] notifier:APINotification_Messages_Post methodType:POST headerKeyArray:nil headerValueArray:nil];
    
    
}


+(void)messageAddResponse:(NSNotification*)notification{
    
    Messages *messageThread=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"m_id == %@",threadIDCurrent]] lastObject];
    
    [messageThread addMessagesDetailsObject:messageDetails];
    [GlobalCall saveContext];
    
    [GlobalCall removeOberverWithNotifier:APINotification_Messages_Post handlerClass:self withObject:nil];
}


+(Messages*)callApiForGetMessageThread:(NSString*)threadID{
    
    __block Messages *response;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:action_get andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:[[NSArray alloc]initWithObjects:threadID, nil]];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            NSDictionary *dic= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                response=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"m_id == %@",threadID]] lastObject];
                response=[self returnMessageObject:dic withObject:response andIsNew:FALSE];
                [GlobalCall saveContext];
                
            });
            
        } else {
            
            
            
        }
        
    });
    
    return response;
    
}


+(void)callApiToMarkRead:(NSString*)threadId{
    
    __block NSMutableArray *response;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:action_markread andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:[[NSArray alloc]initWithObjects:threadId, nil]];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            response= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                Messages *message=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"m_id == %@",threadId]] lastObject];
                [message.messagesDetails setValue:Message_STATUS_Read forKeyPath:@"status"];
                [GlobalCall saveContext];
                
            });

            
        } else {
            
            response=[[NSMutableArray alloc]init];
            
        }
        
    });
    
    
}


+(NSDictionary*)createDictionaryToSendThread:(Messages*)thread{
    
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    [dictionary setObject:((MessagesDetails*)[[thread.messagesDetails allObjects] lastObject]).body forKey:JSON_Body_Message];
    [dictionary setObject:thread.linkedTo forKey:JSON_Body_LinkedTo];
    [dictionary setObject:thread.linkedToRef forKey:JSON_Body_LinkedToRef];
    return dictionary;
}




+(Messages*)returnMessageObject:(NSDictionary*)dic withObject:(Messages*)obj andIsNew:(BOOL)isNew{
    
    
    obj.m_id=[dic valueForKey:JSON_RES_Messages_ID];
    
    
    NSDate *todayDate = [NSDate date]; // get today date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    //Here we can set the format which we need
    NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];
    
    obj.createdOn=todayDate;
    
    
    NSMutableArray *DetailsSet=[NSMutableArray new];
    
    for (NSDictionary *msgDetails in [dic valueForKey:@"Messages"]) {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"MessagesDetails" inManagedObjectContext:[GlobalCall getContext]];
        
        MessagesDetails *objDetails = [[MessagesDetails alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
        objDetails.status=[msgDetails valueForKey:JSON_RES_MESSAGE_Status];
        objDetails.body=[msgDetails valueForKey:JSON_RES_MESSAGE_Body];
        objDetails.direction=[msgDetails valueForKey:JSON_RES_MESSAGE_Direction] ;
        objDetails.createdOn=[self curentDateStringFromUTCDate:        [msgDetails valueForKey:JSON_RES_MESSAGE_Date] withFormat:@"dd-MM-yyyy"];

        
        [DetailsSet addObject:objDetails];
        
        [GlobalCall saveContext];
        
    }
    
    [obj setMessagesDetails:[NSSet setWithArray:DetailsSet]];
    
    [obj setLinkedTo:[dic valueForKey:JSON_RES_Messages_LinkedTo]];
    [obj setLinkedToRef:[dic valueForKey:JSON_RES_Messages_LinkedToRef]];
    [obj setAppUserId:[dic valueForKey:@"AppUserId"]];
    
    return obj;
    
}


+(NSDate *)curentDateStringFromUTCDate:(NSString *)utcDateTimeInLine withFormat:(NSString *)dateFormat {
    
    NSLog(@"UTC Date Time  : %@",utcDateTimeInLine);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [formatter dateFromString:utcDateTimeInLine];
    
    NSLog(@"Date : %@",date);
    
    return date;
    
}



+(BOOL)messageExist:(NSString*)ID
{
    NSManagedObjectContext *managedObjectContext = [GlobalCall getContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Messages" inManagedObjectContext:managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setFetchLimit:1];
    [request setPredicate:[NSPredicate predicateWithFormat:@"m_id == %@", ID]];
    
    NSError *error = nil;
    NSUInteger count = [managedObjectContext countForFetchRequest:request error:&error];
    
    if (count)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


@end
