//
//  TenantCall_Response.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>



#import "AppProductDefaults.h"
#import "Products.h"

@interface ProductCall_Response : NSObject

//Make Call for response
+(void)callApiForProductsForCategory:(NSString*)categoryName;
+(void)callApiForProductsForChildCategory:(NSString*)categoryName;
+(void)callApiForProductsFeatured;


+(Products*)callApiForProductWithID:(NSString*)productID;
+(Products*)callApiForGroupedProductWithID:(NSString*)productID;
    
+(NSArray*)callApiForProductsWithFilter:(NSArray*)arrayOfFilters;


+(void)callApiForSendCart:(NSArray*)details  saveArray:(NSArray*)array;
+(void)sendCartResponse:(NSNotification*)notification;


//WISHLIST

+(BOOL)callApiForAddingProductWithIDToWishlist:(Products*)product;
+(BOOL)callApiForRemovingProductWithIDToWishlist:(NSString*)product;


@end
