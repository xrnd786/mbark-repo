//
//  TenantCall_Response.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//


#define contr @"Products"
#define contrCart @"Cart"
#define contrWishlist @"Wishlist"

#define action_filters @"Filter"
#define action_featured @"Featured"
#define action_get @"Get"
#define action_post @"Post"
#define action_getGrouped @"GetGroup"
#define action_Activity @"Activity"

//***Products RequestBody keys****//
#define JSON_Body_AppUserId @"AppUserId"
#define JSON_Body_ProductId @"ProductId"
#define JSON_Body_Action @"Action"


//***Products RequestBody keys****//
#define JSON_REQ_FilterOn @"FilterOn"
#define JSON_REQ_Value @"Value"
#define JSON_REQ_Comparer @"Comparer"
#define JSON_REQ_Parameter @"Parameter"

//***Products RequestBody FilterOn keys****//
#define JSON_REQ_FilterOn_Category @"Category"
#define JSON_REQ_FilterOn_Attributes @"Attributes"
#define JSON_REQ_FilterOn_Rate @"Rate"

//***Products RequestBody Comparer keys****//
#define JSON_REQ_Comparer_Equals @"Equals"
#define JSON_REQ_Comparer_GreaterThan @"GreaterThan"
#define JSON_REQ_Comparer_LessThan @"LessThan"


#import "ProductCall_Response.h"
#import "ServerInteraction.h"

#import "Products.h"
#import "ProductCategory.h"
#import "User.h"
#import "Applications.h"
#import "ProductImages.h"
#import "ProductDocument.h"
#import  "ProductAttributes.h"
#import "ProductThumbnail.h"
#import "DCCustomInitialize.h"
#import "CartMaster.h"
#import "Cart.h"

@implementation ProductCall_Response

static NSString *category;
static NSArray *productsArrayLast;
static ServerInteraction *calling;

+(void)callApiForProductsForCategory:(NSString*)categoryName{
    
    category=categoryName;
   
    [GlobalCall removeOberverWithNotifier:APINotification_Product_Filters handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Product_Filters andSelector:@selector(productResponse:) handlerClass:self withObject:nil];
    
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    [dictionary setObject:JSON_REQ_FilterOn_Category forKey:JSON_REQ_FilterOn];
    [dictionary setObject:categoryName forKey:JSON_REQ_Value];
    [dictionary setObject:JSON_REQ_Comparer_Equals forKey:JSON_REQ_Comparer];
    
    NSArray *array=[[NSArray alloc]initWithObjects:dictionary, nil];
    
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_filters andRequestBody:array notifier:APINotification_Product_Filters methodType:POST headerKeyArray:nil headerValueArray:nil];
}

+(void)callApiForProductsForChildCategory:(NSString*)categoryName{
    
    category=categoryName;
    
    [GlobalCall removeOberverWithNotifier:APINotification_Product_Filters handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Product_Filters andSelector:@selector(productResponse:) handlerClass:self withObject:nil];
    
    
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    [dictionary setObject:JSON_REQ_FilterOn_Category forKey:JSON_REQ_FilterOn];
    [dictionary setObject:categoryName forKey:JSON_REQ_Value];
    [dictionary setObject:JSON_REQ_Comparer_Equals forKey:JSON_REQ_Comparer];
    
    NSArray *array=[[NSArray alloc]initWithObjects:dictionary, nil];
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_filters andRequestBody:array notifier:APINotification_Product_Filters methodType:POST headerKeyArray:nil headerValueArray:nil];

}

+(NSArray*)callApiForProductsWithFilter:(NSArray*)arrayOfFilters{
    
    
    __block NSMutableArray *res;
    __block NSArray *arrayResponse;
    
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
      
        
        NSArray *array=[[NSArray alloc]initWithArray:arrayOfFilters];
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:action_filters andRequestBody:array notifier:nil methodType:POST headerKeyArray:nil headerValueArray:nil];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            res= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            NSMutableArray *idArray=[NSMutableArray new];
            
            for(NSDictionary *dic in res){
                
                [idArray addObject:[dic valueForKey:JSON_RES_AppProducts_Id]];
                
            }
            
            ;
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                 arrayResponse =[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id IN %@",idArray]];
                
            });
            
        } else {
            
            res=[[NSMutableArray alloc]init];
            
        }
        
    });
    
    return arrayResponse
    ;
    
}


+(Products*)callApiForGroupedProductWithID:(NSString*)productID{
    
    __block NSDictionary *response;
    __block Products *obj;
    
    
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
        NSLog(@"Product ID%@",productID);
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:action_getGrouped andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:[[NSArray alloc]initWithObjects:productID, nil]];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            
            response= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
            
                NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",productID]]];
                obj=[arrayOfFetch lastObject];
                obj=[self returnMyGroupedObject:(NSArray*)[response valueForKey:@"GroupedProducts"] withObject:obj];
                
                
                NSLog(@"Object returned %@",obj.grouped);
                [GlobalCall saveContext];
    
                [self addAllGroupedProductsInCoreData:[response valueForKey:JSON_RES_AppProducts_GroupedProducts]];
                
            });
            
            
        } else {
            
            response=[[NSDictionary alloc]init];
            
        }
        
    });
    
    return obj;
    
    
}





+(Products*)callApiForProductWithID:(NSString*)productID{
    
    __block NSDictionary *response;
    __block Products *obj;
    
    
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
        NSLog(@"Product ID%@",productID);
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:action_get andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:[[NSArray alloc]initWithObjects:productID, nil]];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            response= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Products" inManagedObjectContext:[GlobalCall getContext]];
            
            NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",productID]]];
            
            BOOL isNew;
            
            if ([arrayOfFetch count]>0) {
                
                isNew=FALSE;
                obj=[arrayOfFetch lastObject];
                
            }else{
                
                isNew=YES;
                obj= [[Products alloc]initWithEntity:entity
                                 insertIntoManagedObjectContext:[GlobalCall getContext]];
                
            }
            

            [self returnMyObject:response withObject:obj andIsNew:isNew];
            [GlobalCall saveContext];
            
            
        } else {
            
            response=[[NSMutableDictionary alloc]init];
            
        }
        
    });
    
    return obj;
    
    
}


+(void)productResponse:(NSNotification*)notification{

    NSDictionary *main_dicationary=notification.userInfo;
    NSLog(@"Main dictionary %@",[main_dicationary valueForKey:JSON_RES_AppProducts_Category]);
    
    NSMutableArray *array =[[NSMutableArray alloc]initWithArray:(NSArray*)main_dicationary];
    
    
    dispatch_sync(dispatch_get_main_queue(), ^{
       
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Products" inManagedObjectContext:[GlobalCall getContext]];
            
            NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"productCategory==%@",category]];
            
            BOOL isNew;
            
            if ([arrayOfFetch count]>0) {
                
                isNew=FALSE;
                
            }else{
                
                isNew=YES;
            }
            
            
            
            for (NSDictionary *dic in array) {
                
                Products *obj;
                
                if (isNew) {
                    
                    obj = [[Products alloc]initWithEntity:entity
                           insertIntoManagedObjectContext:[GlobalCall getContext]];
                    
                }else{
                    
                    if([[arrayOfFetch valueForKeyPath:@"p_id"]containsObject:[dic valueForKey:JSON_RES_AppProducts_Id]]){
                        
                        int indexVal=(int)[[arrayOfFetch valueForKeyPath:@"p_id"]indexOfObject:[dic valueForKey:JSON_RES_AppProducts_Id]];
                        obj=(Products*)[arrayOfFetch objectAtIndex:indexVal];
                        
                    }else{
                        
                        obj = [[Products alloc]initWithEntity:entity
                               insertIntoManagedObjectContext:[GlobalCall getContext]];
                        
                    }
                    
                }
                
                [self returnMyObject:dic withObject:obj andIsNew:isNew];
                
               [GlobalCall saveContext];
                NSLog(@"Product %@ Added with Price %@",obj.name,obj.rate);
           
            }
            
            //NSArray *objArray=[[NSArray alloc]initWithArray:[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:nil]];
            //NSLog(@"Product Added %@",objArray);
            
            [GlobalCall removeOberverWithNotifier:APINotification_Categories_Get handlerClass:self withObject:nil];
            [GlobalCall removeOberverWithNotifier:APINotification_Product_Filters handlerClass:self withObject:nil];
        
        
    });
    
    
}



+(void)callApiForProductsFeatured{
    
    
    [GlobalCall removeOberverWithNotifier:APINotification_Product_FeaturedGet handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Product_FeaturedGet andSelector:@selector(featuredProductResponse:) handlerClass:self withObject:nil];
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_featured andRequestBody:nil notifier:APINotification_Product_FeaturedGet methodType:GET headerKeyArray:nil headerValueArray:nil];
}


+(void)featuredProductResponse:(NSNotification*)notification{
    
    NSDictionary *main_dicationary=notification.userInfo;
    
    NSMutableArray *array =[[NSMutableArray alloc]initWithArray:(NSArray*)main_dicationary];
    
    
    [AppProductDefaults saveFeaturedTotalCount:(int)[array count]];
    
    [AppProductDefaults saveAppFeaturedProducts:(NSArray*)array];
    
    [GlobalCall removeOberverWithNotifier:APINotification_Product_FeaturedGet handlerClass:self withObject:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:APINotification_Tenant_Saved object:nil];
    
 
}



+(void)callApiForProductWithID{
    
    
    [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Logo handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Tenant_Logo andSelector:@selector(singleProductResponse:) handlerClass:self withObject:nil];
    }


+(void)singleProductResponse:(NSNotification*)notification{
    
  
    [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Logo handlerClass:self withObject:nil];
    
}



+(void)callApiForSendCart:(NSArray*)details  saveArray:(NSArray*)productsArray
{

    [GlobalCall removeOberverWithNotifier:APINotification_Cart_Send handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Cart_Send andSelector:@selector(sendCartResponse:) handlerClass:self withObject:nil];
    
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    [dictionary setObject:@"Notes" forKey:@"Notes"];
    [dictionary setObject:details forKey:@"Details"];
    
    productsArrayLast=[[NSArray alloc]initWithArray:details];
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contrCart andAction:action_post andRequestBody:dictionary notifier:APINotification_Cart_Send methodType:POST headerKeyArray:nil headerValueArray:nil];

}


+(void)sendCartResponse:(NSNotification*)notification{
    
    NSDictionary *main_dicationary=notification.userInfo;
    main_dicationary=[main_dicationary mutableCopy];
    
   
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CartMaster" inManagedObjectContext:[GlobalCall getContext]];
    
    
    NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[CartMaster class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"c_id=='%@'",[main_dicationary valueForKey:@"Id"]]]];
    
    
    BOOL isNew;
    
    if ([arrayOfFetch count]>0) {
        
        isNew=false;
        
    } else {
        
        isNew=TRUE;
    }
    
    CartMaster *obj;
    
    if (isNew) {
        
        
        obj = [[CartMaster alloc]initWithEntity:entity
               insertIntoManagedObjectContext:[GlobalCall getContext]];
        
    }else{
        
            obj=[arrayOfFetch lastObject];
        
    }
    
    obj= [self returnCartObject:main_dicationary withObject:obj andIsNew:isNew];
    
    
    [GlobalCall saveContext];
    
    
    NSArray *arrayOfProductsFetched=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id IN %@",[[main_dicationary valueForKey:@"Details"] valueForKeyPath:@"ProductId"]]];
    
    for (Products *products in arrayOfProductsFetched) {
        
        products.isInCart=[NSNumber numberWithBool:FALSE];
        
    }
    
    [GlobalCall saveContext];
    
    [GlobalCall removeOberverWithNotifier:APINotification_Cart_Send handlerClass:self withObject:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:APINotification_Cart_Sent_Saved object:nil];
    
}


+(CartMaster*)returnCartObject:(NSDictionary*)dic withObject:(CartMaster*)obj andIsNew:(BOOL)isNew{
    
    
    obj.c_id=[dic valueForKey:JSON_RES_AppProducts_Id];
    obj.createdOn=[self curentDateStringFromUTCDate:[dic valueForKey:@"CartCreatedOn"] withFormat:@"dd-MM-yyyy"];
    
    NSMutableArray *cartDetailsSet=[NSMutableArray new];
    
    for (NSDictionary *dicDetails in [dic valueForKey:@"Details"]) {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Cart" inManagedObjectContext:[GlobalCall getContext]];
        Cart *objAtt = [[Cart alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
        
        
        objAtt.price=[dicDetails valueForKey:@"Price"];
        objAtt.productId=[dicDetails valueForKey:@"ProductId"];
        objAtt.quantity=[dicDetails valueForKey:@"Quantity"];
        
        [cartDetailsSet addObject:objAtt];
        
        [GlobalCall saveContext];
        
    }
    NSLog(@"Products array %@",[productsArrayLast valueForKeyPath:@"ProductId"]);
    
    
    [obj setProducts:[NSSet setWithArray:[productsArrayLast valueForKeyPath:@"ProductId"]]];
    
    [obj setDetails:[NSSet setWithArray:cartDetailsSet]];
    [obj setNotes:[dic valueForKey:@"Notes"]];
    [obj setStatus:[NSNumber numberWithBool:[[dic valueForKey:@"Status"] boolValue]]];
    
    return obj;
   
}


+(NSDate *)curentDateStringFromUTCDate:(NSString *)utcDateTimeInLine withFormat:(NSString *)dateFormat {
    
    NSLog(@"UTC Date Time  : %@",utcDateTimeInLine);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [formatter dateFromString:utcDateTimeInLine];
    
    NSLog(@"Date : %@",date);
    
    return date;
   
}


#pragma mark - wish list

+(BOOL)callApiForAddingProductWithIDToWishlist:(Products*)product{
    
    __block BOOL response;
    
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    NSArray *keys=[[NSArray alloc]initWithObjects:JSON_Body_AppUserId,JSON_Body_ProductId,JSON_Body_Action, nil];
    
    __block NSString *uId;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
       
      uId =((User*)[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject]).u_id;
        
    });
    
    NSLog(@"UID %@ and ProductID %@",uId,product.p_id);
    
    NSArray *values=[[NSArray alloc]initWithObjects:uId,product.p_id,@"Added", nil];
    
    
    dispatch_sync(Web_Call_queue, ^{
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contrWishlist andAction:action_Activity andRequestBody:[[NSDictionary alloc]initWithObjects:values forKeys:keys] notifier:nil methodType:POST headerKeyArray:nil headerValueArray:nil];
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:201]])
        {
            response= TRUE;
            
            
        } else {
            
            response= FALSE;
            
        }
        
    });
    
    return response;
    
    
}


+(BOOL)callApiForRemovingProductWithIDToWishlist:(NSString*)productID{
    
    __block BOOL response;
    
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    NSArray *keys=[[NSArray alloc]initWithObjects:JSON_Body_AppUserId,JSON_Body_ProductId,JSON_Body_Action, nil];
    NSString *uId=((User*)[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:@"u_id" withPredicate:nil] lastObject]).u_id;
    
    NSArray *values=[[NSArray alloc]initWithObjects:uId,productID,@"Removed", nil];
    
    
    dispatch_sync(Web_Call_queue, ^{
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contrWishlist andAction:action_Activity andRequestBody:[[NSDictionary alloc]initWithObjects:values forKeys:keys] notifier:nil methodType:POST headerKeyArray:nil headerValueArray:nil];
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:201]])
        {
            response= TRUE;
            
        } else {
            
            response= FALSE;
            
        }
        
    });
    
    return response;
    
    
}


+(Products*)returnMyObject:(NSDictionary*)dic withObject:(Products*)obj andIsNew:(BOOL)isNew{
   
    
    obj.p_id=[dic valueForKey:JSON_RES_AppProducts_Id];
    obj.applications=[[dic valueForKey:JSON_RES_AppProducts_Applications] lastObject];
    [obj setName:[dic valueForKey:JSON_RES_AppProducts_Name]];
    obj.discount=[dic valueForKey:JSON_RES_AppProducts_Discount];
    obj.isDiscontinued=[dic valueForKey:JSON_RES_AppProducts_Discontinued];
    obj.isFeatured=[dic valueForKey:JSON_RES_AppProducts_Featured];
    
    if (isNew) {
        
        
        obj.isFavourite=[NSNumber numberWithBool:FALSE];
        obj.isInCart=[NSNumber numberWithBool:FALSE];
    
    }
    
    
    NSMutableArray *attSet=[NSMutableArray new];
    
    for (NSDictionary *dicatt in [dic valueForKey:JSON_RES_AppProducts_Attributes]) {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ProductAttributes" inManagedObjectContext:[GlobalCall getContext]];
        ProductAttributes *objAtt = [[ProductAttributes alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
        objAtt.att_id=[dicatt valueForKey:JSON_RES_AppProducts_Id];
        objAtt.attributeValue=[dicatt valueForKey:JSON_RES_AppProducts_AttributeValue];
        objAtt.name=[dicatt valueForKey:JSON_RES_AppProducts_Name];
        objAtt.type=[dicatt valueForKey:JSON_RES_AppProducts_Type];
        objAtt.group=[dicatt valueForKey:JSON_RES_AppProducts_Group];
        objAtt.product_id=obj;
        
        [attSet addObject:objAtt];
        
        [GlobalCall saveContext];
    }
    
    
   
    [obj setAttributes:[NSSet setWithArray:attSet]];
    
    NSMutableArray *imgSet=[NSMutableArray new];
    NSArray *imgArray=[[NSArray alloc]initWithArray:([dic valueForKey:JSON_RES_AppProducts_Images]!=[NSNull null])?[dic valueForKey:JSON_RES_AppProducts_Images]:[[NSArray alloc]init]];
    
    for (NSDictionary *dicimag in imgArray) {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ProductImages" inManagedObjectContext:[GlobalCall getContext]];
        ProductImages *objAtt = [[ProductImages alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
        objAtt.image_id=[dicimag valueForKey:JSON_RES_AppProducts_Thumbnail_Image];
        objAtt.image_url=obj;
        
        [imgSet addObject:objAtt];
        
        [GlobalCall saveContext];
        
    }
    
    
    if ([imgSet count]>0) {
        
        [obj addImages:[NSSet setWithArray:imgSet]];
        
    }
    
    NSEntityDescription *entityThumb = [NSEntityDescription entityForName:@"ProductThumbnail" inManagedObjectContext:[GlobalCall getContext]];
    ProductThumbnail *objThumb = [[ProductThumbnail alloc]initWithEntity:entityThumb insertIntoManagedObjectContext:[GlobalCall getContext]];
    objThumb.p_id=[[dic valueForKey:JSON_RES_AppProducts_Thumbnail] valueForKey:JSON_RES_AppProducts_Thumbnail_Image];
    objThumb.image_url=obj;
    
    [GlobalCall saveContext];
    
    obj.thumbnail=objThumb;
   
    
    obj.rate=[NSNumber numberWithDouble:[[dic valueForKey:JSON_RES_AppProducts_Rate] doubleValue]];
    
    NSLog(@"Original Value is %@ and Object value is %@",[dic valueForKey:JSON_RES_AppProducts_Rate],obj.rate);
    
    obj.shippedInDays=[dic valueForKey:JSON_RES_AppProducts_ShippedInDays];
    obj.isOutOfStock=[dic valueForKey:JSON_RES_AppProducts_OutOfStock];
    obj.measurementUnit=[dic valueForKey:JSON_RES_AppProducts_MeasurementUnit];
    obj.sKU=[dic valueForKey:JSON_RES_AppProducts_SKU];
    obj.tags=([dic valueForKey:JSON_RES_AppProducts_Tags]!=[NSNull null])?[dic valueForKey:JSON_RES_AppProducts_Tags]:@"";
    obj.type=[dic valueForKey:JSON_RES_AppProducts_Type];
    obj.availableUnits=[dic valueForKey:JSON_RES_AppProducts_AvailableUnits];
    obj.productDescription=([dic valueForKey:JSON_RES_AppProducts_Description]!=[NSNull null])?[dic valueForKey:JSON_RES_AppProducts_Description]:@"";
    obj.productCategory=[dic valueForKey:JSON_RES_AppProducts_Category];
    
    return obj;
    
}



+(Products*)returnMyGroupedObject:(NSArray*)dic withObject:(Products*)obj{
    
    NSMutableArray *objectIds=[NSMutableArray new];
    
    for (NSDictionary *dictionary in dic) {
        
        [objectIds addObject:[dictionary valueForKey:JSON_RES_AppProducts_Id]];
        
    }
    
    NSLog(@"Object Ids %@",objectIds);
    
    [obj setGrouped:[NSSet setWithArray:objectIds]];
    return obj;
    
}


+(void)addAllGroupedProductsInCoreData:(NSArray*)array{
   
    for (NSDictionary *dic in array) {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Products" inManagedObjectContext:[GlobalCall getContext]];
        
        Products *objProd= [[Products alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
        objProd=[self returnMyObject:dic withObject:objProd andIsNew:TRUE];
        objProd.productCategory=@"";
        objProd.isFeatured=[NSNumber numberWithBool:FALSE];
        
        [GlobalCall saveContext];
        
    }
}

@end
