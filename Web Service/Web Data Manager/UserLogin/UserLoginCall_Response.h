//
//  TenantCall_Response.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>

@interface UserLoginCall_Response : NSObject


+(void)callApiForUserLoginCreation:(NSString*)password andUsername:(NSString*)username;
+(void)callApiForUserLoginChange:(NSString*)password newPassword:(NSString*)newPassword  action:(void (^)(bool answer))responseBlock;


+(BOOL)callApiForUserLoginAuthenticaton:(NSString*)password withUsername:(NSString*)username;


@end
