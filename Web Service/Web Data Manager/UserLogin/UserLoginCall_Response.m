//
//  TenantCall_Response.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//


#define contr @"AppUsers"

#define action_Login @"Login"
#define action_Update @"Update"
#define action_CreateLogin @"CreateLogin"
#define action_ChangeLogin @"ChangeLogin"

#define JSON_Body_AppUserLogin_Id @"Id"
#define JSON_Body_AppUserLogin_Password @"Password"
#define JSON_Body_AppUserLogin_PreviousPassword @"PreviousPassword"
#define JSON_Body_AppUserLogin_Device @"Device"
#define JSON_Body_AppUserLogin_Username @"Username"


#import "Global_Server_Constants.h"
#import "UserLoginCall_Response.h"
#import "ServerInteraction.h"

#import "User.h"

@implementation UserLoginCall_Response

static ServerInteraction *calling;

+(void)callApiForUserLoginCreation:(NSString*)password andUsername:(NSString*)username{
    
    NSArray *keyArray=[[NSArray alloc]initWithObjects:JSON_Body_AppUserLogin_Id,JSON_Body_AppUserLogin_Password,JSON_Body_AppUserLogin_Username, nil];
   
    NSString *uId=((User*)[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:@"u_id" withPredicate:nil] lastObject]).u_id;
    
    NSArray *valueArray=[[NSArray alloc]initWithObjects:uId,password,username, nil];
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_CreateLogin andRequestBody:[[NSDictionary alloc]initWithObjects:valueArray forKeys:keyArray] notifier:nil methodType:POST headerKeyArray:nil headerValueArray:nil];
    
    calling=nil;
    
}


+(void)UserCreationResponse:(NSNotification*)notification{

    
}


+(BOOL)callApiForUserLoginAuthenticaton:(NSString*)password withUsername:(NSString*)username{
    
    
    __block BOOL answer;
   
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_sync(Web_Call_queue, ^{
        
        [GlobalCall removeOberverWithNotifier:APINotification_UserLogin_Login handlerClass:self withObject:nil];
        [GlobalCall addObserverWithNotifier:APINotification_UserLogin_Login andSelector:@selector(UserCreationResponse:) handlerClass:self withObject:nil];
        
        NSArray *keyArray=[[NSArray alloc]initWithObjects:JSON_Body_AppUserLogin_Username,JSON_Body_AppUserLogin_Password, nil];
        NSArray *valueArray=[[NSArray alloc]initWithObjects:username,password, nil];
        
        ServerInteraction *calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:action_Login andRequestBody:[[NSDictionary alloc]initWithObjects:valueArray forKeys:keyArray] notifier:APINotification_UserLogin_Login methodType:POST headerKeyArray:nil headerValueArray:nil];
        
        if ([calling.responseHeader integerValue]==200) {
            
            answer=TRUE;
            
        } else {
            
            answer=FALSE;
            
        }
        
    });
    
    
    return answer;
    
}


+(void)callApiForUserLoginChange:(NSString*)password newPassword:(NSString*)newPassword action:(void (^)(bool answer))responseBlock{
    
    NSArray *keyArray=[[NSArray alloc]initWithObjects:JSON_Body_AppUserLogin_Id,JSON_Body_AppUserLogin_Password,JSON_Body_AppUserLogin_PreviousPassword, nil];
    NSString *uId=((User*)[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:@"u_id" withPredicate:nil] lastObject]).u_id;
   
    
    NSArray *valueArray=[[NSArray alloc]initWithObjects:uId,newPassword,password, nil];
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_ChangeLogin  andRequestBody:[[NSDictionary alloc]initWithObjects:valueArray forKeys:keyArray] notifier:nil methodType:POST headerKeyArray:nil headerValueArray:nil];
    
    
    if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]]) {
        
        responseBlock(true);
        
        
    }else{
        
        responseBlock(false);
        
    }
    

}


@end
