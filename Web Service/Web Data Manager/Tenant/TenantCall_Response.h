//
//  TenantCall_Response.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>

@interface TenantCall_Response : NSObject

+(void)callApiForTenantProfile;
+(NSData*)callApiForTenantLogo;


+(NSString*)callApiForTenantBanner;
+(void)callApiForTenantSlider;

@end
