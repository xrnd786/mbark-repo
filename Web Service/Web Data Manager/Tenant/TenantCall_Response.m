//
//  TenantCall_Response.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//


#define contr @"TenantProfile"
#define contr_image @"Images"

#define action_add @"Get"
#define action_logo @"Logo"
#define action_banner @"Banner"
#define action_slider @"Slider"

//***tenant response keys****//
#define JSON_RES_TenantDetails @"TenantDetails"

//***APP UI response keys****//
#define JSON_RES_AppUI @"AppUI"

//***APP Settings response keys****//
#define JSON_RES_AppSettings @"AppSettings"

//***Slider response keys****//
#define JSON_RES_Sliders @"Sliders"


#import "TenantCall_Response.h"
#import "ServerInteraction.h"

#import "AppUIDefaults.h"
#import "AppSettingDefaults.h"
#import "AppTenantDefaults.h"
#import "AppSlidersDefaults.h"


@implementation TenantCall_Response

static int number;
static int current_number;
static ServerInteraction *calling;

+(void)callApiForTenantProfile{
   
    
       [GlobalCall removeOberverWithNotifier:APINotification_Tenant handlerClass:self withObject:nil];
        [GlobalCall addObserverWithNotifier:APINotification_Tenant andSelector:@selector(tenantResponse:) handlerClass:self withObject:nil];
   
      calling=[[ServerInteraction alloc]init];
      calling=[calling initWithController:contr andAction:action_add andRequestBody:nil notifier:APINotification_Tenant methodType:GET headerKeyArray:nil headerValueArray:nil];
        
    
}


+(void)tenantResponse:(NSNotification*)notification{

    NSDictionary *main_dicationary=notification.userInfo;
   
    if (main_dicationary) {
        
        NSDictionary *tenant_Dictionary=[main_dicationary valueForKey:JSON_RES_TenantDetails];
        [AppTenantDefaults saveTenantDetails:tenant_Dictionary];
        
        NSDictionary *appUI_Dictionary=[main_dicationary valueForKey:JSON_RES_AppUI];
        [AppUIDefaults saveAppUIDetails:appUI_Dictionary];
        
        NSDictionary *appSettings_Dictionary=[main_dicationary valueForKey:JSON_RES_AppSettings];
        [AppSettingDefaults saveAppSettings:appSettings_Dictionary];
        
        NSArray *sliders_array=[main_dicationary valueForKey:JSON_RES_Sliders];
        [AppSlidersDefaults saveSliderDetails:sliders_array];
        
        NSString *tenant_ID=[main_dicationary valueForKey:@"TenantId"];
        [AppTenantDefaults saveTenantID:tenant_ID];
        
    }
  
    [GlobalCall removeOberverWithNotifier:APINotification_Tenant handlerClass:self withObject:nil];
    
}

+(NSData*)callApiForTenantLogo{
    
    __block NSData *res;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
   
    dispatch_sync(Web_Call_queue, ^{
        
    
        //[GlobalCall removeOberverWithNotifier:APINotification_Tenant_Logo handlerClass:self withObject:nil];
       // [GlobalCall addObserverWithNotifier:APINotification_Tenant_Logo andSelector:@selector(tenantLogoResponse:) handlerClass:self withObject:nil];
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr_image andAction:action_logo andRequestBody:nil notifier:APINotification_Tenant_Logo methodType:GET headerKeyArray:nil headerValueArray:nil];
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            
            res = [[NSData alloc] initWithData:calling.receivedData];
            
        }
    

    });

    
    return res;

}


+(void)tenantLogoResponse:(NSNotification*)notification{
    
    
    NSString *raw_data=[NSString stringWithFormat:@"%@",notification.object];
    NSString *finalData;
    
    if (raw_data) {
        
        finalData=raw_data;
    
    }
    
    [AppTenantDefaults saveTenantLogo:finalData];
   // [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Logo handlerClass:self withObject:nil];
    
}


+(NSString*)callApiForTenantBanner{
    
    __block NSString *res;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
    
        [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Banner handlerClass:self withObject:nil];
        [GlobalCall addObserverWithNotifier:APINotification_Tenant_Banner andSelector:@selector(tenantBannerResponse:) handlerClass:self withObject:nil];
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr_image andAction:action_banner andRequestBody:nil notifier:APINotification_Tenant_Banner methodType:GET headerKeyArray:nil headerValueArray:nil];
        
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            
            res = [[NSString alloc] initWithData:calling.receivedData encoding:NSASCIIStringEncoding];
            
        }

        
    });
    
    return res;
}


+(void)tenantBannerResponse:(NSNotification*)notification{
    
    
    NSString *raw_data=[NSString stringWithFormat:@"%@",notification.object];
    //NSLog(@"RAW DATA %@",raw_data);
    
    NSString *finalData;
    
    if (raw_data) {
        
        finalData=raw_data;
        
    }
    
    [AppTenantDefaults saveTenantBanner:finalData];
    [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Banner handlerClass:self withObject:nil];
    
}


+(void)callApiForTenantSlider{
    
    
    [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Slider handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Tenant_Slider andSelector:@selector(tenantSliderResponse:) handlerClass:self withObject:nil];
    
    NSArray *images_urls=[[NSArray alloc]initWithArray:[AppSlidersDefaults getSlidersImage]];
    number=(int)[images_urls count];
    
    for (int i=0; i<number; i++) {
    
        NSArray *keys=[[NSArray alloc]initWithObjects:@"image", nil];
        NSArray *values=[[NSArray alloc]initWithObjects:[images_urls objectAtIndex:number], nil];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        
        dispatch_sync(queue, ^{
           
            current_number=i;
            [self FinalCallToSlider:keys andValues:values];
        
        });
    }
    
    
}


+(void)FinalCallToSlider:(NSArray*)keys andValues:(NSArray*)values{
    
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr_image andAction:action_slider andRequestBody:nil notifier:APINotification_Tenant_Slider methodType:GET headerKeyArray:(NSArray*)keys headerValueArray:(NSArray*)values];

    
}


+(void)tenantSliderResponse:(NSNotification*)notification{
    
    NSString *raw_data=[NSString stringWithFormat:@"%@",notification.object];
    //NSLog(@"RAW DATA %@",raw_data);
    
    NSString *finalData;
    
    if (raw_data) {
        
        finalData=raw_data;
        
    }
    
    [AppSlidersDefaults saveSliderImageFor:[NSString stringWithFormat:@"%d",current_number] withData:finalData];
    [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Slider handlerClass:self withObject:nil];
    
}



@end
