//
//  TenantCall_Response.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//


#define contr @"Categories"

#define action_get @"Get"
#define action_filters @"GetFilters"

//***Category response keys****//
#define JSON_RES_AppCategory_Id @"Id"
#define JSON_RES_AppCategory_Name @"Name"
#define JSON_RES_AppCategory_Description @"Description"
#define JSON_RES_AppCategory_Image @"Image"
#define JSON_RES_AppCategory_Image_Url @"Url"
#define JSON_RES_AppCategory_Sequence @"Sequence"
#define JSON_RES_AppCategory_Children @"Children"


#import "CategoryCall_Response.h"
#import "ServerInteraction.h"

#import "CategoryImage.h"
#import "ProductCategory.h"
#import "FilterObject.h"

@implementation CategoryCall_Response


+(void)callApiForCategory{
    
    
    [GlobalCall removeOberverWithNotifier:APINotification_Categories_Get handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Categories_Get andSelector:@selector(categoryResponse:) handlerClass:self withObject:nil];
    
    ServerInteraction *calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_get andRequestBody:nil notifier:APINotification_Categories_Get methodType:GET headerKeyArray:nil headerValueArray:nil];
    calling=nil;
}


+(void)categoryResponse:(NSNotification*)notification{

    NSLog(@"Category response received ");
    
    
    NSMutableArray *array =[[NSMutableArray alloc]initWithArray:(NSArray*)notification.userInfo];
    
    
    NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:nil];
    
    BOOL isNew;
    
    if ([arrayOfFetch count]>0) {
        
        isNew=FALSE;
        
    }else{
        
        isNew=YES;
    }
    
    
    
    
    for (NSDictionary *dic in array) {
        
        ProductCategory *obj;
        
        if (isNew) {
            
            obj =  (ProductCategory *)[NSEntityDescription insertNewObjectForEntityForName:@"ProductCategory" inManagedObjectContext:[GlobalCall getContext]];
            
            
        }else{
            
            if([[arrayOfFetch valueForKeyPath:@"pcat_id"]containsObject:[dic valueForKey:JSON_RES_AppCategory_Id]]){
                
                int indexVal=(int)[[arrayOfFetch valueForKeyPath:@"pcat_id"]indexOfObject:[dic valueForKey:JSON_RES_AppCategory_Id]];
                obj=(ProductCategory*)[arrayOfFetch objectAtIndex:indexVal];
                
            }else{
                
                obj =  (ProductCategory *)[NSEntityDescription insertNewObjectForEntityForName:@"ProductCategory" inManagedObjectContext:[GlobalCall getContext]];
                
                
            }
            
        }

        
        obj.pcat_id=[dic valueForKey:JSON_RES_AppCategory_Id];
        obj.name=[dic valueForKey:JSON_RES_AppCategory_Name];
        obj.categoryDescription=([dic valueForKey:JSON_RES_AppCategory_Description]==[NSNull null])?@"No Description":[dic valueForKey:JSON_RES_AppCategory_Description];
        
        NSEntityDescription *locationEntity = [NSEntityDescription entityForName:@"CategoryImage" inManagedObjectContext:[GlobalCall getContext]];
        
        CategoryImage *catImage=[[CategoryImage alloc]initWithEntity:locationEntity insertIntoManagedObjectContext:[GlobalCall getContext]];
        catImage.image=[[dic valueForKey:JSON_RES_AppCategory_Image] valueForKey:JSON_RES_AppCategory_Image];
        catImage.productCategory = obj;
        
        
        [obj setImage:catImage];
        
        obj.sequence=[dic valueForKey:JSON_RES_AppCategory_Sequence];
        [obj addChildren:[NSSet setWithArray:[dic valueForKey:JSON_RES_AppCategory_Children]]];
        
        NSLog(@"Object value of category %@",obj.name);
        [GlobalCall saveContext];
        
    }
    
    
    NSArray *objArray=[[NSArray alloc]initWithArray:[[GlobalCall getDelegate] fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:nil]];
    
    
     NSLog(@"Categories Added %@",[objArray valueForKeyPath:@"name"]);
    [GlobalCall removeOberverWithNotifier:APINotification_Categories_Get handlerClass:self withObject:nil];
    
}


+(NSArray*)callApiForCategoryFilter:(NSString*)categoryName{
    
    __block NSMutableArray *responseArray=[NSMutableArray new];
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"name == %@", categoryName];
        
        ProductCategory *objReqired=(ProductCategory*)[[[GlobalCall getDelegate] fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil  withPredicate:predicate] lastObject];
        
        NSString *categoryID=objReqired.pcat_id;
        NSArray *titleValue=[[NSArray alloc]initWithObjects:encodeToPercentEscapeStrings(categoryID), nil];
    
    ServerInteraction *calling=[[ServerInteraction alloc]init];
    calling=[calling initWithController:contr andAction:action_filters andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:titleValue];
    NSError *error;
       
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            
            responseArray= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            responseArray=[self returnObject:responseArray];
            
            //[objReqired addFilter:[NSSet setWithArray:res]];
            //[GlobalCall saveContext];
            
           // NSArray *objArray=[[NSArray alloc]initWithArray:[[GlobalCall getDelegate] fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:nil]];
            NSLog(@"Categories Filters Added %@",responseArray);
            
            
        } else {
        
            responseArray=[NSMutableArray new];
            
        }
        
        
    });
    
    return responseArray;
    
}


+(NSMutableArray*)returnObject:(NSArray*)array{
    
    
    NSMutableArray *filterArray=[NSMutableArray new];
    
    for(NSDictionary *dic in array){
        
        FilterObject *object=[FilterObject new];
        
        object.Name=[dic valueForKey:JSON_RES_AppAttribute_Name];
        object.AttributeId=[dic valueForKey:@"AttributeId"];
        object.AttributeName=[dic valueForKey:@"AttributeName"];
        object.AttributeType=[dic valueForKey:@"AttributeType"];
        object.Type=[dic valueForKey:@"Type"];
        object.Values=[dic valueForKey:JSON_RES_AppAttribute_Values];
        
        [filterArray addObject:object];
        
    }
    
    return filterArray;
    
}


// Encode a string to embed in an URL.
NSString* encodeToPercentEscapeStrings(NSString *string) {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}



@end
