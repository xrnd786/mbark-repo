//
//  TenantCall_Response.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>

@interface CategoryCall_Response : NSObject

+(void)callApiForCategory;
+(NSArray*)callApiForCategoryFilter:(NSString*)categoryName;



@end
