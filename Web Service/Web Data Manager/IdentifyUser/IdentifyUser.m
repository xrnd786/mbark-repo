//
//  IdentifyUser.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//


#define contr @"AppUsers"
#define action_add @"Add"
#define action_update @"Update"
#define action_identify @"Identify"


#define JSON_BODY_Name @"Name"
#define JSON_BODY_Username @"Username"
#define JSON_BODY_Organization @"Organization"
#define JSON_BODY_Contact @"Contact"
#define JSON_BODY_Address @"Address"
#define JSON_BODY_Location @"Location"
#define JSON_BODY_Facebook @"Facebook"
#define JSON_BODY_Twitter @"Twitter"
#define JSON_BODY_Email @"Email"
#define JSON_BODY_Pincode @"Pincode"
#define JSON_BODY_Gender @"Gender"
#define JSON_BODY_Password @"Password"


#import "IdentifyUser.h"
#import "ServerInteraction.h"
#import "AppUserDefaults.h"

#import "User.h"
#import "Location.h"

@implementation IdentifyUser
{
}
static UserProfile *profileObj;
static NSMutableArray *arrayOfTitles;
static NSMutableArray *arrayOfValues;
static ServerInteraction *calling;


+(void)addUserForStart{
    
    [GlobalCall removeOberverWithNotifier:APINotification_User_Registration handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_User_Registration andSelector:@selector(firstUserResponse:) handlerClass:self withObject:nil];
  
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initForAppLaunchWithController:contr andAction:action_identify withNotifierString:APINotification_User_Registration];
    
    [GlobalCall removeOberverWithNotifier:APINotification_User_Registration handlerClass:self withObject:nil];
    
    
}

+(void)firstUserResponse:(NSNotification*)notification{
    
    
    User *obj = (User *)[NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:[GlobalCall getContext]];
    obj.u_id = [notification.userInfo valueForKey:@"Id"];
    
    [GlobalCall saveContext];
    
    
}


+(void)UpdateUserWithDataEntered:(UserProfile*)profile{
    
    [GlobalCall removeOberverWithNotifier:APINotification_User_Details handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_User_Details andSelector:@selector(userResponse:) handlerClass:self withObject:nil];
    
    profileObj=profile;
    
    [self getArrayOfTitles];
    
    
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]initWithObjects:arrayOfValues forKeys:arrayOfTitles];
    
    
    calling=[[ServerInteraction alloc]init];
    calling=[calling initWithControllerForProfile:contr andAction:action_update andRequestBody:dictionary notifier:APINotification_User_Details methodType:POST headerKeyArray:nil  headerValueArray:nil];
    
}

+(void)getArrayOfTitles{
    
    
    arrayOfTitles=[[NSMutableArray alloc]init];
    arrayOfValues=[[NSMutableArray alloc]init];
    
    if (profileObj.Firstname!=nil) {
        
        [arrayOfTitles addObject:JSON_BODY_Name];
        [arrayOfValues addObject:[[profileObj.Firstname stringByAppendingString:@" "] stringByAppendingString:profileObj.Lastname]];
        
    }
    
    
    if (profileObj.username!=nil) {
        
        [arrayOfTitles addObject:JSON_BODY_Username];
        [arrayOfValues addObject:profileObj.username];
        
    }
    if (profileObj.Organization) {
        
        [arrayOfTitles addObject:JSON_BODY_Organization];
        [arrayOfValues addObject:profileObj.Organization];
        
    }
    
    if (profileObj.Contact) {
        
        [arrayOfTitles addObject:JSON_BODY_Contact];
        [arrayOfValues addObject:profileObj.Contact];
        
    }
    
    if (profileObj.AddressLine1) {
        
        [arrayOfTitles addObject:JSON_BODY_Address];
        [arrayOfValues addObject:[[[[profileObj.AddressLine1 stringByAppendingString:profileObj.AddressLine2] stringByAppendingString:[profileObj.Pincode stringValue]] stringByAppendingString:profileObj.State] stringByAppendingString:profileObj.Country]];
        
    }
    
    if (profileObj.Location) {
        
        [arrayOfTitles addObject:JSON_BODY_Location];
        [arrayOfValues addObject:profileObj.Location];
        
    }
    
    if (profileObj.Email) {
        
        [arrayOfTitles addObject:JSON_BODY_Email];
        [arrayOfValues addObject:profileObj.Email];
        
    }
    
    if (profileObj.Facebook) {
        
        [arrayOfTitles addObject:JSON_BODY_Facebook];
        [arrayOfValues addObject:profileObj.Facebook];
        
    }
    
    if (profileObj.Twitter) {
        
        [arrayOfTitles addObject:JSON_BODY_Twitter];
        [arrayOfValues addObject:profileObj.Twitter];
        
    }
    
    
    if (profileObj.Gender) {
        
        [arrayOfTitles addObject:JSON_BODY_Gender];
        [arrayOfValues addObject:profileObj.Gender];
        
    }
    
    
}


+(void)userResponse:(NSNotification*)notification{
    
    NSDictionary *responseDictionary=(NSDictionary*)[notification userInfo];
    
    
    User *obj=[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
    
    
    if ([[responseDictionary allKeys] count]!=0) {
        
        profileObj.UserID=[responseDictionary valueForKey:@"Id"];
        obj.u_id = [notification.userInfo valueForKey:@"Id"];
        
    }
    
    
    obj.addressLine1 = profileObj.AddressLine1;
    obj.addressLine2 = profileObj.AddressLine2;
    obj.pincode = profileObj.Pincode;
    obj.country = profileObj.Country;
    obj.state = profileObj.State;
   
    obj.contact = profileObj.Contact;
    obj.email = profileObj.Email;
    
    obj.facebook = profileObj.Facebook;
    obj.twitter=profileObj.Twitter;
    
    obj.gender = profileObj.Gender;
    
    
    NSEntityDescription *locationEntity = [NSEntityDescription entityForName:@"Location" inManagedObjectContext:[GlobalCall getContext]];
  
    Location *location=[[Location alloc]initWithEntity:locationEntity insertIntoManagedObjectContext:[GlobalCall getContext]];
    location.latitude=@"19.76";
    //profileObj.Location.Latitude;
    location.longitude=@"19.67";
    //profileObj.Location.Longitude;
    
    
    
    [obj setLocation:[NSKeyedArchiver archivedDataWithRootObject:location]];
    obj.firstName =profileObj.Firstname;
    obj.lastName =profileObj.Lastname;
    obj.username=profileObj.username;
    
    if (profileObj.Password) {
        obj.password=profileObj.Password;
    }
    
    
    NSLog(@"New obj %@",obj);
    
    [GlobalCall saveContext];
    
    [GlobalCall removeOberverWithNotifier:APINotification_User_Registration handlerClass:self withObject:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:APINotification_User_Registration_Done object:nil];

}


@end
