//
//  IdentifyUser.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"

@interface IdentifyUser : NSObject



+(void)addUserForStart;
+(void)UpdateUserWithDataEntered:(UserProfile*)profile;

@end
