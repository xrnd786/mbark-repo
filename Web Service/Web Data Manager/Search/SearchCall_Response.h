//
//  TenantCall_Response.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>

//***application response keys****//
#define JSON_RES_Application_Name @"Name"
#define JSON_RES_Application_Image @"Image"
#define JSON_RES_Application_Products @"Products"
#define JSON_RES_Application_Id @"Id"
#define JSON_RES_Application_TenantId @"TenantId"


@interface SearchCall_Response : NSObject

+(NSArray*)callApiForSearchListWithText:(NSString*)searchText;
+(NSArray*)callApiForProductListWithSearchText:(NSString*)finalSearchText;
+(NSArray*)callApiForPopularSearchList;

@end
