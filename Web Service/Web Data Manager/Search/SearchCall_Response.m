//
//  TenantCall_Response.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//


#define contr @"Products"

#define action_prompt @"Prompt"
#define action_search @"Search"

#define action_Popsearch @"PopularSearches"
#define contr_tenant @"TenantProfile"


#import "SearchCall_Response.h"
#import "ServerInteraction.h"

#import "ProductAttributes.h"
#import "ProductImages.h"
#import "ProductThumbnail.h"

@implementation SearchCall_Response

static ServerInteraction *calling;


+(NSArray*)callApiForSearchListWithText:(NSString*)searchText{
    
    
    __block NSArray *response;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    searchText=[searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *final=encodeToPercentEscapeString(searchText);
    NSLog(@"Final string is %@",final);
    
    
    dispatch_sync(Web_Call_queue, ^{
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:[[action_prompt stringByAppendingString:@"?text="] stringByAppendingString:final] andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:nil];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            response= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
        } else {
            
            response=[[NSArray alloc]init];
            
        }
        
        
    });
    
    return response;

    
}


+(NSArray*)callApiForProductListWithSearchText:(NSString*)finalSearchText{
    
    __block NSArray *response;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    finalSearchText=[finalSearchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *final=encodeToPercentEscapeString(finalSearchText);
    //[finalSearchText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    
    NSString *actionString=[NSString stringWithFormat:@"%@",[[action_search stringByAppendingString:@"?text="] stringByAppendingString:final]];
    
    
    NSLog(@"Controler adn anction %@ adn %@",contr,actionString);
    
    dispatch_sync(Web_Call_queue, ^{
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr andAction:actionString  andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:nil];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            response= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
            
                 response=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id IN %@",[self returnObjectListWithDictionary:response]]];
                
            });
            
        } else {
            
            response=[[NSArray alloc]init];
            
        }
        
        
    });
    
    return response;
    
    
}


// Encode a string to embed in an URL.
NSString* encodeToPercentEscapeString(NSString *string) {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                            (CFStringRef) string,
                                            NULL,
                                            (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                            kCFStringEncodingUTF8));
}



+(NSArray*)callApiForPopularSearchList{
    
    
    __block NSArray *response;
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_sync(Web_Call_queue, ^{
        
        calling=[[ServerInteraction alloc]init];
        calling=[calling initWithController:contr_tenant andAction:action_Popsearch andRequestBody:nil notifier:nil methodType:GET headerKeyArray:nil headerValueArray:nil];
        
        NSError *error;
        
        if ([calling.responseHeader isEqualToNumber:[NSNumber numberWithInt:200]])
        {
            response= [NSJSONSerialization JSONObjectWithData:calling.receivedData options:NSJSONReadingMutableLeaves error:&error];
            
            
            
        } else {
            
            response=[[NSArray alloc]init];
            
        }
        
        
    });
    
    return response;
    
    
}


+(NSArray*)returnObjectListWithDictionary :(NSArray*)mainDictionary{
    
    
    NSMutableArray *justIds=[NSMutableArray new];
    
    NSMutableArray *array =[NSMutableArray new];
    array=(NSMutableArray*)mainDictionary;
    
    
    for (NSDictionary *dic in array) {
        
        Products *obj;
        
          if([[array valueForKeyPath:@"p_id"]containsObject:[dic valueForKey:@"id"]]){
                
                int indexVal=(int)[[array valueForKeyPath:@"p_id"]indexOfObject:[dic valueForKey:@"id"]];
                obj=(Products*)[array objectAtIndex:indexVal];
                
            }else{
                
                NSEntityDescription *entity=[NSEntityDescription entityForName:@"Products" inManagedObjectContext:[GlobalCall getContext]];
                obj = [[Products alloc]initWithEntity:entity
                       insertIntoManagedObjectContext:[GlobalCall getContext]];
                
            }
        
        
         obj= [self returnMyObject:dic withObject:obj andIsNew:YES];
        
         [justIds addObject:obj.p_id];
        
         [GlobalCall saveContext];
        
        }
        
    return justIds;

}



+(Products*)returnMyObject:(NSDictionary*)dic withObject:(Products*)obj andIsNew:(BOOL)isNew{
    
    
    obj.p_id=[dic valueForKey:JSON_RES_AppProducts_Id];
    obj.applications=[[dic valueForKey:JSON_RES_AppProducts_Applications] lastObject];
    [obj setName:[dic valueForKey:JSON_RES_AppProducts_Name]];
    obj.discount=[dic valueForKey:JSON_RES_AppProducts_Discount];
    obj.isDiscontinued=[dic valueForKey:JSON_RES_AppProducts_Discontinued];
    obj.isFeatured=[dic valueForKey:JSON_RES_AppProducts_Featured];
    
    if (isNew) {
        
        
        obj.isFavourite=[NSNumber numberWithBool:FALSE];
        obj.isInCart=[NSNumber numberWithBool:FALSE];
        
    }
    
    
    NSMutableArray *attSet=[NSMutableArray new];
    
    for (NSDictionary *dicatt in [dic valueForKey:JSON_RES_AppProducts_Attributes]) {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ProductAttributes" inManagedObjectContext:[GlobalCall getContext]];
        ProductAttributes *objAtt = [[ProductAttributes alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
        objAtt.att_id=[dicatt valueForKey:JSON_RES_AppProducts_Id];
        objAtt.attributeValue=[dicatt valueForKey:JSON_RES_AppProducts_AttributeValue];
        objAtt.name=[dicatt valueForKey:JSON_RES_AppProducts_Name];
        objAtt.type=[dicatt valueForKey:JSON_RES_AppProducts_Type];
        objAtt.group=[dicatt valueForKey:JSON_RES_AppProducts_Group];
        objAtt.product_id=obj;
        
        [attSet addObject:objAtt];
        
        [GlobalCall saveContext];
    }
    
    
    
    [obj setAttributes:[NSSet setWithArray:attSet]];
    
    NSMutableArray *imgSet=[NSMutableArray new];
    NSArray *imgArray=[[NSArray alloc]initWithArray:([dic valueForKey:JSON_RES_AppProducts_Images]!=[NSNull null])?[dic valueForKey:JSON_RES_AppProducts_Images]:[[NSArray alloc]init]];
    
    for (NSDictionary *dicimag in imgArray) {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ProductImages" inManagedObjectContext:[GlobalCall getContext]];
        ProductImages *objAtt = [[ProductImages alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
        objAtt.image_id=[dicimag valueForKey:JSON_RES_AppProducts_Thumbnail_Image];
        objAtt.image_url=obj;
        
        [imgSet addObject:objAtt];
        
        [GlobalCall saveContext];
        
    }
    
    
    if ([imgSet count]>0) {
        
        [obj addImages:[NSSet setWithArray:imgSet]];
        
    }
    
    NSEntityDescription *entityThumb = [NSEntityDescription entityForName:@"ProductThumbnail" inManagedObjectContext:[GlobalCall getContext]];
    ProductThumbnail *objThumb = [[ProductThumbnail alloc]initWithEntity:entityThumb insertIntoManagedObjectContext:[GlobalCall getContext]];
    objThumb.p_id=[[dic valueForKey:JSON_RES_AppProducts_Thumbnail] valueForKey:JSON_RES_AppProducts_Thumbnail_Image];
    objThumb.image_url=obj;
    
    [GlobalCall saveContext];
    
    obj.thumbnail=objThumb;
    obj.rate =[dic valueForKey:JSON_RES_AppProducts_Rate];
    obj.shippedInDays=[dic valueForKey:JSON_RES_AppProducts_ShippedInDays];
    obj.isOutOfStock=[dic valueForKey:JSON_RES_AppProducts_OutOfStock];
    obj.measurementUnit=[dic valueForKey:JSON_RES_AppProducts_MeasurementUnit];
    obj.sKU=[dic valueForKey:JSON_RES_AppProducts_SKU];
    obj.tags=([dic valueForKey:JSON_RES_AppProducts_Tags]!=[NSNull null])?[dic valueForKey:JSON_RES_AppProducts_Tags]:@"";
    obj.type=[dic valueForKey:JSON_RES_AppProducts_Type];
    obj.availableUnits=[dic valueForKey:JSON_RES_AppProducts_AvailableUnits];
    obj.productDescription=([dic valueForKey:JSON_RES_AppProducts_Description]!=[NSNull null])?[dic valueForKey:JSON_RES_AppProducts_Description]:@"";
    obj.productCategory=[dic valueForKey:JSON_RES_AppProducts_Category];
    return obj;
    
}





@end
