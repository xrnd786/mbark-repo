//
//  TenantCall_Response.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//


#define contr @"Attributes"

#define action_get @"Get"

//***tenant response keys****//
#define JSON_RES_TenantDetails @"TenantDetails"


#import "AttributeCall_Response.h"
#import "ServerInteraction.h"

#import "AppAttributesDefaults.h"

@implementation AttributeCall_Response

static ServerInteraction *calling;


+(void)callApiForAttributeList{
    
    
    [GlobalCall removeOberverWithNotifier:APINotification_Attributes_Get handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Attributes_Get andSelector:@selector(attributeResponse:) handlerClass:self withObject:nil];
    
     calling=[[ServerInteraction alloc]init];
     calling=[calling initWithController:contr andAction:action_get andRequestBody:nil notifier:APINotification_Attributes_Get methodType:GET headerKeyArray:nil headerValueArray:nil];
     calling=nil;
    
}


+(void)attributeResponse:(NSNotification*)notification{

    NSDictionary *main_dicationary=notification.userInfo;
   
    NSMutableArray *array =[[NSMutableArray alloc]initWithArray:(NSArray*)main_dicationary];
    
    [AppAttributesDefaults saveTotalCount:(int)[array count]];
    
    int i=0;
    for (NSDictionary *attributes in array) {
        
        [AppAttributesDefaults saveAppAttributes:attributes withAttributeNumber:i];
        i++;
    }
    
    [GlobalCall removeOberverWithNotifier:APINotification_Attributes_Get handlerClass:self withObject:nil];
    
    
}



@end
