//
//  TenantCall_Response.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>

@interface AttributeCall_Response : NSObject

+(void)callApiForAttributeList;

+(void)attributeResponse:(NSNotification*)notification;

@end
