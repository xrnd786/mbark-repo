//
//  Global_Server_Constants.m
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import "Global_Server_Constants.h"

@implementation Global_Server_Constants


-(NSString*)getMyTenantName{
    
//    NSArray *paths=[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
//                                                          inDomains:NSUserDomainMask];
//    NSString *documentsDirectory =[paths lastObject];
//    NSString *path = [[NSString stringWithFormat:@"%@",documentsDirectory] stringByAppendingPathComponent:@"ProductSetup.plist"];
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"ProductSetup" ofType:@"plist"];
    NSLog(@"Print Path %@",path);
    
    NSMutableDictionary *dictBedrooms = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    NSString *tenantName=[dictBedrooms valueForKey:@"tenant_name"];
    
    if (tenantName) {
        
        return tenantName;
        
    } else {
        
        UIAlertView *view=[[UIAlertView alloc]initWithTitle:@"Blocker" message:@"Please add tenant name before proceeding" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [view show];
        
        return nil;
        
    }
    
    
}

@end
