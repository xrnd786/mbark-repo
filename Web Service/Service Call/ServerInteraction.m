//
//  ServerInteraction.m
//  Snap Brand
//
//  Created by Xiphi Tech on 18/10/2013.
//  Copyright (c) 2013 com.xiphitech. All rights reserved.
//

#import "ServerInteraction.h"
#import "Global_Server_Constants.h"
#import "JSON.h"

//******HEADER OBJECT******//
#import "DeviceUtils.h"
#import "DeviceUtilsModel.h"

//******USER OBJECT******//
#import "User.h"
#import "AppUserDefaults.h"


#import "JSON.h"
#import "ConnectionToast.h"
#import "LeveyTabBarController.h"


@interface ServerInteraction ()

@end

@implementation ServerInteraction

@synthesize activityView = mActivityView;
@synthesize notifierString;

@synthesize delegate = mDelegate;
@synthesize receivedData;
@synthesize responseHeader;
@synthesize responseData;


#pragma mark - 
#pragma mark Initialize



-(id)initForAppLaunchWithController:(NSString*)controller andAction:(NSString*)action withNotifierString:(NSString*)notifier{
    
    self = [super init];
    
    if (self != nil)
    {
        
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: YES];
       self.notifierString=notifier;
        
        NSString *urlString=[self formURLWithController:controller andAction:action];
        NSURL* aURL = [NSURL URLWithString:urlString];
       
        NSLog(@"URL for launch is %@",urlString);
        
        
        mRequest = [[NSMutableURLRequest alloc]initWithURL:aURL];
        [mRequest setHTTPMethod:GET];
        
        if ([GlobalCall isConnected]) {
            
            NSURLResponse *response;
            NSError *error;
            
            [NSURLConnection cancelPreviousPerformRequestsWithTarget:self];
        
            self.receivedData = (NSMutableData*)[NSURLConnection sendSynchronousRequest:mRequest returningResponse:&response error:&error];
            
            if (receivedData!=nil) {
                
               //NSLog(@"Response code %ld",(long)[(NSHTTPURLResponse*)response statusCode]);
              
                self.responseHeader=[NSNumber numberWithInteger:[(NSHTTPURLResponse*)response statusCode]];
                
                if ([(NSHTTPURLResponse*)response statusCode]==HEADER_ENUM_CREATED) {
                   
                    [self connectionDataLoaded];
                  
                }else if([(NSHTTPURLResponse*)response statusCode]==HEADER_ENUM_CONFLICT){
                
                
                }
                
                
            }else{
                
                
                
            }

            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self CallConnectionAlertWithText:APP_NO_INTERNET];
                
            });
            
        }
        
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
    
    return self;
    
        
        
}




-(id)initWithController:(NSString *)controller andAction:(NSString*)action andRequestBody:(NSObject *)reqBody notifier:(NSString*)notifierString1 methodType:(NSString*)type headerKeyArray:(NSArray*)keys headerValueArray:(NSArray*)values {
    
    self = [super init];
	
    if (self != nil)
	{
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: YES];
    
        
        self.receivedData =[[NSMutableData alloc]init];
        
        self.notifierString=notifierString1;
        //NSLog(@"Notification string %@",self.notifierString);
        
        NSURL* aURL =[[NSURL alloc]init];
        
        
        if (values!=nil) {
            
            NSString *urlString=[self formURLWithController:controller andAction:action];
             NSLog(@"Url first time %@",urlString);
            
            if (keys!=nil) {
                
                for (int i=0; i<[keys count]; i++) {
                    
                    urlString=[[[[urlString stringByAppendingString:@"/"] stringByAppendingString:[keys objectAtIndex:i]] stringByAppendingString:@"="] stringByAppendingString:[values objectAtIndex:i]];
                    NSLog(@"Url after append%@",urlString);
                }
                
                aURL  = [NSURL URLWithString:urlString];
                

                
            } else {
                
                for (int i=0; i<[values count]; i++) {
                    
                    urlString=[[urlString stringByAppendingString:@"/"] stringByAppendingString:[values objectAtIndex:i]];
                    
                }
                
                aURL  = [NSURL URLWithString:urlString];
                
            }
            
            
        }else{
            
            //NSLog(@"Controller %@ and Action %@",controller,action);
            
            aURL  = [NSURL URLWithString:[self formURLWithController:controller andAction:action]];
           
            
        }
        
		mRequest = [[NSMutableURLRequest alloc]initWithURL:aURL];
		[mRequest setHTTPMethod:type];
        [mRequest setValue:@"application/json" forHTTPHeaderField:HEADER_CONTENT_TYPE];
        [mRequest setValue:[[self headerDeviceObjectDictionary] JSONRepresentation] forHTTPHeaderField:HEADER_DEVICE_METRICS];
        [mRequest setValue:[self headerUserObject] forHTTPHeaderField:HEADER_USER];
        NSString *myJSONString;
        
        if (![type isEqual:GET] && reqBody!=nil) {
          
            myJSONString =[reqBody JSONRepresentation];
            NSData *myJSONData =[myJSONString dataUsingEncoding:NSUTF8StringEncoding];
            [mRequest setHTTPBody:myJSONData];
            
        }
        
        NSLog(@"Header Data :\r %@:%@ \r %@:%@ \r %@:%@ \rRequest Body :%@",HEADER_CONTENT_TYPE,@"application/json",HEADER_DEVICE_METRICS,[[self headerDeviceObjectDictionary] JSONRepresentation],HEADER_USER,[self headerUserObject],myJSONString);
        NSLog(@"URL IS  %@ ",aURL);
        
        
        if ([GlobalCall isConnected]) {
            
            NSURLResponse *response;
            NSError *error;
            
            self.responseData=[[NSURLResponse alloc]init];
            [NSURLConnection cancelPreviousPerformRequestsWithTarget:self];
            
            self.receivedData = (NSMutableData*)[NSURLConnection sendSynchronousRequest:mRequest returningResponse:&(response) error:&error];
            responseHeader =[NSNumber numberWithInt:(int)[(NSHTTPURLResponse*)response statusCode]];
            
            if (error==nil) {
                
                
                if (self.receivedData!=nil) {
                    
                    [self connectionDataLoaded];
                    
                }else{
                    
                    [self connectinHasError:error];
                    
                }
                
            } else {
                
                [self connectinHasError:error];
                
            }

            
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self CallConnectionAlertWithText:APP_NO_INTERNET];
                
            });
            
        }
        
        
        
    }
   
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
    
    return self;
    
}



-(id)initWithControllerForProfile:(NSString *)controller andAction:(NSString*)action andRequestBody:(NSObject *)reqBody notifier:(NSString*)notifierString1 methodType:(NSString*)type headerKeyArray:(NSArray*)keys headerValueArray:(NSArray*)values {
    
    self = [super init];
    
    if (self != nil)
    {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: YES];
        
        
        self.receivedData =[[NSMutableData alloc]init];
        self.notifierString=notifierString1;
        
        NSURL* aURL =[[NSURL alloc]init];
        
        
        if (values!=nil) {
            
            NSString *urlString=[self formURLWithController:controller andAction:action];
            
            
            if (keys!=nil) {
                
                for (int i=0; i<[keys count]; i++) {
                    
                    urlString=[[[[urlString stringByAppendingString:@"/"] stringByAppendingString:[keys objectAtIndex:i]] stringByAppendingString:@"="] stringByAppendingString:[values objectAtIndex:i]];
                    
                }
                
                aURL  = [NSURL URLWithString:urlString];
                
                
                
            } else {
                
                for (int i=0; i<[values count]; i++) {
                    
                    urlString=[[urlString stringByAppendingString:@"/"] stringByAppendingString:[values objectAtIndex:i]];
                    
                }
                
                aURL  = [NSURL URLWithString:urlString];
                
            }
            
            
        }else{
            
            aURL  = [NSURL URLWithString:[self formURLWithController:controller andAction:action]];
            
        }
        
        mRequest = [[NSMutableURLRequest alloc]initWithURL:aURL];
        [mRequest setHTTPMethod:type];
        [mRequest setValue:@"application/json" forHTTPHeaderField:HEADER_CONTENT_TYPE];
        
        if ([action isEqualToString:@"Update"]) {
            
            [mRequest setValue:[[self headerDeviceObjectDictionary] JSONRepresentation] forHTTPHeaderField:HEADER_DEVICE_METRICS];
            [mRequest setValue:[self headerUserObject] forHTTPHeaderField:HEADER_USER];
            
        }
        
        NSString *myJSONString;
        
        NSMutableDictionary *reqDic=[[NSMutableDictionary alloc]init];
        reqDic=(NSMutableDictionary*)reqBody;
        [reqBody setValue:[self headerUserObject]  forKey:@"Id"];
        
        if (![type isEqual:GET] && reqBody!=nil) {
            
            myJSONString =[reqBody JSONRepresentation];
            NSData *myJSONData =[myJSONString dataUsingEncoding:NSUTF8StringEncoding];
            [mRequest setHTTPBody:myJSONData];
            
        }
        
        NSLog(@"Header Data :\r %@:%@ \r %@:%@ \r %@:%@ \rRequest Body :%@",HEADER_CONTENT_TYPE,@"application/json",HEADER_DEVICE_METRICS,[[self headerDeviceObjectDictionary] JSONRepresentation],HEADER_USER,[self headerUserObject],myJSONString);
        NSLog(@"URL IS  %@ ",aURL);
        
        
        if ([GlobalCall isConnected]) {
            
            NSURLResponse *response;
            NSError *error;
            
            self.responseData=[[NSURLResponse alloc]init];
            [NSURLConnection cancelPreviousPerformRequestsWithTarget:self];
            
            self.receivedData = (NSMutableData*)[NSURLConnection sendSynchronousRequest:mRequest returningResponse:&(response) error:&error];
            responseHeader =[NSNumber numberWithInt:(int)[(NSHTTPURLResponse*)response statusCode]];
            
            if (error==nil) {
                
                if (self.receivedData!=nil) {
                    
                    [self connectionDataLoaded];
                    
                }else{
                    
                    [self connectinHasError:error];
                    
                }
                
            } else {
                
                [self connectinHasError:error];
                
            }
            
            
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [self CallConnectionAlertWithText:APP_NO_INTERNET];
                
            });
            
        }
        
        
        
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
    
    return self;
    
}



#pragma mark - Task Functions

-(void)connectinHasError:(NSError*)error{
 
    NSLog(@"ERROR CODE %ld \rAND ERROR : %@", (long)error.code,error);
    
    if (error.code==-1001) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self CallConnectionAlertWithText:APP_TIME_OUT];
        });
        
    }else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self CallConnectionAlertWithText:APP_NO_INTERNET];
            
        });
        
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:APINotification_Server_Error object:nil userInfo:nil];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
    
    [SVProgressHUD dismiss];
    
}

-(void)connectionDataLoaded{

    
    BOOL isGood=FALSE;
    
    NSLog(@"Code value %ld",(long)[responseHeader integerValue]);
    
    switch ([responseHeader integerValue]) {
            
        case HEADER_ENUM_CREATED:
            
           
            isGood=TRUE;
            break;
            
        case HEADER_ENUM_OK:
            
           
            isGood=TRUE;
            
            break;
            
        case HEADER_ENUM_SUCESS:
            
            isGood=TRUE;
            
            break;
            
        case HEADER_ENUM_INTERNAL_ERROR:
            
            isGood=FALSE;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
              [SVProgressHUD dismiss];
                
            });

            break;
            
        case HEADER_ENUM_CONFLICT:
            
            isGood=TRUE;
            
            break;
            
        case HEADER_ENUM_UNAUTHORIZED:
            
            isGood=FALSE;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            break;
            
        default:
            
            isGood=FALSE;
            
            break;
            
    }
    
    
    NSError *error;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableLeaves error:&error];
    NSString* responseString = [[NSString alloc] initWithData:self.receivedData encoding:NSASCIIStringEncoding];
    
    NSLog(@"RESPONSE 64%@",responseString);
    
    if (error==nil) {
        
        if (isGood) {
            
            [[NSNotificationCenter defaultCenter]postNotificationName:self.notifierString object:self.receivedData userInfo:res];
            
        }else{
            
           if([responseHeader integerValue]==HEADER_ENUM_UNAUTHORIZED) {
               
               
                   dispatch_async(dispatch_get_main_queue(), ^{
                    
                       [self CallUnauthorisedAlert];
                   });
            }
            
        }
        
    }else{
        
        if (isGood) {
            
            [[NSNotificationCenter defaultCenter]postNotificationName:self.notifierString object:responseString userInfo:nil];
            
        }else{
            
            [self connectinHasError:error];
            
        }
        
    }
    
}


-(NSDictionary*)headerDeviceObjectDictionary{
    
    DeviceUtilsModel *model=[DeviceUtils getDeviceUtilsModel];
    NSMutableDictionary *modelDictionary=[[NSMutableDictionary alloc]init];
    
    [modelDictionary setValue:model.deviceModel forKey:JSON_DeviceModel];
    [modelDictionary setValue:model.deviceSerialNumber forKey:JSON_DeviceUID];
    [modelDictionary setValue:model.deviceToken forKey:JSON_DeviceToken];
    [modelDictionary setValue:model.deviceOS forKey:JSON_DeviceOS];
    [modelDictionary setValue:model.deviceOSVersion forKey:JSON_OSVersion];
    [modelDictionary setValue:model.appVersion forKey:JSON_AppVersion];
    
    return modelDictionary;
}


-(NSString*)headerUserObject{
    
    
    NSString *uId=((User*)[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:@"u_id" withPredicate:nil] lastObject]).u_id;
    return uId;
    
}


-(NSString*)formURLWithController:(NSString*)controller andAction:(NSString*)action{
    
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"ProductSetup" ofType:@"plist"];
   
    //NSLog(@"Print Path %@",path);
    
    NSMutableDictionary *tenantName = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    NSString *tenantString=[tenantName valueForKey:@"tenant_name"];
   
    NSString *finalURL=[NSString new];
    finalURL=URL_BASE;
    finalURL=[finalURL stringByAppendingString:tenantString];
    finalURL=[finalURL stringByAppendingString:URL_APPEND];
    finalURL=[finalURL stringByAppendingString:URL_APPEND_VERSION];
    finalURL=[[finalURL stringByAppendingString:controller] stringByAppendingString:@"/"];
    finalURL=[finalURL stringByAppendingString:action] ;
    
    return finalURL;

}


#pragma mark - Alter on status bar
-(void)CallConnectionAlertWithText:(NSString*)text{
 
    
    [[ConnectionToast new] showToastWithText:text];
    
    
}

-(void)CallUnauthorisedAlert{
    
    
    [[ConnectionToast new] showToastForUnauthorised];
    
    
}




@end
