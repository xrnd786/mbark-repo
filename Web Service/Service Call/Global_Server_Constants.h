//
//  Global_Server_Constants.h
//  P1
//
//  Created by Xiphi Tech on 11/11/2014.
//
//

#import <Foundation/Foundation.h>


//URL names
#define URL_BASE @"http://"
#define URL_APPEND @".mbark.in/api/"
#define URL_APPEND_VERSION @"v1/"

#define URL_USER_Registration @""


//URL JSON KEYS REQUEST BODY

#define JSON_user @"user"
#define JSON_userid @"Id"


//DEVICE JSON KEYS
#define JSON_DeviceModel @"Model"
#define JSON_DeviceUID @"UID"
#define JSON_DeviceToken @"Token"
#define JSON_DeviceOS @"OS"
#define JSON_OSVersion @"OSVersion"
#define JSON_AppVersion @"AppVersion"


//URL JSON KEYS RESPONSE BODY

#define JSON_RES_userid @"Id"

//URL REQUEST HEADER
#define HEADER_CONTENT_TYPE @"Content-Type"
#define HEADER_DEVICE_METRICS @"DeviceMetrics"
#define HEADER_USER @"UserId"
#define HEADER_DEVICE @"Device"



@interface Global_Server_Constants : NSObject

-(NSString*)getMyTenantName;


@end
