//
//  ServerInteraction.h
//  Snap Brand
//
//  Created by Xiphi Tech on 18/10/2013.
//  Copyright (c) 2013 com.xiphitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ServiceInteractionDelegate <NSObject>

@optional

@end

typedef enum : NSUInteger {
    
    HEADER_ENUM_CREATED = 201,
    HEADER_ENUM_OK = 200,
    HEADER_ENUM_SUCESS = 400,
    HEADER_ENUM_UNAUTHORIZED = 403,
    HEADER_ENUM_CONFLICT = 409,
    HEADER_ENUM_INTERNAL_ERROR = 500,
    
} HEADER_ENUM;



@interface ServerInteraction : UIViewController<NSObject,NSURLConnectionDelegate>

{
   NSDate* mLastRequest;
	
   NSMutableURLRequest* mRequest;
   NSURLConnection* mConnection;
   NSMutableData* mURLConnectionData;

   __unsafe_unretained id<ServiceInteractionDelegate> mDelegate;

    UIView *mActivityView;
    
    NSURLResponse *responseData;
    
}

@property (nonatomic, assign) id<ServiceInteractionDelegate> delegate;
@property (nonatomic, retain) UIView* activityView;
@property (nonatomic, retain) NSString* notifierString;
@property (nonatomic, retain) NSMutableData* receivedData;
@property (nonatomic, retain) NSURLResponse* responseData;
@property (nonatomic,retain) NSNumber *responseHeader;


#pragma mark -
#pragma mark Initialization

-(id)initForAppLaunchWithController:(NSString*)contname andAction:(NSString*)action withNotifierString:(NSString*)notifier;

- (id)initWithController:(NSString *)contrname andAction:(NSString*)action andRequestBody:(NSObject *)reqBody notifier:(NSString*)notifierString1 methodType:(NSString*)type headerKeyArray:(NSArray*)keys headerValueArray:(NSArray*)values;


-(id)initWithControllerForProfile:(NSString *)controller andAction:(NSString*)action andRequestBody:(NSObject *)reqBody notifier:(NSString*)notifierString1 methodType:(NSString*)type headerKeyArray:(NSArray*)keys headerValueArray:(NSArray*)values;

@end
