//
//  BottomBorderView.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "UpperBorderView.h"

@implementation UpperBorderView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    // Add a bottomBorder.
    CALayer *upperBorder = [CALayer layer];
    
    upperBorder.frame = CGRectMake(rect.origin.x ,0, rect.size.width, 1.0f);
    
    upperBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    
    [self.layer addSublayer:upperBorder];
    
}


@end
