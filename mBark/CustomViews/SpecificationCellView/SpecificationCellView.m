//
//  BottomBorderView.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "SpecificationCellView.h"

@implementation SpecificationCellView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect {
   
    // Drawing code
   
    CALayer *leftBorder = [CALayer layer];
    CALayer *rightBorder = [CALayer layer];
    CALayer *topBorder = [CALayer layer];
    CALayer *bottomBorder = [CALayer layer];
    
    leftBorder.frame = CGRectMake(0,0, 1, rect.size.height);
    rightBorder.frame = CGRectMake(rect.size.width-1,0, 1, rect.size.height);
    topBorder.frame = CGRectMake(0,0, rect.size.width, 1);
    bottomBorder.frame = CGRectMake(0,rect.size.height-1, rect.size.width, 1);
    
    leftBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    rightBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                   alpha:1.0f].CGColor;
    topBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                   alpha:1.0f].CGColor;
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                   alpha:1.0f].CGColor;
    
   // [self.layer addSublayer:leftBorder];
    [self.layer addSublayer:rightBorder];
    //[self.layer addSublayer:topBorder];
    [self.layer addSublayer:bottomBorder];
}

@end
