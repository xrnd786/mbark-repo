//
//  LeveyTabBar.m
//  LeveyTabBarController
//
//  Created by zhang on 12-10-10.
//  Copyright (c) 2012年 jclt. All rights reserved.
//
//

#import "LeveyTabBar.h"


#import "RNThemeImageButton.h"
#import "RNThemeLabel.h"
#import "UpperBorderView.h"

@implementation LeveyTabBar
@synthesize backgroundView = _backgroundView;
@synthesize delegate = _delegate;
@synthesize buttons = _buttons;
@synthesize labels = _labels;

- (id)initWithFrame:(CGRect)frame buttonImages:(NSArray *)imageArray :(NSArray *)labelsArray
{
    self = [super initWithFrame:frame];
    if (self)
	{
		self.backgroundColor = [UIColor whiteColor];
		_backgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
		
        [self addSubview:_backgroundView];
		
		self.buttons = [NSMutableArray arrayWithCapacity:[imageArray count]];
        self.labels = [NSMutableArray arrayWithCapacity:[labelsArray count]];
        
        RNThemeImageButton *btn;
        RNThemeLabel *lbl;
        UIView *buttonView;
        
        CGFloat width = ([GlobalCall getMyWidth]) / (2*[imageArray count]);
        
    for (int i = 0; i < [imageArray count]; i++)
    {
        
        CGFloat aPart=([GlobalCall findBounds].size.width/[imageArray count]);
        
        buttonView =[[UIView alloc]initWithFrame:CGRectMake(aPart*i, 0,aPart , self.frame.size.height)];
        buttonView.tag = i;
        
        btn = [RNThemeImageButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColorKey=@"secondaryColor";
        btn.highlightedColorKey=@"primaryColor";
        
        lbl =[[RNThemeLabel alloc]init];
        lbl.textColorKey=@"secondaryColor";
        lbl.fontKey=@"generalFont";
        lbl.text=[labelsArray objectAtIndex:i];
        lbl.textAlignment=NSTextAlignmentCenter;
        lbl.adjustsFontSizeToFitWidth=FALSE;
        
        [lbl applyTheme];
        
            switch (i) {
                case 0:
                    
                    btn.backgroundImageKey=@"home";
                    break;
                
                case 1:
                  
                    btn.backgroundImageKey=@"search";
                    break;
                
                case 2:
                   
                    btn.backgroundImageKey=@"store";
                    break;
                    
                case 3:
                    
                    btn.backgroundImageKey=@"myBag";
                    break;
                    
                default:
                    break;
            }

            [btn applyTheme];
        
            btn.showsTouchWhenHighlighted = YES;
		
            btn.tag = i;
            lbl.tag =i;
			
        
        CGFloat yCenter=self.frame.size.height/2;
        
        if ([GlobalCall isIpad]) {
            
//            btn.bounds=CGRectMake(0, 0, 60, 60);
//            btn.center=CGPointMake(xCenter+((i)*xCenter),yCenter-10 );
//            
//            lbl.bounds=CGRectMake(0, 0, 25, 20);
//            lbl.center=CGPointMake(xCenter+((i)*xCenter),yCenter);
            
            btn.bounds=CGRectMake(0, 0, 50  , 50 );
            btn.center=CGPointMake(aPart/2, 15);
            
            lbl.bounds=CGRectMake(0, 0, 60, 25);
            lbl.center=CGPointMake((aPart/2)+1,yCenter-6);

            
        } else {
            
            btn.bounds=CGRectMake(0, 0, 28, 30);
            btn.center=CGPointMake(aPart/2, 25-6);
            
            lbl.bounds=CGRectMake(0, 0, 40, 12);
            lbl.center=CGPointMake((aPart/2),25+15);
            
        }
        
        UIView *viewOver=buttonView;
        viewOver.backgroundColor=[UIColor clearColor];
        
        UIView *top=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], 0.5f)];
        top.backgroundColor=[UIColor lightGrayColor];
        [viewOver addSubview:top];
        
        UITapGestureRecognizer *tapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tabBarButtonClicked:)];
        [viewOver addGestureRecognizer:tapped];
        
        btn.userInteractionEnabled=FALSE;
        
        [self.buttons addObject:btn];
        [self.labels addObject:lbl];
        
        [buttonView addSubview:btn];
        [buttonView addSubview:lbl];
        
        
        [self addSubview:buttonView];
        [self addSubview:viewOver];
        
      }
        
        self.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
      
    }
    
    return self;
}

- (void)setBackgroundImage:(UIImage *)img
{
	[_backgroundView setImage:img];
}

- (void)tabBarButtonClicked:(UITapGestureRecognizer*)gesture
{
	UIView *btn = gesture.view;
    [self selectTabAtIndex:btn.tag];
    
    RNThemeLabel *lbl = [self.labels objectAtIndex:btn.tag];
    lbl.textColorKey=@"primaryColor";
    [lbl sizeToFit];
    [lbl applyTheme];
    
    NSLog(@"Select index: %ld",(long)btn.tag);
    
    
    if ([_delegate respondsToSelector:@selector(tabBar:didSelectIndex:)])
    {
        [_delegate tabBar:self didSelectIndex:btn.tag];
    }
}

- (void)selectTabAtIndex:(NSInteger)index
{
	for (int i = 0; i < [self.buttons count]; i++)
	{
		UIButton *b = [self.buttons objectAtIndex:i];
		b.selected = NO;
		//b.userInteractionEnabled = YES;
        RNThemeLabel *lbl = [self.labels objectAtIndex:i];
        lbl.textColorKey=@"secondaryColor";
        [lbl sizeToFit];
        [lbl applyTheme];
	}
	
    UIButton *btn = [self.buttons objectAtIndex:index];
	btn.selected = YES;
	//btn.userInteractionEnabled = NO;
    
    RNThemeLabel *lbl = [self.labels objectAtIndex:btn.tag];
    lbl.textColorKey=@"primaryColor";
    [lbl sizeToFit];
    [lbl applyTheme];
    
}

- (void)removeTabAtIndex:(NSInteger)index
{
    // Remove button
    [(UIButton *)[self.buttons objectAtIndex:index] removeFromSuperview];
    [self.buttons removeObjectAtIndex:index];
   
    // Re-index the buttons
     CGFloat width = 320.0f / [self.buttons count];
   
    for (UIButton *btn in self.buttons)
    {
        if (btn.tag > index)
        {
            btn.tag --;
        }
        btn.frame = CGRectMake(width * btn.tag, 0, width, self.frame.size.height);
    }
    
}


- (void)insertTabWithImageDic:(NSDictionary *)dict atIndex:(NSUInteger)index
{
   
    // Re-index the buttons
    CGFloat width = 320.0f / ([self.buttons count] + 1);
    for (UIButton *b in self.buttons) 
    {
        if (b.tag >= index)
        {
            b.tag ++;
        }
        b.frame = CGRectMake(width * b.tag, 0, width, self.frame.size.height);
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.showsTouchWhenHighlighted = YES;
    btn.tag = index;
    btn.frame = CGRectMake(width * index, 0, width, self.frame.size.height);
    [btn setImage:[dict objectForKey:@"Default"] forState:UIControlStateNormal];
    [btn setImage:[dict objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
    [btn setImage:[dict objectForKey:@"Selected"] forState:UIControlStateSelected];
    [self.buttons insertObject:btn atIndex:index];
    [self addSubview:btn];
    
}


@end
