//
//  LeveyTabBarControllerViewController.m
//  LeveyTabBarController
//
//  Created by zhang on 12-10-10.
//  Copyright (c) 2012年 jclt. All rights reserved.
//
//

#import "LeveyTabBarController.h"
#import "LeveyTabBar.h"

#import "Home.h"
#import "UIViewController+REFrostedViewController.h"

#define kTabBarHeight 49.0f
#define kTopBarHeight 49.0f
#define kTabBariPadHeight 100.0f


static LeveyTabBarController *leveyTabBarController;

@implementation UIViewController (LeveyTabBarControllerSupport)


- (LeveyTabBarController *)leveyTabBarController
{
	return leveyTabBarController;
}

@end

@interface LeveyTabBarController (private)
- (void)displayViewAtIndex:(NSUInteger)index;
@end

@implementation LeveyTabBarController

@synthesize _transitionView;

@synthesize delegate = _delegate;
@synthesize selectedViewController = _selectedViewController;
@synthesize viewControllers = _viewControllers;
@synthesize selectedIndex = _selectedIndex;
@synthesize tabBarHidden = _tabBarHidden;
@synthesize animateDriect;


#pragma mark -
#pragma mark lifecycle


- (id)initWithImageArray:(NSArray *)arr labelsArray:(NSArray*)labels withViewController:(NSArray *)vc
{
    self = [super init];
    if (self) {
        
        CGRect applicationFrame=[[UIScreen mainScreen] bounds];
        _containerView = [[UIView alloc] initWithFrame:applicationFrame];
        _containerView.backgroundColor=[UIColor clearColor];
       
        
        self._transitionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], _containerView.frame.size.height)];
        self._transitionView.backgroundColor =  [UIColor clearColor];
        
        
        leveyTabBarController = self;
        animateDriect = 0;
       
        
        _tabBar = [[LeveyTabBar alloc] initWithFrame:CGRectMake(0, [GlobalCall getMyHeight] - ([GlobalCall isIpad]?kTabBariPadHeight:kTabBarHeight), [GlobalCall getMyWidth], [GlobalCall isIpad]?kTabBariPadHeight:kTabBarHeight) buttonImages:arr :labels];
        _tabBar.delegate = self;
        _tabBar.backgroundColor=[UIColor whiteColor];
        
        _viewControllers = [NSMutableArray arrayWithArray:vc];
    
        
        [_containerView addSubview:_transitionView];
        [_containerView addSubview:_tabBar];
        
        
        [[NSNotificationCenter defaultCenter] addObserverForName:@"BottomBarShouldHide" object:nil queue:nil
            usingBlock:^(NSNotification *note) {

              [self hidesTabBar:TRUE animated:YES];
                
                                                      }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:@"BottomBarShouldUnhide" object:nil
                                                           queue:nil
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          [self hidesTabBar:FALSE animated:YES];
                                                          
                                                      }];
        
        
    }

    return self;
}



- (void)loadView 
{
    
    [super loadView];
	
    
    self.view = _containerView;
    [self.view setNeedsDisplay];
    
}


- (void)viewDidLoad 
{
    
    [super viewDidLoad];
	
    self.selectedIndex = 0;
    
    
}

- (void)viewDidUnload
{
	
    [super viewDidUnload];
	
	//_tabBar = nil;
	//_viewControllers = nil;

}



#pragma mark - instant methods

- (LeveyTabBar *)tabBar
{
	return _tabBar;
}

- (BOOL)tabBarTransparent
{
	return _tabBarTransparent;
}

- (void)setTabBarTransparent:(BOOL)yesOrNo
{
	if (yesOrNo == YES)
	{
		self._transitionView.frame = _containerView.bounds;
	}
	else
	{
		self._transitionView.frame = CGRectMake(0, 0, [GlobalCall getMyWidth], _containerView.frame.size.height - kTabBarHeight);
	}
}




- (void)hidesTabBar:(BOOL)yesOrNO animated:(BOOL)animated
{
    
    CGFloat myHeight=[GlobalCall getMyHeight];
    
	if (yesOrNO == YES)
	{
		if (self.tabBar.frame.origin.y == myHeight)
		{
			return;
		}
	}
	else 
	{
		if (self.tabBar.frame.origin.y == myHeight - kTabBarHeight)
		{
			return;
		}
	}
	
	if (animated == YES)
	{
        
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.3f];
		
        if (yesOrNO == YES)
		{
			//_tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            _tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, myHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
		}
		else 
		{
			//_tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            _tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, myHeight - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            
		
        }
		
        [UIView commitAnimations];
	
    }
	else 
	{
		if (yesOrNO == YES)
		{
			_tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
		}
		else 
		{
			_tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
		}
	
    }
    
}


- (NSUInteger)selectedIndex
{
	return _selectedIndex;
}


- (UIViewController *)selectedViewController
{
    return [_viewControllers objectAtIndex:_selectedIndex];
}


-(void)setSelectedIndex:(NSUInteger)index
{
    [self displayViewAtIndex:index];
    [_tabBar selectTabAtIndex:index];
    
    [GlobalCall showBottomBar];
    
}

- (void)removeViewControllerAtIndex:(NSUInteger)index
{
   
    if (index >= [_viewControllers count])
    {
        return;
    }
    
    // Remove view from superview.
    
    //[[[self selectedViewController] view]removeFromSuperview];
    
    [[(UIViewController *)[_viewControllers objectAtIndex:index] view] removeFromSuperview];
    
    // Remove viewcontroller in array.
    // [_viewControllers removeObjectAtIndex:index];
    
    // Remove tab from tabbar.
    //[_tabBar removeTabAtIndex:index];

}

- (void)insertViewController:(UIViewController *)vc withImageDic:(NSDictionary *)dict atIndex:(NSUInteger)index
{
    [_viewControllers insertObject:vc atIndex:index];
    [_tabBar insertTabWithImageDic:dict atIndex:index];
}


#pragma mark - Private methods

- (void)displayViewAtIndex:(NSUInteger)index
{
    // Before change index, ask the delegate should change the index.
    if ([_delegate respondsToSelector:@selector(tabBarController:shouldSelectViewController:)]) 
    {
        if (![_delegate tabBarController:self shouldSelectViewController:[self.viewControllers objectAtIndex:index]])
        {
            return;
        }
    }
   
    // If target index if equal to current index, do nothing.
    if (_selectedIndex == index && [[self._transitionView subviews] count] != 0) 
    {
        return;
    }
    //NSLog(@"Display View.");
    _selectedIndex = index;
    
	UIViewController *selectedVC = [self.viewControllers objectAtIndex:index];
	
	selectedVC.view.frame = self._transitionView.frame;
	
    if ([selectedVC.view isDescendantOfView:self._transitionView])
	{
		
        [self._transitionView bringSubviewToFront:selectedVC.view];
       
	}
	else
	{
		[self._transitionView addSubview:selectedVC.view];
       
        //[selectedVC.parentViewController loadView];
        //NSLog(@" transition frame %@ ", NSStringFromCGRect(selectedVC.view.frame));
        
	}
    
    NSMutableArray *removalIndices=[[NSMutableArray alloc]init];
    for (int i=0; i<[self.viewControllers count]; i++) {
        
        [removalIndices addObject:[NSNumber numberWithInt:i]];
        
    }
    
    [removalIndices removeObject:[NSNumber numberWithInt:(int)index]];
   
    NSLog(@"removal indices %@",removalIndices);
    
    for (NSNumber *remove in removalIndices) {
        
        [self removeViewControllerAtIndex:[remove integerValue]];
        
    }
    
    
    
    // Notify the delegate, the viewcontroller has been changed.
    
    if ([_delegate respondsToSelector:@selector(tabBarController:didSelectViewController:)])
    {
        [_delegate tabBarController:self didSelectViewController:selectedVC];
    }

}

#pragma mark -
#pragma mark tabBar delegates
- (void)tabBar:(LeveyTabBar *)tabBar didSelectIndex:(NSInteger)index
{
	if (self.selectedIndex == index) {
        
        
        UINavigationController *nav = [self.viewControllers objectAtIndex:index];
        
        if (index==0) {
            
            REFrostedViewController *ViewController=(REFrostedViewController*)[nav.viewControllers objectAtIndex:0];
            [(UINavigationController*)ViewController.contentViewController popToRootViewControllerAnimated:YES];
             
        }else{
            
            [nav popToRootViewControllerAnimated:YES];
            
       }
        
    }else {
        
        [self displayViewAtIndex:index];
    
    }
    
}


/*
 - (void)hidesTabBar:(BOOL)yesOrNO animated:(BOOL)animated
 {
	if (yesOrNO == YES)
	{
 if (self.tabBar.frame.origin.y == self.view.frame.size.height)
 {
 return;
 }
	}
	else
	{
 if (self.tabBar.frame.origin.y == self.view.frame.size.height - kTabBarHeight)
 {
 return;
 }
	}
	
	if (animated == YES)
	{
 
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDuration:0.3f];
 
 if (yesOrNO == YES)
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 }
 else
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 
 }
 
 [UIView commitAnimations];
	
 }
	else
	{
 if (yesOrNO == YES)
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 }
 else
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 }
	
 }
 
 }

 */
@end
