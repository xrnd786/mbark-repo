//
//  Scale_Image.m
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import "Scale_Image.h"

@implementation Scale_Image

-(id)initWithFrame:(CGRect)frame{
    
    self=[super initWithFrame:frame];
    if (self) {
        
        UIPinchGestureRecognizer *scaleGes = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scaleImage:)];
        [self addGestureRecognizer:scaleGes];
        
        UIPanGestureRecognizer *moveGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveImage:)];
        [moveGes setMinimumNumberOfTouches:1];
        [moveGes setMaximumNumberOfTouches:1];
        [self addGestureRecognizer:moveGes];
        
    }
    
    return self;
}

float _lastTransX = 0.0, _lastTransY = 0.0;

- (void)moveImage:(UIPanGestureRecognizer *)sender
{
    CGPoint translatedPoint = [sender translationInView:self];
    
    if([sender state] == UIGestureRecognizerStateBegan) {
        _lastTransX = 0.0;
        _lastTransY = 0.0;
    }
    
    CGAffineTransform trans = CGAffineTransformMakeTranslation(translatedPoint.x - _lastTransX, translatedPoint.y - _lastTransY);
    CGAffineTransform newTransform = CGAffineTransformConcat(self.transform, trans);
    _lastTransX = translatedPoint.x;
    _lastTransY = translatedPoint.y;
    
    NSLog(@"Trasnform Frame %@",NSStringFromCGAffineTransform(newTransform));
    
    
    if (newTransform.tx>-150 && newTransform.tx<150 && newTransform.ty>-150 && newTransform.ty<150) {
        
        self.transform = newTransform;
        
    } else {
        
        return;
    }
    
    
}

float _lastScale = 1.0;
- (void)scaleImage:(UIPinchGestureRecognizer *)sender
{
    if([sender state] == UIGestureRecognizerStateBegan) {
        
        _lastScale = 1.0;
        return;
    }
    
    CGFloat scale = [sender scale]/_lastScale;
    
    CGAffineTransform currentTransform = self.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);
    [self setTransform:newTransform];
    
    _lastScale = [sender scale];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
