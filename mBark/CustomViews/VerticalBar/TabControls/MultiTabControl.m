//
//  CustomTabControl.m
//  LSTabs
//
//  Created by Marco Mussini on 6/26/12.
//  Copyright (c) 2012 Lucky Software. All rights reserved.
//

#import "MultiTabControl.h"
#import "LSTabControl_Protected.h"
#import "LSTintedButton.h"
#import "BadgeView.h"
#import "ThemeManager.h"
#import "UIImage+Color.h"

NSString *MultiTabControlSelectedImageName      = @"tab-selected-2";
NSString *MultiTabControlNormalImageName        = @"tab-normal-colored";
NSString *MultiTabControlNormalImageBWName      = @"tab-normal";
NSString *MultiTabControlHighlightedImageName   = @"tab-highlighted-colored";
NSString *MultiTabControlHighlightedImageBWName = @"tab-highlighted";

NSString *filterSelectionImage =@"filter_selected.png";
NSString *filterDeselectionImage =@"filter_deselected.png";


@interface MultiTabControl ()

- (void)_updateButtonsImages;
- (void)_refreshTintColor:(UIColor *)newColor;

@end



@implementation MultiTabControl

- (void)flashMe {
    
    [UIView animateWithDuration:0.1f
                          delay:0.0f
                        options:(UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         [UIView setAnimationRepeatCount:3.0f];
                         button.titleLabel.alpha = 0.0f;
                     } 
                     completion:^(BOOL completed) {
                         button.titleLabel.alpha = 1.0f;
                     }];
}


#pragma mark -
#pragma mark AMTTabItemDelegate

- (void)tabItem:(LSTabItem *)item tabColorChangedTo:(UIColor *)newColor {
   
    [self _refreshTintColor:newColor];

}


#pragma mark -
#pragma mark Protected methods

- (void)configureButton {
   
    button.titleLabel.numberOfLines = 3;
    
    button.titleLabel.textAlignment=NSTextAlignmentCenter;
    
    [button setTitleColor:[[ThemeManager sharedManager]colorForKey:@"secondaryColor"]
                  forState:UIControlStateNormal];
    
     [button setTitleColor:[[ThemeManager sharedManager]colorForKey:@"lightColor"]
                 forState:UIControlStateHighlighted];
    
    [button setTitleColor:[[ThemeManager sharedManager]colorForKey:@"lightColor"]
                 forState:UIControlStateSelected];

    
    button.titleEdgeInsets = UIEdgeInsetsMake(5.0f, 8.0f, 0.0f, 6.0f);
   
    
    [button setBackgroundImage:[[UIImage imageNamed:@"filter_selected.png"] imageWithTint:[[ThemeManager sharedManager]colorForKey:@"lightColor"]]  forState:UIControlStateNormal];
    
    [button setBackgroundImage:[[UIImage imageNamed:@"filter_selected.png"] imageWithTint:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]]  forState:UIControlStateSelected|UIControlStateHighlighted];
    
    ((LSTintedButton *)button).backgroundTintColor = tintColor;
    
    [self _updateButtonsImages];

}


#pragma mark -
#pragma mark Private methods

- (void)_updateButtonsImages {
   
    [button setBackgroundImage:[[UIImage imageNamed:@"filter_selected.png"] imageWithTint:[[ThemeManager sharedManager]colorForKey:@"lightColor"]]  forState:UIControlStateNormal];
   
    [button setBackgroundImage:[[UIImage imageNamed:@"filter_selected.png"] imageWithTint:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]]  forState:UIControlStateHighlighted];
    
    [button setBackgroundImage:[[UIImage imageNamed:@"filter_selected.png"] imageWithTint:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]]  forState:UIControlStateSelected];
    
}


- (void)_refreshTintColor:(UIColor *)newColor {
  
    [tintColor release];
    tintColor = [newColor retain];
    ((LSTintedButton *)button).backgroundTintColor = newColor;
    
    [self _updateButtonsImages];

}


@end
