//
//  HomeTable.m
//  P1
//
//  Created by Xiphi Tech on 04/10/2014.
//
//

#import "HorizontalTable.h"

@interface HorizontalTable ()

@end

@implementation HorizontalTable


#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder {
	
	assert([aDecoder isKindOfClass:[NSCoder class]]);
	
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		
		const CGFloat k90DegreesCounterClockwiseAngle = (CGFloat) -(90 * M_PI / 180.0);
		
		CGRect frame = self.frame;
		self.transform = CGAffineTransformRotate(CGAffineTransformIdentity, k90DegreesCounterClockwiseAngle);
		self.frame = frame;
		
        self.showsHorizontalScrollIndicator=FALSE;
        self.showsVerticalScrollIndicator=FALSE;
        
        self.separatorStyle=UITableViewCellSeparatorStyleNone;
        
	}
	
	assert(self);
	return self;
}



@end
