//
//  HomeTableCell.h
//  P1
//
//  Created by Xiphi Tech on 04/10/2014.
//
//

#import <UIKit/UIKit.h>

@interface HorizontalTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *smallLabel;


@end
