//
//  HomeTableCell.m
//  P1
//
//  Created by Xiphi Tech on 04/10/2014.
//
//

#import "HorizontalTableCell.h"

@implementation HorizontalTableCell

@synthesize titleLabel,mainImage,smallLabel;

- (id)initWithCoder:(NSCoder *)aDecoder {
	
	assert([aDecoder isKindOfClass:[NSCoder class]]);
	
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		
		CGFloat k90DegreesClockwiseAngle = (CGFloat) (90 * M_PI / 180.0);
		
		self.transform = CGAffineTransformRotate(CGAffineTransformIdentity, k90DegreesClockwiseAngle);
       
	}
	
	return self;
}

@end
