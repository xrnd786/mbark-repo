//
//  SearchTableView.m
//  P1
//
//  Created by Xiphi Tech on 07/10/2014.
//
//

#import "SearchTableView.h"

@implementation SearchTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.separatorColor=[UIColor blackColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
