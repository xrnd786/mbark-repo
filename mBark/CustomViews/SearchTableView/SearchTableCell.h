//
//  SearchTableCell.h
//  P1
//
//  Created by Xiphi Tech on 07/10/2014.
//
//

#import <UIKit/UIKit.h>


//*****NAVIGATION HELPER*****//
#import "RNThemeLabel.h"


@interface SearchTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *FixedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *fixedIcon;

@end
