//
//  lastSearchedValueCell.h
//  mBark
//
//  Created by Xiphi Tech on 20/04/2015.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeLabel.h"

@interface lastSearchedValueCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *lastSearchedText;

@end
