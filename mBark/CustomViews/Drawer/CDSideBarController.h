//
//  CDSideBarController.h
//  CDSideBar
//
//  Created by Christophe Dellac on 9/11/14.
//  Copyright (c) 2014 Christophe Dellac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CDSideBarControllerDelegate <NSObject>

- (void)menuButtonClicked:(int)index;

@end

@interface CDSideBarController : NSObject<UITableViewDataSource,UITableViewDelegate>
{

    UIView  *_backgroundMenuView;
    UITableView *mainTable;
    
}


@property (nonatomic, retain) UIColor *menuColor;
@property (nonatomic) BOOL isOpen;

@property (nonatomic, retain) id<CDSideBarControllerDelegate> delegate;

- (CDSideBarController*)initWithViews;


- (void)insertOnView:(UIView*)view atClickBy:(UIButton*)myClick;
@end

