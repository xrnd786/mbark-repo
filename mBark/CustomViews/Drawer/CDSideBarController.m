//
//  CDSideBarController.m
//  CDSideBar
//
//  Created by Christophe Dellac on 9/11/14.
//  Copyright (c) 2014 Christophe Dellac. All rights reserved.
//

#import "CDSideBarController.h"

#import "EditableTableCell.h"

@implementation CDSideBarController

@synthesize menuColor = _menuColor;
@synthesize isOpen = _isOpen;

#pragma mark - 
#pragma mark Init

- (CDSideBarController*)initWithViews
{
  
    if (self) {
        
        _backgroundMenuView = [[UIView alloc] init];
        _menuColor = [UIColor whiteColor];
        
    }
    
    return self;
}

- (void)insertOnView:(UIView*)view atClickBy:(UIButton*)myClick
{
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissMenu)];
    [view addGestureRecognizer:singleTap];
    
  
    [myClick addTarget:self action:@selector(onMenuButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _backgroundMenuView.frame = CGRectMake(view.frame.size.width, 0, 450, view.frame.size.height);
    _backgroundMenuView.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.5f];
    
    
    mainTable = [[UITableView alloc] initWithFrame:_backgroundMenuView.frame];
   
    mainTable.delegate=self;
    mainTable.dataSource=self;
    
    
    [view addSubview:mainTable];
    [view bringSubviewToFront:mainTable];
    
    [view addSubview:_backgroundMenuView];
    [view bringSubviewToFront:_backgroundMenuView];
    
}


#pragma mark - 
#pragma mark Menu button action

- (void)dismissMenuWithSelection:(UIButton*)button
{
    [UIView animateWithDuration:0.3f
                          delay:0.0f
         usingSpringWithDamping:.2f
          initialSpringVelocity:10.f
                        options:0 animations:^{
                            button.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2);
                        }
                     completion:^(BOOL finished) {
                         [self dismissMenu];
                     }];
}

- (void)dismissMenu
{
    if (_isOpen)
    {
        _isOpen = !_isOpen;
       
     [self performSelectorInBackground:@selector(performDismissAnimation) withObject:nil];
    }

}

- (void)showMenu
{
    
    if (!_isOpen)
    {
        _isOpen = !_isOpen;
        [self performSelectorInBackground:@selector(performOpenAnimation) withObject:nil];
        [mainTable reloadData];
    }

}


- (void)onMenuButtonClick:(UIButton*)button
{
    if (!_isOpen) {
        
        
        [self showMenu];
        
    } else {
        
        [self dismissMenu];
        
    }
}

#pragma mark -
#pragma mark - Animations

- (void)performDismissAnimation
{
    
   [UIView animateWithDuration:0.4 animations:^{
       
       CGRect mainFrame=mainTable.frame;
       //mainTable.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 0);
       [mainTable setFrame:CGRectMake(mainFrame.origin.x+450, 0, mainFrame.size.width,mainFrame.size.height)];
       
    }];
    
}

- (void)performOpenAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:^{
          
            CGRect mainFrame=mainTable.frame;
            //mainTable.transform = CGAffineTransformTranslate(CGAffineTransformMake(1, 0, 0, 1, 0, 0), -90, 0);
            [mainTable setFrame:CGRectMake(mainFrame.origin.x-450, 0, mainFrame.size.width,mainFrame.size.height)];
       
        }];
    });
   
}


#pragma mark - tableview delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 5;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* cellIdentifier;
    cellIdentifier=@"EditableTableCell";
        
    EditableTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // optionally specify a width that each set of utility buttons will share
    
    if (!cell)
	{
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
		cell = (EditableTableCell*) [nib objectAtIndex:0];
		
		assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
	}
	
    
    cell.textLabel.text = [NSString stringWithFormat:@"Section: %ld, Seat: %ld", (long)indexPath.section, (long)indexPath.row];
    
    return cell;
    

    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self dismissMenu];
    
}


@end

