//
//  FilterCheckTableView.m
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import "FilterCheckTableView.h"

#import "FilterSelectionCell.h"

@interface FilterCheckTableView ()

@property (strong,nonatomic) NSMutableArray *titleList;
@property (strong,nonatomic) NSMutableArray *numberList;
@property (strong,nonatomic) NSMutableArray *attributeName;
@property (strong,nonatomic) NSMutableArray *parametersName;
@property (strong,nonatomic) NSNumber *myNumber;

@property (strong,nonatomic) NSMutableArray *listOfIndexSelected;


@end

@implementation FilterCheckTableView

@synthesize titleList,numberList,localTableCopy,delegate,myNumber,attributeName,parametersName,listOfIndexSelected;

-(id)initWithMainView:(UIView*)view withTitleValues:(NSArray*)titleValues withMyNumber:(int)number withAttributesName:(NSArray *)attributeNames parameter:(NSArray*)paramters withSelectedArray:(NSArray*)list{
    
    self = [super init];
    if (self) {
        
        self.myNumber=[NSNumber numberWithInt:number];
        self.attributeName=[[NSMutableArray alloc]initWithArray:attributeNames];
        self.parametersName=[[NSMutableArray alloc]initWithArray:paramters];
        
        self.listOfIndexSelected=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)list];
        
        [self initializeTableWithFeatured:titleValues];
        
        localTableCopy=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
        [localTableCopy setShowsVerticalScrollIndicator:NO];
        [localTableCopy setBackgroundColor:[UIColor whiteColor]];
        
        if ([localTableCopy respondsToSelector:@selector(setSeparatorInset:)]) {
           [localTableCopy setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([localTableCopy respondsToSelector:@selector(setLayoutMargins:)]) {
            [localTableCopy setLayoutMargins:UIEdgeInsetsZero];
        }
        
        localTableCopy.delegate=self;
        localTableCopy.dataSource=self;
        
        
        
        [view addSubview:localTableCopy];
        
        
    }
   
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)initializeTableWithFeatured:(NSArray*)titles{
    
    titleList=[[NSMutableArray alloc]initWithArray:titles];
    
}


#pragma mark - table View Functions

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40;
}




-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    cell.backgroundColor=[UIColor clearColor];
    
}


#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [titleList count];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"FilterSelectionCell";
    FilterSelectionCell *cell =(FilterSelectionCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"FilterSelectionCell" owner:self options:nil] objectAtIndex:0];
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    cell.titleLabel.text=[titleList objectAtIndex:indexPath.row];
    
    if ([self.listOfIndexSelected containsObject:[NSNumber numberWithInt:indexPath.row]]) {
        
        cell.checkMark.selected =TRUE;
        
        
    }else{
        cell.checkMark.selected =FALSE;
        
    }
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    if ([[self.attributeName objectAtIndex:[myNumber integerValue]] isEqualToString:@"Category"]) {
    
        
        for (int i=0;i<[titleList count];i++) {
            
            FilterSelectionCell *cell=(FilterSelectionCell*)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            if (i==indexPath.row) {
                
                cell.checkMark.selected = ![cell.checkMark isSelected];
                
                [self.delegate filterValueAtIndex:(int)indexPath.row forMyValue:(int)[self.myNumber integerValue] withAtributeName:[self.attributeName objectAtIndex:[myNumber integerValue]] withParameter:[self.parametersName objectAtIndex:[myNumber integerValue]] withAnswer:cell.checkMark.selected];
                
            } else {
                
                cell.checkMark.selected =FALSE;
            
            }
            
        }
    
        
    } else {
        
        FilterSelectionCell *cell=(FilterSelectionCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        cell.checkMark.selected = ![cell.checkMark isSelected];
        
        [self.delegate filterValueAtIndex:(int)indexPath.row forMyValue:(int)[self.myNumber integerValue] withAtributeName:[self.attributeName objectAtIndex:[myNumber integerValue]] withParameter:[self.parametersName objectAtIndex:[myNumber integerValue]] withAnswer:cell.checkMark.selected];
        
    }
    
}


#pragma mark - clear

-(void)changeUIForClear{
    
    for (int i=0;i<[self.titleList count];i++) {
        
       FilterSelectionCell *cell=(FilterSelectionCell*) [self.localTableCopy cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        cell.checkMark.selected=FALSE;
    }
    
}

@end
