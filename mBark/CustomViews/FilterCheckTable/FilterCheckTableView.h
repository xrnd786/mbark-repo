//
//  FilterCheckTableView.h
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import <UIKit/UIKit.h>

@protocol FilterCheckClicked <NSObject>

-(void)filterValueAtIndex:(int)indexValue forMyValue:(int)myValue withAtributeName:(NSString*)name withParameter:(NSString*)parameter withAnswer:(BOOL)isSelected;

@end

@interface FilterCheckTableView : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) id<FilterCheckClicked> delegate;

-(id)initWithMainView:(UIView*)view withTitleValues:(NSArray*)titleValues withMyNumber:(int)number withAttributesName:(NSArray*)attributeNames parameter:(NSArray*)paramters withSelectedArray:(NSArray*)list;

-(void)changeUIForClear;

@end
