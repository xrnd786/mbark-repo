//
//  FilterSlider.h
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "NMRangeSlider.h"

@protocol sliderChanged <NSObject>

-(void)sliderMovedWithLowValue:(float)lowValue andHighValue:(float)high;

@end

@interface FilterSlider : UIViewController

@property (weak, nonatomic) IBOutlet NMRangeSlider *slider_Main;
@property (strong, nonatomic) id<sliderChanged> delegate;

-(id)initWithStartValue:(int)startPrice endPrice:(int)endPrice;

@property (weak, nonatomic) IBOutlet RNThemeLabel *lowValue;
@property (weak, nonatomic) IBOutlet RNThemeLabel *highValue;
@property (strong, nonatomic)  NSNumber *lowMain;
@property (strong, nonatomic)  NSNumber *highMain;

- (void) updateSliderLabels;

- (IBAction)sliderValueChanged:(id)sender;

@end
