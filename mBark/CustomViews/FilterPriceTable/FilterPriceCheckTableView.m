//
//  FilterCheckTableView.m
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import "FilterPriceCheckTableView.h"

#import "FilterPriceSelectionCell.h"

@interface FilterPriceCheckTableView ()<sliderChanged>

@property (strong,nonatomic) NSMutableArray *titleList;
@property (strong,nonatomic) NSMutableArray *numberList;
@property (strong,nonatomic) NSNumber *myNumber;
@property (strong,nonatomic) NSArray *attributeName;

@property (strong,nonatomic) NSMutableArray *listOfIndexSelected;


@end

@implementation FilterPriceCheckTableView

@synthesize titleList,numberList,localTableCopy,customSlider,delegate,attributeName,listOfIndexSelected;

-(id)initWithMainView:(UIView*)view filtersBounds:(NSArray*)bounds withTitleValues:(NSArray*)titleValues withMyNumber:(int)number  withAttributesName:(NSArray*)attributeNames withSelectedArray:(NSArray*)list{
    
    self = [super init];
    if (self) {
        
        [self initializeTableWithFeatured:titleValues];
        
        customSlider=[[FilterSlider alloc]initWithStartValue:[[bounds objectAtIndex:0] integerValue] endPrice:[[bounds objectAtIndex:1] integerValue]];
        
        self.listOfIndexSelected=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)list];
        
        
        
        customSlider.delegate=self;
        [view addSubview:customSlider.view];
        
        localTableCopy=[[UITableView alloc]initWithFrame:CGRectMake(0, 200, view.frame.size.width, view.frame.size.height)];
        [localTableCopy setShowsVerticalScrollIndicator:NO];
        [localTableCopy setBackgroundColor:[UIColor whiteColor]];
        
        if ([localTableCopy respondsToSelector:@selector(setSeparatorInset:)]) {
            [localTableCopy setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([localTableCopy respondsToSelector:@selector(setLayoutMargins:)]) {
            [localTableCopy setLayoutMargins:UIEdgeInsetsZero];
        }
        
        localTableCopy.delegate=self;
        localTableCopy.dataSource=self;
        
        [view addSubview:localTableCopy];
        
        self.myNumber=[NSNumber numberWithInt:number];
        self.attributeName=[[NSArray alloc]initWithArray:attributeNames];
        
    }
    
    return self;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)initializeTableWithFeatured:(NSArray*)array{
    
    titleList=[[NSMutableArray alloc]initWithArray:array];
    
}



#pragma mark - table View Functions

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    return nil;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.backgroundColor=[UIColor clearColor];
    
}


#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return ([titleList count]-1);
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"FilterPriceSelectionCell";
    FilterPriceSelectionCell *cell =(FilterPriceSelectionCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"FilterPriceSelectionCell" owner:self options:nil] objectAtIndex:0];
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    cell.startsWith.text=[NSString stringWithFormat:@"%.0f",([[titleList objectAtIndex:indexPath.row] floatValue])];
    
    
    if (indexPath.row==([titleList count]-2)) {
        
        
        cell.EndsWith.text=[NSString stringWithFormat:@"%.0f",([[titleList objectAtIndex:indexPath.row+1] floatValue])];
        
        
    } else {
        
        cell.EndsWith.text=[NSString stringWithFormat:@"%.0f",([[titleList objectAtIndex:indexPath.row+1] floatValue]-1)];
        
    }
    
    
    if ([self.listOfIndexSelected containsObject:[NSNumber numberWithInt:indexPath.row]]) {
        
        cell.checkMark.selected =TRUE;
        customSlider.slider_Main.lowerValue = [cell.startsWith.text floatValue];
        customSlider.slider_Main.upperValue = [cell.EndsWith.text floatValue];
        [customSlider updateSliderLabels];
        
        
    }else{
        
        cell.checkMark.selected =FALSE;
        
    }
    
    //cell.numberLabel.text=@"₹";
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    for (int i=0;i<[titleList count]-1;i++) {
        
        FilterPriceSelectionCell *cell=(FilterPriceSelectionCell*)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        
        if (i==indexPath.row) {
         
            cell.checkMark.selected = ![cell.checkMark isSelected];
            
            if (cell.checkMark.selected) {
                
                customSlider.slider_Main.lowerValue =[cell.startsWith.text floatValue];
                customSlider.slider_Main.upperValue = [cell.EndsWith.text floatValue];
                [customSlider updateSliderLabels];

                [self.delegate priceValueAtIndex:(int)indexPath.row forMyValue:(int)[self.myNumber integerValue] withAtributeName:[self.attributeName objectAtIndex:[self.myNumber integerValue]]];
                

            } else {
             
                customSlider.slider_Main.lowerValue = [customSlider.lowMain floatValue];
                customSlider.slider_Main.upperValue = [customSlider.highMain floatValue];
                [customSlider updateSliderLabels];
                [self.delegate clearPriceArray];
                
            }
            
        } else {
        
            cell.checkMark.selected =FALSE;
        }
        
    }
    
    
}


#pragma mark - clear

-(void)changeCheckBoxesForClear{
    
    for (int i=0;i<([self.titleList count]-1);i++) {
        
        FilterPriceSelectionCell *cell=(FilterPriceSelectionCell*) [self.localTableCopy cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        cell.checkMark.selected=FALSE;
    }
    
    [self.delegate clearPriceArray];


}

-(void)changeSliderUIForClear{

    customSlider.slider_Main.lowerValue = customSlider.slider_Main.minimumValue;
    customSlider.slider_Main.upperValue = customSlider.slider_Main.maximumValue;
    [customSlider updateSliderLabels];

}


#pragma mark - custom slider delegate

-(void)sliderMovedWithLowValue:(float)lowValue andHighValue:(float)high{
    
    [self changeCheckBoxesForClear];
    [self.delegate priceValueLowValue:lowValue highVaue:high];
    
    
}

@end
