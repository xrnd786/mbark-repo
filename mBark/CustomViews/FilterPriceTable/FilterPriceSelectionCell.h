//
//  FilterSelectionCell.h
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeImageButton.h"
#import "RNThemeLabel.h"

@interface FilterPriceSelectionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet RNThemeImageButton *checkMark;
@property (weak, nonatomic) IBOutlet RNThemeLabel *titleLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *numberLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *startsWith;
@property (weak, nonatomic) IBOutlet RNThemeLabel *EndsWith;

@end
