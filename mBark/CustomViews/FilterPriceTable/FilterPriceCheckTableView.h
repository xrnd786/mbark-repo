//
//  FilterCheckTableView.h
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "FilterSlider.h"
@protocol PriceCheckClicked <NSObject>

-(void)priceValueAtIndex:(int)indexValue forMyValue:(int)myValue  withAtributeName:(NSString*)name;
-(void)priceValueLowValue:(float)lowValue highVaue:(float)highValue;

-(void)clearPriceArray;

@end

@interface FilterPriceCheckTableView : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) id<PriceCheckClicked> delegate;
@property (strong,nonatomic) FilterSlider *customSlider;


-(id)initWithMainView:(UIView*)view filtersBounds:(NSArray*)bounds withTitleValues:(NSArray*)titleValues withMyNumber:(int)number withAttributesName:(NSArray*)attributeNames withSelectedArray:(NSArray*)list;

-(void)changeCheckBoxesForClear;
-(void)changeSliderUIForClear;

@end
