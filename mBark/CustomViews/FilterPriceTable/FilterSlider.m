//
//  FilterSlider.m
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import "FilterSlider.h"
#import "ThemeManager.h"
#import "UIImage+Color.h"

@interface FilterSlider ()

@end

@implementation FilterSlider

@synthesize slider_Main,lowValue,highValue,lowMain,highMain,delegate;

-(id)initWithStartValue:(int)price endPrice:(int)endPrice{
    
    self=[super init];
    if (self) {
    
        self.lowMain=[NSNumber numberWithInt:price];
        self.highMain=[NSNumber numberWithInt:endPrice];
        
        
        self.view=[[[NSBundle mainBundle]loadNibNamed:@"FilterSlider" owner:self options:nil] objectAtIndex:0];
        
        self.slider_Main.minimumValue = price;
        self.slider_Main.maximumValue = endPrice;
        
        [self.slider_Main setLowerValue:price upperValue:endPrice animated:YES];
        [self.slider_Main setLowerValue:price upperValue:endPrice animated:YES];
        
        //self.slider_Main.upperMinimumValue=self.slider_Main.minimumValue+50;
        //self.slider_Main.lowerMaximumValue=self.slider_Main.maximumValue-50;
        
        self.slider_Main.minimumRange = 10;
        self.slider_Main.trackImage=[UIImage imageWithImage:[[UIImage imageNamed:@"filter_selected.png"]imageWithTint:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]] scaledToSize:CGSizeMake(self.slider_Main.frame.size.width,3)];
     
        [self updateSliderLabels];
   
    }
    
    return self;
    
}


- (void) updateSliderLabels
{
    // You get get the center point of the slider handles and use this to arrange other subviews
    CGPoint lowerCenter;
    lowerCenter.x = (self.slider_Main.lowerCenter.x + self.slider_Main.frame.origin.x);
    lowerCenter.y = (self.slider_Main.center.y - 30.0f);
    
    //self.lowValue.center = lowerCenter;
    
    NSLog(@"Final lower value to update %@",[NSString stringWithFormat:@"₹ %0.f", self.slider_Main.lowerValue]);
    
    self.lowValue.text = [NSString stringWithFormat:@"₹ %0.f", self.slider_Main.lowerValue];
    
    CGPoint upperCenter;
    upperCenter.x = (self.slider_Main.upperCenter.x + self.slider_Main.frame.origin.x);
    upperCenter.y = (self.slider_Main.center.y - 30.0f);
   
    NSLog(@"Final lower value to update %@",[NSString stringWithFormat:@"₹ %0.f", self.slider_Main.upperValue]);
    
    //self.highValue.center = upperCenter;
    self.highValue.text = [NSString stringWithFormat:@"₹ %0.f", self.slider_Main.upperValue];

}


- (IBAction)sliderValueChanged:(id)sender {
    
    [self updateSliderLabels];
    [self.delegate sliderMovedWithLowValue:self.slider_Main.lowerValue andHighValue:self.slider_Main.upperValue];
    
}


@end
