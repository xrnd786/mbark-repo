//
//  EditableTable.m
//  P1
//
//  Created by Xiphi Tech on 07/10/2014.
//
//

#import "EditableTable.h"

@implementation EditableTable

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        [self setEditing: YES animated: YES];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
