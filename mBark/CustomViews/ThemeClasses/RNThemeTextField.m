//
//  RNThemeTextField.m
//  DT
//
//  Created by Ryan Nystrom on 2/5/13.
//
//

#import "RNThemeTextField.h"
#import "ThemeManager.h"
#import "UIImage+Color.h"
#import "UIImage+ImageEffects.h"

@implementation RNThemeTextField

@synthesize leftImage;
@synthesize toolbar,previousBarButton,nextBarButton;

struct CGColor *textBorder;


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self _init];
        
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self _init];
        
    }
    return self;
}

- (id)init {
    if (self = [super init]) {
        [self _init];
        
    }
    return self;
}


- (void)_init {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeDidChangeNotification:) name:ThemeManagerDidChangeThemes object:nil];
    //[self setup];

}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self applyTheme];
}

- (void)dealloc {
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    @catch (NSException *exception) {
        // do nothing, only unregistering self from notifications
    }
    
}


#pragma mark - Setup

- (void)setup
{
    //Responders setup
    //self.delegate=self;
    
    //Appearance setup
    
    //Color
    textBorder=self.layer.borderColor;
    self.backgroundColor=[UIColor whiteColor];
    
    
    //Size
    //self.frame=CGRectMake(self.frame.origin.x, self.frame.origin.y, [GlobalCall findBounds].size.width*0.6, 60);
    
    //style
    self.borderStyle=UITextBorderStyleNone;
    
    //custom View Initialization
    /*
    toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, self.window.frame.size.width, 44);
    // set style
    [toolbar setBarStyle:UIBarStyleDefault];
    
    self.previousBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Previous", @"Previous") style:UIBarButtonItemStyleBordered target:self action:@selector(previousButtonIsClicked:)];
    self.nextBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", @"Next") style:UIBarButtonItemStyleBordered target:self action:@selector(nextButtonIsClicked:)];
    
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonIsClicked:)];
    
    NSArray *barButtonItems = @[self.previousBarButton, self.nextBarButton, flexBarButton, doneBarButton];
    
    toolbar.items = barButtonItems;
    */
    //Image
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    imageView.contentMode = UIViewContentModeCenter;
    imageView.image=self.leftImage.image;
   
    self.leftView = imageView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
}


#pragma mark - Theme Functions

- (void)applyTheme {
   
    UIFont *font = nil;
    
    if (self.fontKey && (font = [[ThemeManager sharedManager] fontForKey:self.fontKey])) {
        self.font = font;
    }
    
    UIColor *textColor = nil;
    
    if (self.textColorKey && (textColor = [[ThemeManager sharedManager] colorForKey:self.textColorKey])) {
        self.textColor = textColor;
    }
   
    UIImage *image = nil;
    
    if (self.imageKey && (image = [[ThemeManager sharedManager] imageForKey:self.imageKey])) {
        
        if (textColor!=nil) {
            //[UIImage imageWithImage: scaledToSize:CGSizeMake(25,25)]
            self.leftImage =[[UIImageView alloc]initWithImage:[image imageWithTint:textColor]];
            
        } else {
            
            //[UIImage imageWithImage:image scaledToSize:CGSizeMake(25,25)]
            self.leftImage = [[UIImageView alloc]initWithImage:image];
        
        }
        
        [self setup];
       
    }else{
        
        self.leftImage.image = nil;
        self.leftView=nil;
        
    }

}


- (void)themeDidChangeNotification:(NSNotification *)notification {
    
    [self applyTheme];

}


#pragma mark - TEXT FIELD FUNCTIONS

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    self.layer.masksToBounds=YES;
    
    UIColor *textColor = nil;
    
    if (self.textColorKey && (textColor = [[ThemeManager sharedManager] colorForKey:self.textColorKey])) {
       
        self.layer.borderColor=[textColor CGColor];
        self.layer.borderWidth= 0.2f;
     
    }
    
    [self setInputAccessoryView:toolbar];
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
   
     self.layer.borderColor=textBorder;
    
}


@end
