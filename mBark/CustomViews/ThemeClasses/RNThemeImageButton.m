//
//  RNThemeButton.m
//  DT
//
//  Created by Ryan Nystrom on 2/5/13.
//
//

#import "RNThemeImageButton.h"
#import "ThemeManager.h"
#import "UIImage+Color.h"
#import "UIImage+ImageEffects.h"

@implementation RNThemeImageButton

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self _init];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        [self _init];
    }
    
    return self;
}

- (id)init {
    if (self = [super init]) {
        [self _init];
    }
    return self;
}

- (void)_init {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeDidChangeNotification:) name:ThemeManagerDidChangeThemes object:nil];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self applyTheme];
    
}

- (void)dealloc {
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    @catch (NSException *exception) {
       
    }
}

- (void)applyTheme {
  
    CGFloat finalWidth=self.frame.size.width-10;
    CGFloat finalHeight=self.frame.size.height-10;
   
    UIColor *backgroundColor = nil;
   
    if (self.backgroundColorKey && (backgroundColor = [[ThemeManager sharedManager] colorForKey:self.backgroundColorKey])) {
        
        UIImage *backgroundImage = nil;
        
        if (self.imageView.image!=nil) {
            
            backgroundImage=self.imageView.image;
            UIImage *img=[backgroundImage imageWithTint:backgroundColor];
            
            if (finalHeight<=0) {
                finalHeight=self.imageView.frame.size.height-10;
            }
            
            if(finalWidth<=0){
                finalWidth=self.imageView.frame.size.width-10;
            }
            //[UIImage imageWithImage:img scaledToSize:CGSizeMake(finalWidth,finalHeight)]
            [self setImage:img forState:UIControlStateNormal];
            
        }else if(self.backgroundImageKey && (backgroundImage = [[ThemeManager sharedManager] imageForKey:self.backgroundImageKey])) {
            
            UIImage *img=[backgroundImage imageWithTint:backgroundColor];
            if (finalHeight<=0) {
                finalHeight=30;
            }
            
            if(finalWidth<=0){
                finalWidth=30;
            }

            //[UIImage imageWithImage:img scaledToSize:CGSizeMake(finalWidth,finalHeight)]
            [self setImage:img forState:UIControlStateNormal];
            
        }
        
    }
    
    UIColor *selectedColor = nil;

  if (self.highlightedColorKey && (selectedColor = [[ThemeManager sharedManager] colorForKey:self.highlightedColorKey])) {
      
      
      UIImage *backgroundImage = nil;
      if (self.imageView.image!=nil) {
          
          backgroundImage=self.imageView.image;
          UIImage *img=[backgroundImage imageWithTint:selectedColor];
          
          if (finalHeight<=0) {
              finalHeight=self.imageView.frame.size.height-10;
          }
          
          if(finalWidth<=0){
              finalWidth=self.imageView.frame.size.width-10;
          }
//[UIImage imageWithImage:img scaledToSize:CGSizeMake(finalWidth,finalHeight)]
          [self setImage:img forState:UIControlStateSelected];
          
      }else if(self.backgroundImageKey && (backgroundImage = [[ThemeManager sharedManager] imageForKey:self.backgroundImageKey])) {
          
          UIImage *img=[backgroundImage imageWithTint:selectedColor];
          
          if (finalHeight<=0) {
              finalHeight=30;
          }
          
          if(finalWidth<=0){
              finalWidth=30;
          }

          //[UIImage imageWithImage:img scaledToSize:CGSizeMake(finalWidth,finalHeight)]
          
          [self setImage:img forState:UIControlStateSelected];
          
      }

    }
    
}

- (void)themeDidChangeNotification:(NSNotification *)notification {
    
    [self applyTheme];

}

@end
