//
//  UIImage+Color.h
//  P1
//
//  Created by Xiphi Tech on 08/10/2014.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)



+ (UIImage*)setBackgroundImageByColor:(UIColor *)backgroundColor withFrame:(CGRect )rect;


+ (UIImage*) replaceColor:(UIColor*)color inImage:(UIImage*)image withTolerance:(float)tolerance;

//resizing Stuff...
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
- (UIImage *)imageWithTint:(UIColor *)tintColor;


@end
