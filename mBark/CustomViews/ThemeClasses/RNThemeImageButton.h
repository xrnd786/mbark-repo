//
//  RNThemeButton.h
//  DT
//
//  Created by Ryan Nystrom on 2/5/13.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeUpdateProtocol.h"

@interface RNThemeImageButton : UIButton
<RNThemeUpdateProtocol>

@property (nonatomic, strong) NSString *backgroundColorKey;
@property (nonatomic, strong) NSString *highlightedColorKey;
@property (nonatomic, strong) NSString *backgroundImageKey;

- (void)applyTheme;
@end
