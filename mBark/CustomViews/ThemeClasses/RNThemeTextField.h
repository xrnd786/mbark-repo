//
//  RNThemeTextField.h
//  DT
//
//  Created by Ryan Nystrom on 2/5/13.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeUpdateProtocol.h"

@interface RNThemeTextField : UITextField
<RNThemeUpdateProtocol,UITextFieldDelegate>

@property (nonatomic, strong) NSString *fontKey;
@property (nonatomic, strong) NSString *textColorKey;
@property (nonatomic, strong) NSString *imageKey;

@property (nonatomic,strong) UIImageView *leftImage;


@property (nonatomic, strong) UIToolbar *toolbar;

@property (nonatomic , strong) UIBarButtonItem *previousBarButton;
@property (nonatomic , strong) UIBarButtonItem *nextBarButton;
- (void)setup;

@end
