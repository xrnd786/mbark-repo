//
//  BottomBorderView.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "BottomBorderView.h"

@implementation BottomBorderView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    // Add a bottomBorder.
    
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(rect.origin.x ,rect.size.height-1, rect.size.width, 1.0f);
    
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    
    
    //bottomBorder.shadowOffset = CGSizeMake(-5, 5);
    //bottomBorder.shadowRadius = 3;
    //bottomBorder.shadowOpacity = 0.3;
    
    [self.layer addSublayer:bottomBorder];
    
}


@end
