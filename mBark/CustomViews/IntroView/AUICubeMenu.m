//
//  AUICubeMenu.m
//  TestCubeMenu
//
//  Created by AlimysoYang on 13-4-11.
//  Copyright (c) 2013年 AlimysoYang. All rights reserved.
//

#import "AUICubeMenu.h"
#import <QuartzCore/QuartzCore.h>

@implementation AUICubeMenu

@synthesize fw = _fw;
@synthesize sw = _sw;
@synthesize tw = _tw;
@synthesize delegate;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        self.currentIntro=[NSNumber numberWithInt:0];
        // Initialization code
        self.fw = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
        [self.fw setShowsTouchWhenHighlighted:YES];
        [self.fw setBackgroundColor:[UIColor redColor]];
        [self.fw setImage:[UIImage imageNamed:@"cloud@2x.png"] forState:UIControlStateNormal];
        [self.fw setTag:100];
        [self.fw addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.fw];
        
       
        self.sw = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
        [self.sw setShowsTouchWhenHighlighted:YES];
        [self.sw setBackgroundColor:[UIColor blueColor]];
        [self.sw setImage:[UIImage imageNamed:@"email@2x.png"] forState:UIControlStateNormal];
        [self.sw setTag:101];
        [self.sw addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.sw];
        
        self.tw = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width , frame.size.height)];
        [self.tw setShowsTouchWhenHighlighted:YES];
        [self.tw setBackgroundColor:[UIColor greenColor]];
        [self.tw setImage:[UIImage imageNamed:@"website@2x.png"] forState:UIControlStateNormal];
        [self.tw setTag:102];
        [self.tw addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.tw];
        
        UISwipeGestureRecognizer *grl = [[UISwipeGestureRecognizer alloc] init];
        [grl setDirection:UISwipeGestureRecognizerDirectionLeft];
        [grl addTarget:self action:@selector(selectMenuItemLeft)];
        [self addGestureRecognizer:grl];
       
        UISwipeGestureRecognizer *grr = [[UISwipeGestureRecognizer alloc] init];
        [grr setDirection:UISwipeGestureRecognizerDirectionRight];
        [grr addTarget:self action:@selector(selectMenuItemRight)];
        [self addGestureRecognizer:grr];
        i = 0;
    }
    return self;
}

- (void) selectMenuItemLeft
{
    CATransition *animation = [CATransition animation];
    animation.duration = 0.8;
    animation.type = @"cube";
    animation.subtype = kCATransitionFromRight;
    
    
    UIButton *lastView = (UIButton *)[self.subviews lastObject];
    if (lastView.tag==100){
        
        [self bringSubviewToFront:self.tw];
        self.currentIntro=[NSNumber numberWithInt:2];
        
    }else if (lastView.tag==101)
    {
        [self bringSubviewToFront:self.fw];
        self.currentIntro=[NSNumber numberWithInt:0];
        
    }else if (lastView.tag==102)
    {
        [self bringSubviewToFront:self.sw];
        self.currentIntro=[NSNumber numberWithInt:1];
        
        
    }
    
    [self.layer addAnimation:animation forKey:@"animation"];
    [self.delegate swipedLeft];
    
     //NSLog(@"%i",i);
}


- (void) selectMenuItemRight
{
    CATransition *animation = [CATransition animation];
    animation.duration = 0.8;
    animation.type = @"cube";
    animation.subtype = kCATransitionFromLeft;
    
    UIButton *lastView = (UIButton *)[self.subviews lastObject];
    
    if (lastView.tag==100){
        
        [self bringSubviewToFront:self.sw];
        self.currentIntro=[NSNumber numberWithInt:1];
        
    }else if (lastView.tag==101)
    {
        [self bringSubviewToFront:self.tw];
        self.currentIntro=[NSNumber numberWithInt:2];
        
    }else if (lastView.tag==102)
    {
        [self bringSubviewToFront:self.fw];
        self.currentIntro=[NSNumber numberWithInt:0];
        
    }
    
    [self.layer addAnimation:animation forKey:@"animation"];
    [self.delegate swipedRight];
    
}

- (void) buttonClicked:(id) sender
{
 
    UIButton *button = (UIButton *)sender;
    NSLog(@"%i", button.tag);
    
}


@end
