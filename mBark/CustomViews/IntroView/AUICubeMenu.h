//
//  AUICubeMenu.h
//  TestCubeMenu
//
//  Created by AlimysoYang on 13-4-11.
//  Copyright (c) 2013年 AlimysoYang. All rights reserved.
//  QQ:86373007

#import <UIKit/UIKit.h>

@protocol Swiped <NSObject>

-(void)swipedRight;
-(void)swipedLeft;


@end

@interface AUICubeMenu : UIView
{
    int i;
}

@property (strong,nonatomic) id<Swiped> delegate;

@property (strong,nonatomic) NSNumber *currentIntro;


@property (strong, nonatomic) UIButton *fw;
@property (strong, nonatomic) UIButton *sw;
@property (strong, nonatomic) UIButton *tw;

@end
