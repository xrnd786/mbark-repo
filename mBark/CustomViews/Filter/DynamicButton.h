//
//  UIButton+DynamicButton.h
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import <UIKit/UIKit.h>
#import  "RNThemeUpdateProtocol.h"


@interface DynamicButton :UIButton<RNThemeUpdateProtocol>


@property (nonatomic, strong) NSString *backgroundColorKey;
@property (nonatomic, strong) NSString *fontKey;
@property (nonatomic, strong) NSString *textColorKey;
@property (nonatomic, strong) NSString *highlightedTextColorKey;




-(void)buttonWithImageLeftIcon:(UIImage*)icon andTitle:(NSString*)title;

-(void)buttonWithImageRightIcon:(UIImage*)icon andTitle:(NSString*)title;

-(void)buttonWithOnlyImageIcon;

@end
