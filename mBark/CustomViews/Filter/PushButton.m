//
//  PushButton.m
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import "PushButton.h"

#import "ThemeManager.h"
#import "UIImage+Color.h"

@implementation PushButton




- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self _init];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self _init];
    }
    return self;
}

- (id)init {
    if (self = [super init]) {
        [self _init];
    }
    return self;
}



- (void)_init {
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeDidChangeNotification:) name:ThemeManagerDidChangeThemes object:nil];
    
    [self addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    

}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyTheme];
}

- (void)dealloc {
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    @catch (NSException *exception) {
        // do nothing, only unregistering self from notifications
    }
}


-(void)setBaseStyle{
    
    self.layer.borderWidth = 0.1;
    self.layer.borderColor = (__bridge CGColorRef)([UIColor colorWithRed:220.0f green:220.0f blue:220.0f alpha:0.2f]);
    
    self.layer.cornerRadius = 4.0;
    self.layer.masksToBounds = YES;
    [self setClipsToBounds:false];
    
    [self setAdjustsImageWhenHighlighted:NO];
   
    
    
}

-(void)buttonWithImage:(UIImage*)icon andSelectedImage:(UIImage*)selectedIcon{
    
    [self applyTheme];
    
}



-(UIColor*)getCurrentTextColor{
    
    UIColor *textColor = nil;
    if (self.textColorKey && (textColor = [[ThemeManager sharedManager] colorForKey:self.textColorKey])) {
        return textColor;
    }
    
    return nil;
}



-(UIColor*)getCurrentBackgroundColor{
    
    UIColor *backgroundColor = nil;
    if (self.backgroundColorKey && (backgroundColor = [[ThemeManager sharedManager] colorForKey:self.backgroundColorKey])) {
        return backgroundColor;
    }
    
    return nil;
}


-(UIImage*)getCurrentImage{
    
    UIImage *image = nil;
    if (self.backgroundImageKey && (image = [[ThemeManager sharedManager] imageForKey:self.backgroundImageKey])) {
        return image;
    }
    
    return nil;
}

-(UIImage*)getCurrentHighlightedImage{
    
    UIImage *image = nil;
    if (self.highlightedImageKey && (image = [[ThemeManager sharedManager] imageForKey:self.highlightedImageKey])) {
        return image;
    }
    
    return nil;
}


- (void)applyTheme {
    
    
    UIColor *textColor = [self getCurrentTextColor];
    
    
//    if (textColor!=nil) {
//        
//        [self setTitleColor:textColor forState:UIControlStateNormal];
//        
//    }
    
    UIColor *backgroundColor = [self getCurrentBackgroundColor];
    
    if (backgroundColor!=nil) {
        
        self.backgroundColor = backgroundColor;
        
    }
    
//    UIColor *selectedTextColor = [self getCurrentHighlightedTextColor];
//    if (selectedTextColor!=nil) {
//        [self setTitleColor:selectedTextColor forState:UIControlStateHighlighted];
//    }
    
    UIImage *image = [self getCurrentImage];
    if (image!=nil) {
        
        if (textColor) {
            
            [self setImage:[image imageWithTint:textColor] forState:UIControlStateNormal];
 
        } else {
            
            [self setImage:image forState:UIControlStateNormal];
            
        }
        
        
    }
    
    UIImage *imageSelected = [self getCurrentHighlightedImage];
    if (imageSelected!=nil) {
        
        if (textColor) {
            
            [self setImage:[imageSelected imageWithTint:textColor] forState:UIControlStateSelected];
            
        } else {
            
            [self setImage:imageSelected forState:UIControlStateSelected];
            
        }
        
    }
    
}

- (void)themeDidChangeNotification:(NSNotification *)notification {
    [self applyTheme];
}



-(void)buttonPushed:(id)sender{
 
    UIButton *button=sender;
    
    if (![button isSelected]) {
        
        [button setSelected:YES];
        
    }else{
        
        
        [button setSelected:NO];
    }
    
}


@end
