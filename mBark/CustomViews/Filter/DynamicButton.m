//
//  UIButton+DynamicButton.m
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import "DynamicButton.h"

#import "ThemeManager.h"
#import "UIImage+Color.h"


@implementation DynamicButton : UIButton


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self _init];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self _init];
    }
    return self;
}

- (id)init {
    if (self = [super init]) {
        [self _init];
    }
    return self;
}



- (void)_init {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeDidChangeNotification:) name:ThemeManagerDidChangeThemes object:nil];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyTheme];
}

- (void)dealloc {
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    @catch (NSException *exception) {
        // do nothing, only unregistering self from notifications
    }
}


-(void)setBaseStyle{
 
    self.layer.borderWidth = 0.1;
    //self.layer.cornerRadius = 4.0;
    self.layer.masksToBounds = YES;
    [self setClipsToBounds:false];
    
    [self setAdjustsImageWhenHighlighted:NO];
    [self setTitleColor:[self getCurrentTextColor] forState:UIControlStateNormal];
    self.titleLabel.font = [self getCurrentFont];
    
    self.titleLabel.adjustsFontSizeToFitWidth = TRUE;

    [self setTitleEdgeInsets:UIEdgeInsetsMake(1.0f, 3.0f, 1.0f, 1.0f)];
    [self setImageEdgeInsets:UIEdgeInsetsMake(1.0f, 0.0f, 1.0f, 1.0f)];
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [self setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    
    
}

-(void)buttonWithImageLeftIcon:(UIImage*)icon andTitle:(NSString*)title{
   
    [self setBaseStyle];
    
    [self setTitle:[NSString stringWithFormat:@"%@", title] forState:UIControlStateNormal];
    
    [self setImage:[icon imageWithTint:[self getCurrentTextColor]] forState:UIControlStateNormal];
    
}

-(void)buttonWithImageRightIcon:(UIImage*)icon andTitle:(NSString*)title{
 
    
    
}

-(void)buttonWithOnlyImageIcon{
    
}


-(UIFont*)getCurrentFont{
    
    UIFont *font = nil;
    if (self.fontKey && (font = [[ThemeManager sharedManager] fontForKey:self.fontKey])) {
        return  font;
    }
    
    return nil;
    
}


-(UIColor*)getCurrentTextColor{
    
    UIColor *textColor = nil;
    if (self.textColorKey && (textColor = [[ThemeManager sharedManager] colorForKey:self.textColorKey])) {
        return textColor;
    }

    return nil;
}



-(UIColor*)getCurrentBackgroundColor{
    
    UIColor *backgroundColor = nil;
    if (self.backgroundColorKey && (backgroundColor = [[ThemeManager sharedManager] colorForKey:self.backgroundColorKey])) {
        return backgroundColor;
    }
    
    return nil;
}

-(UIColor*)getCurrentHighlightedTextColor{
    
    UIColor *selectedTextColor = nil;
    if (self.highlightedTextColorKey && (selectedTextColor = [[ThemeManager sharedManager] colorForKey:self.highlightedTextColorKey])) {
        return selectedTextColor;
    }
    
    return nil;
}


- (void)applyTheme {
   
    UIFont *font=[self getCurrentFont];
    
    if (font!=nil) {
   
        self.titleLabel.font = font;
        
    }
    
    UIColor *textColor = [self getCurrentTextColor];
    
    if (textColor!=nil) {
       
        [self setTitleColor:textColor forState:UIControlStateNormal];
    
    }
    
    UIColor *backgroundColor = [self getCurrentBackgroundColor];
    
    if (backgroundColor!=nil) {
       
        self.backgroundColor = backgroundColor;
    
    }
    
    UIColor *selectedTextColor = [self getCurrentHighlightedTextColor];
    if (selectedTextColor!=nil) {
        [self setTitleColor:selectedTextColor forState:UIControlStateHighlighted];
    }
    
}

- (void)themeDidChangeNotification:(NSNotification *)notification {
    [self applyTheme];
}


@end
