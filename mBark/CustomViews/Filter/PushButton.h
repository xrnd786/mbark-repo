//
//  PushButton.h
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeUpdateProtocol.h"

@protocol ButtonPushedProtocol <NSObject>

-(void)buttonPushed:(id)sender;

@end

@interface PushButton : UIButton<RNThemeUpdateProtocol>



@property (nonatomic, strong) NSString *backgroundColorKey;
@property (nonatomic, strong) NSString *textColorKey;
@property (nonatomic, strong) NSString *backgroundImageKey;
@property (nonatomic, strong) NSString *highlightedImageKey;

-(void)buttonWithImage:(UIImage*)icon andSelectedImage:(UIImage*)selectedIcon;



@end
