//
//  ACPScrollContainer.h
//  ACPScrollMenu
//
//  Created by Antonio Casero Palmero on 8/4/13.
//  Copyright (c) 2013 ACP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACPItemFull.h"


@class ACPFullScrollMenu;

@protocol ACPFullScrollDelegate <NSObject>

/**
 	Delegate method, executed when the user press any item
    Optional because you can use blocks instead.
 
    @see initACPItem:iconImage:label:andAction
 
 	@param	menu	ACPScrollMenu component
 	@param	selectedIndex	index of the item selected by the user
*/

@optional

- (void)scrollMenu:(ACPFullScrollMenu *)menu didSelectIndex:(NSInteger)selectedIndex;

@end

@interface ACPFullScrollMenu : UIView <ACPItemFullDelegate, UIScrollViewDelegate>

//Type of animations
typedef enum {
	ACPFullFadeZoomIn,
	ACPFullFadeZoomOut,
	ACPFullShake,
	ACPFullClassicAnimation,
	ACPFullZoomOut
} ACPFullAnimation;


@property (nonatomic, weak) id <ACPFullScrollDelegate> delegate;

@property (nonatomic, strong) NSArray *menuArray;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) ACPFullAnimation animationType;

/**
 	Set up the menu programatically, if you are using storyboard, you dont need to use it.
 
 	@param	frame	frame of the container
 	@param	bgColor	background color of the container
 	@param	menuItems	Array with the ACPItems
 
 	@return	The container with the items in order
*/
- (id)initACPScrollMenuWithFrame:(CGRect)frame withBackgroundColor:(UIColor *)bgColor menuItems:(NSArray *)menuItems;


/**
 	The same as the method before, but we should have defined the component in the storyboard or xib file.
 
 	@param	frame	frame of the container
 	@param	bgColor	background color of the container
 	@param	menuItems	Array with the ACPItems
 
 	@return	The container with the items in order
*/
- (void)setUpACPScrollMenu:(NSArray *)menuItems;

/**
 	If you want an item selected by default
 
 	@param	itemNumber	This number represent the item in itemsArray
*/
- (void)setThisItemHighlighted:(NSInteger)itemNumber;

/**
 	Change the background color of this view.
 
 	@param	color	your favorite color.
*/
- (void)setACPBackgroundColor:(UIColor *)color;

@end
