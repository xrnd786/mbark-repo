//
//  ACPItem.h
//  ACPScrollMenu
//
//  Created by Antonio Casero Palmero on 8/4/13.
//  Copyright (c) 2013 ACP. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ACPItemLast;

@protocol ACPItemLastDelegate <NSObject>

- (void)itemTouchesBegan:(ACPItemLast *)item;
- (void)itemTouchesEnd:(ACPItemLast *)item;

@end

@interface ACPItemLast : UIView

typedef void(^actionBlockLast)(ACPItemLast *item);

typedef enum {
    
	ACPImageItemLast,
	ACPLabelItemLast,
	ACPImageAndLabeItemLast
    
} ACPItemTypeLast;

@property (nonatomic, weak) id <ACPItemLastDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *labelItem;

@property (nonatomic, copy) actionBlockLast block;
@property (assign, nonatomic) BOOL highlighted;


/**
 	Initialization of the item inside of the scrollview - using delegate methods
 
 	@param	backgroundImage	The background image
 	@param	iconImage	Icon of the item. Could be nil if you dont want an icon
 	@param	labelItem	Text of the item. Could be nil if you only want the icon
 
 	@return	Item
 */


- (id)initACPItemAndLabel:(NSString *)labelItem;


/**
 *  Initialization of the item inside of the scrollview - using blocks
 *
 *  @param backgroundImage The background image
 *  @param iconImage       Icon of the item. Could be nil if you dont want an icon
 *  @param labelItem       Text of the item. Could be nil if you only want the icon
 *  @param block           Block Action
 *
 *  @return Item
 */


- (id)initACPItemLabel:(NSString *)labelItem andAction:(actionBlockLast)block;

/**
  Implement this method if you want a custom highlighted state for you item
 */

- (void)setHighlightedBackgroundTextColorHighlighted:(UIColor *)texColorHighlighted;

@end
