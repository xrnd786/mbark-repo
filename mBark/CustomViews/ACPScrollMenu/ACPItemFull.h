//
//  ACPItem.h
//  ACPScrollMenu
//
//  Created by Antonio Casero Palmero on 8/4/13.
//  Copyright (c) 2013 ACP. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ACPItemFull;

@protocol ACPItemFullDelegate <NSObject>

- (void)itemTouchesBegan:(ACPItemFull *)item;
- (void)itemTouchesEnd:(ACPItemFull *)item;

@end

@interface ACPItemFull : UIView

typedef void(^actionBlockFull)(ACPItemFull *item);

typedef enum {
    
	ACPFullImageItem,
	ACPFullLabelItem,
	ACPFullImageAndLabeItem
    
} ACPItemFullType;

@property (nonatomic, weak) id <ACPItemFullDelegate> delegate;
@property (strong, nonatomic) UIImageView *iconImageNew;
@property (strong, nonatomic)  NSString *iconImageName;
@property (strong, nonatomic) IBOutlet UILabel *labelItem;

@property (nonatomic, copy) actionBlockFull block;
@property (assign, nonatomic) BOOL highlighted;



/**
 *  Initialization of the item inside of the scrollview - using blocks
 *
 *  @param backgroundImage The background image
 *  @param iconImage       Icon of the item. Could be nil if you dont want an icon
 *  @param labelItem       Text of the item. Could be nil if you only want the icon
 *  @param block           Block Action
 *
 *  @return Item
 */
- (id)initACPItem:(UIImage *)backgroundImage iconImage:(NSString *)iconImage label:(NSString *)labelItem andAction:(actionBlockFull)block;

/**
  Implement this method if you want a custom highlighted state for you item
 */

- (void)setHighlightedBackground:(UIImage *)backgroundImageHightlighted iconHighlighted:(UIImage *)iconImageHighlighted textColorHighlighted:(UIColor *)texColorHighlighted;

@end
