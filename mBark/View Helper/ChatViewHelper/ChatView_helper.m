//
//  ChatView_helper.m
//  mBark
//
//  Created by Xiphi Tech on 25/11/2014.
//
//

#import "ChatView_helper.h"
#import "ContentManager.h"
#import "Messages.h"
#import "Message.h"
#import "MessagesCall_Response.h"

@interface ChatView_helper ()

@property (strong, nonatomic) NSMutableArray *dataSource;

@property (strong, nonatomic) UIImage *myImage;
@property (strong, nonatomic) UIImage *partnerImage;
@property (strong, nonatomic) NSMutableArray *chatArray;


@end

@implementation ChatView_helper

@synthesize baseView=_baseView;
@synthesize controller=_controller;
@synthesize myImage,partnerImage,dataSource,chatArray;

-(id)initWithBaseView:(UIView*)baseView andMainController:(SOMessagingViewController*)controller withThreadObj:(Messages*)threadObj{
   
    self = [super init];
    
    if (self) {
        
        [super setup:baseView];
        
        
        NSString *imagePath =[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.png",Local_User_Name]];
        self.myImage=[UIImage imageWithContentsOfFile:imagePath]!=nil?[UIImage imageWithContentsOfFile:imagePath]:[UIImage imageNamed:@"me.png"];
        
        imagePath =[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.png",Local_Logo_Name]];
        self.partnerImage=[UIImage imageWithContentsOfFile:imagePath]!=nil?[UIImage imageWithContentsOfFile:imagePath]:[UIImage imageNamed:@"username.png"];
       
        
       
        
        self.threadMain=threadObj;
        self.chatArray =[[NSMutableArray alloc]initWithArray:[threadObj.messagesDetails allObjects]];
        self.dataSource =(NSMutableArray*)[self getChat:self.chatArray];
        
        for (Message *entity in self.dataSource) {
            
            NSLog(@"Chat value%@",entity.text);
           
        }
        
        _controller=controller;
        _controller.delegate=self;
        
        
    }
    
    return self;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];

}

-(void)loadData{
 
    self.chatArray=[[NSMutableArray alloc]initWithArray:[self.threadMain.messagesDetails allObjects]];
    self.dataSource =(NSMutableArray*) [self getChat:self.chatArray];
    [self refreshMessages];
    
}


#pragma mark - data

-(NSArray*)getChat:(NSArray*)data{
    
    NSMutableArray *result = [NSMutableArray new];
   
    for (MessagesDetails *messageObj in data) {
        
        Message *message = [[Message alloc] init];
        message.fromMe = [messageObj.direction isEqualToString:Message_Direction_FromAppUser]?TRUE:FALSE;
        message.text = messageObj.body;
        message.type = SOMessageTypeText;
        NSDate * date = messageObj.createdOn;
        
        message.date=date;
        
        int index = (int)[data indexOfObject:messageObj];
        if (index > 0) {
            Message *prevMesage = result.lastObject;
            message.date = [NSDate dateWithTimeInterval:((index % 2) ? 2 * 24 * 60 * 60 : 120) sinceDate:prevMesage.date];
        }
        
        message.attributes=[[NSDictionary alloc]initWithObjectsAndKeys:[[ThemeManager sharedManager]fontForKey:@"generalFont"],NSFontAttributeName,[[ThemeManager sharedManager]colorForKey:@"secondaryColor"],NSForegroundColorAttributeName, nil];
        
        
        [result addObject:message];
    
    }
    
    return result;

}

#pragma mark - SOMessaging data source
- (NSMutableArray *)messages
{
    return self.dataSource;
}

- (NSTimeInterval)intervalForMessagesGrouping
{
    // Return 0 for disableing grouping
    return 2 * 24 * 3600;
}

- (void)configureMessageCell:(SOMessageCell *)cell forMessageAtIndex:(NSInteger)index
{
    Message *message = self.dataSource[index];
    
    // Adjusting content for 3pt. (In this demo the width of bubble's tail is 3pt)
    if (!message.fromMe) {
        cell.contentInsets = UIEdgeInsetsMake(0, 3.0f, 0, 0); //Move content for 3 pt. to right
        cell.textView.textColor = [UIColor blackColor];
    } else {
        cell.contentInsets = UIEdgeInsetsMake(0, 0, 0, 3.0f); //Move content for 3 pt. to left
        cell.textView.textColor = [UIColor whiteColor];
    }
    
    cell.userImageView.layer.cornerRadius = self.userImageSize.width/2;
    
    // Fix user image position on top or bottom.
    cell.userImageView.autoresizingMask = message.fromMe ? UIViewAutoresizingFlexibleTopMargin : UIViewAutoresizingFlexibleBottomMargin;
    
    // Setting user images
    cell.userImage = message.fromMe ? self.myImage : self.partnerImage;
}

- (CGFloat)messageMaxWidth
{
    return 140;
}

- (CGSize)userImageSize
{
    return CGSizeMake(40, 40);
}

- (CGFloat)messageMinHeight
{
    return 0;
}
#pragma mark - SOMessaging delegate


- (void)didSelectMedia:(NSData *)media inMessageCell:(SOMessageCell *)cell
{
    // Show selected media in fullscreen
    [super didSelectMedia:media inMessageCell:cell];
}

- (void)messageInputView:(SOMessageInputView *)inputView didSendMessage:(NSString *)message
{
    if (![[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        return;
    }
    
    Message *msg = [[Message alloc] init];
    msg.text = message;
    msg.fromMe = YES;
    
    [self sendMessage:msg];
    
   NSEntityDescription *entity = [NSEntityDescription entityForName:@"MessagesDetails" inManagedObjectContext:[GlobalCall getContext]];
       
                                 
    MessagesDetails *messageDetails=[[MessagesDetails alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
    messageDetails.body=message;
    messageDetails.direction=Message_Direction_FromAppUser;
    messageDetails.status=@"false";
    
    NSDateFormatter *dateFormat=[NSDateFormatter new];
    [dateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss ZZZZ"];
    
    NSDate *date=[dateFormat dateFromString:[NSString stringWithFormat:@"%@",[NSDate date]]];
    
    NSLog(@"Date here %@",date);
    
    messageDetails.createdOn=date;
    
    [self.chatArray addObject:entity];
    [self performSelectorInBackground:@selector(sendMessageToServer:) withObject:messageDetails];
    
}

-(void)sendMessageWithBody:(NSString *)body{
    
}

-(void)sendMessageToServer:(MessagesDetails*)details{
    
    [MessagesCall_Response callApiForMessageAddition:details threadID:self.threadMain.m_id];
    
}


-(void)DoDismissTask{
    
    [self performSelectorInBackground:@selector(removeNotificationsInBackground) withObject:nil];
    
}

-(void)removeNotificationsInBackground{
    
    [self.inputView removeNotifications];
    
}

- (void)messageInputViewDidSelectMediaButton:(SOMessageInputView *)inputView
{

}

@end
