//
//  ChatView_helper.h
//  mBark
//
//  Created by Xiphi Tech on 25/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "SOMessagingViewController.h"
#import "Messages.h"
@interface ChatView_helper : SOMessagingViewController<MessageSentFromMe>

@property (strong,nonatomic) UIView *baseView;
@property (strong,nonatomic) SOMessagingViewController *controller;


-(id)initWithBaseView:(UIView*)baseView andMainController:(SOMessagingViewController*)controller withThreadObj:(Messages*)threadObj;
@property (strong, nonatomic) Messages *threadMain;


-(void)DoDismissTask;
-(void)loadData;


@end
