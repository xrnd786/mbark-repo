//
//  MasterTab_helper.h
//  mBark
//
//  Created by Xiphi Tech on 21/04/2015.
//
//

#import <Foundation/Foundation.h>

@interface MasterTab_helper : NSObject

-(id)initWithMyTabBar:(UITabBar*)tabBar;

@end
