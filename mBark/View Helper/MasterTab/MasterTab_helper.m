//
//  MasterTab_helper.m
//  mBark
//
//  Created by Xiphi Tech on 21/04/2015.
//
//

#import "MasterTab_helper.h"
#import "UIImage+Color.h"
#import "AppUIDefaults.h"
#import "ThemeManager.h"

@implementation MasterTab_helper

-(id)initWithMyTabBar:(UITabBar*)tabBar{
    
    self=[super init];
    if (self) {
        
        [self customizeNow:tabBar];
        
    }
    
    return self;
    
}


-(void)customizeNow:(UITabBar*)tabBar{
    
    if (![[AppUIDefaults getPrimaryColor] isEqualToString:@""]) {
        
        
        
        UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
        UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
        UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
        UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
        
        tabBarItem1.title = @"Home";
        tabBarItem2.title = @"Search";
        tabBarItem3.title = @"Stores";
        tabBarItem4.title = @"Cart";
        
        UIImage *home=[UIImage imageNamed:@"home.png"];
        UIImage *search=[UIImage imageNamed:@"search.png"];
        UIImage *store=[UIImage imageNamed:@"store.png"];
        UIImage *cart=[UIImage imageNamed:@"myBag.png"];
        
        NSLog(@"Primary Color string is %@",[AppUIDefaults getPrimaryColor]);
        NSLog(@"Secondary Color string is %@",[AppUIDefaults getSecondaryColor]);
        
        
        UIColor *primary=[[ThemeManager sharedManager]colorForKey:[AppUIDefaults getPrimaryColor]];
        UIColor *secondary=[[ThemeManager sharedManager]colorForKey:[AppUIDefaults getSecondaryColor]];
        
        
        [tabBarItem1 setFinishedSelectedImage:[home imageWithTint:primary] withFinishedUnselectedImage:[home imageWithTint:secondary]];
        
        
        [tabBarItem2 setFinishedSelectedImage:[search imageWithTint:[[ThemeManager sharedManager]colorForKey:[AppUIDefaults getPrimaryColor]]] withFinishedUnselectedImage:[search imageWithTint:[[ThemeManager sharedManager]colorForKey:[AppUIDefaults getSecondaryColor]]]];
        
        
        [tabBarItem3 setFinishedSelectedImage:[store imageWithTint:primary] withFinishedUnselectedImage:[store imageWithTint:secondary]];
        
        
        [tabBarItem4 setFinishedSelectedImage:[cart imageWithTint:[[ThemeManager sharedManager]colorForKey:[AppUIDefaults getPrimaryColor]]] withFinishedUnselectedImage:[cart imageWithTint:[[ThemeManager sharedManager]colorForKey:[AppUIDefaults getSecondaryColor]]]];
        
        // Change the title color of tab bar items
       /* [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[[ThemeManager sharedManager]colorForKey:[AppUIDefaults getSecondaryColor]], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
       
        UIColor *titleHighlightedColor = [[ThemeManager sharedManager]colorForKey:[AppUIDefaults getPrimaryColor]];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: titleHighlightedColor, UITextAttributeTextColor,nil] forState:UIControlStateHighlighted];
       */
        
    }
}

@end
