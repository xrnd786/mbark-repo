//
//  PopularSearch_helper.m
//  mBark
//
//  Created by Xiphi Tech on 28/04/2015.
//
//

#import "PopularSearch_helper.h"

#import "PopularSearchViewCell.h"

@interface PopularSearch_helper()<UITableViewDataSource,UITableViewDelegate>

@end


@implementation PopularSearch_helper

@synthesize localCopyOfController,localCopyOfTable,searchList,delegate;

-(id)initWithTable:(UITableView*)tableView andWithController:(UIViewController*)controller{
    
    self=[super init];
    if (self) {
        
        tableView.delegate=self;
        tableView.dataSource=self;
        tableView.separatorStyle=UITableViewCellSelectionStyleNone;
        
        
        localCopyOfTable=[UITableView new];
        localCopyOfTable=tableView;
        
        
    }
    
    return self;
}

#pragma mark - refresh fucntion

-(void)refreshTableWithNewData{

    [localCopyOfTable reloadData];
    
}


#pragma mark- table Data Functions

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 45.0f;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([searchList count]>10) {
        
        return 10;
        
    }else{
        
       return [searchList count];
    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSString* cellIdentifier;
    
    if ([GlobalCall isIpad]) {
        
        cellIdentifier=@"PopularSearchViewCell";
        
    } else {
        
        cellIdentifier=@"PopularSearchViewCell";
        
    }
    
    PopularSearchViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (PopularSearchViewCell*) [nib objectAtIndex:0];
        assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
        
    }
    
    cell.popSearchText.text=[searchList objectAtIndex:indexPath.row];
    cell.popSearchText.tag=indexPath.row;
    cell.popSearchText.userInteractionEnabled=true;
    cell.userInteractionEnabled=true;
    
    cell.popSearchText.highlightedTextColor=AppBarColor;
    
    [cell.popSearchText addGestureRecognizer:[[UIGestureRecognizer alloc]initWithTarget:self action:@selector(searchClicked:)]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [delegate didSelectRowAtIndex:indexPath.row];
    
}


#pragma mark - button events

-(void)searchClicked:(RNThemeLabel*)button{
    
    [delegate didSelectRowAtIndex:button.tag];
   
}



@end
