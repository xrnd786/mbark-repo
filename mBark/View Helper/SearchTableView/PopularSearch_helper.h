//
//  PopularSearch_helper.h
//  mBark
//
//  Created by Xiphi Tech on 28/04/2015.
//
//

#import <Foundation/Foundation.h>

@protocol PopularSearch_helperDelegate <NSObject>

-(void)didSelectRowAtIndex:(int)indexValue;


@end


@interface PopularSearch_helper : NSObject

@property (strong,nonatomic) id<PopularSearch_helperDelegate> delegate;

@property (strong,nonatomic) NSMutableArray *searchList;
@property (strong,nonatomic) UIViewController *localCopyOfController;
@property (strong,nonatomic) UITableView *localCopyOfTable;


-(id)initWithTable:(UITableView*)tableView andWithController:(UIViewController*)controller;
-(void)refreshTableWithNewData;


@end
