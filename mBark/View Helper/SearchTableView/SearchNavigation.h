//
//  HomeNavigation.h
//  P1
//
//  Created by Xiphi Tech on 09/10/2014.
//
//

#import <UIKit/UIKit.h>



@interface SearchNavigation : UIView


@property (nonatomic, retain) UIViewController *baseView;

+ (id)sharedManager:(UIViewController*)myView;


//PUBLIC (AVAILABLE TO HOME)
-(void)tableRecentCell:(NSIndexPath*)indexPath;
-(void)openProductListView;
-(void)openCategoryList;
-(void)openApplicationList;
-(void)openPopularList;

@end
