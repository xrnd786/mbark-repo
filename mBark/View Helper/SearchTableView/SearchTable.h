//
//  SearchTable.h
//  P1
//
//  Created by Xiphi Tech on 07/10/2014.
//
//

#import <UIKit/UIKit.h>

@protocol searchRowSelected <NSObject>

-(void)rowSelected:(int)value;
-(void)searchRowSelected:(NSString*)value;

@end


@interface SearchTable : UIView<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIViewController *localMainViewCopy;
@property (strong,nonatomic) NSNumber *isSearch;

@property (strong,nonatomic) id<searchRowSelected> delegate;
@property (nonatomic) NSMutableArray *lastSearched;



-(id)initWithTable:(UITableView*)table andMainView:(UIViewController*)controller;

-(void)toggleMode;
-(void)updateSearchTableWithContent:(NSArray*)listOfPrompts withSearchText:(NSString*)searchText;


@end
