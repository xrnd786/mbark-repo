//
//  PopularSearchViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 28/04/2015.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"

@interface PopularSearchViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet RNThemeLabel *popSearchText;


@end
