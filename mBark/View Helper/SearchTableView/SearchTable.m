//
//  SearchTable.m
//  P1
//
//  Created by Xiphi Tech on 07/10/2014.
//
//

#import "SearchTable.h"


//*****Custom Cell*****//
#import "SearchTableCell.h"
#import "SearchValuesViewCell.h"


//*****THEME CONTROLS*****//
#import "RNThemeImageButton.h"
#import "Search.h"
#import "AppSettingDefaults.h"
#import "lastSearchedValueCell.h"
#import "LastSearch.h"


@interface SearchTable ()

@property (nonatomic) NSMutableArray *searchResults;

@end

@implementation SearchTable

@synthesize delegate,localTableCopy,searchResults,localMainViewCopy,isSearch,lastSearched;


-(id)initWithTable:(UITableView*)table andMainView:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        table.delegate=self;
        table.dataSource=self;
        table.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        
        //Initialization code
        localTableCopy=table;
        localMainViewCopy=controller;
        
        isSearch=[NSNumber numberWithBool:FALSE];
        searchResults=[[NSMutableArray alloc]init];
        
        self.lastSearched=[[NSMutableArray alloc]initWithArray: [[GlobalCall getDelegate]fetchStatementWithEntity:[LastSearch class] withSortDescriptor:@"searchedOn" withPredicate:nil]];
        
    }
    
    return self;
    
}



#pragma mark - table View Functions


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [((Search*)(self.localMainViewCopy)).mySearchBar resignFirstResponder];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([GlobalCall isIpad]) {
        
        return 100;
        
    } else {
       
        return 50;
        
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    
    
    if ([isSearch boolValue]) {
        
        UIView *baseView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,tableView.frame.size.width, 20)];
        
        baseView.backgroundColor=AppBarColor;
        
        RNThemeLabel *titleLabel=[[RNThemeLabel alloc]initWithFrame:CGRectMake(5, 0, tableView.frame.size.width, 20)];
        
        titleLabel.fontKey=@"smallFont";
        titleLabel.textColorKey=@"secondaryColor";
        [titleLabel applyTheme];
        titleLabel.text=@"Suggested product(s)";
       
        [baseView addSubview:titleLabel];
        return baseView;
        
        
    }else{
        
        UIView *baseView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,tableView.frame.size.width, 20)];
        
        baseView.backgroundColor=AppBarColor;
        
        RNThemeLabel *titleLabel=[[RNThemeLabel alloc]initWithFrame:CGRectMake(5, 0, tableView.frame.size.width, 20)];
        
        titleLabel.fontKey=@"generalFont";
        titleLabel.textColorKey=@"secondaryColor";
        [titleLabel applyTheme];
        
        
        if (section==0) {
            
            titleLabel.text=@"Browse By";
            [baseView addSubview:titleLabel];
            
        }else if (section==1) {
            
            titleLabel.text=@"Recent Search";
            
            RNThemeImageButton *clearHistory;
            clearHistory=[RNThemeImageButton buttonWithType:UIButtonTypeCustom];
            [clearHistory setFrame:CGRectMake(tableView.frame.size.width-25, 0, 20, 20)];
            clearHistory.highlightedColorKey=@"secondaryColor";
            clearHistory.backgroundColorKey=@"secondaryColor";
            clearHistory.backgroundImageKey=@"clearHistory";
            
            [clearHistory addTarget:self action:@selector(clearRecentHistory:) forControlEvents:UIControlEventTouchUpInside];
            [clearHistory applyTheme];
            
            [baseView addSubview:titleLabel];
            [baseView addSubview:clearHistory];
            
            baseView.userInteractionEnabled=TRUE;
            
        }
        
        return baseView;

        
    }

    
}


#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([isSearch boolValue]) {
        
        return 1;
        
    }else{
        
        return 2;
        
    }
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([isSearch boolValue]) {
        
        return [searchResults count];
        
    }else{
        
        int rows=-1;
        
        switch (section) {
            case 0:
                
                rows = 3;
                
                break;
            case 1:
                
                rows=(int)[self.lastSearched count];
                break;
            default:
                
                rows=0;
                return rows;
                break;
        
        }
        
        return rows;
    
    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([isSearch boolValue]) {
        
        NSString* cellIdentifier;
        
        cellIdentifier=@"SearchValuesViewCell";
            
        SearchValuesViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell)
        {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell = (SearchValuesViewCell*) [nib objectAtIndex:0];
            assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
            
        }
        
        cell.searchValueTitle.text=[searchResults objectAtIndex:indexPath.row];
        return cell;
        
    } else {
        
        NSLog(@"Index path Section :%ld",(long)indexPath.section);
        NSLog(@"Index path Row :%ld",(long)indexPath.row);
        
        if (indexPath.section==0) {
            
            
            switch (indexPath.row) {
                    
                case 0:
                    
                    return [self getCellForCategorySearch:tableView :indexPath];
                    break;
                    
                case 1:
                    
                    return [self getCellForPopularSearches:tableView :indexPath];
                    break;
                    
                case 2:
                    
                    return [self getCellForApplication:tableView :indexPath];
                    break;
                    
                default:
                    
                    
                    break;
                    
            }
            
        }else if (indexPath.section==1){
            
             return [self getCellForLastSearchedText:tableView :indexPath];
        }
        
        
   
    }
    
    return nil;
    

}


-(lastSearchedValueCell*)getCellForLastSearchedText:(UITableView*)tableView :(NSIndexPath*)indexPath{
    
    
    NSString* cellIdentifier;
    cellIdentifier=@"lastSearchedValueCell";
  
    
    lastSearchedValueCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (lastSearchedValueCell*) [nib objectAtIndex:0];
        assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
        
    }
    
    cell.lastSearchedText.text=((LastSearch*)[lastSearched objectAtIndex:indexPath.row]).name;
    
    
    return cell;
    
}

-(SearchTableCell*)getCellForCategorySearch:(UITableView*)tableView :(NSIndexPath*)indexPath{
    
    NSString* cellIdentifier;
    cellIdentifier=@"SearchTableCell";
    
    SearchTableCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (SearchTableCell*) [nib objectAtIndex:0];
        
        assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
    }
    
    cell.FixedLabel.text=@"Categories";
    cell.fixedIcon.image=[UIImage imageNamed:@"browseByCategory.png"];
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
    
}


-(SearchTableCell*)getCellForPopularSearches:(UITableView*)tableView :(NSIndexPath*)indexPath{
    
    NSString* cellIdentifier;
    cellIdentifier=@"SearchTableCell";
    
    
    SearchTableCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (SearchTableCell*) [nib objectAtIndex:0];
        assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
    }
    
    cell.FixedLabel.text=@"Popular Searches";
    cell.fixedIcon.image=[UIImage imageNamed:@"popularSearch.png"];
        
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
    
}


-(SearchTableCell*)getCellForApplication:(UITableView*)tableView :(NSIndexPath*)indexPath{
    
    NSString* cellIdentifier;
    cellIdentifier=@"SearchTableCell";
        
    
    SearchTableCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (SearchTableCell*) [nib objectAtIndex:0];
        
        assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
    }
    
    
    NSString *applicationName=[AppSettingDefaults getApplicationDisplayName];
    
    if (![applicationName isEqualToString:@""] && applicationName){
        cell.FixedLabel.text=applicationName;
    } else {
        cell.FixedLabel.text=@"Application";
    }
    
    cell.fixedIcon.image=[UIImage imageNamed:@"application.png"];
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    
    return cell;
    
}



#pragma mark - Table Action Funtions

-(void)clearRecentHistory:(id)sender{
    
    if ([self.lastSearched count]>0) {
        
        [((Search*)self.localMainViewCopy).mySearchBar resignFirstResponder];
        
        SCLAlertView *alert=[[SCLAlertView alloc]init];
        [alert addButton:@"Yes" actionBlock:^{
            
            NSMutableArray *arrayToDelete=[[NSMutableArray alloc]init];
            
            NSLog(@"Last searched text count %lu",(unsigned long)[lastSearched count]);
            
            
            for (int i=0;i<[lastSearched count];i++) {
                
                [arrayToDelete addObject:[NSIndexPath indexPathForRow:i inSection:2]];
                
            }
            self.lastSearched =[[NSMutableArray alloc]init];
            [self deleteAllSearch];
            
            [localTableCopy beginUpdates];
            [localTableCopy deleteRowsAtIndexPaths:arrayToDelete withRowAnimation:UITableViewRowAnimationFade];
            [localTableCopy endUpdates];
            
        }];
        
        
        [alert showTitle:self.localMainViewCopy title:@"Clear history" subTitle:@"Do you wish to clear all your previous search?" style:Warning closeButtonTitle:@"No" duration:0.0f];
        
    }
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
   
    return YES;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([isSearch boolValue]) {
        
        NSString *searchFor=[searchResults objectAtIndex:(int)indexPath.row];
        
        [self.delegate searchRowSelected:searchFor];
        [self addSearchText:searchFor];
        
    } else {
        
        if (indexPath.section==0) {
            
            [self.delegate rowSelected:(int)indexPath.row];
            
        }else if (indexPath.section==1){
         
            NSString *searchFor=[self.lastSearched objectAtIndex:indexPath.row];
            [self.delegate searchRowSelected:searchFor];
            [self addSearchText:searchFor];
        }
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    }
    
}



#pragma mark - Update Search

-(void)updateSearchTableWithContent:(NSArray*)listOfPrompts withSearchText:(NSString*)searchText{
    
    if ([searchText isEqualToString:@""]) {
        
        isSearch=[NSNumber numberWithBool:FALSE];
        [self.localTableCopy reloadData];
        
    } else {
        
        if ([isSearch boolValue]) {
            
            
            if ([listOfPrompts count]==0) {
                
                isSearch=[NSNumber numberWithBool:FALSE];
                [self.localTableCopy reloadData];
                
            }else{
                
                self.searchResults=[[NSMutableArray alloc]initWithArray:listOfPrompts];
                [self.localTableCopy reloadData];
                
            }
            
            
        } else {
            
            [self.localTableCopy reloadData];
            
        }
    
    }
    
}



-(void)toggleMode{
    
    isSearch=[NSNumber numberWithBool:![isSearch boolValue]];
    
}


#pragma mark - search text database

-(void)addSearchText:(NSString*)searchText{
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"LastSearch" inManagedObjectContext:[GlobalCall getContext]];
    
    
    LastSearch *search=[[LastSearch alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
    
    
    search.name=searchText;
    search.searchedOn=[NSDate date];
    
    [GlobalCall saveContext];
    
}


-(void)deleteAllSearch{
    
    NSArray *search=[ [GlobalCall getDelegate]fetchStatementWithEntity:[LastSearch class] withSortDescriptor:nil withPredicate:nil];
    
    NSManagedObjectContext *context=[GlobalCall getContext];
    
    for (NSManagedObject *sr in search) {
        
        [context deleteObject:sr];
        
    }
    
    [GlobalCall saveContext];
}

@end
