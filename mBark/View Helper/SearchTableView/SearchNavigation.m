//
//  HomeNavigation.m
//  P1
//
//  Created by Xiphi Tech on 09/10/2014.
//
//

#import "SearchNavigation.h"

//*****PRESENTATION CONTORLLERS*****//
#import "ProductListController.h"
#import "CategoryMainListViewController.h"
#import "PopularSearchViewController.h"

@implementation SearchNavigation


@synthesize baseView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       
        // Initialization code
    
    }
    return self;
}

+ (id)sharedManager:(UIViewController*)myView {
    
    static SearchNavigation *sharedMyManager = nil;
    
    @synchronized(self) {
        
    if (sharedMyManager == nil)
         
        sharedMyManager = [[self alloc] initWithView:myView];
    
    }
    
    return sharedMyManager;
}


- (id)initWithView:(UIViewController*)myView
{
    self = [super init];
    
    if (self) {
        
        self.baseView=myView;
        // Initialization code
        
    }
    
    return self;
    
}

#pragma mark - helper Function

-(void)tableRecentCell:(NSIndexPath*)indexPath{
    
    switch (indexPath.section) {
        case 0:
            
            if (indexPath.row==0) {
                
                [self openProductListView];
                
            } else {
                
            }
            
            break;
        case 1:
            
            break;
            
        default:
            break;
    
    }
    
}


#pragma mark - navigation functions


-(void)openProductListView{
    
    [self.baseView.navigationController pushViewController:[[ProductListController alloc]init] animated:YES];
    
}

-(void)openCategoryList{
    
    [self.baseView.navigationController pushViewController:[[CategoryMainListViewController alloc]init] animated:YES];
    
}


-(void)openApplicationList{
    
    [self.baseView.navigationController pushViewController:[[CategoryMainListViewController alloc]initWithApplicationList] animated:YES];
    
}




-(void)openPopularList{
    
    [self.baseView.navigationController pushViewController:[[PopularSearchViewController alloc]init] animated:YES];
    
}


@end
