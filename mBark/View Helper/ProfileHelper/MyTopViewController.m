//
//  MyViewController.m
//  Sample Storyboard
//
//  Created by Peter Paulis on 1.4.2013.
//  Copyright (c) 2013 Min60 s.r.o. - http://min60.com. All rights reserved.
//

#import "MyTopViewController.h"
#import "M6ParallaxController.h"

#import "UIImageView+Letters.h"
#import "UIImageView+LBBlurredImage.h"

@interface MyTopViewController ()

@property (weak, nonatomic) IBOutlet UIImageView * imageView;
@property (weak, nonatomic) IBOutlet UIImageView *gradientImageView;

@end

@implementation MyTopViewController

@synthesize name,bannerImage,iconImage,profileName,editButton;


-(id)initWithName:(NSString*)nameNew{
    
    self=[super init];
    
    if (self) {
    
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"MyTopViewController" owner:self options:nil] objectAtIndex:0];
            
        self.name =nameNew;
        
        
    }
    
    return self;
    
}

-(void)updateViewWithName:(NSString *)nameNew{
    
    self.name=nameNew;
    [self setImageNow];

}



-(void)setImageNow{
    
    [self.editButton applyTheme];
    [self.profileName applyTheme];
    
    [self.bannerImage  setImageWithString:self.name color:[UIColor grayColor] circular:FALSE];
    [self.bannerImage setImageToBlur:bannerImage.image
                          blurRadius:kLBBlurredImageDefaultBlurRadius
                     completionBlock:^(){
                         NSLog(@"The blurred image has been set");
                     }];
    
    
    [self.iconImage setImageWithString:self.name color:[[ThemeManager sharedManager]colorForKey:@"secondaryColor"] circular:true];
    [self cacheMyImage:self.iconImage.image];
     
    self.profileName.text=self.name;

}

#pragma mark - save image locally

-(void)cacheMyImage:(UIImage*)image{
    
    NSError *error;
    NSString  *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",[Local_User_Name stringByAppendingString:@".png"]]];
    [UIImagePNGRepresentation(image) writeToFile:pngPath atomically:YES];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
    
    
}


////////////////////////////////////////////////////////////////////////
#pragma mark - Public
////////////////////////////////////////////////////////////////////////

- (void)willChangeHeightFromHeight:(CGFloat)oldHeight toHeight:(CGFloat)newHeight {

    M6ParallaxController * parallaxController = [self parallaxController];
    
    if (newHeight >= parallaxController.topViewControllerStandartHeight) {
    
        [self.imageView setAlpha:1];
        
        float r = (parallaxController.topViewControllerStandartHeight * 1.25f) / newHeight;
        
        [self.gradientImageView setAlpha:r*r];
        
    } else {
    
        float r = newHeight / parallaxController.topViewControllerStandartHeight;
        [self.imageView setAlpha:r];
        [self.gradientImageView setAlpha:r*r*r*r];
        
    }

}

- (IBAction)editClicked:(id)sender {
    
    [self.delegate callEdit];
    
}
@end
