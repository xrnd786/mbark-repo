//
//  MyTableViewController.h
//  Sample
//
//  Created by Peter Paulis on 6.4.2013.
//  Copyright (c) 2013 Peter Paulis. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol MyTableNavigateDelegate <NSObject>

-(void)logOut;
-(void)changePassword;


@end


@interface MyTableViewController : UITableViewController


@property (strong,nonatomic) NSMutableArray *arrayOfCaptured;
@property (strong,nonatomic) id<MyTableNavigateDelegate> delegate;

-(id)init;
-(void)reloadMyData;


@end
