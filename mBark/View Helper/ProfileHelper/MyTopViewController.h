//
//  MyViewController.h
//  Sample Storyboard
//
//  Created by Peter Paulis on 1.4.2013.
//  Copyright (c) 2013 Min60 s.r.o. - http://min60.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNThemeLabel.h"
#import "RNThemeButton.h"

@protocol EditCalled <NSObject>

-(void)callEdit;

@end

@interface MyTopViewController : UIViewController


-(id)initWithName:(NSString*)nameNew;
-(void)setImageNow;
- (void)willChangeHeightFromHeight:(CGFloat)oldHeight toHeight:(CGFloat)newHeight;


@property (strong, nonatomic) id<EditCalled> delegate;
@property (strong, nonatomic)  NSString *name;


@property (weak, nonatomic) IBOutlet UIImageView *bannerImage;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet RNThemeButton *editButton;
@property (weak, nonatomic) IBOutlet RNThemeLabel *profileName;

- (IBAction)editClicked:(id)sender;
-(void)updateViewWithName:(NSMutableString *)nameNew;

@end
