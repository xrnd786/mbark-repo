//
//  ProfileCell.h
//  mBark
//
//  Created by Xiphi Tech on 14/04/2015.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeLabel.h"

@interface ProfileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *title;
@property (weak, nonatomic) IBOutlet RNThemeLabel *value;

@end
