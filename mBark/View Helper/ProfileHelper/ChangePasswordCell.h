//
//  LogoutCell.h
//  mBark
//
//  Created by Xiphi Tech on 15/04/2015.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeButton.h"

@interface ChangePasswordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeButton *cpButton;

@end
