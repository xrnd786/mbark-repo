//
//  MyTableViewController.m
//  Sample
//
//  Created by Peter Paulis on 6.4.2013.
//  Copyright (c) 2013 Peter Paulis. All rights reserved.
//

#import "MyTableViewController.h"
#import "M6ParallaxController.h"


#import "AppSettingDefaults.h"
#import "RNThemeLabel.h"
#import "RNThemeButton.h"
#import "ThemeManager.h"

//#import "AppUserDefaults.h"
#import "User.h"
#import "UserLoginCall_Response.h"
#import "UIImageView+Letters.h"

#import "ProfileCell.h"
#import "LogoutCell.h"
#import "ChangePasswordCell.h"

#define placeholder_username @"Username"
#define placeholder_name @"Name"
#define placeholder_firstname @"First Name"
#define placeholder_lastname @"Last Name"
#define placeholder_organization @"Organization"
#define placeholder_email @"Email"
#define placeholder_mobile @"Mobile"
#define placeholder_addressline1 @"Address Line 1"
#define placeholder_addressline2 @"Address Line 2"
#define placeholder_pincode @"Pincode"
#define placeholder_state @"State"
#define placeholder_country @"Contry"
#define placeholder_gender @"Gender"
#define placeholder_password @"Password"



@interface MyTableViewController ()

@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIViewController *localController;

@property (strong,nonatomic) NSMutableArray *arrayOfTitle;



@end

@implementation MyTableViewController

@synthesize delegate=_delegate;

-(id)init{
    
    
    self = [super init];
    if (self) {
        
        self.arrayOfTitle=[[NSMutableArray alloc]init];
        self.arrayOfCaptured=[[NSMutableArray alloc]initWithArray:[self getArrayOffieldsToBeCaptured]];
      
        self.tableView.showsVerticalScrollIndicator=FALSE;
    }
    
    return self;
    
}

-(UIView*)addFooter{
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], [GlobalCall isIpad]?70:60)];
        
    RNThemeButton *logOut=[RNThemeButton buttonWithType:UIButtonTypeCustom];
    
        logOut.frame=view.frame;
        logOut.titleLabel.text=@"Log out";
        logOut.textColorKey=@"lightColor";
        logOut.backgroundColorKey=@"primaryColor";
        logOut.fontKey=@"secondaryFont";
        logOut.highlightedTextColorKey=@"lightColor";
        [logOut applyTheme];
    
    
    [logOut addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:logOut];
    
    return view;
    
    
}


-(void)logout{
    
    User *user=[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
    user.password=nil;
    [GlobalCall saveContext];
    
    User *userNew=[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
    NSLog(@"New object values %@",userNew);
    
    //[AppUserDefaults logoutNow];
    [_delegate logOut];
    
}

-(void)changePassword{
    
    [_delegate changePassword];
    
}

-(void)reloadMyData{
    
    self.arrayOfCaptured=[[NSMutableArray alloc]initWithArray:[self getArrayOffieldsToBeCaptured]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}


#pragma mark - Table View Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3+[self.arrayOfCaptured count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section==0) {
        return 4;
        
    }else {
        
        return 1;
    }
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        
        return 44.0f;
        
    }else{
        
        if ((indexPath.row==[self.arrayOfCaptured count]-1) || (indexPath.row==[self.arrayOfCaptured count]-1)){
            
            return 60.0;
            
        }else{
            
            return 50.0f;
        }
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.0f;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    
    if (indexPath.section==0) {
     
        return [self getBlankCellForTableView:tableView cellForRowAtIndexPath:indexPath];
        
    }else if(indexPath.section==[self.arrayOfCaptured count]+1){
        
        return  [self getChangePasswordCellForTableView:tableView cellForRowAtIndexPath:indexPath];
        
    }else if(indexPath.section==[self.arrayOfCaptured count]+2){
        
       return  [self getLogoutCellForTableView:tableView cellForRowAtIndexPath:indexPath];
       
    }else if (indexPath.section>1 && indexPath.section!=([self.arrayOfCaptured count]+1)){
        
       return  [self getValuesCellForTableView:tableView cellForRowAtIndexPath:indexPath];
        
    }else{

        return [self getBlankCellForTableView:tableView cellForRowAtIndexPath:indexPath];
        

    }
    
    
}


-(UITableViewCell*)getBlankCellForTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellNew=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
        cellNew.textLabel.text =@"";
        cellNew.backgroundColor=[UIColor clearColor];
        return cellNew;
    
}



-(ProfileCell*)getValuesCellForTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellReuseIdentifier   = @"ProfileCell";
    
    ProfileCell *cell = nil;
    
    cell = (ProfileCell*)[tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProfileCell" owner:self options:nil] lastObject];
        
    }
    
    
    cell.value.text=[self.arrayOfCaptured objectAtIndex:indexPath.section-1];
    
    cell.title.text=[self.arrayOfTitle objectAtIndex:indexPath.section-1];
    return cell;

    
}


-(LogoutCell*)getLogoutCellForTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellReuseIdentifier   = @"LogoutCell";
    
    LogoutCell *cell = nil;
    
    cell = (LogoutCell*)[tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LogoutCell" owner:self options:nil] lastObject];
        
    }
    
    [cell.logOutButton addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}



-(ChangePasswordCell*)getChangePasswordCellForTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellReuseIdentifier   = @"ChangePasswordCell";
    
    ChangePasswordCell *cell = nil;
    
    cell = (ChangePasswordCell*)[tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ChangePasswordCell" owner:self options:nil] lastObject];
        
    }
    
    [cell.cpButton addTarget:self action:@selector(changePassword) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}



#pragma mark - data


-(NSMutableArray*)getArrayOffieldsToBeCaptured{
    
    NSMutableArray *arrayToBeCaputured=[[NSMutableArray alloc]init];
    
    User *user=[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
    
    NSLog(@"User vlaue %@",user);
    
    [arrayToBeCaputured addObject:user.username];
    [self.arrayOfTitle addObject:placeholder_username];
    
    
    if ([AppSettingDefaults isFNameCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.firstName]];
        [self.arrayOfTitle addObject:placeholder_firstname];
    
    }
    
    if ([AppSettingDefaults isLNameCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.lastName]];
        [self.arrayOfTitle addObject:placeholder_lastname];
        
    }
    
    
    if ([AppSettingDefaults isGenderCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.gender]];
        [self.arrayOfTitle addObject:placeholder_gender];
    
    }
    
    if ([AppSettingDefaults isMobileCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.contact]];
        [self.arrayOfTitle addObject:placeholder_mobile];
    }
    
    
    if ([AppSettingDefaults isEmailCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.email]];
        [self.arrayOfTitle addObject:placeholder_email];
    }
    
    
    if ([AppSettingDefaults isOrganizationCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.organization]];
        [self.arrayOfTitle addObject:placeholder_organization];
    }
    
    if ([AppSettingDefaults isAddressCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.addressLine1]];
        [self.arrayOfTitle addObject:placeholder_addressline1];
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.addressLine2]];
        [self.arrayOfTitle addObject:placeholder_addressline2];
    }
    
    if ([AppSettingDefaults isPinCodeCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.pincode]];
        [self.arrayOfTitle addObject:placeholder_pincode];
    }
    
    if ([AppSettingDefaults isCountryCaptured]) {
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.state]];
        [self.arrayOfTitle addObject:placeholder_state];
        
        [arrayToBeCaputured addObject:[self ifThisNilReturnEmptyString:user.country]];
        [self.arrayOfTitle addObject:placeholder_country];

    }
    
    return arrayToBeCaputured;
    
}

-(NSString*)ifThisNilReturnEmptyString:(id)value{
    
    return value==nil?@"":[NSString stringWithFormat:@"%@",value];
    
}
//

#pragma mark - Table view delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self.parallaxController tableViewControllerDidScroll:self];
    
}




@end
