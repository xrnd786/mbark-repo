//
//  ProductListViewHelper.h
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import <UIKit/UIKit.h>

@interface ProductListViewHelper : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIViewController *baseView;


-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view andProductArray:(NSArray*)array withController:(UIViewController*)controller;
//-(id)initWithGroupedProductsTable:(UITableView*)table  withMainView:(UIView*)view andProductId:(NSString*)Id  withController:(UIViewController*)controller;
-(id)initWithCartProductsTable:(UITableView*)table  withMainView:(UIView*)view andCartId:(NSString*)Id withController:(UIViewController*)controller;

-(id)initWithApplicationProductsTable:(UITableView*)table  withMainView:(UIView*)view andApplicationId:(NSString*)Id withController:(UIViewController*)controller;

-(id)initWithSearchProductsTable:(UITableView*)table withMainView:(UIView*)view withController:(UIViewController*)controller withSearchText:(NSString*)searchText;
@end
