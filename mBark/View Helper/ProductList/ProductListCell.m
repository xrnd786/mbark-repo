//
//  ProductLitsCell.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "ProductListCell.h"

@implementation ProductListCell

@synthesize productImage,productTitle,mrpPrice,discountedPrice,favButton,delegate,stockDetails,discountValue,dicountView;


- (void)awakeFromNib {
    
    [favButton setBackgroundImage:[UIImage imageNamed:@"toWishlist_selected.png"] forState:UIControlStateSelected];
    [favButton setBackgroundImage:[UIImage imageNamed:@"toWishlist_selected.png"] forState:UIControlStateHighlighted];
    [favButton setBackgroundImage:[UIImage imageNamed:@"toWishlist.png"] forState:UIControlStateNormal];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state

}

- (IBAction)FavouriteToggle:(UIButton*)sender {
    
    UIButton *button=(UIButton*)sender;
    
    if (button.selected) {
        
        [self.delegate markAsFavourite:FALSE withButton:button];
        
    } else {
        
        [self.delegate markAsFavourite:TRUE withButton:button];
    
    }
    
}

@end
