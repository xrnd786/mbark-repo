//
//  ProductListViewHelper.m
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import "ProductListViewHelper.h"

#import "ProductListCell.h"
#import "AppProductDefaults.h"

#import "UIImageView+WebCache.h"
#import "ProductViewController.h"
#import "GroupedProductViewController.h"

#import "SingleProduct_DataHelper.h"
#import "Application_DataHelper.h"
#import "Search_DataHelper.h"

#import "Products.h"
#import "ProductThumbnail.h"

@interface ProductListViewHelper ()<FavouriteClicked>
{
    BOOL isCart;
    CGFloat lastContentOffset;
}

@property (strong,nonatomic) NSArray *productMasterArray;
@property (strong,nonatomic) NSMutableArray *cartIdArray;
@property (strong,nonatomic) NSMutableArray *applicationIdArray;
@property (strong,nonatomic) NSMutableArray *searchArray;

@property (strong,nonatomic) SingleProduct_DataHelper *singleProduct;
@property (strong,nonatomic) Application_DataHelper *applicationProducts;
@property (strong,nonatomic) Search_DataHelper *searchProducts;

@property (strong,nonatomic) Products *productDetails;

@property (strong,nonatomic) NSDictionary *theProductDictionary;



@end


@implementation ProductListViewHelper

@synthesize localTableCopy,productMasterArray,baseView,singleProduct,cartIdArray,applicationProducts,applicationIdArray,searchArray,searchProducts,productDetails,theProductDictionary;

-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view  andProductArray:(NSArray*)array withController:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        productMasterArray=[[NSArray alloc]initWithArray:array];
        
        
        [self initializeMyTable:table];
       
        localTableCopy=table;
        
        if ([productMasterArray count]==0) {
            
            table.hidden=TRUE;
            
        } else {
            
            table.hidden=FALSE;
            
        }
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        isCart=NO;
        
        
    }
    
    return self;
}

-(id)initWithCartProductsTable:(UITableView*)table  withMainView:(UIView*)view andCartId:(NSString*)Id withController:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        self.singleProduct=[[SingleProduct_DataHelper alloc]init];
        
        Products *dictionary=[self.singleProduct getCartDictionary:Id];
        
        self.cartIdArray=[[NSMutableArray alloc]initWithArray:[[dictionary valueForKey:@"Details"] valueForKey:@"ProductId"]];
        
        productMasterArray=[[NSArray alloc]initWithArray:[dictionary valueForKey:@"Products"]];
        
        
        [self initializeMyTable:table];
        
        localTableCopy=table;
        
        if ([productMasterArray count]==0) {
            
            table.hidden=TRUE;
            
        } else {
            
            table.hidden=FALSE;
            
        }
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        isCart=TRUE;
    }
    
    return self;
    
}



-(id)initWithApplicationProductsTable:(UITableView*)table  withMainView:(UIView*)view andApplicationId:(NSString*)Id withController:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        self.applicationProducts=[[Application_DataHelper alloc]init];
        
        [SVProgressHUD show];
        [self performSelectorInBackground:@selector(backgroundCallsForApplicationWithId:) withObject:Id];
        
        [self initializeMyTable:table];
        
        localTableCopy=table;
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        isCart=FALSE;
    }
    
    return self;
    
}



-(id)initWithSearchProductsTable:(UITableView*)table withMainView:(UIView*)view withController:(UIViewController*)controller withSearchText:(NSString*)searchText{
    
    self = [super init];
    
    if (self) {
        
        self.searchProducts=[[Search_DataHelper alloc]init];
        
        
        [SVProgressHUD show];
        
        [self performSelectorInBackground:@selector(backgroundCallsForSearchWithText:) withObject:searchText];
        
        [self initializeMyTable:table];
        
        localTableCopy=table;
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        isCart=FALSE;
        
    }
    
    return self;
    
}


#pragma mark - background calls

-(void)backgroundSeverCallForProd:(NSString*)productID{
    
    NSDictionary *dictionary=[[NSDictionary alloc]initWithDictionary:[self.singleProduct getProductDictionary:productID]];
    
    [self loadTableNow];
    
}

-(void)backgroundCallsForApplicationWithId:(NSString*)Id{
    
    productMasterArray=[[NSArray alloc]initWithArray:[self.applicationProducts loadApplicationsProductsWithId:Id]];
    
    
    [self loadTableNow];
    
}


-(void)backgroundCallsForSearchWithText:(NSString*)text{
    
    productMasterArray=[[NSArray alloc]initWithArray:[self.searchProducts loadProductsListForSearch:text]];
    
    [self loadTableNow];
    
}

-(void)initializeMyTable:(UITableView*)table{
 
    [table setShowsVerticalScrollIndicator:NO];
    
    if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
        [table setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
        [table setLayoutMargins:UIEdgeInsetsZero];
    }
    
    self.baseView.navigationController.view.layer.shadowOffset = CGSizeMake(3, 3);
     self.baseView.navigationController.view.layer.shadowRadius = 2;
     self.baseView.navigationController.view.layer.shadowOpacity = 0.3;
    
    table.delegate=self;
    table.dataSource=self;
    
    
    table.tableFooterView = ({
        
        UIView *viewBase=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], 90)];;
        
        RNThemeLabel *label=[[RNThemeLabel alloc]initWithFrame:viewBase.frame];
        label.text=@"No more products";
        label.textAlignment=NSTextAlignmentCenter;
        label.font=[UIFont italicSystemFontOfSize:9.0f];
        label.textColorKey=@"secondaryColor";
        [label applyTheme];
        
        [viewBase addSubview:label];
        viewBase;
        
    });
    
    if ([productMasterArray count]==0) {
        
        table.hidden=TRUE;
        
    } else {
        
        table.hidden=FALSE;
        
    }
    
}

-(void)loadTableNow{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD dismiss];
        
        if ([productMasterArray count]==0) {
            
            self.localTableCopy.hidden=TRUE;
            
        } else {
            
            self.localTableCopy.hidden=FALSE;
            
        }
        
        [self.localTableCopy reloadData];
        
    });
    
    
}

#pragma mark - initialize tables



#pragma mark - UIScrollView Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
    if (scrollView==self.localTableCopy) {
        
        ScrollDirection scrollDirection;
        
        if (lastContentOffset > scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionUp;
        else if (lastContentOffset < scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionDown;
        else
            scrollDirection=ScrollDirectionNone;
        
        
        lastContentOffset = scrollView.contentOffset.y;
        
        if (scrollDirection== ScrollDirectionNone) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];
            
        }else if (scrollDirection==ScrollDirectionDown){
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldHide" object:self];
            
        }else if (scrollDirection==ScrollDirectionUp){
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];
            
        }
        
    }else{
        
       // currentImageOpen=ceilf((scrollView.contentOffset.x)/([[UIScreen mainScreen]bounds].size.width));
        
    }
}


#pragma mark - table View Functions



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    return nil;
}



#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [productMasterArray count];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"ProductListCell";
    ProductListCell *cell =(ProductListCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
   
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"ProductListCell" owner:self options:nil] objectAtIndex:0];
    }
    
    Products *products=[productMasterArray objectAtIndex:indexPath.row];
    
    cell.productImage.layer.shadowOffset = CGSizeMake(-5, 5);
    cell.productImage.layer.shadowRadius = 3;
    cell.productImage.layer.shadowOpacity = 0.3;
    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:products.thumbnail.p_id done:^(UIImage *image, SDImageCacheType cacheType) {
        
        if (image) {
   
            [cell.productImage setImage:image];
             
        }else{
            
            
            [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:products.thumbnail.p_id] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                
            } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
             {
                 if (image && finished)
                 {
                     [[SDImageCache sharedImageCache] storeImage:image forKey:products.thumbnail.p_id];
                     [cell.productImage setImage:image];
                 }
             }];
            /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
                [cell.productImage sd_setImageWithURL:[NSURL URLWithString:products.thumbnail.p_id] placeholderImage:[UIImage imageNamed:@"no_product.png"]];
                
            });*/
            
            
        }
    }];
    
    cell.productTitle.text=products.name;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.delegate=self;
    
    if (!isCart) {
        
        float rate=[products.rate floatValue];
        float discount=[products.discount floatValue];
        float discounted=rate - (rate*(discount/100));
        
        cell.mrpPrice.text=[NSString stringWithFormat:@"₹%.02f",rate];
        cell.discountedPrice.text=[NSString stringWithFormat:@"₹%.02f",discounted];
        cell.discountedPrice.textColor=[UIColor redColor];
        
        
        if (discount==0) {
            
            cell.discountedPrice.hidden=TRUE;
            
        }else{
           
            NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:cell.mrpPrice.text];
            [titleString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
            [cell.mrpPrice  setAttributedText:titleString];
            
        }
        
        cell.favButton.tag=indexPath.row;
        cell.favButton.selected=[products.isFavourite boolValue];
        cell.favButton.hidden=FALSE;
        
        int dicountValue=(int)[products.discount integerValue];
        int available=(int)[products.availableUnits doubleValue];
        
        if ([products.type isEqualToString:@"Grouped"]) {
       
            cell.favButton.hidden=TRUE;
            
        }else{
            
            cell.favButton.hidden=FALSE;
            
        }
        
        if (dicountValue>0) {
        
            cell.dicountView.hidden=FALSE;
            cell.discountValue.hidden=FALSE;
            cell.discountValue.text=[NSString stringWithFormat:@"%d%%",dicountValue];
            
            
        }else if(dicountValue==0){
            
            
            cell.dicountView.hidden=TRUE;
            cell.discountValue.text=[NSString stringWithFormat:@""];
            
        }else{
            cell.dicountView.hidden=TRUE;
            cell.discountValue.text=[NSString stringWithFormat:@""];
        }
        
        if (available>0) {
            
            cell.stockDetails.hidden=FALSE;
            cell.stockDetails.textColor=[UIColor colorWithRed:48.0f/255.0f green:150.0f/255.0f blue:69.0f/255.0f alpha:1.0f];
            cell.stockDetails.text=[NSString stringWithFormat:@"%d %@ in Stock",available,products.measurementUnit];
            
        } else if(available==0){
            
            cell.stockDetails.hidden=FALSE;
            cell.stockDetails.textColor=[UIColor redColor];
            cell.stockDetails.text=[NSString stringWithFormat:@"Out of Stock"];
            
        }else{
           
            cell.stockDetails.hidden=TRUE;
            cell.stockDetails.text=[NSString stringWithFormat:@""];
            
        }
       
        
    }else{
        
        cell.favButton.hidden=TRUE;
        
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    return cell;

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (isCart) {
        
        
        [self.baseView.navigationController pushViewController:[[ProductViewController alloc]initWithProductID:[self.cartIdArray objectAtIndex:indexPath.row]] animated:YES];
        
    } else {
        
        
        
        if ([((Products*)[self.productMasterArray objectAtIndex:indexPath.row]).type  isEqualToString:@"Simple"]) {
        
            Products *prod=[self.productMasterArray objectAtIndex:indexPath.row];
            ProductViewController *controller=[GlobalCall productViewController];
            controller.productID=prod.p_id;
            
            [self.baseView.navigationController pushViewController:controller animated:YES];
        
        } else {
            
            
            GroupedProductViewController *controller=[GlobalCall groupedProductViewController];
            controller.productID=((Products*)[self.productMasterArray objectAtIndex:indexPath.row]).p_id;
            
            @try {
                
                [self.baseView.navigationController pushViewController:controller animated:NO];
                
            } @catch (NSException * ex) {
                
                [self.baseView.navigationController popToViewController:controller animated:YES];
                
                
            }@finally{
                
            }
            
            
        }
        
    }
    
}


#pragma mark - Cell Delegate

-(void)markAsFavourite:(BOOL)isFav withButton:(UIButton *)button{
    
    
    if (isFav) {
        
        button.selected=TRUE;
    [self performSelectorInBackground:@selector(performAddInBg:) withObject:button];
        
       
    } else {
        
        button.selected=FALSE;
        
        [self performSelectorInBackground:@selector(performRemoveInBg:) withObject:button];
        
        
    }
    
    
}


-(void)performAddInBg:(UIButton*)button{
    
    SCLAlertView *view=[[SCLAlertView alloc]init];
    
    if ([ProductCall_Response callApiForAddingProductWithIDToWishlist:[self.productMasterArray objectAtIndex:button.tag]]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            Products *product=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id == %@",((Products*)[self.productMasterArray objectAtIndex:button.tag]).p_id]] lastObject];
            product.isFavourite=[NSNumber numberWithBool:TRUE];
            [GlobalCall saveContext];
            
        });
        
        
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [view showError:self.baseView title:@"Not added" subTitle:@"Sorry,currently we can not add your product to favourites." closeButtonTitle:@"Ok" duration:0.0f];
            /*
            dispatch_async(dispatch_get_main_queue(), ^{
                
                Products *product=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id == %@",((Products*)[self.productMasterArray objectAtIndex:button.tag].p_id)]] lastObject];
                product.isFavourite=[NSNumber numberWithBool:FALSE];
                [GlobalCall saveContext];
                
            });*/
        });
        
    }
    
    
}


-(void)performRemoveInBg:(UIButton*)button{
    
    NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",((Products*)[self.productMasterArray objectAtIndex:button.tag]).p_id]]];
    Products *prodNow=[arrayOfFetch lastObject];
    
    if ([ProductCall_Response callApiForRemovingProductWithIDToWishlist:((Products*)[self.productMasterArray objectAtIndex:button.tag]).p_id]) {
        
        prodNow.isFavourite=[NSNumber numberWithBool:FALSE];
        [GlobalCall saveContext];
        
    }else{
        
        prodNow.isFavourite=[NSNumber numberWithBool:TRUE];
        [GlobalCall saveContext];
        
    }
}

@end
