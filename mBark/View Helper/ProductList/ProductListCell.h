//
//  ProductLitsCell.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeImageButton.h"

@protocol FavouriteClicked <NSObject>

-(void)markAsFavourite:(BOOL)isFav withButton:(UIButton*)button;

@end

@interface ProductListCell : UITableViewCell



@property (weak, nonatomic) IBOutlet RNThemeLabel *discountValue;
@property (strong, nonatomic) id<FavouriteClicked> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet RNThemeLabel *productTitle;
@property (weak, nonatomic) IBOutlet RNThemeImageButton *favButton;
- (IBAction)FavouriteToggle:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *dicountView;

@property (weak, nonatomic) IBOutlet RNThemeLabel *mrpPrice;
@property (weak, nonatomic) IBOutlet RNThemeLabel *discountedPrice;
@property (weak, nonatomic) IBOutlet RNThemeLabel *stockDetails;

@end
