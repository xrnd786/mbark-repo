//
//  SigninViewHelper.m
//  mBark
//
//  Created by Xiphi Tech on 27/12/2014.
//
//

#import "SigninViewHelper.h"

@interface SigninViewHelper ()

@property (strong,nonatomic) UIViewController *controller_copy;

@end

@implementation SigninViewHelper

@synthesize controller_copy=_controller_copy;

-(id)initWithMainSelf:(UIViewController*)viewController{
    
    self = [super init];
    if (self) {
        
        
        self.controller_copy=[[UIViewController alloc]init];
        self.controller_copy=viewController;
        
        
    }
    
    return self;
    
}



@end
