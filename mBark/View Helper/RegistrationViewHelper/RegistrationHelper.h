//
//  RegistrationHelper.h
//  mBark
//
//  Created by Xiphi Tech on 22/12/2014.
//
//

#import <UIKit/UIKit.h>
#import "Validator.h"
#import "TooltipView.h"
#import "QuartzCore/QuartzCore.h"


@interface RegistrationHelper : UIViewController<ValidatorDelegate>
{
    UIBarButtonItem *extraSpace;
    UIBarButtonItem *done;
     TooltipView    *_tooltipView;
}

@property (nonatomic, retain) UIToolbar *keyboardToolbar;

-(id)initWithScroller:(UIScrollView*)scroller withMainSelf:(UIViewController*)viewController inEditMode:(BOOL)editMode;



@end
