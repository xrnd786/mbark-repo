//
//  RegistrationHelper.m
//  mBark
//
//  Created by Xiphi Tech on 22/12/2014.
//
//
#define placeholder_username @"Username"

#define placeholder_firstname @"First Name"
#define placeholder_lastname @"Last Name"
#define placeholder_organization @"Organization"
#define placeholder_email @"Email"
#define placeholder_mobile @"Mobile"
#define placeholder_addressline1 @"Address Line 1"
#define placeholder_addressline2 @"Address Line 2"
#define placeholder_pincode @"Pincode"
#define placeholder_state @"State"
#define placeholder_country @"Contry"
#define placeholder_gender @"Gender"
#define placeholder_password @"Password"
#define placeholder_passwordrepeat @"Repeat Password"

#define validator_message_password_min  @"Password length must be minimum 6 characters"
#define validator_message_password_max  @"Password length must be maximum 16 characters"
#define validator_message_email  @"Please enter valid email address"
#define validator_message_phone  @"Plese enter valid phone number"
#define validator_message_firstname @"Please enter valid first name"
#define validator_message_lastname @"Please enter valid last name"


#import "RegistrationHelper.h"

#import "AppSettingDefaults.h"
#import "AppTenantDefaults.h"
#import "RNThemeTextField.h"
#import "RNThemeButton.h"
#import "ThemeManager.h"

#import "InvalidTooltipView.h"
#import "ValidTooltipView.h"
#import "QuartzCore/QuartzCore.h"
#import "UIImage+Color.h"

#import "AppUserDefaults.h"
#import "UserLoginCall_Response.h"
#import "IQDropDownTextField.h"
#import "UserProfile.h"
#import "IdentifyUser.h"
#import "User.h"

@interface RegistrationHelper ()<UITextFieldDelegate,IQDropDownTextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    BOOL success;
    BOOL isEditMode;
    UserProfile *profileObj;
    
}

@property (strong,nonatomic) UIScrollView *scrollerCopy;
@property (strong,nonatomic) UIViewController *controller_copy;

@property (strong,nonatomic) Validator *ValidatorPassword;
@property (strong,nonatomic) Validator *ValidatorFirstName;
@property (strong,nonatomic) Validator *ValidatorLastName;
@property (strong,nonatomic) Validator *ValidatorEmail;
@property (strong,nonatomic) Validator *ValidatorMobile;

@property (strong,nonatomic) NSMutableArray *profileData;
@property (strong,nonatomic) NSMutableArray *arrayRequired;

@end


@implementation RegistrationHelper

@synthesize keyboardToolbar;
@synthesize scrollerCopy;
@synthesize controller_copy;
@synthesize ValidatorEmail=_ValidatorEmail;
@synthesize ValidatorFirstName=_ValidatorFirstName;
@synthesize ValidatorLastName=_ValidatorLastName;
@synthesize ValidatorMobile=_ValidatorMobile;
@synthesize ValidatorPassword=_ValidatorPassword;
@synthesize profileData=_profileData;
@synthesize arrayRequired=_arrayRequired;


-(id)initWithScroller:(UIScrollView*)scroller withMainSelf:(UIViewController*)viewController inEditMode:(BOOL)editMode{
    
    self = [super init];
    if (self) {
     
        self.scrollerCopy=[[UIScrollView alloc]init];
        self.scrollerCopy=scroller;
        
        self.controller_copy=[[UIViewController alloc]init];
        self.controller_copy=viewController;
        
        [self.scrollerCopy addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(scrollViewTapped:)]];
        
        
        _arrayRequired=[[NSMutableArray alloc]initWithArray:[self getArrayOfFieldsRequired]];
        
        isEditMode=editMode;
        
        if (isEditMode) {
            
            [self loadProfileData];
            
        }
        
        
        [self setValidator];
        [self createFields];
        [self setKeyBoard];
        success=TRUE;
        profileObj=[UserProfile new];
        
        
    }
    
    return self;

}

-(void)scrollViewTapped:(UITapGestureRecognizer*)tapped{
    
    scrollerCopy.contentOffset=CGPointMake(0, 0);
    
    for (UITextField *fields in [self.scrollerCopy subviews]) {
        
        if (fields.isFirstResponder) {
            
            if ( [fields isKindOfClass:[UIResponder class]] == YES && [(UIResponder*)fields canResignFirstResponder] == YES )
                [fields resignFirstResponder];
            
        }
    }
    
}

-(void)loadProfileData{
    
    self.profileData=[[NSMutableArray alloc]init];
    NSArray *array=[self getArrayOffieldsToBeCaptured];
    
    
    NSArray *result=[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil];
    User *myUserData=(User*)[result lastObject];
    
    for (NSString *placeholder in array) {
        
        if ([placeholder isEqualToString:placeholder_username]) {
            
            [self.profileData addObject:((id)myUserData.username!=nil?myUserData.username:@"")];
            
        }else if ([placeholder isEqualToString:placeholder_firstname]) {
          
            [self.profileData addObject:((id)myUserData.firstName!=nil?myUserData.firstName:@"")];
            
            
        }else if([placeholder isEqualToString:placeholder_lastname]){
            
            [self.profileData addObject:((id)myUserData.lastName!=nil?myUserData.lastName:@"")];
            
            
        }else if([placeholder isEqualToString:placeholder_organization]){
            
            [self.profileData addObject:((id)myUserData.organization!=nil?myUserData.organization:@"")];
            
            
        }else if([placeholder isEqualToString:placeholder_mobile]){
            
            [self.profileData addObject:((id)myUserData.contact!=nil?myUserData.contact:@"")];
            
        }else if([placeholder isEqualToString:placeholder_addressline1]){
            
            [self.profileData addObject:((id)myUserData.addressLine1!=nil?myUserData.addressLine1:@"")];
            
            
        }else if([placeholder isEqualToString:placeholder_addressline2]){
            
            [self.profileData addObject:((id)myUserData.addressLine2!=nil?myUserData.addressLine2:@"")];
            
        }else if ([placeholder isEqualToString:placeholder_country]){
         
            [self.profileData addObject:((id)myUserData.country!=nil?myUserData.country:@"")];
            
            
        }else if ( [placeholder isEqualToString:placeholder_state] ){
          
            [self.profileData addObject:((id)myUserData.state!=nil?myUserData.state:@"")];
            
        }else if([placeholder isEqualToString:placeholder_pincode]){
           
            [self.profileData addObject:((id)myUserData.pincode!=nil?myUserData.pincode:@"")];
            
        
        }else if([placeholder isEqualToString:placeholder_email]){
            
            [self.profileData addObject:((id)myUserData.email!=nil?myUserData.email:@"")];
            
            
        }else if([placeholder isEqualToString:placeholder_gender]){
            
            [self.profileData addObject:((id)myUserData.gender!=nil?myUserData.gender:@"")];
            
        }else if([placeholder isEqualToString:placeholder_password]){
            
            [self.profileData addObject:((id)myUserData.password!=nil?myUserData.password:@"")];
            
        }
        
    }
    
}

-(void)createFields{
    
    CGFloat dynamicY=20.0f;
    
    NSMutableArray *arrayOfCaptured=[[NSMutableArray alloc]initWithArray:[self getArrayOffieldsToBeCaptured]];
    int i=0;
    
    for (NSString *placeholder in arrayOfCaptured) {
       
        CGRect frame=CGRectMake(([GlobalCall getMyWidth]/2)-(270/2), dynamicY, 270, 40);
      
        if ([placeholder isEqualToString:placeholder_gender]) {
            
            [self.scrollerCopy addSubview:[self createDropDownField:frame withPlaceholder:placeholder andNumber:i]];
            
        } else {
            
            [self.scrollerCopy addSubview:[self createField:frame withPlaceholder:placeholder andNumber:i]];
            
        }
        i++;
        dynamicY+=60.0f;
        
    }
    
    self.scrollerCopy.contentSize=CGSizeMake(320.0f, i*70);
    
    RNThemeButton *registerButton=[[RNThemeButton alloc]init];
    
    registerButton.frame=CGRectMake(([GlobalCall getMyWidth]/2)-(270/2), dynamicY, 270, 40);
    registerButton.fontKey=@"generalFont";
    registerButton.backgroundColorKey=@"primaryColor";
    registerButton.textColorKey=@"lightColor";
    
    if (isEditMode) {
        
        [registerButton setTitle:@"Save changes" forState:UIControlStateNormal];
    } else {
        
        [registerButton setTitle:@"Register" forState:UIControlStateNormal];
    }
    
    registerButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    [registerButton applyTheme];
    [registerButton addTarget:self action:@selector(doRegistration:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollerCopy addSubview:registerButton];
    
}

-(void)setKeyBoard{
   
    if (keyboardToolbar == nil)
    {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,self.view.bounds.size.width, 44)];
        
        extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemFlexibleSpace) target:nil action:nil];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemDone) target:self action:@selector(resignKeyboard)];
        
        [keyboardToolbar setItems:[[NSArray alloc] initWithObjects:extraSpace, done, nil]];
        
        [keyboardToolbar setTranslucent:YES];
        [keyboardToolbar setTintColor:[UIColor blackColor]];
    }
    
    for (id view in self.controller_copy.view.subviews) {
      
        if ([view isKindOfClass:[RNThemeTextField class]]) {
          
            [(RNThemeTextField *)view setInputAccessoryView:keyboardToolbar];
        
        }
        
    }
    
}


-(void)setValidator{
    
   _ValidatorEmail = [[Validator alloc] init];
   _ValidatorEmail.delegate   = self;
   
    _ValidatorFirstName = [[Validator alloc] init];
    _ValidatorFirstName.delegate   = self;
   
    _ValidatorLastName = [[Validator alloc] init];
    _ValidatorLastName.delegate   = self;
   
    _ValidatorMobile = [[Validator alloc] init];
    _ValidatorMobile.delegate   = self;
   
    _ValidatorPassword = [[Validator alloc] init];
    _ValidatorPassword.delegate   = self;
    
}


-(void) resignKeyboard
{
    
    [self.scrollerCopy endEditing:YES];

}


-(NSMutableArray*)getArrayOffieldsToBeCaptured{
    
    NSMutableArray *arrayToBeCaputured=[[NSMutableArray alloc]init];
   
    
    [arrayToBeCaputured addObject:placeholder_username];
    
    if ([AppSettingDefaults isFNameCaptured]) {
        
        [arrayToBeCaputured addObject:placeholder_firstname];
        
    }
    
    if ([AppSettingDefaults isLNameCaptured]) {
        
        [arrayToBeCaputured addObject:placeholder_lastname];
        
    }
    
    if ([AppSettingDefaults isOrganizationCaptured]) {
        
        [arrayToBeCaputured addObject:placeholder_organization];
        
    }
    
    if ([AppSettingDefaults isEmailCaptured]) {
        
        [arrayToBeCaputured addObject:placeholder_email];
        
    }
    
    if ([AppSettingDefaults isMobileCaptured]) {
        
        [arrayToBeCaputured addObject:placeholder_mobile];
        
    }
    
    if ([AppSettingDefaults isAddressCaptured]) {
        
        [arrayToBeCaputured addObject:placeholder_addressline1];
        [arrayToBeCaputured addObject:placeholder_addressline2];
        
        [arrayToBeCaputured addObject:placeholder_pincode];
        
        [arrayToBeCaputured addObject:placeholder_state];
        [arrayToBeCaputured addObject:placeholder_country];
        
    }
    
   
    if ([AppSettingDefaults isGenderCaptured]) {
        
        [arrayToBeCaputured addObject:placeholder_gender];
        
    }
    
    if (!isEditMode) {
        
        [arrayToBeCaputured addObject:placeholder_password];
        [arrayToBeCaputured addObject:placeholder_passwordrepeat];
    
    }
    
    return arrayToBeCaputured;
    
}

-(NSMutableArray*)getArrayOfFieldsRequired{
    
    NSMutableArray *arrayRequired=[[NSMutableArray alloc]init];
    
    [arrayRequired addObject:[NSNumber numberWithBool:TRUE]];
    
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isFNameRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isLNameRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isOrganizationRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isEmailRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isMobileRequired]]];
   
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isAddressRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isAddressRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isAddressRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isAddressRequired]]];
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isAddressRequired]]];
    
    [arrayRequired addObject:[NSNumber numberWithBool:[AppSettingDefaults isGenderRequired]]];
    
    if (!isEditMode) {
        
        [arrayRequired addObject:[NSNumber numberWithBool:TRUE]];
        [arrayRequired addObject:[NSNumber numberWithBool:TRUE]];
        
    }
    
    return arrayRequired;
    
}


-(IQDropDownTextField*)createDropDownField:(CGRect)frame withPlaceholder:(NSString*)placeholder andNumber:(int)i{
    
    IQDropDownTextField *newField=[[IQDropDownTextField alloc]initWithFrame:frame];
    
    newField.backgroundColor=[UIColor whiteColor];
    newField.tag=1000+i;
    newField.textAlignment=NSTextAlignmentCenter;
    newField.delegate=self;
    newField.textColorKey=@"secondaryColor";
    newField.fontKey=@"generalFont";
    newField.layer.borderWidth=0.5f;
    newField.layer.borderColor=[[ThemeManager sharedManager]colorForKey:@"secondaryColor"].CGColor;
    
    if ([[_arrayRequired objectAtIndex:i] boolValue]) {
        
        newField.placeholder=[NSString stringWithFormat:@"%@ *",placeholder];
        
    }else{
        
        newField.placeholder=placeholder;
        
    }
    
    if (isEditMode) {
        
        newField.text=([self.profileData objectAtIndex:i]!=[NSNull null])?[self.profileData objectAtIndex:i]:@"";
        
    }
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [toolbar sizeToFit];
    
    UIBarButtonItem *buttonflexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *buttonDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
    
    [toolbar setItems:[NSArray arrayWithObjects:buttonflexible,buttonDone, nil]];
    newField.inputAccessoryView = toolbar;
    
    newField.isOptionalDropDown = NO;
    [newField setItemList:[NSArray arrayWithObjects:@"Female",@"Male", nil]];
    
    newField.imageKey=@"usernameimage";
    
    [newField applyTheme];
    
    return newField;
    
}

-(void)doneClicked:(UIBarButtonItem*)button
{
    [self.controller_copy.view endEditing:YES];
    profileObj.Gender=button.title;
    
}


-(RNThemeTextField*)createField:(CGRect)frame withPlaceholder:(NSString*)placeholder andNumber:(int)i{
    
    RNThemeTextField *newField=[[RNThemeTextField alloc]initWithFrame:frame];
    
    //NSLog(@"Placeholder %@ and Tag %d",placeholder,i);
    
    newField.backgroundColor=[UIColor whiteColor];
    newField.tag=1000+i;
    newField.textAlignment=NSTextAlignmentCenter;
    newField.delegate=self;
    newField.textColorKey=@"secondaryColor";
    newField.fontKey=@"generalFont";
    newField.layer.borderWidth=0.5f;
    newField.layer.borderColor=[[ThemeManager sharedManager]colorForKey:@"secondaryColor"].CGColor;
 
    if ([[_arrayRequired objectAtIndex:i] boolValue]) {
        
        newField.placeholder=[NSString stringWithFormat:@"%@ *",placeholder];
        newField=[self checkFieldAndReturnNewField:newField isRequired:TRUE];
        
    }else{
        
        newField.placeholder=placeholder;
        newField=[self checkFieldAndReturnNewField:newField isRequired:FALSE];
        
    }
    
    if (isEditMode) {
        
        newField.text=([self.profileData objectAtIndex:i]!=nil)?[NSString stringWithFormat:@"%@",[[self.profileData objectAtIndex:i] stringValue]]:@"";
    
    }

    [newField applyTheme];
    
    return newField;

}

-(UIPickerView*)createPicker:(CGRect)frame withArray:(NSString*)placeholder andNumber:(int)i{
    
    UIPickerView *newPicker=[[UIPickerView alloc]initWithFrame:frame];
    newPicker.dataSource=self;
    newPicker.delegate=self;
    
    return newPicker;

}



-(RNThemeTextField*)checkFieldAndReturnNewField:(RNThemeTextField*)newfield isRequired:(BOOL)isReq{
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_username stringByAppendingString:@" *"]:placeholder_username]) {
     
        newfield.imageKey=@"usernameimage";
        
    }
    
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_password stringByAppendingString:@" *"]:placeholder_password]) {
        
        newfield.secureTextEntry=TRUE;
        newfield.imageKey=@"passwordimage";
        
    }
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_passwordrepeat stringByAppendingString:@" *"]:placeholder_passwordrepeat]) {
        
        newfield.secureTextEntry=TRUE;
        newfield.imageKey=@"passwordrepeatimage";
        
    }
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_mobile stringByAppendingString:@" *"]:placeholder_mobile]) {
    
        newfield.imageKey =@"mobile";
        
    }
    
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_firstname stringByAppendingString:@" *"]:placeholder_firstname]) {
        
        newfield.imageKey =@"usernameimage";
        
    }
    
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_lastname stringByAppendingString:@" *"]:placeholder_lastname]) {
        
        newfield.imageKey =@"usernameimage";
        
    }
    
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_email stringByAppendingString:@" *"]:placeholder_email]) {
       
        newfield.imageKey =@"email";
        
   }
    
    
    if ([newfield.placeholder isEqualToString:isReq?[placeholder_organization stringByAppendingString:@" *"]:placeholder_organization]) {
        
        newfield.imageKey =@"organization";
        
    }


    
    return newfield;
    
}


#pragma mark - Text Field delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    int tagValue=(int)textField.tag;
    
    scrollerCopy.contentOffset=CGPointMake(0, 60*(tagValue-1000));
    
    textField.layer.borderWidth=1.0f;
    textField.layer.borderColor=[[ThemeManager sharedManager]colorForKey:@"primaryColor"].CGColor;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    int tagValue=(int)textField.tag;
    
    NSLog(@"End text editing for tag %d",tagValue);
    
    
    RNThemeTextField *newfield=(RNThemeTextField*)[self.scrollerCopy viewWithTag:tagValue];
    newfield.text=[newfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    [self doNecessaryValidationForTextField:tagValue];
    
    scrollerCopy.contentOffset=CGPointMake(0, 0);
    
    if ( [textField isKindOfClass:[UIResponder class]] == YES && [(UIResponder*)textField canResignFirstResponder] == YES )
        [textField resignFirstResponder];
    
    textField.layer.borderWidth=0.5f;
    textField.layer.borderColor=[[ThemeManager sharedManager]colorForKey:@"secondaryColor"].CGColor;
   
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if ( [textField isKindOfClass:[UIResponder class]] == YES && [(UIResponder*)textField canResignFirstResponder] == YES )
        [textField resignFirstResponder];
    
    return TRUE;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // Hides Tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    
    return YES;
}



#pragma mark - Tasks

-(void)doRegistration:(RNThemeButton*)button{
    
    
    [self.view endEditing:YES];
    
    BOOL answer=TRUE;
    
    for (id fields in [self.scrollerCopy subviews]) {
        
        
        if ([fields isKindOfClass:[RNThemeTextField class]]) {
            
            if ([((RNThemeTextField*)fields).text isEqualToString:@""]) {
                
                answer=[self checkForCompulsoryCriteria:(RNThemeTextField*)fields];
                
            }
        }
        
    }
    
    
    if (!answer) {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showWarning:self.controller_copy title:@"Field(s) required" subTitle:@"Please fill all the required data" closeButtonTitle:@"Ok" duration:0.0f];
        
    }else if(success){
        
        [self saveThisUser];
        
    }else{
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self.controller_copy title:@"Failure" subTitle:@"Sorry, We could not register you." closeButtonTitle:@"Ok" duration:0.0f];
        
    }
    
    
    
    
}

-(BOOL)checkForCompulsoryCriteria:(RNThemeTextField*)field{
    
    BOOL answer=TRUE;
    NSArray *arry=[self getArrayOffieldsToBeCaptured];

    for (NSString *placeholder in arry) {
        
        if ([placeholder isEqualToString:placeholder_firstname]) {
            
            if ([AppSettingDefaults isFNameRequired]) {
                 answer=FALSE;
            }
           
            
        }else if([placeholder isEqualToString:placeholder_lastname]){
            
            if ([AppSettingDefaults isLNameRequired]) {
                answer=FALSE;
            }
            
        }else if([placeholder isEqualToString:placeholder_organization]){
            
            if ([AppSettingDefaults isOrganizationRequired]) {
                answer=FALSE;
            }
            
        }else if([placeholder isEqualToString:placeholder_mobile]){
            
            if ([AppSettingDefaults isMobileRequired]) {
                answer=FALSE;
            }
            
        }else if([placeholder isEqualToString:placeholder_addressline1] || [placeholder isEqualToString:placeholder_country] || [placeholder isEqualToString:placeholder_pincode] || [placeholder isEqualToString:placeholder_state] || [placeholder isEqualToString:placeholder_country]){
            
            if ([AppSettingDefaults isAddressRequired]) {
                answer=FALSE;
            }
            
        }else if([placeholder isEqualToString:placeholder_email]){
            
            if ([AppSettingDefaults isEmailRequired]) {
                answer=FALSE;
            }
            
        }else if([placeholder isEqualToString:placeholder_gender]){
            
            if ([AppSettingDefaults isGenderRequired]) {
                answer=FALSE;
            }
        }
        
    }
    
    return answer;
}



-(void)doNecessaryValidationForTextField:(int)tag{
    
    RNThemeTextField *newfield=(RNThemeTextField*)[self.scrollerCopy viewWithTag:tag];
    NSString *placeholder=[newfield.placeholder containsString:@"*"]?[[[newfield.placeholder componentsSeparatedByString:@"*"] objectAtIndex:0]  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]:newfield.placeholder;
    
    NSLog(@"Placeholder text %@",placeholder);

        if ([placeholder isEqualToString:placeholder_firstname]) {
            
            [_ValidatorFirstName putRule:[Rules checkIfAlphabeticalWithFailureString:validator_message_firstname forTextField:newfield]];
            [_ValidatorFirstName validate];
            
        }else if([placeholder isEqualToString:placeholder_lastname]){
            
            [_ValidatorLastName putRule:[Rules checkIfAlphabeticalWithFailureString:validator_message_lastname forTextField:newfield]];
            [_ValidatorLastName validate];
            
            
        }else if([placeholder isEqualToString:placeholder_organization]){
            
            
            
        }else if([placeholder isEqualToString:placeholder_mobile]){
            
            [_ValidatorMobile putRule:[Rules checkIfNumericWithFailureString:validator_message_phone forTextField:newfield]];
            [_ValidatorMobile validate];
            
        }else if([placeholder isEqualToString:placeholder_email]){
            
           [_ValidatorEmail putRule:[Rules checkIsValidEmailWithFailureString:validator_message_email forTextField:newfield]];
           [_ValidatorEmail validate];
           
            
        }else if([placeholder isEqualToString:placeholder_gender]){
            
        
        }else if([placeholder isEqualToString:placeholder_password]){
            
            [_ValidatorPassword putRule:[Rules minLength:6 withFailureString:validator_message_password_min forTextField:newfield]];
            [_ValidatorPassword validate];
            
        }else{
            return;
        }
    
}


#pragma mark - Validator Deleagtes
#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    for (RNThemeTextField *textField in self.controller_copy.view.subviews) {
        textField.layer.borderWidth = 0;
    }
    
    NSLog(@"Called before the validation begins");

}

- (void)onSuccess
{
    success=TRUE;
    
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView  = nil;
    }
    
}

- (void)onFailure:(Rule *)failedRule
{
    
    success=FALSE;
    //NSLog(@"Failed");
   
    failedRule.textField.layer.borderColor   = [[UIColor redColor] CGColor];
    failedRule.textField.layer.cornerRadius  = 0;
    failedRule.textField.layer.borderWidth   = 2;
    
    CGPoint point = [(RNThemeTextField*)failedRule.textField convertPoint:CGPointMake(0.0, ((RNThemeTextField*)failedRule.textField).frame.size.height - 4.0) toView:self.scrollerCopy];
   
    CGRect tooltipViewFrame = CGRectMake(6.0, point.y, 309.0, 20);
    
    _tooltipView       = [[InvalidTooltipView alloc] init];
    _tooltipView.frame = tooltipViewFrame;
    _tooltipView.text  = [NSString stringWithFormat:@"%@",failedRule.failureMessage];
    _tooltipView.rule  = failedRule;
   
    [self.scrollerCopy addSubview:_tooltipView];
    
}


#pragma mark - webservice funtions

-(void)saveThisUser{
    
    NSArray *array=[self getArrayOffieldsToBeCaptured];
    
    int i=0;
    for (NSString *string in array) {
        
        id viewType=[self.scrollerCopy viewWithTag:(i+1000)];
        RNThemeTextField *filed;
        
        if ([viewType isKindOfClass:([RNThemeTextField class])]) {
            
            filed=(RNThemeTextField*)viewType;
            
        }
        
        
        if ([string isEqualToString:placeholder_username]) {
        
            profileObj.username=filed.text;
            
        }else if ([string isEqualToString:placeholder_firstname]) {
            
           profileObj.Firstname=filed.text;
            
        } else if([string isEqualToString:placeholder_lastname]){
            
            profileObj.Lastname=filed.text;
            
        }else if([string isEqualToString:placeholder_organization]){
            
            profileObj.Organization=filed.text;
            
        }else if([string isEqualToString:placeholder_email]){
            
            profileObj.Email=filed.text;
            
        }else if([string isEqualToString:placeholder_mobile]){
            
            profileObj.Contact=filed.text;
            
            
        }else if([string isEqualToString:placeholder_addressline1]){
            
            
            profileObj.AddressLine1=filed.text;
            
            
            
        }else if([string isEqualToString:placeholder_addressline2]){
            
            profileObj.AddressLine2=filed.text;
            
        }else if([string isEqualToString:placeholder_pincode]){
            
            profileObj.Pincode=[NSNumber numberWithInteger:[filed.text integerValue]];
            
        }else if([string isEqualToString:placeholder_state]){
            
            profileObj.State=filed.text;
            
        }else if([string isEqualToString:placeholder_country]){
            
            profileObj.Country=filed.text;
            
        }else if([string isEqualToString:placeholder_gender]){
            
            profileObj.Gender=filed.text;
            
        }else if([string isEqualToString:placeholder_password]){
            
            profileObj.Password=filed.text;
            
        }
        i++;
    
    }
    
    
    
    
    if ([GlobalCall isConnected]) {
        
        [SVProgressHUD show];
        [self performSelectorInBackground:@selector(saveThisUserOnWeb) withObject:nil];
        
    }else{
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:[[UIApplication sharedApplication]keyWindow].rootViewController title:@"No Connection" subTitle:[NSString stringWithFormat:@"Please connect you device to wifi/3G for %@",isEditMode?@"updating info":@"Registration"] closeButtonTitle:nil duration:0.0f];
        
    }
    
}


#pragma mark - web call

-(void)saveThisUserOnWeb{
  
    [[NSNotificationCenter defaultCenter] removeObserver:self name:APINotification_User_Registration_Done object:nil];
    [GlobalCall addObserverWithNotifier:APINotification_User_Registration_Done andSelector:@selector(userSaved) handlerClass:self withObject:nil];
    
    if (isEditMode) {
        
        [IdentifyUser UpdateUserWithDataEntered:profileObj];
        
    } else {
        
      
        [UserLoginCall_Response callApiForUserLoginCreation:profileObj.Password andUsername:profileObj.username];
        
        [IdentifyUser UpdateUserWithDataEntered:profileObj];
        
    }
    
    
}

-(void)userSaved{

    
    dispatch_async(dispatch_get_main_queue(), ^{
   
        [SVProgressHUD dismiss];
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert addButton:@"Ok" actionBlock:^{
            
        }];
        
        [self.controller_copy dismissViewControllerAnimated:YES completion:^{
            
            if (isEditMode) {
                
                [alert showSuccess:[[UIApplication sharedApplication]keyWindow].rootViewController title:@"Success" subTitle:[NSString stringWithFormat: @"Your changes have been saved"] closeButtonTitle:nil duration:0.0f];
                
            } else {
                
                [alert showSuccess:[[UIApplication sharedApplication]keyWindow].rootViewController title:@"Success" subTitle:[NSString stringWithFormat: @"Congratualtions, welcome to %@",[AppTenantDefaults getOrganization]] closeButtonTitle:nil duration:0.0f];
                
            }
            
        }];

        
        
    });
    
    
}


@end
