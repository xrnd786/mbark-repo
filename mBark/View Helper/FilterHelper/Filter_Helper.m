//
//  CategoryListView_Helper.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "Filter_Helper.h"

#import "FilterCheckTableView.h"
#import "FilterPriceCheckTableView.h"

#import "MultiTabControl.h"
#import "LSTabItem.h"

#define Comaprer_Equals @"Equals"
#define Comaprer_GreaterThan @"GreaterThan"
#define Comaprer_LessThan @"LessThan"
#define Comaprer_GreaterThanOrEqual @"GreaterThanOrEqual"
#define Comaprer_LessThanOrEqual @"LessThanOrEqual"
#define Comaprer_NotEqual @"NotEqual"
#define Comaprer_GreaterThan @"GreaterThan"
#define Comaprer_StartsWith @"StartsWith"
#define Comaprer_EndsWith @"EndsWith"
#define Comaprer_Contains @"Contains"

#define FilterOn_Category @"Category"
#define FilterOn_Attributes @"Attributes"
#define FilterOn_Rate @"Rate"

#import "ProductCall_Response.h"

// View shortcuts
#import "UIView+Addictions.h"

#define Checkbox @"Checkbox"
#define Slider @"Slider"

@interface Filter_Helper ()<PriceCheckClicked,FilterCheckClicked>
{
    FilterCheckTableView *checkTable;
    FilterPriceCheckTableView *checkPriceTable;
    
}
@property (strong,nonatomic) NSArray *attributesValues;
@property (strong,nonatomic) NSArray *minMaxBounds;
@property (strong,nonatomic) NSArray *controlType;
@property (strong,nonatomic) NSArray *attributeNames;
@property (strong,nonatomic) NSArray *parameterArray;


@property (strong,nonatomic) UIView *baseView;
@end


@implementation Filter_Helper

//@synthesize controlPanelView;
@synthesize selectedTabLabel;
@synthesize localCopyOfContainer;
@synthesize attributesValues;
@synthesize minMaxBounds;
@synthesize controlType;
@synthesize filterComparerArray,filterOnCheckedArray,filterPrameterCheckedArray,filterValueCheckedArray,attributeNames,parameterArray,baseView;
@synthesize delegate,currentTabSelected,localCopyOfTabView;

+ (NSString *)viewTitle {
    
    return @"Vertical Scroll Tab View";

}

-(id)initWithView:(UIView*)view andTabView:(VerticalScrollTabBarView*)tabView withLeftTabView:(UIView*)leftTabView :(UIView*)container withAttributeTitleArray:(NSArray*)attributeTitles withAttributeValuesList:(NSArray*)valuesList withBoundsArray:(NSArray*)valuesBound typeOfFilterControl:(NSArray*)filteControlArray attributeName:(NSArray*)names parameter:(NSArray*)parameters{

    self = [super init];

     if (self) {
         
         container.backgroundColor = [UIColor whiteColor];
         view.backgroundColor = [UIColor whiteColor];
         leftTabView.backgroundColor=[UIColor whiteColor];
         
         NSMutableArray *tabItems=[[NSMutableArray alloc]init];
        
         self.filterComparerArray=[[NSMutableArray alloc]init];
         self.filterOnCheckedArray=[[NSMutableArray alloc]init];
         self.filterPrameterCheckedArray=[[NSMutableArray alloc]init];
         self.filterValueCheckedArray=[[NSMutableArray alloc]init];
         
         attributesValues=[[NSArray alloc]initWithArray:valuesList];
         minMaxBounds=[[NSArray alloc]initWithArray:valuesBound];
         controlType=[[NSArray alloc]initWithArray:filteControlArray];
         attributeNames=[[NSArray alloc]initWithArray:names];
         parameterArray=[[NSArray alloc]initWithArray:parameters];
         
         for (NSString *attributeTitle in attributeTitles) {
             
            LSTabItem *item=[[LSTabItem alloc]initWithTitle:attributeTitle];
            [tabItems addObject:item];
            
         }
         
         tabView = [[VerticalScrollTabBarView alloc] initWithItems:tabItems delegate:self];
         tabView.autoresizingMask |= UIViewAutoresizingFlexibleHeight;
         tabView.itemPadding = -50.0f;
         tabView.margin = 0.0f;
         tabView.frame = CGRectMake(0.0f, 0.0f, 100.0f, leftTabView.frame.size.height);
         [leftTabView addSubview:tabView];
         
         self.localCopyOfTabView=[[VerticalScrollTabBarView alloc]init];
         self.localCopyOfTabView=tabView;
         
         
         self.localCopyOfContainer=[[UIView alloc]init];
         self.localCopyOfContainer=container;
         
         //self.localCopyOfTabView.selectedTabIndex=0;
         
         self.baseView=view;
         
         
     }
    
    return self;
    
}


-(void)updateFinalFilters:(NSArray *)finalFilters withTabIndex:(int)index{
    
    self.filterComparerArray=[[NSMutableArray alloc] init];
    self.filterOnCheckedArray=[[NSMutableArray alloc] init];
    self.filterPrameterCheckedArray=[[NSMutableArray alloc] init];
    self.filterValueCheckedArray=[[NSMutableArray alloc] init];
    

    
    for (NSDictionary *dictionay in finalFilters) {
        
        NSLog(@"Dictionary %@",dictionay);
        
        if ([[dictionay valueForKey:FilterOn] isEqualToString:@"Category"]) {
            
            [self.filterOnCheckedArray addObject:[NSString stringWithFormat:@"%@",[dictionay valueForKey:FilterOn]]];
            //NSLog(@"Category Array now check%@",self.filterOnCheckedArray);
            
            [self.filterValueCheckedArray addObject:[NSString stringWithFormat:@"%@",[dictionay valueForKey:Value]]];
            //NSLog(@"Category Array now value%@",self.filterValueCheckedArray);
            
            [self.filterComparerArray addObject:[NSString stringWithFormat:@"%@",[dictionay valueForKey:Comparer]]];
            //NSLog(@"Category Array now comparer%@",self.filterComparerArray);
            
            [self.filterPrameterCheckedArray addObject:@""];
            //NSLog(@"Category Array now parameter%@",self.filterPrameterCheckedArray);
            
        } else {
            
            [self.filterOnCheckedArray addObject:[NSString stringWithFormat:@"%@",[dictionay valueForKey:FilterOn]]];
            //NSLog(@"Array now check%@",self.filterOnCheckedArray);
            
            [self.filterValueCheckedArray addObject:[dictionay valueForKey:Value]];
            //NSLog(@"Array now value%@",self.filterValueCheckedArray);
            
            [self.filterComparerArray addObject:[dictionay valueForKey:Comparer]];
            //NSLog(@"Array now comparer%@",self.filterComparerArray);
            
            [self.filterPrameterCheckedArray addObject:[dictionay valueForKey:Parameter]];
            //NSLog(@"Array now parameter%@",self.filterPrameterCheckedArray);
            
        }
        
    }
    
    self.localCopyOfTabView.selectedTabIndex=index;
    self.localCopyOfTabView.selectedTabIndex=0;
    
}

#pragma mark -
#pragma mark LSTabBarViewDelegate Methods

- (LSTabControl *)tabBar:(LSTabBarView *)tabBar
          tabViewForItem:(LSTabItem *)item
                 atIndex:(NSInteger)index
{
    return [[MultiTabControl alloc] initWithItem:item] ;
}

-(CGFloat)tabBar:(LSTabBarView *)tabBar paddingForItem:(LSTabItem *)item atIndex:(NSInteger)index{
    
    return 0.5f;
    
}

- (void)tabBar:(LSTabBarView *)tabBar
   tabSelected:(LSTabItem *)item
       atIndex:(NSInteger)selectedIndex
{
    
    currentTabSelected=[NSNumber numberWithInt:(int)selectedIndex];
    NSMutableArray *filerSelected=[[NSMutableArray alloc]init];
    
    NSLog(@"Array of filter selected names: %@and Values: %@",self.filterPrameterCheckedArray,self.filterValueCheckedArray);
    
    
    if ([[controlType objectAtIndex:selectedIndex] isEqualToString:Checkbox]) {
        
        for (int i=0;i<[self.filterPrameterCheckedArray count];i++) {
            
            if ([[self.filterPrameterCheckedArray objectAtIndex:i] isEqualToString:[self.parameterArray objectAtIndex:(int)selectedIndex]]) {
                
                NSNumber *newIndex=[NSNumber numberWithInt:(int)[[self.attributesValues objectAtIndex:(int)selectedIndex] indexOfObject:[self.filterValueCheckedArray objectAtIndex:i]]];
                [filerSelected addObject:newIndex];
                
            }
            
        }
        
        
        NSLog(@"Array of filter selected index: %@For attribute name %@",filerSelected,[self.parameterArray objectAtIndex:(int)selectedIndex]);
       
        checkTable=[[FilterCheckTableView alloc]initWithMainView:localCopyOfContainer withTitleValues:[attributesValues objectAtIndex:selectedIndex] withMyNumber:(int)selectedIndex withAttributesName:self.attributeNames parameter:self.parameterArray withSelectedArray:filerSelected];
        checkTable.delegate=self;
    
        
    } else if([[controlType objectAtIndex:selectedIndex] isEqualToString:Slider]){
        
        
        for (int i=0;i<[self.filterPrameterCheckedArray count];i++) {
            
            if ([[self.filterOnCheckedArray objectAtIndex:i] isEqualToString:@"Rate"]) {
                
                
                NSNumber *newIndex=[NSNumber numberWithInt:(int)[[self.attributesValues objectAtIndex:(int)selectedIndex] indexOfObject:[self.filterValueCheckedArray objectAtIndex:i]]];
                
                if ([[self.filterComparerArray objectAtIndex:i] isEqualToString:Comaprer_GreaterThan]) {
                    
                    [filerSelected addObject:newIndex];
                    //[filerSelected addObject:[NSNumber numberWithInt:[newIndex integerValue]+1]];
                    
                }
                /*else if([[self.filterComparerArray objectAtIndex:i] isEqualToString:Comaprer_LessThanOrEqual]){
                    
                    [filerSelected addObject:newIndex];
                    [filerSelected addObject:[NSNumber numberWithInt:[newIndex integerValue]-1]];
                    
                }*/
                
            }
            
        }
        
        
        NSLog(@"Array of filter selected index: %@For attribute name %@",filerSelected,[self.parameterArray objectAtIndex:(int)selectedIndex]);
        
        checkPriceTable=[[FilterPriceCheckTableView alloc]initWithMainView:localCopyOfContainer filtersBounds:[minMaxBounds objectAtIndex:selectedIndex] withTitleValues:[attributesValues objectAtIndex:selectedIndex] withMyNumber:(int)selectedIndex withAttributesName:self.attributeNames withSelectedArray:filerSelected];
        checkPriceTable.delegate=self;
    
    }
  
    
}

#pragma mark - filter delegates

-(void)filterValueAtIndex:(int)indexValue forMyValue:(int)myValue withAtributeName:(NSString *)name withParameter:(NSString *)parameter withAnswer:(BOOL)isSelected{
    
    NSObject *value=[[self.attributesValues objectAtIndex:myValue] objectAtIndex:indexValue];
    
    if ([name isEqualToString:@"Category"]) {
        
        [self.filterOnCheckedArray removeAllObjects];
        [self.filterValueCheckedArray removeAllObjects];
        [self.filterComparerArray  removeAllObjects];
        [self.filterPrameterCheckedArray removeAllObjects];
        
        
        if (isSelected) {
            
            [self.filterValueCheckedArray addObject:value];
            [self.filterOnCheckedArray addObject:name];
            [self.filterComparerArray addObject:Comaprer_Equals];
            [self.filterPrameterCheckedArray addObject:parameter];
            
        }
        
    } else {
        
        if ([self.filterValueCheckedArray count]==0) {
            
            [self.filterValueCheckedArray addObject:value];
            [self.filterOnCheckedArray addObject:name];
            [self.filterComparerArray addObject:Comaprer_Equals];
            [self.filterPrameterCheckedArray addObject:parameter];
            
        }else{
           
            if ([self.filterValueCheckedArray containsObject:value]) {
                
                NSIndexSet *indexesValue=[self.filterValueCheckedArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                  
                    
                    return [[self.filterValueCheckedArray objectAtIndex:idx]isEqual:value];
                    
                    
                }];
                
               // NSLog(@"index set %@",[indexesValue description]);
                
                NSArray *newObjects=[[NSArray alloc]initWithArray:[self.filterPrameterCheckedArray objectsAtIndexes:indexesValue]];
                
                //NSLog(@"Array of matched paramters%@",[self.filterPrameterCheckedArray objectsAtIndexes:indexesValue]);
                
                
                NSMutableArray *indexSetArray=[[NSMutableArray alloc]init];
                
                [indexesValue enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                    
                    [indexSetArray addObject:[NSNumber numberWithInteger:idx]];
                    
                }];
                
                if ([newObjects containsObject:parameter]) {
                
                    int ind=(int)[newObjects indexOfObject:parameter];
                    NSNumber *indexValue=[indexSetArray objectAtIndex:ind];
                    
                    //NSLog(@"Index value of selected object Value %ld",(long)[indexValue integerValue]);
                
                    
                    [self.filterValueCheckedArray removeObjectAtIndex:[indexValue integerValue]];
                    [self.filterOnCheckedArray removeObjectAtIndex:[indexValue integerValue]];
                    [self.filterComparerArray removeObjectAtIndex:[indexValue integerValue]];
                    [self.filterPrameterCheckedArray removeObjectAtIndex:[indexValue integerValue]];
                    
                }else{
                    
                    [self.filterValueCheckedArray addObject:value];
                    [self.filterOnCheckedArray addObject:name];
                    [self.filterComparerArray addObject:Comaprer_Equals];
                    [self.filterPrameterCheckedArray addObject:parameter];
                    
                }
                
                
            } else {
                
                [self.filterValueCheckedArray addObject:value];
                [self.filterOnCheckedArray addObject:name];
                [self.filterComparerArray addObject:Comaprer_Equals];
                [self.filterPrameterCheckedArray addObject:parameter];
                
            }
            
        }
    
    }
    
}

-(void)priceValueAtIndex:(int)indexValue forMyValue:(int)myValue withAtributeName:(NSString *)name{
    
    NSString *startsWith=[[self.attributesValues objectAtIndex:myValue] objectAtIndex:indexValue];
    NSString *endsWith=[[self.attributesValues objectAtIndex:myValue] objectAtIndex:indexValue+1];
    
    [self clearPriceArray];
        
        [self.filterValueCheckedArray addObject:startsWith];
        [self.filterOnCheckedArray addObject:@"Rate"];
        [self.filterComparerArray addObject:Comaprer_GreaterThan];
        [self.filterPrameterCheckedArray addObject:@""];
        
        [self.filterValueCheckedArray addObject:endsWith];
        [self.filterOnCheckedArray addObject:@"Rate"];
        [self.filterComparerArray addObject:Comaprer_LessThanOrEqual];
        [self.filterPrameterCheckedArray addObject:@""];
        
    
}


-(void)priceValueLowValue:(float)lowValue highVaue:(float)highValue{
    
  
    [self clearPriceArray];
    
    [self.filterValueCheckedArray addObject:[NSString stringWithFormat:@"%.f",lowValue]];
    [self.filterOnCheckedArray addObject:@"Rate"];
    [self.filterComparerArray addObject:Comaprer_GreaterThan];
    [self.filterPrameterCheckedArray addObject:@""];
    
    [self.filterValueCheckedArray addObject:[NSString stringWithFormat:@"%.f",highValue]];
    [self.filterOnCheckedArray addObject:@"Rate"];
    [self.filterComparerArray addObject:Comaprer_LessThanOrEqual];
    [self.filterPrameterCheckedArray addObject:@""];
    
    
}


-(void)clearPriceArray{

    NSMutableArray *removableIndices=[[NSMutableArray alloc]init];
    
    for (NSString *name in self.filterOnCheckedArray) {
        
        if ([name isEqualToString:@"Rate"]) {
            
            [removableIndices addObject:[NSNumber numberWithInt:(int)[self.filterOnCheckedArray indexOfObject:name]]];
            
        }
        
    }
    
    for (NSNumber *number in removableIndices) {
        
        [self.filterValueCheckedArray removeObjectAtIndex:[number intValue]];
        [self.filterOnCheckedArray removeObjectAtIndex:[number intValue]];
        [self.filterComparerArray removeObjectAtIndex:[number intValue]];
        [self.filterPrameterCheckedArray removeObjectAtIndex:[number intValue]];
        
    }
    
    
}




#pragma mark - Filter Actions


-(void)clear{
    
    [self.filterValueCheckedArray removeAllObjects];
    [self.filterOnCheckedArray removeAllObjects];
    [self.filterComparerArray  removeAllObjects];
    [self.filterPrameterCheckedArray removeAllObjects];
    [self clearUI];
    
    
}

-(void)clearUI{
    
    [checkTable changeUIForClear];
    [checkPriceTable changeCheckBoxesForClear];
    [checkPriceTable changeSliderUIForClear];
    
    
}

-(void)apply{
   
    [self.baseView.window.rootViewController dismissViewControllerAnimated:YES completion:^{
      
        
        [self.delegate filterApplied];
    
    }];
   
}

@end
