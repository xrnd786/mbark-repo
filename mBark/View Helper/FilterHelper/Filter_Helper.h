//
//  CategoryListView_Helper.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "DemoViewControllerProtocol.h"
#import "LSTabBarView.h"

#import "VerticalScrollTabBarView.h"


#define FilterOn @"FilterOn"
#define Value @"Value"
#define Comparer @"Comparer"
#define Parameter @"Parameter"


@protocol FiltersApplied <NSObject>

-(void)filterApplied;
-(void)filterCleared;

@end

@interface Filter_Helper : UIViewController<LSTabBarViewDelegate, DemoViewControllerProtocol>


//@property (nonatomic, retain)  UIImageView *controlPanelView;
//@property (nonatomic, retain)  UIImageView *borderImageView;
@property (nonatomic, retain)  UILabel     *selectedTabLabel;
@property (nonatomic, retain)  UIView     *localCopyOfContainer;
@property (nonatomic, retain)  VerticalScrollTabBarView     *localCopyOfTabView;

@property (strong,nonatomic) id<FiltersApplied> delegate;
@property (strong,nonatomic) NSMutableArray *filterValueCheckedArray;
@property (strong,nonatomic) NSMutableArray *filterOnCheckedArray;
@property (strong,nonatomic) NSMutableArray *filterPrameterCheckedArray;
@property (strong,nonatomic) NSMutableArray *filterComparerArray;
@property (strong,nonatomic) NSNumber *currentTabSelected;


-(id)initWithView:(UIView*)view andTabView:(VerticalScrollTabBarView*)tabView withLeftTabView:(UIView*)leftTabView :(UIView*)container withAttributeTitleArray:(NSArray*)attributeTitles withAttributeValuesList:(NSArray*)valuesList withBoundsArray:(NSArray*)valuesBound typeOfFilterControl:(NSArray*)filteControlArray attributeName:(NSArray*)names
    parameter:(NSArray*)parameters ;

-(void)updateFinalFilters:(NSArray *)finalFilters withTabIndex:(int)index;

-(void)clear;
-(void)apply;
@end
