//
//  ProductLitsCell.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeImageButton.h"
#import "MGSwipeTableCell.h"


@interface CartListCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet RNThemeLabel *productTitle;
@property (weak, nonatomic) IBOutlet RNThemeLabel *MRPLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *qtyTitle;
@property (weak, nonatomic) IBOutlet RNThemeLabel *qtyTextfld;
@property (weak, nonatomic) IBOutlet RNThemeLabel *totalPrice;


@end
