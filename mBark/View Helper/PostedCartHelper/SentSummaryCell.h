//
//  SummaryCell.h
//  mBark
//
//  Created by Xiphi Tech on 18/02/2015.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeLabel.h"
#import "RNThemeButton.h"

@interface SentSummaryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *label2;
@property (weak, nonatomic) IBOutlet RNThemeLabel *totalCartAmout;
@property (weak, nonatomic) IBOutlet RNThemeLabel *totalDiscount;
@property (weak, nonatomic) IBOutlet RNThemeButton *sendCartButton;

@end
