//
//  ProductLitsCell.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "CartListCell.h"

@implementation CartListCell

@synthesize productImage,productTitle,MRPLabel,qtyTextfld,qtyTitle,totalPrice;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)FavouriteToggle:(id)sender {
    
    UIButton *button=(UIButton*)sender;
    
    if (button.selected) {
        
        button.selected=FALSE;
        
    } else {
        
        button.selected=TRUE;
    
    }
    
}

-(void)willTransitionToState:(UITableViewCellStateMask)state{
   
}



@end
