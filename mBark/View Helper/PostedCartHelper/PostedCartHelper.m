//
//  PostedCartHelper.m
//  mBark
//
//  Created by Xiphi Tech on 30/03/2015.
//
//

#import "PostedCartHelper.h"

#import "AppTenantDefaults.h"
#import "AppSettingDefaults.h"
#import "AppMessagesDefaults.h"

#import "RNThemeLabel.h"
#import "UIImageView+WebCache.h"

#import "ProductViewController.h"
#import "ChatViewController.h"

#import "SentSummaryCell.h"
#import "CartListCell.h"

#import "addNewMessageSubject.h"
#import "MessagesCall_Response.h"
#import "AFPopupView.h"
#import "AuthChoiceController.h"

#import "Products.h"
#import "ProductThumbnail.h"
#import "Cart.h"
#import "User.h"

@interface PostedCartHelper()<UITableViewDataSource,UITableViewDelegate>
{
    CartMaster *cartMaster;
    NSArray *productsArray;
}

@property (strong,nonatomic) AFPopupView *popup;
@property (strong,nonatomic) addNewMessageSubject *MessageSubject;


@end

@implementation PostedCartHelper

@synthesize localTableCopy,baseView,popup,MessageSubject;

-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view withCartObj:(CartMaster*)CartObj  withController:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        cartMaster=CartObj;
        
        productsArray =[NSArray new];
        
        NSLog(@"Products %@",[cartMaster.products allObjects]);
        
        productsArray=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id IN %@",[cartMaster.products allObjects]]];
        
        [table setShowsVerticalScrollIndicator:NO];
        [table setBackgroundColor:[UIColor clearColor]];
        
        if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
            [table setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
            [table setLayoutMargins:UIEdgeInsetsZero];
        }
        
        table.delegate=self;
        table.dataSource=self;
        
        localTableCopy=table;
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        self.MessageSubject=[[addNewMessageSubject alloc]init];
        
        
    }
    
    return self;
}





#pragma mark - table View Functions


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.section==0) {
        
        return 150.0f;
        
    } else {
        
        return 170.0f;
        
    }
    
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section==0) {
        
        
        return nil;
        
    } else {
        
        RNThemeLabel *title=[[RNThemeLabel alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
        title.text=@"Summary";
        title.fontKey=@"generalFont";
        title.textColorKey=@"secondaryColor";
        
        UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
        [view addSubview:title];
        
        return view;
        
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    if (section==0) {
        
        return 0.0f;
        
    }else{
        
        return 50.0f;
    }
    
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
    
}



#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   
    return 2;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section==0) {
        
        return [cartMaster.products count];
        
    }else{
        
        return 1;
    }
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *headerTitle=@"";
    
    if ([cartMaster.products count]>0) {
        
        if (section==1) {
            
            headerTitle=@"Summary";
            
        }
        
    }
    
    return headerTitle;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        
        return [self getCellForPostedCarts:tableView indexPath:indexPath];
        
    } else {
        
        
        return [self getCellForSummary:tableView indexPath:indexPath];
        
    }
    
}



-(UITableViewCell*)getCellForPostedCarts:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath{
    
    static NSString *identifier = @"CartListCell";
    
    CartListCell *cell =(CartListCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"CartListCell" owner:self options:nil] objectAtIndex:0];
    }
    
    
    Cart *cartDetails=[[cartMaster.details allObjects]objectAtIndex:indexPath.row];
    Products *productObj=[productsArray objectAtIndex:indexPath.row];
    NSLog(@"Image is %@",productObj.thumbnail.p_id);
    
    cell.productImage.layer.shadowOffset = CGSizeMake(-5, 5);
    cell.productImage.layer.shadowRadius = 3;
    cell.productImage.layer.shadowOpacity = 0.3;
    
    
    [cell.productImage setImageWithURL:[NSURL URLWithString:productObj.thumbnail.p_id] placeholderImage:[UIImage imageNamed:@"bag.png"]];
   
    cell.productTitle.text=productObj.name;
    cell.MRPLabel.text=[NSString stringWithFormat:@"%0.f",[cartDetails.price floatValue]];
   
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.swipeBackgroundColor = [UIColor clearColor];
    
    cell.qtyTextfld.text=[NSString stringWithFormat:@"%@",cartDetails.quantity];
    
    cell.totalPrice.text=[NSString stringWithFormat:@"%.1f",([cartDetails.quantity floatValue] *[cartDetails.price floatValue])];
    
   
    return cell;
    
}

-(UITableViewCell*)getCellForSummary:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath{
    
    static NSString *identifier = @"SentSummaryCell";
    
    SentSummaryCell *cell =(SentSummaryCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"SentSummaryCell" owner:self options:nil] objectAtIndex:0];
    }
    
    
    cell.label2.text=[cell.label2.text stringByAppendingString:[NSString stringWithFormat:@"  %@",[AppTenantDefaults getOrganization]]];
    
    NSArray *priceList=[[cartMaster.details allObjects]valueForKeyPath:@"price"];
    NSArray *qtyList=[[cartMaster.details allObjects]valueForKeyPath:@"quantity"];
    
    double sum=0;
    //double discount=0;
    
    
    for (int i=0;i<[priceList count];i++) {
        
        CGFloat price=[priceList[i] floatValue];
        CGFloat qty=[qtyList[i] floatValue];
        
        sum+=price*qty;
        
    }
    
    cell.totalCartAmout.text=[NSString stringWithFormat:@"%.2f",sum];
    
    [cell.totalCartAmout sizeToFit];
    [cell.sendCartButton addTarget:self action:@selector(queryCartNow:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    ProductViewController *controller=[GlobalCall productViewController];
    controller.productID=[[cartMaster.products allObjects]objectAtIndex:indexPath.row];
    
    @try {
        
        [self.baseView.navigationController pushViewController:controller animated:NO];
        
    } @catch (NSException * ex) {
        
        [self.baseView.navigationController popToViewController:controller animated:YES];
        
        
    }@finally{
        
        
    }
    
    
}


#pragma mark - Query Now

-(void)queryCartNow:(id)sender{
    
    
    User *user=[[[GlobalCall getDelegate]fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([[AppSettingDefaults getRegistrationType] isEqualToString:User_Registration_Call_Type_Query] && user.password==nil) {
            
            AuthChoiceController *cont=[[AuthChoiceController alloc]initWithTheView:self.baseView.view];
            
            UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:cont];
            navigation.navigationBarHidden=TRUE;
            
            [self.baseView.view.window.rootViewController presentViewController:navigation animated:YES completion:nil];
            
        }else{
            
            
            Messages *thread=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"linkedTo == %@ and linkedToRef == %@",message_thread_cart,cartMaster.c_id]] lastObject];
            
            if (thread.m_id)
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD show];
                    
                    
                });
                
                
                ChatViewController  *chatViewController= [[ChatViewController alloc] initWithThreadObj:thread];
                
                
                [self.baseView.view.window.rootViewController presentViewController:chatViewController animated:YES completion:^{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        
                    });
                    
                }];
                
                
            } else {
                
                
                [self callEnquiryView];
                
                
            }
        
        }
        
    });

    
    
}


#pragma mark - Navigaton Calls

-(void)callEnquiryView{
    
    
    self.MessageSubject.subjectField.text=[NSString stringWithFormat:@"Enquiry Of Cart number %@",cartMaster.c_id];
    
    [self.MessageSubject.messageField becomeFirstResponder];
    
    self.popup = [AFPopupView popupWithView:self.MessageSubject.view];
    [self.popup show];
    
    
}

-(void)hide{
    
    [self.popup hide];
    
}

-(void)viewDismissed{
    
    
}

#pragma mark - functional Task

-(void)saveMessage{
    
    
    [self hide];
    
    self.MessageSubject.threadObj.linkedTo=LinkedTo_Cart;
    self.MessageSubject.threadObj.linkedToRef=cartMaster.c_id;
    
            [self performSelectorInBackground:@selector(sendMessageToServerInBackground) withObject:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
        
    
}



-(void)sendMessageToServerInBackground{
    
    [MessagesCall_Response callApiForMessageCreation:self.MessageSubject.threadObj];
    
}


@end
