//
//  PostedCartHelper.h
//  mBark
//
//  Created by Xiphi Tech on 30/03/2015.
//
//

#import <Foundation/Foundation.h>

#import "CartMaster.h"

@interface PostedCartHelper : NSObject

-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view withCartObj:(CartMaster*)CartObj  withController:(UIViewController*)controller;

@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIViewController *baseView;

-(void)hide;
-(void)saveMessage;


@end
