//
//  DescriptionView.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "DescriptionView.h"
#import "HomeCustomNavBarController.h"

@interface DescriptionView ()

@end

@implementation DescriptionView

@synthesize descriptionView=_descriptionView;

-(id)initWithDescription:(NSString*)string{
    
    self = [super init];
    if (self) {
        
        
        if ([GlobalCall isIpad]) {
            
            [[NSBundle mainBundle] loadNibNamed:@"DescriptionView" owner:self options:nil];
            
        }else{
            
            [[NSBundle mainBundle] loadNibNamed:@"DescriptionView" owner:self options:nil];
            
        }
        
        [self loadCustomViews:string];
        
        
    }
    return self;
    
    
}

-(void)loadCustomViews:(NSString*)string{
    
    [_descriptionView loadHTMLString:string baseURL:nil];
   
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@"Description"];
    
    self.navigationController.navigationBarHidden=FALSE;
}


- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
