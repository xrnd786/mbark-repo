//
//  DescriptionView.h
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface DescriptionView : UIViewController<UINavigationControllerDelegate,UIGestureRecognizerDelegate>

-(id)initWithDescription:(NSString*)string;

@property (weak, nonatomic) IBOutlet UIWebView *descriptionView;


@end
