//
//  RelatedProductsViewCell.m
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#define Thumbnail @"Thumbnail"
#define Id @"Id"
#define SKU @"SKU"
#define Discount @"Discount"
#define Name @"Name"
#define MRP @"MRP"
#define Discountinued @"Discountinued"
#define Image @"Image"

#import "GroupedProductsViewCell.h"
#import "RNThemeLabel.h"
#import "UIImageView+WebCache.h"
#import "UIImage+animatedGIF.h"

@interface GroupedProductsViewCell()

@property (strong,nonatomic) NSArray *relatedIds;

@end

@implementation GroupedProductsViewCell

@synthesize scrollMenu=_scrollMenu;
@synthesize baseView;
@synthesize relatedIds=_relatedIds;
@synthesize delegate=_delegate;

- (void)setUpACPScrollWithRelatedProductsThumnails:(NSArray*)thumbnails andIDs:(NSArray*)ids{
    
    _scrollMenu=[[ACPScrollMenu alloc]initWithFrame:CGRectMake(0, 0, self.baseView.frame.size.width, self.baseView.frame.size.height)];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    _relatedIds = [[NSArray alloc] initWithArray:ids];
    
    if ((NSObject*)thumbnails !=[NSNull null]) {
        
        for (int i = 0; i < [thumbnails count]; i++)
        {
            
            NSString *imgName = [[thumbnails objectAtIndex:i]  valueForKey:Image];
            NSString *imgSelectedName = [[thumbnails objectAtIndex:i] valueForKey:Image];
           
            __block ACPItem *item;
           
            
             item = [[ACPItem alloc] initACPItem:[UIImage imageNamed:@"bg.png"] iconImage:imgName  label:[NSString stringWithFormat:@"%@", [[ids objectAtIndex:i] valueForKey:Name]] andAction: ^(ACPItem *item) {
                
                NSLog(@"Block called! %d", i);
           
             }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [item.iconImageNew setImageWithURL:[NSURL URLWithString:imgName] placeholderImage:[UIImage animatedImageNamed:@"30.gif" duration:0.0f]];
                
            });

            RNThemeLabel *price=[[RNThemeLabel alloc]initWithFrame:CGRectMake(30, 130, 100, 40)];
            price.fontKey=@"smallFont";
            price.textColorKey=@"secondaryColor";
            [price applyTheme];
            price.text=([[ids objectAtIndex:i] valueForKey:MRP]!=[NSNull null])?[[ids objectAtIndex:i] valueForKey:MRP]:@"";
            
            [item addSubview:price];
            [item setHighlightedBackground:nil iconHighlighted:[UIImage imageNamed:imgSelectedName] textColorHighlighted:[UIColor redColor]];
            [array addObject:item];
            
        }
        
        [_scrollMenu setUpACPScrollMenu:array];
        [_scrollMenu setAnimationType:ACPZoomOut];
        _scrollMenu.delegate = self;
        
        [self.baseView addSubview:_scrollMenu];
        
    } 
    
    

}

- (void)awakeFromNib {
    // Initialization code
}



-(void)scrollMenu:(ACPScrollMenu *)menu didSelectIndex:(NSInteger)selectedIndex{
    
    [_delegate groupedItemSelected:[_relatedIds objectAtIndex:selectedIndex]];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
