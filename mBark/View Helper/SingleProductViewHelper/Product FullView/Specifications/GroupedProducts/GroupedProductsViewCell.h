//
//  RelatedProductsViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "ACPScrollMenu.h"

@protocol GroupedProductClicked <NSObject>

-(void)groupedItemSelected:(NSString*)idValue;

@end

@interface GroupedProductsViewCell : UITableViewCell<ACPScrollDelegate>

@property (strong, nonatomic)  id<GroupedProductClicked> delegate;
@property (strong, nonatomic)  ACPScrollMenu *scrollMenu;
@property (weak, nonatomic) IBOutlet UIView *baseView;


- (void)setUpACPScrollWithRelatedProductsThumnails:(NSArray*)thumbnails andIDs:(NSArray*)ids;


@end
