//
//  SpecificationView.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface SpecificationView : UIViewController<UINavigationControllerDelegate,UIGestureRecognizerDelegate>


-(id)initWithMetrixArray:(NSArray*)specsArray withGroupedIds:(NSMutableArray*)idArray withGroupedThumbnails:(NSMutableArray*)thumbnails;

@property (weak, nonatomic) IBOutlet UITableView *specsTable;

@end
