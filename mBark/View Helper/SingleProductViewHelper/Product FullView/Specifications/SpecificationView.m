//
//  SpecificationView.m
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import "SpecificationView.h"

#import "SpecificationViewHelper.h"
#import "AppProductDefaults.h"

#import "HomeCustomNavBarController.h"

#define General @"General"
#define Physical @"Physical"
#define Technical @"Technical"
#import "ProductAttributes.h"

@interface SpecificationView ()

@property (strong,nonatomic) SpecificationViewHelper *helper;
@property (strong,nonatomic) NSMutableArray *dataMetrix;
@property (strong,nonatomic) NSMutableArray *dataMetrixTitle;

@property (strong,nonatomic) NSMutableArray *groupIds;
@property (strong,nonatomic) NSMutableArray *groupThumbnails;


@end

@implementation SpecificationView

@synthesize helper=_helper;
@synthesize specsTable=_specsTable;
@synthesize dataMetrix=_dataMetrix;
@synthesize dataMetrixTitle=_dataMetrixTitle;
@synthesize groupIds=_groupIds;
@synthesize groupThumbnails=_groupThumbnails;


-(id)initWithMetrixArray:(NSArray*)specsArray withGroupedIds:(NSMutableArray*)idArray withGroupedThumbnails:(NSMutableArray*)thumbnails{
    
    self = [super init];
    if (self) {
        
        if ([GlobalCall isIpad]) {
            
            self.view= [[[NSBundle mainBundle] loadNibNamed:@"SpecificationView" owner:self options:nil] objectAtIndex:0];
            
        }else{
            
            self.view=[[[NSBundle mainBundle] loadNibNamed:@"SpecificationView" owner:self options:nil] objectAtIndex:0];
            
        }
        
        _dataMetrix=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)specsArray];
        _groupIds=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)idArray];
        _groupThumbnails=[[NSMutableArray alloc]initWithArray:(NSMutableArray*)thumbnails];
        
        [self initializeTable];
        [self loadCustomViews];
        
    }
    
    return self;
    
}


#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@"Specifications"];
    
    self.navigationController.navigationBarHidden=FALSE;
}

- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initializeTable{
   
    _dataMetrixTitle=[[NSMutableArray alloc]init];
    
    [_dataMetrixTitle addObject:@"General"];
    [_dataMetrixTitle addObject:@"Physical"];
    [_dataMetrixTitle addObject:@"Technical"];
    
    NSMutableArray *generalArray=[[NSMutableArray alloc]init];
    NSMutableArray *physicalArray=[[NSMutableArray alloc]init];
    NSMutableArray *technicalArray=[[NSMutableArray alloc]init];
    
    
    for (ProductAttributes *dictionary in _dataMetrix) {
        
        NSString *typeValue= dictionary.group;
        NSArray *arrayOfValues=[[NSArray alloc]initWithObjects:dictionary.attributeValue,dictionary.name,nil];
        
        if ([typeValue isEqualToString:General]) {
            
            [generalArray addObject:arrayOfValues];
            
        } else if([typeValue isEqualToString:Physical]) {
           
            [physicalArray addObject:arrayOfValues];
            
        }else{
          
            [technicalArray addObject:arrayOfValues];
            
        }
        
    }
    
    _dataMetrix=[[NSMutableArray alloc]init];
    
    [_dataMetrix addObject:generalArray];
    [_dataMetrix addObject:physicalArray];
    [_dataMetrix addObject:technicalArray];
    
}


-(void)loadCustomViews{
    
    _helper=[[SpecificationViewHelper alloc]initWithMainView:self.specsTable WithDataMetrix:_dataMetrix withDataMetrixTitle:_dataMetrixTitle withGroupedIds:_groupIds withGroupedThumbnails:_groupThumbnails];
    
}

@end
