//
//  ApplicationView.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface ApplicationView : UIViewController
- (IBAction)goBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *applicationWebView;

@end
