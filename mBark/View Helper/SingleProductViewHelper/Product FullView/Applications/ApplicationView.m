//
//  ApplicationView.m
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import "ApplicationView.h"

@interface ApplicationView ()

@end

@implementation ApplicationView

@synthesize applicationWebView=_applicationWebView;

-(id)init{
    
    self = [super init];
    if (self) {
        
        
        if ([GlobalCall isIpad]) {
            
            [[NSBundle mainBundle] loadNibNamed:@"ApplicationView" owner:self options:nil];
            
        }else{
            
            [[NSBundle mainBundle] loadNibNamed:@"ApplicationView" owner:self options:nil];
            
        }
        
        self.navigationController.navigationBarHidden=FALSE;
        [self loadCustomViews];
        
        
    }
    return self;
    
    
}

-(void)loadCustomViews{
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [_applicationWebView loadHTMLString:htmlString baseURL:nil];
    self.navigationController.navigationBar.backgroundColor=AppBarColor;
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Content

- (void) loadWebViewWithHTML{
    
    
    
}


- (IBAction)goBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
