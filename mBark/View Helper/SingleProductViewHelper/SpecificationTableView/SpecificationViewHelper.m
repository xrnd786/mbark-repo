//
//  ProductListViewHelper.m
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import "SpecificationViewHelper.h"

#import "SpecificationViewCell.h"
#import "GroupedProductsViewCell.h"

@interface SpecificationViewHelper ()<GroupedProductClicked>

@property (strong,nonatomic) NSMutableArray *dataMetrix;
@property (strong,nonatomic) NSMutableArray *dataMetrixTitle;


@property (strong,nonatomic) NSMutableArray *groupIds;
@property (strong,nonatomic) NSMutableArray *groupThumbnails;

@end


@implementation SpecificationViewHelper

@synthesize localTableCopy,dataMetrix,dataMetrixTitle,groupIds,groupThumbnails;

-(id)initWithMainView:(UITableView*)tableView WithDataMetrix:(NSMutableArray*)array withDataMetrixTitle:(NSMutableArray*)title withGroupedIds:(NSMutableArray*)idArray withGroupedThumbnails:(NSMutableArray*)thumbnails{
    
    self = [super init];
    
    if (self) {
        
        dataMetrix=array;
        dataMetrixTitle=title;
        
        if ([thumbnails count]!=0) {
            
            [dataMetrixTitle addObject:@"Products in group"];
            
        }
        
        groupIds=idArray;
        groupThumbnails=thumbnails;
        
        [tableView setShowsVerticalScrollIndicator:NO];
        [tableView setBackgroundColor:[UIColor clearColor]];
        [tableView setSeparatorColor:[UIColor clearColor]];
        
        if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [tableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
        tableView.delegate=self;
        tableView.dataSource=self;
    
        localTableCopy=tableView;
        tableView.frame=CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 40*[dataMetrix count]);
        
       
    }
    
    return self;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



#pragma mark - table View Functions


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([groupIds count]!=0) {
        
        if ([dataMetrix count]==indexPath.section) {
            
            return 150.0f;
            
        }else{
            
            return heightOfTableCell;
        }

    } else {
        
        return heightOfTableCell;
    
    }
    
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if ([dataMetrix count]==section) {
        
        return 40.0f;
        
    } else {
       
        if ( (int)[[dataMetrix objectAtIndex:section] count]==0) {
            
            return 0;
            
        } else {
            
            return 40.0f;
            
        }
    }
    
   
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *header=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    RNThemeLabel *label=[[RNThemeLabel alloc]initWithFrame:CGRectMake(10, 5, 320, 30)];
   
    if ([groupThumbnails count]!=0) {
        
        if (section==[dataMetrix count]) {
            
            label.text=@"Products in group";
            
        }else{
            
            label.text=[dataMetrixTitle objectAtIndex:section];
            
        }
        
    }else{
        
        label.text=[dataMetrixTitle objectAtIndex:section];
        
    }
    
    label.textColorKey=@"secondaryColor";
    label.fontKey=@"generalFont";
    [label applyTheme];
    
    [header addSubview:label];
    
    header.backgroundColor=[UIColor colorWithWhite:0.8f alpha:0.9f];
    
    
    if ([groupThumbnails count]!=0) {
        
        if (section==[dataMetrix count]) {
            
           return header;
            
        }else{
            
            
            if ((int)[[dataMetrix objectAtIndex:section] count]==0) {
                
                return nil;
                
            } else{
                
                return header;
                
            }
        
        }
        
    }else{
        
        if ((int)[[dataMetrix objectAtIndex:section] count]==0) {
            
            return nil;
            
        } else{
            
            return header;
            
        }
    
    
    }
    
    
   
    
    return header;
    
}


#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   
    if ([groupIds count]!=0) {
        
        return ((int)[dataMetrix count]+1);

        
    } else{
        
        return [dataMetrix count];
        
    }
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section==[dataMetrix count]) {
    
        return 1;
        
    } else {
        
        return (int)[[dataMetrix objectAtIndex:section] count];
        
    }
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==[dataMetrix count]) {

        return [self getCellForGroupType:tableView WithIndex:indexPath];
        
    } else {
        
        return [self getCellForSpecsType:tableView WithIndex:indexPath];
   
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - Custom Cell Views

-(SpecificationViewCell*)getCellForSpecsType:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
   
    static NSString *identifier = @"SpecificationViewCell";
    SpecificationViewCell *cell =(SpecificationViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"SpecificationViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    cell.titleLabel.text=[[[dataMetrix objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectAtIndex:1];
    cell.valueLabel.text=[[[dataMetrix objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectAtIndex:0];
    
    return cell;

}

-(GroupedProductsViewCell*)getCellForGroupType:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    static NSString *identifier = @"GroupedProductsViewCell";
    GroupedProductsViewCell *cell =(GroupedProductsViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"GroupedProductsViewCell" owner:self options:nil] objectAtIndex:0];
    
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    cell.delegate=self;
    
    
    if ((NSObject*)groupIds !=[NSNull null]) {
        
        [cell setUpACPScrollWithRelatedProductsThumnails:groupThumbnails andIDs:groupIds];
        
    }
    
    return cell;
}

-(void)groupedItemSelected:(NSString *)idValue{
    
}


@end
