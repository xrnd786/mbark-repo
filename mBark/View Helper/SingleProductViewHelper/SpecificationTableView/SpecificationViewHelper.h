//
//  ProductListViewHelper.h
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import <UIKit/UIKit.h>

#define heightOfTableCell 40.0f

@interface SpecificationViewHelper : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *localTableCopy;


-(id)initWithMainView:(UITableView*)tableView WithDataMetrix:(NSMutableArray*)array withDataMetrixTitle:(NSMutableArray*)title withGroupedIds:(NSMutableArray*)idArray withGroupedThumbnails:(NSMutableArray*)thumbnails;

@end
