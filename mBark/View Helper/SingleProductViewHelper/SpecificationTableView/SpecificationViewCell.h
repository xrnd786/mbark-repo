//
//  SpecificationViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"

@interface SpecificationViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *valueLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *titleLabel;

@end
