//
//  ProductListViewHelper.h
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import <UIKit/UIKit.h>

#import "Products.h"

@interface ProductViewHelper : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIView *localView;
@property (strong,nonatomic) UIViewController *base;


-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view :(UIViewController*)baseViewController  WithDictionary:(Products*)dictionary withIsFavourite:(BOOL)isItFav withIsAdded:(BOOL)isItInCart ;

-(void)callEnquiryView;
-(void)reloadTheViews;


@property (strong,nonatomic) NSMutableArray *imageList;
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *price;
@property (strong,nonatomic) NSString *discountPrice;
@property (strong,nonatomic) NSArray *applicationList;
@property (strong,nonatomic) NSString *thumbnailImage;

@property (strong,nonatomic) NSString *stockLeft;
@property (strong,nonatomic) NSString *unitValue;
@property (strong,nonatomic) NSString *descriptionValue;
@property (strong,nonatomic) NSMutableArray *specsArray;

@property (strong,nonatomic) UIScrollView *embededScrollView;

@property (strong,nonatomic) NSMutableArray *related_thumbnails;
@property (strong,nonatomic) NSMutableArray *related_ids;

@property (strong,nonatomic) NSMutableArray *groupedThumbnails;
@property (strong,nonatomic) NSMutableArray *groupedIds;

@property (strong,nonatomic) Products *productDictionary;


-(void)updateAllData;
-(void)clearData;

-(void)hide;
-(void)saveMessage;

@end
