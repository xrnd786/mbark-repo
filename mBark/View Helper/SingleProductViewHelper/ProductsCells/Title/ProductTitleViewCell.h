//
//  ProductTitleViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeImageButton.h"
#import "RNThemeLabel.h"
#import "RNThemeButton.h"

@interface ProductTitleViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet RNThemeLabel *priceLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *mrpLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountinPercent;
@property (weak, nonatomic) IBOutlet RNThemeLabel *inStockDetails;

@end
