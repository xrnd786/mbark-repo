//
//  SpecificationCell.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface SpecificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *rightDetailsButton;

@end
