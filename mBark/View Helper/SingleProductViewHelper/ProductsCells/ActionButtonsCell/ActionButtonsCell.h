//
//  ProductTitleViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeButton.h"
#import "RNThemeImageButton.h"

@protocol productAdded <NSObject>

-(void)addedToCartForCheckOut:(id)sender;
-(void)addedToWishList:(id)sender;
-(void)addedToCart:(id)sender;


@end

@interface ActionButtonsCell : UITableViewCell

@property (strong, nonatomic) id<productAdded> delegate;

@property (weak, nonatomic) IBOutlet RNThemeImageButton *buttonWishList;
@property (weak, nonatomic) IBOutlet RNThemeButton *buttonCartForCheckout;
@property (weak, nonatomic) IBOutlet RNThemeImageButton *buttonCart;

- (IBAction)addToCartForCheckout:(id)sender;

- (IBAction)addToCart:(id)sender;

- (IBAction)addToFavAction:(id)sender;

@end
