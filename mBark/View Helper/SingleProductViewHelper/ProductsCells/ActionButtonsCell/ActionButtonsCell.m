//
//  ProductTitleViewCell.m
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import "ActionButtonsCell.h"

@implementation ActionButtonsCell

@synthesize buttonWishList,buttonCart,buttonCartForCheckout,delegate;

- (void)awakeFromNib {
    
    
    [self.buttonWishList applyTheme];
    [self.buttonCart applyTheme];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)addToCartForCheckout:(id)sender {
   
    [self.delegate addedToCartForCheckOut:sender];
}

- (IBAction)addToCart:(id)sender {
    
    [self.delegate addedToCart:sender];
    
}



- (IBAction)addToFavAction:(id)sender {
    
    [self.delegate addedToWishList:sender];
    
}
@end
