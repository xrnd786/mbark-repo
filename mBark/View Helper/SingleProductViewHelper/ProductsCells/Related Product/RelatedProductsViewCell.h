//
//  RelatedProductsViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "ACPScrollMenu.h"

@protocol RelatedProductClicked <NSObject>

-(void)relatedItemSelected:(NSString*)idValue;

@end

@interface RelatedProductsViewCell : UITableViewCell<ACPScrollDelegate>

@property (strong, nonatomic)  id<RelatedProductClicked> delegate;
@property (strong, nonatomic)  ACPScrollMenu *scrollMenu;
@property (weak, nonatomic) IBOutlet UIView *baseView;


- (void)setUpACPScrollWithRelatedProductsThumnails:(NSArray*)thumbnails andIDs:(NSArray*)ids;


@end
