//
//  DescriptionCell.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeLabel.h"

@interface DescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *descriptionDetails;
@property (weak, nonatomic) IBOutlet UIButton *rightNavigationButton;

@end
