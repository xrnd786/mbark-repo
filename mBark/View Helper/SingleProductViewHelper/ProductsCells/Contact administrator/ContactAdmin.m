//
//  ProductTitleViewCell.m
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import "ContactAdmin.h"

@implementation ContactAdmin

@synthesize contactSeller,delegate;


- (void)awakeFromNib {
    
        // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)contactSellerClicked:(id)sender {
    
    [self.delegate openMessageEditor];
    
}

@end
