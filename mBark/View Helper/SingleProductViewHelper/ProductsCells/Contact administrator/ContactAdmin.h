//
//  ProductTitleViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 24/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeButton.h"

@protocol contactCalled <NSObject>

-(void)openMessageEditor;

@end

@interface ContactAdmin : UITableViewCell

@property (strong, nonatomic) id<contactCalled> delegate;
@property (weak, nonatomic) IBOutlet RNThemeButton *contactSeller;
- (IBAction)contactSellerClicked:(id)sender;


@end
