//
//  PageScrollViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 26/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "ScrollMenu.h"

@protocol scrollItemClicked <NSObject>

-(void)itemClicked:(int)value;


@end

@interface PageScrollViewCell : UITableViewCell<ScrollDelegate>


@property (strong,nonatomic) id<scrollItemClicked> delegate;
@property (strong, nonatomic)  ScrollMenu *scrollMenu;
@property (weak, nonatomic) IBOutlet UIView *baseView;

-(void)setUpACPScroll;


@end
