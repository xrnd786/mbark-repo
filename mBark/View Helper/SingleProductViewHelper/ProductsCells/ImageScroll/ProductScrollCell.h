//
//  ProductLitsCell.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeImageButton.h"

@interface ProductScrollCell : UITableViewCell

@property (weak, nonatomic) IBOutlet RNThemeLabel *offerLabel;


@end
