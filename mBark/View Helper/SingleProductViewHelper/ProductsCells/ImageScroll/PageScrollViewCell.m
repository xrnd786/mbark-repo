//
//  PageScrollViewCell.m
//  mBark
//
//  Created by Xiphi Tech on 26/11/2014.
//
//

#import "PageScrollViewCell.h"

@implementation PageScrollViewCell

@synthesize scrollMenu=_scrollMenu;
@synthesize baseView,delegate;

- (void)setUpACPScroll {
    
    _scrollMenu=[[ScrollMenu alloc]initWithFrame:CGRectMake(0, 0, self.baseView.frame.size.width, self.baseView.frame.size.height)];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (int i = 1; i < 4; i++)
    {
        
        NSString *imgName = @"offer1.png";
        NSString *imgSelectedName = @"offer1.png";
        //You can choose between work with delegates or with blocks
        //This sample project has commented the delegate functionality
        //ACPItem *item = [[ACPItem alloc] initACPItem:[UIImage imageNamed:@"bg.png"] iconImage:[UIImage imageNamed:imgName] andLabel:@"Test"];
        //Item working with blocks
        
        
        ScrollItem *item = [[ScrollItem alloc] initScrollItem:[UIImage imageNamed:@"offer1.jpg"] iconImage:[UIImage imageNamed:imgName]label:[NSString stringWithFormat:@"Test%d", i] andAction: ^(ScrollItem *item) {
            
            item.layer.borderColor=[UIColor redColor].CGColor;
            item.layer.borderWidth=1.0f;
            [self.delegate itemClicked:i];
            
            
            NSLog(@"Block called! %d", i);
            //DO somenthing here
        }];
        
        //Set highlighted behaviour
        [item setHighlightedBackground:nil iconHighlighted:[UIImage imageNamed:imgSelectedName] textColorHighlighted:[UIColor redColor]];
        [array addObject:item];
    }
    
    _scrollMenu.backgroundColor=[UIColor whiteColor];
    _scrollMenu.alpha=0.7f;
    [_scrollMenu setUpScrollMenu:array];
    
    //We choose an animation when the user touch the item (you can create your own animation)
    [_scrollMenu setAnimationType:ZoomOut];
    _scrollMenu.delegate = self;
    [self.baseView addSubview:_scrollMenu];
    
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
