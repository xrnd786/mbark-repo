//
//  ProductTabbedCell.h
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface ProductTabbedCell : UITableViewCell


@property (strong,nonatomic) NSMutableArray *viewControllers;

-(void)setUpViews:(NSArray*)views withFrames:(CGRect)cellFrame;

@end
