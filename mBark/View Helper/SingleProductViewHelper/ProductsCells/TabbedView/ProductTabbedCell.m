//
//  ProductTabbedCell.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "ProductTabbedCell.h"

#import "DAPagesContainer.h"

@interface ProductTabbedCell()

@property (strong, nonatomic) DAPagesContainer *pagesContainer;

@end

@implementation ProductTabbedCell

@synthesize viewControllers;

- (void)awakeFromNib {
    
    self.pagesContainer = [[DAPagesContainer alloc] init];
    //[self.pagesContainer willMoveToParentViewController:self];
    self.pagesContainer.view.frame = self.bounds;
    self.pagesContainer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.contentView addSubview:self.pagesContainer.view];
    //[self.pagesContainer didMoveToParentViewController:self];
    
    // Initialization code
}



-(void)setUpViews:(NSArray*)views withFrames:(CGRect)cellFrame{

    self.viewControllers =(NSMutableArray*)views;
   
    self.pagesContainer.viewControllers = self.viewControllers;
    self.contentView.frame=cellFrame;
    
    self.pagesContainer.view.frame=cellFrame;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
