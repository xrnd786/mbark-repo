//
//  ApplicationCell.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeLabel.h"


@interface ApplicationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *applicationValuesTextView;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end
