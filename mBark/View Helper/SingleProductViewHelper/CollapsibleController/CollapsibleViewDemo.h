//
//  CollapsibleViewDemo.h
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "CollapseClick.h"

@protocol collapseButtonClicked <NSObject>

-(void)buttonClicked;

@end

@interface CollapsibleViewDemo : UIViewController<CollapseClickDelegate>

{
    
}

-(void)loadViewFromCustomCell;

@property (strong,nonatomic) id<collapseButtonClicked>delegate;

@property (weak, nonatomic) IBOutlet CollapseClick *myCollapseClick;
@property (strong, nonatomic) IBOutlet UITableView *generalTable;
@property (strong, nonatomic) IBOutlet UITableView *physicalTable;
@property (strong, nonatomic) IBOutlet UITableView *technicalTable;

@property (strong,nonatomic) NSNumber *totalHeight;

@end
