//
//  CollapsibleViewDemo.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "CollapsibleViewDemo.h"

#import "SpecificationViewHelper.h"
#import "SpecificationViewCell.h"
#import "ProductTitleViewCell.h"


@interface CollapsibleViewDemo ()

@property (strong,nonatomic) SpecificationViewHelper *helper1;
@property (strong,nonatomic) SpecificationViewHelper *helper2;
@property (strong,nonatomic) SpecificationViewHelper *helper3;

@property (strong,nonatomic) NSMutableArray *openCloseMetrix;
@property (strong,nonatomic) NSMutableArray *dataMetrix;
@property (strong,nonatomic) NSMutableArray *dataMetrixTitle;


@end

@implementation CollapsibleViewDemo

@synthesize helper1,generalTable,myCollapseClick,openCloseMetrix,dataMetrix,dataMetrixTitle,physicalTable,technicalTable,helper2,helper3,totalHeight,delegate;

-(id)init{
    
    self = [super init];
    if (self) {
        
        if ([GlobalCall isIpad]) {
            
           self.view= [[[NSBundle mainBundle] loadNibNamed:@"CollapsibleViewDemo" owner:self options:nil] objectAtIndex:0];
            
        }else{
            
            self.view=[[[NSBundle mainBundle] loadNibNamed:@"CollapsibleViewDemo" owner:self options:nil] objectAtIndex:0];
            
        }
        
        [self loadViewFromCustomCell];
        
        [self loadCustomViews];
       
    }

    return self;
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}


-(void)loadCustomViews{
    
    myCollapseClick.CollapseClickDelegate = self;
    [myCollapseClick reloadCollapseClick];
    
    for (int i=0;i<[dataMetrix count];i++) {
        
        [myCollapseClick openCollapseClickCellAtIndex:i animated:NO];
        
    }
    
   //self.view.frame=CGRectMake(myCollapseClick.frame.origin.x, myCollapseClick.frame.origin.y, myCollapseClick.frame.size.width, 480);
    //myCollapseClick.frame=self.view.frame;
    
}

-(void)loadViewFromCustomCell{
    
    openCloseMetrix =[[NSMutableArray alloc]init];
    dataMetrix=[[NSMutableArray alloc]init];
    dataMetrixTitle=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *openDictionary=[[NSMutableDictionary alloc]init];
    [openDictionary setValue:[NSNumber numberWithBool:YES] forKey:@"isOpen"];
    
    [dataMetrixTitle addObject:@"General"];
    [dataMetrixTitle addObject:@"Physical"];
    [dataMetrixTitle addObject:@"Technical"];
    
    
    [dataMetrix addObject:[[NSMutableArray alloc]initWithObjects:[NSArray arrayWithObjects:@"Black",@"Color", nil],[NSArray arrayWithObjects:@"Valentino",@"Brand", nil],
                           [NSArray arrayWithObjects:@"XS",@"Size", nil], nil]];
    
    
    [dataMetrix addObject:[[NSMutableArray alloc]initWithObjects:[NSArray arrayWithObjects:@"Black",@"Color", nil],[NSArray arrayWithObjects:@"Valentino",@"Brand", nil],
                           [NSArray arrayWithObjects:@"XS",@"Size", nil], nil]];
    
    [dataMetrix addObject:[[NSMutableArray alloc]initWithObjects:[NSArray arrayWithObjects:@"Black",@"Color", nil],[NSArray arrayWithObjects:@"Valentino",@"Brand", nil],
                           [NSArray arrayWithObjects:@"XS",@"Size", nil], nil]];
    
    for (int i=0;i<[dataMetrix count];i++) {
        
        [openCloseMetrix addObject:openDictionary];
        //NSArray *arr;
        //arr=array;
        
    }

    totalHeight=[NSNumber numberWithFloat:[self getTotalCollapseViewHeight]];
    
    
   // helper1=[[SpecificationViewHelper alloc]initWithMainView:self.generalTable WithDataMetrix:dataMetrix];
    //helper2=[[SpecificationViewHelper alloc]initWithMainView:self.physicalTable WithDataMetrix:dataMetrix];
    //helper3=[[SpecificationViewHelper alloc]initWithMainView:self.technicalTable WithDataMetrix:dataMetrix];
    
}


- (void)didReceiveMemoryWarning {
   
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Collapse Click Delegate


// Required Methods
-(int)numberOfCellsForCollapseClick {

    return (int)[dataMetrix count];

}

-(NSString *)titleForCollapseClickAtIndex:(int)index {
   
    return [dataMetrixTitle objectAtIndex:index];
    
}

-(UIView *)viewForCollapseClickContentViewAtIndex:(int)index {
    
    
    switch (index) {
       
        case 0:
            
            return self.generalTable;
            break;
        
        case 1:
            
            return self.physicalTable;
            break;
        
        case 2:
           
            return self.technicalTable;
            break;
            
        default:
            return self.generalTable;

            break;
    }
    
}


-(void)didClickCollapseClickCellAtIndex:(int)index isNowOpen:(BOOL)open {
    
    NSDictionary *openValue=[openCloseMetrix objectAtIndex:index];
    [openValue setValue:[NSNumber numberWithBool:open] forKey:@"isOpen"];
    [openCloseMetrix replaceObjectAtIndex:index withObject:openValue];
    
    totalHeight=[NSNumber numberWithFloat:[self getTotalCollapseViewHeight]];
    
    //[self.delegate buttonClicked];
    
}


#pragma mark - PUBLIC METHODS

-(CGFloat)getTotalCollapseViewHeight{
    
    CGFloat totalH=0;
    
    for (NSDictionary *isOpen in openCloseMetrix) {
        
        if ([[isOpen valueForKey:@"isOpen"] boolValue]) {
            
            int index=(int)[openCloseMetrix indexOfObject:isOpen] ;
            NSArray *totalNumber=[dataMetrix objectAtIndex:index];
            totalH+=[totalNumber count]*heightOfTableCell;
    
        }
        
        totalH+=kCCHeaderHeight;
    }
    
    return totalH;
    
}


// Optional Methods
/*
-(UIColor *)colorForCollapseClickTitleViewAtIndex:(int)index {
    return [UIColor colorWithRed:223/255.0f green:47/255.0f blue:51/255.0f alpha:1.0];
}


-(UIColor *)colorForTitleLabelAtIndex:(int)index {
    return [UIColor colorWithWhite:1.0 alpha:0.85];
}

-(UIColor *)colorForTitleArrowAtIndex:(int)index {
    return [UIColor colorWithWhite:0.0 alpha:0.25];
}
*/



@end
