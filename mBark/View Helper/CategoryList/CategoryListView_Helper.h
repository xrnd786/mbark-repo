//
//  CategoryListView_Helper.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RATreeView.h"

@protocol categoryClicked <NSObject>

-(void)categoryClickedWithCategoryName:(NSString*)name;
-(void)applicationClickedWithApplicationName:(NSString*)applicationName andProductsIds:(NSArray*)Id;

@end

@interface CategoryListView_Helper : UIViewController



@property (strong,nonatomic) id<categoryClicked> delegate;

-(id)initWithView:(UIView*)view;
-(id)initWithViewForApplications:(UIView*)view;


@end
