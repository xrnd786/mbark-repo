//
//  CategoryListView_Helper.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "CategoryListView_Helper.h"

#import "RADataObject.h"

#import "RATableViewCell.h"
#import "Category_DataHelper.h"
#import "Application_DataHelper.h"

#import "UIImage+animatedGIF.h"
#import "UIImageView+WebCache.h"

#import "ProductCategory.h"
#import "CategoryImage.h"

@interface CategoryListView_Helper () <RATreeViewDelegate, RATreeViewDataSource>
{
    BOOL isApplication;
}
@property (strong, nonatomic) NSArray *data;
@property (weak, nonatomic) RATreeView *treeView;

@property (strong, nonatomic) UIBarButtonItem *editButton;
@property (strong, nonatomic) Category_DataHelper *data_helper;
@property (strong, nonatomic) Application_DataHelper *applicationdata_helper;

@property (strong, nonatomic) NSMutableArray  *applicationIds;
@property (strong, nonatomic) NSMutableArray  *applicationNames;
@property (strong,nonatomic) NSArray *array;


@end



@implementation CategoryListView_Helper

@synthesize data_helper=_data_helper;
@synthesize delegate=_delegate;
@synthesize applicationdata_helper=_applicationdata_helper;
@synthesize applicationIds,applicationNames,array;

-(id)initWithView:(UIView*)view
{

    self = [super init];

     if (self) {
        
         _data_helper=[[Category_DataHelper alloc]init];
        [self loadData];
        
        RATreeView *treeView = [[RATreeView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        treeView.delegate = self;
        treeView.dataSource = self;
        treeView.separatorStyle = RATreeViewCellSeparatorStyleSingleLine;
        
        [treeView reloadData];
        [treeView setBackgroundColor:[UIColor colorWithWhite:0.97 alpha:1.0]];
        
        
        self.treeView = treeView;
        [view insertSubview:treeView atIndex:0];
        
        [self updateNavigationItemButton];
        
        [self.treeView registerNib:[UINib nibWithNibName:NSStringFromClass([RATableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RATableViewCell class])];
        
         isApplication=FALSE;
        
     }
    
     return self;
    
}



-(id)initWithViewForApplications:(UIView*)view
{
    
    self = [super init];
    
    if (self) {
        
        _applicationdata_helper=[[Application_DataHelper alloc]init];
        
        self.applicationIds=[[NSMutableArray alloc]init];
        self.applicationNames=[[NSMutableArray alloc]init];
        
        [self performSelectorInBackground:@selector(loadApplicationData) withObject:nil];
        
        RATreeView *treeView = [[RATreeView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        treeView.delegate = self;
        treeView.dataSource = self;
        treeView.separatorStyle = RATreeViewCellSeparatorStyleSingleLine;
        
        [treeView reloadData];
        [treeView setBackgroundColor:[UIColor colorWithWhite:0.97 alpha:1.0]];
        
        
        self.treeView = treeView;
        [view insertSubview:treeView atIndex:0];
        
        [self.treeView registerNib:[UINib nibWithNibName:NSStringFromClass([RATableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RATableViewCell class])];
        isApplication=TRUE;
        
        
    }
    return self;
    
}


#pragma mark - Actions

- (void)editButtonTapped:(id)sender
{
    [self.treeView setEditing:!self.treeView.isEditing animated:YES];
    [self updateNavigationItemButton];
}

- (void)updateNavigationItemButton
{
    UIBarButtonSystemItem systemItem = self.treeView.isEditing ? UIBarButtonSystemItemDone : UIBarButtonSystemItemEdit;
    self.editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:systemItem target:self action:@selector(editButtonTapped:)];
    self.navigationItem.rightBarButtonItem = self.editButton;
}


#pragma mark TreeView Delegate methods


- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item
{
    return 44;
}

- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item
{
    return YES;
}

- (void)treeView:(RATreeView *)treeView willExpandRowForItem:(id)item
{
   
    RATableViewCell *cell = (RATableViewCell *)[treeView cellForItem:item];
    RADataObject *dataObject = item;
    
    if ([dataObject.children count]>0) {
        
        cell.detailText.titleLabel.text=@"-";
   
    }
    
}


-(void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item{
    
    RADataObject *dataObject = item;
    
    if ([dataObject.children count]>0) {
      
        
    }else{
        
        [self goToProductList:dataObject.name];
        
    }
    
}

- (void)treeView:(RATreeView *)treeView willCollapseRowForItem:(id)item
{
    RATableViewCell *cell = (RATableViewCell *)[treeView cellForItem:item];
    RADataObject *dataObject = item;
    
    if ([dataObject.children count]>0) {
        
        cell.detailText.titleLabel.text=@"+";
        
    }
   
}

- (void)treeView:(RATreeView *)treeView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowForItem:(id)item
{
    if (editingStyle != UITableViewCellEditingStyleDelete) {
        return;
    }
    
    RADataObject *parent = [self.treeView parentForItem:item];
    NSInteger index = 0;
    
    if (parent == nil) {
        index = [self.data indexOfObject:item];
        NSMutableArray *children = [self.data mutableCopy];
        [children removeObject:item];
        self.data = [children copy];
        
    } else {
        index = [parent.children indexOfObject:item];
        [parent removeChild:item];
    }
    
    [self.treeView deleteItemsAtIndexes:[NSIndexSet indexSetWithIndex:index] inParent:parent withAnimation:RATreeViewRowAnimationRight];
    if (parent) {
        [self.treeView reloadRowsForItems:@[parent] withRowAnimation:RATreeViewRowAnimationNone];
    }
}

#pragma mark TreeView Data Source

- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item
{
    RADataObject *dataObject = item;
    
    NSInteger level = [self.treeView levelForCellForItem:item];
    NSInteger numberOfChildren = [dataObject.children count];
    
    BOOL expanded = [self.treeView isCellForItemExpanded:item];
    
    RATableViewCell *cell = [self.treeView dequeueReusableCellWithIdentifier:NSStringFromClass([RATableViewCell class])];
    NSString *detailLabel;
    
    [cell setupWithTitle:dataObject.name detailText:detailLabel level:level additionButtonHidden:!expanded];
    
    NSURL *url = [NSURL URLWithString:dataObject.image];
    
    if (url && url.scheme && url.host)
    {
        
        [cell.iconImage setImageWithURL:[NSURL URLWithString:dataObject.image] placeholderImage:[UIImage animatedImageNamed:@"30.gif" duration:0.0f]];
        
    }else{
        
        [cell.iconImage setImage:[UIImage imageNamed:dataObject.image]];
        
    }
    
    
    
    if (numberOfChildren>0) {
        
        cell.detailText.hidden=FALSE;
        cell.detailText.titleLabel.text=@"+";
    } else {
        
        cell.detailText.hidden=TRUE;
        cell.detailText.titleLabel.text=@"";
    }
    
   
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    __weak typeof(self) weakSelf = self;
    
    cell.additionButtonTapAction = ^(id sender){
        
        if (![weakSelf.treeView isCellForItemExpanded:dataObject] || weakSelf.treeView.isEditing) {
            return;
        }
        
        RADataObject *newDataObject = [[RADataObject alloc] initWithName:@"Added value" children:@[] andImage:@""];
        [dataObject addChild:newDataObject];
        [weakSelf.treeView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:0] inParent:dataObject withAnimation:RATreeViewRowAnimationLeft];
        [weakSelf.treeView reloadRowsForItems:@[dataObject] withRowAnimation:RATreeViewRowAnimationNone];
    
    };
    
    return cell;
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    if (item == nil) {
        return [self.data count];
    }
    
    RADataObject *data = item;
    return [data.children count];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    RADataObject *data = item;
    if (item == nil) {
        return [self.data objectAtIndex:index];
    }
    
    return data.children[index];
}

#pragma mark - Helpers

- (void)loadData
{
    
    NSMutableArray *main_array=[[NSMutableArray alloc]init];
    array=[[NSArray alloc]initWithArray:[_data_helper getAllCategories]];
    
    for (ProductCategory *categories in array) {
        
        
        if ([categories.children count]>0) {
        
            NSMutableArray *mainChildren_array=[[NSMutableArray alloc]init];
            
            for (ProductCategory *subCategories in categories.children) {
                
                RADataObject *childObject = [RADataObject dataObjectWithName:subCategories.name children:nil withImage:subCategories.image.image];
                [mainChildren_array addObject:childObject];
            }
            
            RADataObject *object = [RADataObject dataObjectWithName:categories.name children:mainChildren_array withImage:categories.image.image];
            [main_array addObject:object];
            
        }else{
            
            RADataObject *object = [RADataObject dataObjectWithName:categories.name children:nil withImage:categories.image.image];
            [main_array addObject:object];
            
        }
        
    }
    
    self.data = [NSArray arrayWithArray:main_array];
    
}


-(void)loadApplicationData{
    
    NSMutableArray *main_array=[[NSMutableArray alloc]init];
    array=[[NSArray alloc]initWithArray:[_applicationdata_helper loadApplicationsList]];
 
    for (NSDictionary *categories in array) {
        
        RADataObject *object;
        
        if ([categories valueForKey:JSON_RES_Application_Image] == [NSNull null])
        {
            object =[[RADataObject alloc]initWithName:[categories valueForKey:JSON_RES_Application_Name] children:nil andImage:@"noimage_about.png"];
            
        }else{
            
            object =[[RADataObject alloc]initWithName:[categories valueForKey:JSON_RES_Application_Name] children:nil andImage:[[categories valueForKey:JSON_RES_Application_Image] valueForKey:JSON_RES_Application_Image]];
            
        }
        
        
        [main_array addObject:object];
        [self.applicationNames addObject:[categories valueForKey:JSON_RES_Application_Name]];
        [self.applicationIds addObject:[categories valueForKey:JSON_RES_Application_Id]];
        
    }
    
    self.data = [NSArray arrayWithArray:main_array];
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [self.treeView reloadData];
        [SVProgressHUD dismiss];
        
    });
    
}



#pragma mark - Navigation

-(void)goToProductList:(NSString*)category{
    
    if (isApplication) {
        
        [self.delegate applicationClickedWithApplicationName:category andProductsIds:[self.applicationIds objectAtIndex:[self.applicationNames indexOfObject:category]]];
        
    } else {
        
        [self.delegate categoryClickedWithCategoryName:category];
        
    }
    
    
}


@end
