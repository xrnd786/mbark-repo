//
//  StoreLocatorHelper.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "MapView.h"


@interface StoreLocatorHelper : NSObject

@property (strong,nonatomic) UIView *baseView;
@property (strong,nonatomic) UIViewController *controller;
@property (strong,nonatomic) MapView *map;

-(id)initWithMainView:(UIView*)baseView withController:(UIViewController*)cont;

-(void)initialiseCustomViewsWithStoresArray:(NSArray*)stores;
-(void)deallocateMe;

@end

