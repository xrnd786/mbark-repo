//
//  DealerDetails.m
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//



#import "DealerDetails.h"


@interface DealerDetails ()

{
    Store *store;
}

@end

@implementation DealerDetails

@synthesize delegate;
@synthesize addressLabel=_addressLabel;
@synthesize phoneLabel=_phoneLabel;
@synthesize emailLabel=_emailLabel;
@synthesize nameLabel=_nameLabel;


-(id)init{
    
    self =[super self];
    if (self) {
        
        
        self.view=[[[NSBundle mainBundle]loadNibNamed:@"DealerDetails" owner:self options:nil] objectAtIndex:0];
        
        [self addGestures];
       
    
    }
    return self;
}


-(void)addGestures{

    _phoneLabel.userInteractionEnabled=TRUE;
    _emailLabel.userInteractionEnabled=TRUE;
    
    UITapGestureRecognizer *tapPhone=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(phoneTapped:)];
    UITapGestureRecognizer *tapEmail=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(emailTapped:)];
    
    [_phoneLabel addGestureRecognizer:tapPhone];
    [_emailLabel addGestureRecognizer:tapEmail];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setStoresValue:(Store*)storeObj{
    
    store=storeObj;
    [self loadDealerValues];
}

-(void)loadDealerValues{
    
    _addressLabel.text=store.Address;
    _phoneLabel.text=store.Contact;
    _emailLabel.text=store.Email;
    _nameLabel.text=store.Name;
    
    [_addressLabel setNeedsDisplay];
    [_phoneLabel setNeedsDisplay];
    [_emailLabel setNeedsDisplay];
    [_nameLabel setNeedsDisplay];
    
}

-(void)phoneTapped:(UITapGestureRecognizer*)gesture{
  
    NSString *phNo = ((RNThemeLabel*)gesture.view).text;
    
    if ([self.delegate respondsToSelector:@selector(callNowClicked:)]) {
        [self.delegate callNowClicked:phNo];
    }
    
}


-(void)emailTapped:(UITapGestureRecognizer*)gesture{
    
    if ([self.delegate respondsToSelector:@selector(emailNowClicked:)]) {
        
      [self.delegate emailNowClicked:_emailLabel.text];
    
    }
}


#pragma mark - Nib Events

- (IBAction)closeNow:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(closeClicked)]) {
    
        [self.delegate closeClicked];
        
    }
}

- (IBAction)navigateNow:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(navigateClicked)]) {
        
        [self.delegate navigateClicked];

    }
   
}




@end
