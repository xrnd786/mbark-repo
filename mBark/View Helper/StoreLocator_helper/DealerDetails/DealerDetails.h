//
//  DealerDetails.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeButton.h"
#import "Store.h"


@protocol DetailsViewProtocol <NSObject>

-(void)closeClicked;
-(void)navigateClicked;
-(void)callNowClicked:(NSString*)number;
-(void)emailNowClicked:(NSString*)toAddress;
-(void)swipedDown;

@end


@interface DealerDetails : UIViewController
@property (weak, nonatomic) IBOutlet RNThemeButton *navigateButton;

@property (weak, nonatomic) IBOutlet RNThemeLabel *addressLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *phoneLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *emailLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *nameLabel;


    
@property (strong,nonatomic) id<DetailsViewProtocol> delegate;


- (IBAction)closeNow:(id)sender;
- (IBAction)navigateNow:(id)sender;

-(void)setStoresValue:(Store*)storeObj;

@end
