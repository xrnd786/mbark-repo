//
//  StoreLocatorHelper.m
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import "StoreLocatorHelper.h"

#import <MessageUI/MessageUI.h>
#import <INKMapsHandler.h>
#import <INKActivityPresenter.h>
#import <CoreLocation/CoreLocation.h>

#import "DealerDetails.h"
#import "AppUserDefaults.h"

@interface StoreLocatorHelper()<MapAnnotationClicked,DetailsViewProtocol,MFMailComposeViewControllerDelegate,CLLocationManagerDelegate>
{
    
   BOOL detailIsOpen;
    int currentOpenDealer;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}
@property (strong,nonatomic) DealerDetails *dealer;
@property (strong,nonatomic) NSArray *allStores;


@end

@implementation StoreLocatorHelper

@synthesize map=_map;
@synthesize dealer=_dealer;
@synthesize baseView;
@synthesize controller;


-(id)initWithMainView:(UIView*)baseViewNew withController:(UIViewController*)cont{
    
    self=[super init];
    if (self) {
        
        self.baseView=baseViewNew;
        self.controller=cont;
        
        _map=[[MapView alloc]init];
        _dealer =[[DealerDetails alloc]init];
        _dealer.view.frame=CGRectMake(0, [[UIScreen mainScreen]bounds].size.height, _dealer.view.frame.size.width, _dealer.view.frame.size.height);
        
        locationManager=[[CLLocationManager alloc]init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        geocoder=[[CLGeocoder alloc]init];
        
        [self.baseView addSubview:_map.view];
        [self.baseView addSubview:_dealer.view];
        
        currentOpenDealer=-1;
    }
    return self;

}

-(void)initialiseCustomViewsWithStoresArray:(NSArray*)stores{

    self.allStores=[[NSArray alloc]initWithArray:stores];
    
    @autoreleasepool {
        
        _map.delegate=self;
        _map.mapView.delegate=_map;
        _dealer.delegate=self;
       
        [_map removeAllAnnotations];
        for (int i=0; i<[stores count] ;i++) {
            
            Store *store=(Store*)[stores objectAtIndex:i];
            [_map loadMap:store.Location.Latitude Longitude:store.Location.Longitude title:store.Name Address:store.Address withID:store.StoreID];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_map updateVisibleView];
            
            _map.mapView.showsUserLocation = NO;
            _map.mapView.showsUserLocation = YES;
            
        });
        
        
        detailIsOpen=FALSE;
        
    }
    
}




-(void)deallocateMe{
    
   _map=nil;
   _dealer=nil;
    
   self.baseView=nil;
   controller=nil;
    
    
}

#pragma mark - Update Functions

-(void)updateValues:(int)value{

    [_dealer setStoresValue:(Store*)[self.allStores objectAtIndex:value]];
    
}


#pragma mark - map delegate

-(void)annotationClicked:(int)value{
    
    currentOpenDealer=value;
    
    if (detailIsOpen) {
        
        [self updateValues:value];
        
    } else {
        
        [self updateValues:value];
        [self animateViewToShow];
        
    }
}

-(void)closeClicked{
    
    
    currentOpenDealer=-1;
    
    [self animteToHide];
    
}

-(void)navigateClicked{
   
    if ([AppUserDefaults getUserLocation_Latitude] == nil || [[AppUserDefaults getUserLocation_Latitude] isEqualToString:@""]) {
        
        locationManager.distanceFilter=kCLDistanceFilterNone;
        [locationManager requestWhenInUseAuthorization];
        [locationManager startMonitoringSignificantLocationChanges];
        
        [locationManager startUpdatingLocation];
    
    }else{
     
        
        CLLocation *location=[[CLLocation alloc] initWithLatitude:[[AppUserDefaults getUserLocation_Latitude] doubleValue] longitude:[[AppUserDefaults getUserLocation_Longitude] doubleValue]];
        [self getAddressFromLocation:location];
        
        
    }

}


-(void)callNowClicked:(NSString*)phNo{
  
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)emailNowClicked:(NSString*)toAddress{
    
    if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"A Message User ID 34"];
        
        [mailer setToRecipients:[[NSArray alloc]initWithObjects:toAddress, nil]];
        
        
        UIImage *myImage = [UIImage imageNamed:@"icon152"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mBarkImage"];
        
        NSString *emailBody = @"Hi Can you send me your details?";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        [self.controller presentViewController:mailer animated:YES completion:nil];
        
    }
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }

}


-(void)swipedDown{
    
    if (detailIsOpen) {
        
      [self animteToHide];
        
    }

}


#pragma mark - Animation Functions

-(void)animateViewToShow{
    
    [UIView animateWithDuration:1.5 delay:0 options:0 animations:^{
        
        
        [_dealer.view setFrame:CGRectMake(0,[[UIScreen mainScreen]bounds].size.height*0.3, _dealer.view.frame.size.width, _dealer.view.frame.size.height)];
        [_map.mapView setFrame:CGRectMake(0, 0, _map.view.frame.size.width, _map.view.frame.size.height-([[UIScreen mainScreen]bounds].size.height*0.3))];
        
                
    }completion:^(BOOL finished){
        
        detailIsOpen = true;
        
    }];
    

    
}

-(void)animteToHide{
    
    [UIView animateWithDuration:1.5 delay:0 options:0 animations:^{
        
        
        [_dealer.view setFrame:CGRectMake(0,[[UIScreen mainScreen]bounds].size.height, _dealer.view.frame.size.width, _dealer.view.frame.size.height)];
        [_map.mapView setFrame:CGRectMake(0, 0, _map.view.frame.size.width, 450)];
        
        
    }completion:^(BOOL finished){
        
        detailIsOpen = FALSE;
        
    }];
    
}


#pragma mark- mailer delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self.controller dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - location manager

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
   
    [errorAlert show];

}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
      
        [AppUserDefaults saveThisUserLocationLatitudeToDefaults:[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude]];
        [AppUserDefaults saveThisUserLongitudeToDefaults:[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude]];
        
    }
    
    
    // Stop Location Manager
    [locationManager stopUpdatingLocation];
    
    [self getAddressFromLocation:currentLocation];
    
}


-(void)getAddressFromLocation:(CLLocation*)currentLocation{
    
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        
        if (error == nil && [placemarks count] > 0) {
            
            placemark = [placemarks lastObject];
            NSString *address = [NSString stringWithFormat:@"%@ %@,%@ %@,%@ %@",
                                 placemark.subThoroughfare, placemark.thoroughfare,
                                 placemark.postalCode, placemark.locality,
                                 placemark.administrativeArea,
                                 placemark.country];
            
            [self startNavigationSequenceWithAddress:address];
            
        } else {
            
            NSLog(@"%@", error.debugDescription);
            
        }
    } ];

}

#pragma mark - navigate now

-(void)startNavigationSequenceWithAddress:(NSString*)address{
    
    
    INKMapsHandler *handler = [[INKMapsHandler alloc] init];
    handler.alwaysShowActivityView = TRUE;
    handler.useSystemDefault = TRUE;
    
    
    INKActivityPresenter *presenter = [handler searchForLocation:_dealer.addressLabel.text];
    
    if ([GlobalCall isIpad]) {
        
        [presenter presentActivitySheetFromViewController:self.controller popoverFromBarButtonItem:[[UIBarButtonItem alloc]initWithCustomView:self.dealer.navigateButton] permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES completion:^{
           
            [GlobalCall showBottomBar];
        }];
        
    }else{
        
        [presenter presentModalActivitySheetFromViewController:self.controller completion:^{
            
            [GlobalCall showBottomBar];
            
        }];
    }
    

    
}


@end
