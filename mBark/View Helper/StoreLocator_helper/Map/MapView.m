//
//  MapView.m
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import "MapView.h"

#import "MyLocation.h"
#import "UIImage+Color.h"

@interface MapView ()


@property (strong,nonatomic) NSMutableArray *titleList;
@property (strong,nonatomic) NSMutableArray *idList;


@end

@implementation MapView

@synthesize delegate,titleList,idList;
@synthesize mapView=_mapView;


-(id)init{
    
    self=[super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle]loadNibNamed:@"MapView" owner:self options:nil] objectAtIndex:0];
        
        self.titleList=[[NSMutableArray alloc]init];
        self.idList=[[NSMutableArray alloc]init];
        
    }
    
    return self;

}




-(void)loadMap:(NSString *)lat Longitude:(NSString *)Longitude title:(NSString *)title Address:(NSString *)address withID:(NSString*)lID
{
 
    /*CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [lat floatValue];
    zoomLocation.longitude= [Longitude floatValue];
    
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 20*METERS_PER_MILE, 20*METERS_PER_MILE);
    
    MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
    [_mapView setRegion:adjustedRegion animated:YES];
    */
    
    float latitude = [lat floatValue];
    float longitude = [Longitude floatValue];
    
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    
    [self.titleList addObject:title];
    [self.idList addObject:lID];
    
    MyLocation *annotation = [[MyLocation alloc] initWithName:title address:address coordinate:coordinate withID:lID] ;
    [_mapView addAnnotation:annotation];
    
}

#pragma mark - Public methods

-(void)removeAllAnnotations{
    
    for (id<MKAnnotation> annotation in _mapView.annotations)
     {
          [_mapView removeAnnotation:annotation];
     }
}


-(void)updateVisibleView
{
    if([_mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    NSLog(@"Annotations Count %d",[_mapView.annotations count]);
    
    for(id<MKAnnotation> annotation in _mapView.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
        
        
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.6; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.6; // Add a little extra space on the sides
    
    region = [_mapView regionThatFits:region];
    [_mapView setRegion:region animated:YES];
}




#pragma mark MKMapView delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapview viewForAnnotation:(id <MKAnnotation>)annotation
{
   
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
   
    static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
    
    MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    
    if(annotationView)
        
        return annotationView;
    
    else
    {
        
        
        UIImage *annotationImage=[UIImage imageWithImage:[UIImage imageWithContentsOfFile:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]stringByAppendingPathComponent:Local_Logo_Name]] scaledToSize:CGSizeMake(30,30)];
      
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] ;
        annotationView.canShowCallout = YES;
        annotationView.image = annotationImage;
        annotationView.canShowCallout = YES;
        annotationView.draggable = YES;
      
        
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:self action:@selector(callOut:) forControlEvents:UIControlEventTouchUpInside];
        [rightButton setTitle:annotation.title forState:UIControlStateNormal];
        
        for (NSString *titleNow in self.titleList)
        {
            if ([titleNow isEqualToString:annotation.title]) {
            
                
                rightButton.tag=(int)[self.titleList indexOfObject:titleNow];
                
                NSLog(@"tag is %d",rightButton.tag);
                break;
            }
            
        }
        
        annotationView.rightCalloutAccessoryView = rightButton;
        
        return annotationView;
    
    }
    
    return nil;
    
}


-(void)callOut:(UIButton*)button{
    
    [self.delegate annotationClicked:button.tag];
    
}


@end
