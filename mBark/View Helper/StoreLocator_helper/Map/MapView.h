//
//  MapView.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol MapAnnotationClicked <NSObject>

-(void)annotationClicked:(int)value;

@end


@interface MapView : UIViewController<MKMapViewDelegate>


@property (strong,nonatomic) id<MapAnnotationClicked> delegate;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

-(void)updateVisibleView;
-(void)removeAllAnnotations;
-(void)loadMap:(NSString *)lat Longitude:(NSString *)Longitude title:(NSString *)title Address:(NSString *)address withID:(NSString*)lID;

@end
