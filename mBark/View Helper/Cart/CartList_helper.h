//
//  WishList_helper.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

@protocol callSendCart <NSObject>

-(void)send:(NSArray*)sender;


@end

@interface CartList_helper : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIViewController *baseView;
@property (strong,nonatomic) id <callSendCart> delegate;
@property (strong,nonatomic) NSMutableArray *productMasterArray;

-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view andProductArray:(NSArray*)array withController:(UIViewController*)controller;

-(id)initWithTableForCart:(UITableView*)table  withMainView:(UIView*)view andProductArray:(NSArray*)array withController:(UIViewController*)controller ;

-(id)initWithCartProductsTable:(UITableView*)table  withMainView:(UIView*)view  andCartId:(NSString*)Id withController:(UIViewController*)controller;


@end
