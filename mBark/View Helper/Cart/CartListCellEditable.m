//
//  ProductLitsCell.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "CartListCellEditable.h"



@implementation CartListCellEditable

@synthesize productImage,productTitle,MRPLabel,totalPrice,delegate,moveToWishlistView,stepper;


- (void)awakeFromNib {
    
    
    stepper.Minimum=1;
    stepper.Maximum=100000;
    stepper.Step=1;
    stepper.textField.text=@"1";
    
    [stepper addTarget:self
                action:@selector(programmaticallyCreatedStepperDidStep:)
      forControlEvents:UIControlEventValueChanged];
    
   
    // Initialization code

}


- (void)programmaticallyCreatedStepperDidStep:(TextStepperField *)stepper {
    
    
    [self.delegate quantityEdited:self.stepper];
    
}


#pragma mark - send button

- (void)doneClicked:(id)sender
{
    [self.delegate quantityEdited:self.stepper];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    
}

- (IBAction)FavouriteToggle:(id)sender {
    
    UIButton *button=(UIButton*)sender;
    
    if (button.selected) {
        
        button.selected=FALSE;
        
    } else {
        
        button.selected=TRUE;
    
    }
    
}

-(void)willTransitionToState:(UITableViewCellStateMask)state{
   
}


- (IBAction)actionRemove:(id)sender {
    
    RNThemeButton *button=(RNThemeButton*)sender;
    [self.delegate moveToWishList:button];
    
}


- (IBAction)actionMovetoWishlist:(id)sender{
   
    RNThemeButton *button=(RNThemeButton*)sender;
    [self.delegate removeFromList:button];
    
    
}


- (IBAction)stepperValueChanged:(id)sender {
    
    [self.delegate quantityEdited:self.stepper];
    
}
@end
