//
//  WishList_helper.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "CartList_helper.h"

#import "CartListCell.h"
#import "CartListCellEditable.h"
#import "Products.h"
#import "ProductThumbnail.h"
#import "CartMaster.h"


#import "MGSwipeButton.h"
#import "SummaryCell.h"
#import "PostedCartCell.h"

#import "UIImageView+WebCache.h"
#import "CartListViewController.h"
#import "ProductViewController.h"
#import "ProductListController.h"
#import "SingleProduct_DataHelper.h"
#import "DXPopover.h"

#import "PostedCarts.h"

#import "AppTenantDefaults.h"

@interface CartList_helper ()<MGSwipeTableCellDelegate,UITextFieldDelegate,EditCartList>
{
    
    BOOL isCart;
    UITextField *field;
    int currentEdit;
    BOOL isSent;
    CGFloat lastContentOffset;
}

@property (strong,nonatomic) NSMutableArray *imageList;
@property (strong,nonatomic) NSMutableArray *titleList;
@property (strong,nonatomic) NSMutableArray *priceList;
@property (strong,nonatomic) NSMutableArray *discountList;
@property (strong,nonatomic) NSMutableArray *qtyList;
@property (strong,nonatomic) NSMutableArray *statusList;



@property (strong,nonatomic) NSMutableArray *cartIdArray;

@property (strong,nonatomic) SingleProduct_DataHelper *singleProduct;


@end

@implementation CartList_helper

@synthesize imageList,titleList,priceList,discountList,localTableCopy,productMasterArray,baseView,qtyList,delegate,singleProduct,cartIdArray,statusList;

-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view andProductArray:(NSArray*)array withController:(UIViewController*)controller {
    
    self = [super init];
    
    if (self) {
        
        self.productMasterArray=[[NSMutableArray alloc]initWithArray:array];
        
        
        [self initializeTableWithFeatured];
        
        [table setShowsVerticalScrollIndicator:NO];
        
        if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
            [table setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
            [table setLayoutMargins:UIEdgeInsetsZero];
        }
       
        table.delegate=self;
        table.dataSource=self;
        
        localTableCopy=table;
        
        if ([imageList count]==0) {
            
            table.hidden=TRUE;
            
        } else {
            
            table.hidden=FALSE;
            
        }
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        
        isCart=FALSE;
    
        currentEdit=-1;
    
        
    }
    
    return self;
}


-(id)initWithCartProductsTable:(UITableView*)table  withMainView:(UIView*)view andCartId:(NSString*)Id withController:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        self.singleProduct=[[SingleProduct_DataHelper alloc]init];
        
        self.productMasterArray=[[NSMutableArray alloc]initWithArray:[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"IsInCart == true"]]]];
        
        [self initializeTableWithCart];
        
        [table setShowsVerticalScrollIndicator:NO];
        
        if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
            [table setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
            [table setLayoutMargins:UIEdgeInsetsZero];
        }
        
        table.delegate=self;
        table.dataSource=self;
        
        localTableCopy=table;
        
        if ([imageList count]==0) {
            
            table.hidden=TRUE;
            
        } else {
            
            table.hidden=FALSE;
            
        }
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        isCart=FALSE;
    
    }
    
    return self;
    
}



-(id)initWithTableForCart:(UITableView*)table  withMainView:(UIView*)view  andProductArray:(NSArray*)array withController:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        isCart=TRUE;
        
        self.productMasterArray=[[NSMutableArray alloc]initWithArray:array];
        
        [self initializeTableWithForSentCart];
        
        [table setShowsVerticalScrollIndicator:NO];
        if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
            [table setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
            [table setLayoutMargins:UIEdgeInsetsZero];
        }
        table.delegate=self;
        table.dataSource=self;
        
        table.separatorStyle=UITableViewCellSelectionStyleNone;
        
        localTableCopy=table;
        
        if ([imageList count]==0) {
            table.hidden=TRUE;
        } else {
            table.hidden=FALSE;
        }
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        
        
    }
    
    return self;
}


-(void)initializeTableWithFeatured{
    
    imageList=[[NSMutableArray alloc]init];
    titleList=[[NSMutableArray alloc]init];
    priceList=[[NSMutableArray alloc]init];
    discountList=[[NSMutableArray alloc]init];
   
    
    for (Products *productObject in self.productMasterArray) {
            
        
            [imageList addObject:[NSString stringWithFormat:@"%@",productObject.thumbnail.p_id]];
            [priceList addObject:[NSString stringWithFormat:@"%@",productObject.rate]];
            [titleList addObject:[NSString stringWithFormat:@"%@",productObject.name]];
            [discountList addObject:[NSString stringWithFormat:@"%@",productObject.discount]];
        
        }
   
}

-(void)initializeTableWithCart{
    
    imageList=[[NSMutableArray alloc]init];
    titleList=[[NSMutableArray alloc]init];
    priceList=[[NSMutableArray alloc]init];
    discountList=[[NSMutableArray alloc]init];
    
    
    for (Products *productObject in self.productMasterArray) {
        
        [imageList addObject:[NSString stringWithFormat:@"%@",productObject.thumbnail.p_id]];
        [priceList addObject:[NSString stringWithFormat:@"%@",productObject.rate]];
        [titleList addObject:[NSString stringWithFormat:@"%@",productObject.name]];
        [discountList addObject:[NSString stringWithFormat:@"%@",productObject.discount]];
        
    }
    
    
}


-(void)initializeTableWithForSentCart{
   
    imageList=[[NSMutableArray alloc]init];
    titleList=[[NSMutableArray alloc]init];
    priceList=[[NSMutableArray alloc]init];
    discountList =[[NSMutableArray alloc]init];
    statusList=[[NSMutableArray alloc]init];
    
    for (CartMaster *dictionary in self.productMasterArray) {
        
        [imageList addObject:@"bag.png"];
        [titleList addObject:dictionary.c_id];
        [priceList addObject:[dictionary.details valueForKeyPath:@"@sum.price"]];
        
        NSDateFormatter *dateformat=[[NSDateFormatter alloc]init];
        [dateformat setDateFormat:@"dd-MM-yyyy HH:mm"];
        
        NSString *finalUTCDate=[NSString stringWithFormat:@"%@",[dateformat stringFromDate:dictionary.createdOn]];
    
        
        [discountList addObject:finalUTCDate];
        [statusList addObject:dictionary.status];
    }
    
}


#pragma mark- dismiss 

-(void)dismissResponder:(UITapGestureRecognizer*)gesture{
    
    for (int i=0;i<[imageList count];i++) {
        
        [((CartListCell*)[localTableCopy cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]]).qtyTextfld resignFirstResponder];
        
    }
    
}



#pragma mark - UIScrollView Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
}

#pragma mark - table View Functions


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isCart) {
        
           return 150;
        
    } else {
        
        
        if (indexPath.section==0) {
            
            return 300;
            
        } else {
            
            return 150;
            
        }
        
    }
    
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section==0) {
        
        UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
        return view;
        
    } else {
        
        RNThemeLabel *title=[[RNThemeLabel alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
        title.text=@"Summary";
        title.fontKey=@"generalFont";
        title.textColorKey=@"secondaryColor";
        
        UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
        [view addSubview:title];
        
        return view;
        
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    if (section==0) {
        
        return 0.0f;
        
    }else{
        
        return 50.0f;
    }

    
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;

}



#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (isCart) {
      
        return 1;
        
    } else {
        
        
        if ([self.productMasterArray count]>0) {
            
            return 2;
            
        }else{
            
            return 0;
            
        }

    }
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section==0) {
        
        return [titleList count];
        
    }else{
        
        return 1;
    }
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *headerTitle=@"";
    
    if ([self.productMasterArray count]>0) {
        
        if (section==1) {
            
            headerTitle=@"Summary";
            
        }
        
    }
    
    return headerTitle;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (isCart) {
       
        return [self getCellForPostedCarts:tableView indexPath:indexPath];
        
    } else {
        
        return  [self getCellForCartList:tableView indexPath:indexPath];
        
        
    }
    
}


-(UITableViewCell*)getCellForCartList:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath{
   
    Products *objectNow=[self.productMasterArray objectAtIndex:indexPath.row];
    
        if (indexPath.section==0) {
            
            static NSString *identifier = @"CartListCellEditable";
            
            CartListCellEditable *cell =(CartListCellEditable*) [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (cell == nil) {
                
                cell =[[[NSBundle mainBundle]loadNibNamed:@"CartListCellEditable" owner:self options:nil] objectAtIndex:0];
            }
            
            
            
            cell.productImage.layer.shadowOffset = CGSizeMake(-5, 5);
            cell.productImage.layer.shadowRadius = 3;
            cell.productImage.layer.shadowOpacity = 0.3;
           
            
            
            [cell.productImage setImageWithURL:[imageList objectAtIndex:indexPath.row] placeholderImage:[UIImage imageNamed:@"bag.png"]];
            
            cell.productTitle.text=[titleList objectAtIndex:indexPath.row];
            cell.MRPLabel.text=[NSString stringWithFormat:@"%@",[priceList objectAtIndex:indexPath.row]];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.stepper.textField.text=[NSString stringWithFormat:@"%@",objectNow.cartQty];
            cell.totalPrice.text=[NSString stringWithFormat:@"%.2f",([objectNow.cartQty doubleValue]*[[priceList objectAtIndex:indexPath.row] doubleValue])];
            cell.stepper.tag=indexPath.row;
            
            if (objectNow.isFavourite) {
                
                cell.moveToWishlistView.hidden=TRUE;
                
            } else {
                
                cell.moveToWishlistView.hidden=FALSE;
            }
            
#if !TEST_USE_MG_DELEGATE
            
            cell.rightSwipeSettings.transition = MGSwipeTransition3D;
            cell.rightExpansion.buttonIndex = 0;
            cell.rightExpansion.fillOnTrigger = NO;
            
#endif
            
            cell.delegate=self;
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            
            cell.buttonMovetoWishlist.tag=indexPath.row;
            cell.buttonRemove.tag=indexPath.row;
            cell.selectionStyle=UITableViewCellSelectionStyleDefault;
            
            cell.delegate=self;
            cell.tag=indexPath.row;
            
            return cell;
            
            
        } else {
            
            static NSString *identifier = @"SummaryCell";
            
            SummaryCell *cell =(SummaryCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (cell == nil) {
                
                cell =[[[NSBundle mainBundle]loadNibNamed:@"SummaryCell" owner:self options:nil] objectAtIndex:0];
            }
            
            cell.label2.text=[cell.label2.text stringByAppendingString:[NSString stringWithFormat:@"  %@",[AppTenantDefaults getOrganization]]];
            
            double sum=0;
            double discount=0;
            
            
            for (NSNumber *num in priceList) {
                
                int index=(int)[priceList indexOfObject:num];
                
                sum+=([num doubleValue]*[objectNow.cartQty doubleValue]);
                discount+=([[discountList objectAtIndex:index] doubleValue]*[objectNow.cartQty doubleValue]);
                
            }
            
            cell.totalCartAmout.text=[NSString stringWithFormat:@"%.2f",sum];
            [cell.totalCartAmout sizeToFit];
            [cell.sendCartButton addTarget:self action:@selector(sendCartNow:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.selectionStyle=UITableViewCellEditingStyleNone;
            
            return cell;
            
        }
    
    
}


-(UITableViewCell*)getCellForPostedCarts:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath{
    
    static NSString *identifier = @"PostedCartCell";
    
    PostedCartCell *cell =(PostedCartCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"PostedCartCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.cartTitle.text=[titleList objectAtIndex:indexPath.row];
    cell.totalCost.text=[NSString stringWithFormat:@"%@",[priceList objectAtIndex:indexPath.row]];
    cell.status.text=[[statusList objectAtIndex:indexPath.row] boolValue]?@"Sent":@"Received";
    
    NSString *dateOnly = [NSString stringWithFormat:@"%@",[discountList objectAtIndex:indexPath.row]];
    
    NSLog(@"DateOnly: %@",dateOnly);
    
    cell.selectionStyle=UITableViewCellEditingStyleNone;
    cell.postedDate.text=dateOnly;
    
    return cell;
    
}
#pragma mark - cell touched



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (isCart) {
        
        [self.baseView.navigationController pushViewController:[[PostedCarts alloc]initWithCartDictionary:[self.productMasterArray objectAtIndex:indexPath.row]] animated:YES];
        
    } else {
        
        if (indexPath.section==0) {
            
            ProductViewController *controller=[GlobalCall productViewController];
            controller.productID=[[self.productMasterArray objectAtIndex:indexPath.row] valueForKeyPath:@"p_id"];
            
            [self.baseView.navigationController pushViewController:controller animated:YES];
            
        }else{
            
            //[self tableView:tableView didDeselectRowAtIndexPath:indexPath];
            
        }
        
    }
    
    
}




#pragma mark - Date formatter

- (NSString *)curentDateStringFromUTCDate:(NSString *)utcDateTimeInLine withFormat:(NSString *)dateFormat {
    
    NSLog(@"UTC Date Time  : %@",utcDateTimeInLine);
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [formatter dateFromString:utcDateTimeInLine];
    
    NSLog(@"Date : %@",date);
    
    NSDateFormatter *formatterFinal = [[NSDateFormatter alloc]init];
    [formatterFinal setDateFormat:dateFormat];
    
    NSString *convertedString = [formatterFinal stringFromDate:date];
    
    return convertedString;
}



#pragma mark - cart button

-(void)sendCartNow:(UIButton*)sender{
    
    [self.delegate send:(NSArray*)productMasterArray];
    
}


#pragma mark - edit cart

-(void)removeFromList:(RNThemeButton *)button{
    
    
    NSArray *objArray=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id == %@",((Products*)[self.productMasterArray objectAtIndex:button.tag]).p_id]];
    Products *prodObj=[objArray lastObject];
    
    prodObj.isInCart=[NSNumber numberWithBool:FALSE];
    
    [GlobalCall saveContext];

    [self removeItemAtIndexandUpdate:(int)button.tag];
    
}

-(void)moveToWishList:(RNThemeButton *)button{
    
    
    NSArray *objArray=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id == %@",((Products*)[self.productMasterArray objectAtIndex:button.tag]).p_id]]];
    Products *prodObj=[objArray lastObject];
    prodObj.isFavourite=[NSNumber numberWithBool:TRUE];
    prodObj.isInCart=[NSNumber numberWithBool:FALSE];
//    
//    [AppProductDefaults removeFromCartList:[ valueForKey:JSON_RES_AppProducts_Id]];
//    [AppProductDefaults saveWishList:[[ProductMapper new]mapThisServerDictionaryToProduct:[self.productMasterArray objectAtIndex:button.tag]]];
    [self removeItemAtIndexandUpdate:(int)button.tag];
    [GlobalCall showBottomBar];
    
}

-(void)removeItemAtIndexandUpdate:(int)indexValue{
    
    
    NSMutableArray *array=[[NSMutableArray alloc]initWithArray:self.productMasterArray];
    
    [array removeObjectAtIndex:indexValue];
    
    self.productMasterArray =[[NSMutableArray alloc]initWithArray:array];
    [self initializeTableWithFeatured];
    [self.localTableCopy reloadData];
    
    if ([self.productMasterArray count]==0) {
        
        self.localTableCopy.hidden=TRUE;
        ((CartListViewController*)self.baseView).sorryView.hidden=FALSE;
        
    } else {
        
        self.localTableCopy.hidden=FALSE;
        ((CartListViewController*)self.baseView).sorryView.hidden=TRUE;
        
    }
    
    
}

#pragma mark - textfield delegate


-(void)quantityEdited:(TextStepperField*)stepper{
    
    if ([stepper.textField.text intValue] == nil) {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self.baseView title:@"Quantity" subTitle:@"Please enter quantity to be ordered" closeButtonTitle:@"Ok" duration:0.0f];
        stepper.textField.text=@"1";
        
    } else {
        
        if ([stepper.textField.text intValue]==0) {
            
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showWarning:self.baseView title:@"Invalid entry" subTitle:@"Atleast 1 quantity must be entered" closeButtonTitle:@"Ok" duration:0.0f];
            
            stepper.textField.text=@"1";
            
        }else{
            
            
           Products *productForQuantity= [self.productMasterArray objectAtIndex:stepper.textField.tag];
            productForQuantity.cartQty=[NSNumber numberWithInt:[stepper.textField.text intValue]];
            [self.productMasterArray replaceObjectAtIndex:stepper.textField.tag withObject:productForQuantity];
                         
            
            
            NSArray *objArray=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id == %@",((Products*)[self.productMasterArray objectAtIndex:stepper.textField.tag]).p_id]];
            Products *prodObj=[objArray lastObject];
            
            prodObj.cartQty=[NSNumber numberWithInt:[stepper.textField.text intValue]];
            
            [GlobalCall saveContext];
            
            
            [self.localTableCopy beginUpdates];
            [self.localTableCopy reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:stepper.textField.tag inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
             [self.localTableCopy reloadRowsAtIndexPaths:[[NSArray alloc] initWithObjects:[NSIndexPath indexPathForItem:0 inSection:1], nil] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.localTableCopy endUpdates];
            
            
        }
        
    }
    
    
}


@end
