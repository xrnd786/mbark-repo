//
//  SummaryCell.m
//  mBark
//
//  Created by Xiphi Tech on 18/02/2015.
//
//

#import "SummaryCell.h"

@implementation SummaryCell

@synthesize label2,sendCartButton;

- (void)awakeFromNib {
    // Initialization code
    [sendCartButton applyTheme];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
