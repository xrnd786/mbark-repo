//
//  ProductLitsCell.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeImageButton.h"
#import "MGSwipeTableCell.h"
#import "RNThemeTextField.h"
#import "RNThemeButton.h"
#import "TextStepperField.h"

@protocol EditCartList <NSObject>

-(void)moveToWishList:(RNThemeButton*)button;
-(void)removeFromList:(RNThemeButton*)button;

-(void)quantityEdited:(TextStepperField*)stepper;

@end

@interface CartListCellEditable : MGSwipeTableCell

@property (strong,nonatomic) id<EditCartList> delegate;
@property (weak, nonatomic) IBOutlet TextStepperField *stepper;

- (IBAction)stepperValueChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet RNThemeLabel *productTitle;
@property (weak, nonatomic) IBOutlet RNThemeLabel *MRPLabel;
@property (weak, nonatomic) IBOutlet RNThemeLabel *totalPrice;
@property (weak, nonatomic) IBOutlet UIView *moveToWishlistView;

@property (weak, nonatomic) IBOutlet RNThemeButton *buttonMovetoWishlist;
@property (weak, nonatomic) IBOutlet RNThemeButton *buttonRemove;
@property (weak, nonatomic) IBOutlet UIImageView *removeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *wishlistIcon;

- (IBAction)actionMovetoWishlist:(id)sender;
- (IBAction)actionRemove:(id)sender;


@end
