//
//  PostedCartCell.m
//  mBark
//
//  Created by Xiphi Tech on 18/02/2015.
//
//

#import "PostedCartCell.h"

@implementation PostedCartCell

@synthesize cartPicture,cartTitle,totalCost,postedDate,status;

- (void)awakeFromNib {
    // Initialization code
    //self.cartPicture.layer.shadowOffset = CGSizeMake(3, 3);
    //self.cartPicture.layer.shadowRadius = 2;
    self.cartPicture.layer.shadowOpacity = 0.3;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
   
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state

}

@end
