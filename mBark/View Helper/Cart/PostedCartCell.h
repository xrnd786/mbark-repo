//
//  PostedCartCell.h
//  mBark
//
//  Created by Xiphi Tech on 18/02/2015.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"


@interface PostedCartCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cartPicture;
@property (weak, nonatomic) IBOutlet RNThemeLabel *cartTitle;
@property (weak, nonatomic) IBOutlet RNThemeLabel *postedDate;
@property (weak, nonatomic) IBOutlet RNThemeLabel *totalCost;
@property (weak, nonatomic) IBOutlet RNThemeLabel *status;

@end
