//
//  ProductListViewHelper.m
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import "GroupedProductViewHelper.h"

#import <NYTPhotoViewer/NYTPhotosViewController.h>
#import "NYTExamplePhoto.h"

#import "ProductScrollCell.h"
#import "PageScrollViewCell.h"

#import "ProductTitleViewCell.h"

#import "DescriptionCell.h"
#import "ApplicationCell.h"
#import "SpecificationCell.h"

#import "RelatedProductsViewCell.h"

#import "DescriptionView.h"
#import "ApplicationView.h"
#import "SpecificationView.h"
#import "GroupSelectionCell.h"
#import "ActionButtonsCell.h"
#import "ContactAdmin.h"

#import "ThemeManager.h"

#import "addNewMessageSubject.h"
#import "AFPopupView.h"
#import "MessagesCall_Response.h"

#import "ProductViewController.h"


#import "UIImageView+WebCache.h"
#import "UIImage+animatedGIF.h"

#import "SelectAttribute.h"
#import "selectAttributeCell.h"
#import "Attribute.h"
#import "DXPopover.h"

#import "AppSettingDefaults.h"
#import "AuthChoiceController.h"

#import "LeveyTabBarController.h"
#import "ProductCall_Response.h"
#import "Products.h"
#import "ProductImages.h"
#import "ProductThumbnail.h"
#import "User.h"
#import "ProductAttributes.h"

#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5

@interface GroupedProductViewHelper ()<scrollItemClicked,RelatedProductClicked,UIViewControllerTransitioningDelegate,UINavigationControllerDelegate,hideSelection,productAdded,dismissPopOver,contactCalled,NYTPhotosViewControllerDelegate>
{
    CGFloat topViewHeight;
    CGFloat displayRadio;
    BOOL isFullScreen;
    
   
    CGRect applicationLabelSize;
    
    int currentImageOpen;
    int currentGroupOpen;
    
    UIPanGestureRecognizer *moveGes;
    
    BOOL isFav;
    BOOL isCart;
    
    RNThemeTextField *field;
    
    CGFloat lastContentOffset;
    
    NYTPhotosViewController *photosViewController;
    NSMutableArray *nyListPhotos;
    UIImageView *viewForSingle;
    
}

@property (strong,nonatomic)  NSArray *imageViews;

@property (strong,nonatomic) UIScrollView *embededScrollView;
@property (strong,nonatomic) UIView *siblingHeader;
@property (strong,nonatomic) UIView *pageScrollView;



@property (strong,nonatomic) AFPopupView *popup;
@property (strong,nonatomic) addNewMessageSubject *MessageSubject;
@property (strong,nonatomic) SelectAttribute *selectAttribute;



@end


@implementation GroupedProductViewHelper

@synthesize imageList,name,price,stockLeft,unitValue,descriptionValue,localTableCopy,localView,embededScrollView,siblingHeader,pageScrollView,base,productDictionary,discountPrice,specsArray,applicationList,related_ids,related_thumbnails,popup,MessageSubject,thumbnailImage,groupedIds,groupedThumbnails,allGroupProducts,selectAttribute;




-(id)initWithGroupedProductsTable:(UITableView*)table  withMainView:(UIView*)view  withController:(UIViewController*)controller withProductDictionary:(NSDictionary*)productDic withProductObj:(Products *)product{
    
    self = [super init];
    
    if (self) {
        
        self.productDictionary=product;
        // NSLog(@"Grouped Value %@",self.productDictionary.grouped);
        if ([self.productDictionary.grouped count]== 0) {
            
            self.allGroupProducts=[NSMutableArray new];
            
        } else {
            
            self.allGroupProducts=[NSMutableArray new];
            for (Products *dic in self.productDictionary.grouped) {
                [self.allGroupProducts addObject:dic];
             }
            
        }
        
        currentGroupOpen=0;
        
        if (self.allGroupProducts != Nil && [self.allGroupProducts count]>0) {
           
            self.productDictionary=[allGroupProducts objectAtIndex:0];
            
        }else{
      
            self.productDictionary=nil;
            
        }
        
        
        isFav=[self.productDictionary.isFavourite boolValue];
        isCart=[self.productDictionary.isInCart boolValue];
        
        
        [self initializeTableWithFeatured];
        
        if (self.allGroupProducts != Nil && [self.allGroupProducts count]>0) {
       
        }else{
            
            siblingHeader=[self getTopView];
            
        }
        
        [table setShowsVerticalScrollIndicator:NO];
        
        table.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        table.separatorColor=AppBarColor;
        
        if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
            [table setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
            [table setLayoutMargins:UIEdgeInsetsZero];
        }
        table.delegate=self;
        table.dataSource=self;
        
        
        currentImageOpen=0;
        
        
        table.tableHeaderView=({
            
            UIView *viewFinal=[self getTopView];
            
            UITapGestureRecognizer *tapToFullScreen=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
            [viewFinal addGestureRecognizer:tapToFullScreen];
            
            viewFinal.userInteractionEnabled=TRUE;
            viewFinal;
            
        });
        
        
        nyListPhotos=[NSMutableArray new];
        nyListPhotos=[self newTestPhotos];
        
        
        [view bringSubviewToFront:table];
        
        localTableCopy=table;
        localView=view;
        isFullScreen=FALSE;
        
        self.base=controller;
        self.MessageSubject=[[addNewMessageSubject alloc]init];
        
        
        self.selectAttribute=[[SelectAttribute alloc]init];
    
    }
    
    return self;
    
}


-(void)updateAllData{
    
    [self performSelectorInBackground:@selector(updateHeaderInBackground) withObject:self];
    
    int rowsCount;
    
    if ([related_thumbnails count]==0) {
        
        rowsCount= 6;
        
    } else {
        
        rowsCount= 7;
        
    }
    
    
    isFav=[self.productDictionary.isFavourite boolValue];
    isCart=[self.productDictionary.isInCart boolValue];
    
    for (int i=0; i<rowsCount; i++) {
        
        [self.localTableCopy reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:[NSIndexPath indexPathForItem:i inSection:0], nil] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    
    
}


-(void)updateHeaderInBackground{
   
    
    for (UIImageView *views in [self.embededScrollView subviews]) {
        [views removeFromSuperview];
    }
    
    NSMutableArray *objects=[NSMutableArray new];
    
    if ([imageList count]==0) {
       
        
        viewForSingle=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], self.embededScrollView.frame.size.height)];
        
        objects =[[NSMutableArray alloc]init];
        
        NSLog(@"Thumbnail image %@",thumbnailImage);
        
        if (thumbnailImage !=nil || ![thumbnailImage isEqualToString:@""] || (NSObject*)thumbnailImage !=[NSNull null]) {
            
            [[SDImageCache sharedImageCache] queryDiskCacheForKey:thumbnailImage done:^(UIImage *image, SDImageCacheType cacheType) {
                
                if (image) {
                    
                    [viewForSingle setImage:image];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self.embededScrollView addSubview:viewForSingle];
                        
                        viewForSingle.clipsToBounds=TRUE;
                        [viewForSingle setNeedsDisplay];
                        [self.embededScrollView setNeedsDisplay];
                        nyListPhotos=[self newTestPhotos];
                        
                    });
                    
                    
                    self.imageList=[NSMutableArray new];
                    [self.imageList addObject:thumbnailImage];
                    
                }else{
                    
                    [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnailImage]]] forKey:thumbnailImage];
                    [viewForSingle sd_setImageWithURL:[NSURL URLWithString:thumbnailImage] placeholderImage:[UIImage imageNamed:@"no_product.png"]];
                    [self.embededScrollView addSubview:viewForSingle];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        viewForSingle.clipsToBounds=TRUE;
                        [viewForSingle setNeedsDisplay];
                        [self.embededScrollView setNeedsDisplay];
                        nyListPhotos=[self newTestPhotos];
                        
                    });
                    
                }
                
            }];
            
            
        } else {
            
            viewForSingle.image=[UIImage imageNamed:@"noproduct.png"];
            self.imageList=[[NSMutableArray alloc]init];
            
            [self.embededScrollView addSubview:viewForSingle];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                viewForSingle.clipsToBounds=TRUE;
                [viewForSingle setNeedsDisplay];
                [self.embededScrollView setNeedsDisplay];
                nyListPhotos=[self newTestPhotos];
                
            });
        }
        
        
    } else {
        
        objects =[[NSMutableArray alloc]initWithArray:imageList];
        self.embededScrollView.contentSize=CGSizeMake([objects count]*self.embededScrollView.frame.size.width, self.embededScrollView.frame.size.height);
        
        int i=0;
        for (NSString *imageName in objects) {
            
            UIImageView *view=[[UIImageView alloc]initWithFrame:CGRectMake(i*  self.embededScrollView.frame.size.width, 0,  self.embededScrollView.frame.size.width,  self.embededScrollView.frame.size.height)];
            
            
            NSURL *url = [NSURL URLWithString:imageName];
            
            if (url && url.scheme && url.host)
            {
                [[SDImageCache sharedImageCache] queryDiskCacheForKey:[url absoluteString] done:^(UIImage *image, SDImageCacheType cacheType) {
                    
                    if (image) {
                        [view setImage:image];
                    }else{
                        
                        [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:url]] forKey:[url absoluteString]];
                        [view sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"no_product.png"]];
                    }
                    
                }];
                
                
            }else{
                
                [view setImage:[UIImage imageNamed:imageName]];
                
            }
            
            view.tag=i;
            view.clipsToBounds=TRUE;
            
            [self.embededScrollView addSubview:view];
            i++;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [self.embededScrollView setNeedsDisplay];
            nyListPhotos=[self newTestPhotos];
            
        });
        
    }
    

    
    
}




-(void)clearData{
    
    self.imageList=[NSMutableArray new];
    self.price=@"0";
    self.descriptionValue=@"";
    self.name=@"";
    self.stockLeft=@"0";
    self.thumbnailImage=@"";
    self.unitValue=@"";
    
    
    int rowsCount;
    
    if ([related_thumbnails count]==0) {
        
        rowsCount= 6;
        
    } else {
        
        rowsCount= 7;
        
    }
    
    for (int i=0; i<rowsCount; i++) {
        
        [self.localTableCopy reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:[NSIndexPath indexPathForItem:i inSection:0], nil] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    
    [self performSelectorInBackground:@selector(updateHeaderInBackground) withObject:nil];
    
}


-(void)initializeTableWithFeatured{
 
   
    name =self.productDictionary.name;
    price =[self.productDictionary.rate stringValue];
    discountPrice= [self.productDictionary.discount stringValue];
    
    NSMutableArray *tempArray=[[NSMutableArray alloc]initWithArray:[self.productDictionary.images allObjects]];
    
    imageList=[[NSMutableArray alloc]init];
    
    for (ProductImages *dic in tempArray) {
        
        [imageList addObject:dic.image_id];
        
    }
    
    NSLog(@"First evern decription %@",self.productDictionary.description);
    
    descriptionValue = self.productDictionary.description;
    stockLeft = [self.productDictionary.availableUnits stringValue];
    unitValue = self.productDictionary.measurementUnit;
    specsArray = [[NSMutableArray alloc]initWithArray:[self.productDictionary.attributes allObjects]];
    applicationList=self.productDictionary.applications;
    thumbnailImage=self.productDictionary.thumbnail.p_id;
    
    NSString *applicationsString=@"";
    
    int i=1;
    
    for (NSString *string in applicationList) {
        
        applicationsString=[applicationsString stringByAppendingString:[NSString stringWithFormat:@"%d. %@\r",i,string]];
        i++;
    }
    
    
    CGSize maximumLabelSize = CGSizeMake(320.0f, MAXFLOAT);
    
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    
    @try {
        
        NSDictionary *attr = @{NSFontAttributeName:[[ThemeManager sharedManager]fontForKey:@"generalFont"]};
        
        applicationLabelSize  = [applicationsString boundingRectWithSize:maximumLabelSize  options:options attributes:attr context:nil];
        
    }
    @catch (NSException *exception) {
        
        applicationLabelSize=CGRectMake(0, 0, 320, 0);
        
    }@finally {}
    
}

-(void)initializeTableWithAttributeChange{
    
    
    name =self.productDictionary.name;
    price =[self.productDictionary.rate stringValue];
    discountPrice= [self.productDictionary.discount stringValue];
    
    NSMutableArray *tempArray=[[NSMutableArray alloc]initWithArray:[self.productDictionary.images allObjects]];
    
    imageList=[[NSMutableArray alloc]init];
    
    for (ProductImages *dic in tempArray) {
        
        [imageList addObject:dic.image_id];
        
    }
    
    stockLeft = [self.productDictionary.availableUnits stringValue];
    unitValue = self.productDictionary.measurementUnit;
    thumbnailImage=self.productDictionary.thumbnail.p_id;
    
}





-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
   
    
}


-(void)reloadTheViews{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD dismiss];
        [self hide];
        
    });
    
}

-(void)nowHideWithSelection:(NSArray *)selectionArray andCurrentIndex:(int)index{
    
    
    if (index!=-1) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self hide];
            
        });
        
        self.productDictionary = [selectionArray lastObject];
        currentGroupOpen=index;
        
        isFav=[self.productDictionary.isFavourite boolValue];
        isCart=[self.productDictionary.isInCart boolValue];
        
        [self initializeTableWithAttributeChange];
        [self.localTableCopy reloadData];
        [self updateHeader];
       
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self hide];
            
        });
 
        
    }
    
    
}





-(void)updateHeader{
    
    localTableCopy.tableHeaderView=({
        
        UIView *viewFinal=[self getTopView];
        
        UITapGestureRecognizer *tapToFullScreen=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
        [viewFinal addGestureRecognizer:tapToFullScreen];
        
        viewFinal.userInteractionEnabled=TRUE;
        viewFinal;
        
    });

    
}




#pragma mark - Top View for Scrolling images

-(UIView*)getTopView{

    UIImageView *topView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0 ,320, 350)];
    topViewHeight=topView.frame.size.height;
    
    UIView *topHeaderView;
    displayRadio=0.8;
    
    self.embededScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, topView.frame.size.width, topViewHeight)];
    self.embededScrollView.pagingEnabled=TRUE;
    self.embededScrollView.delegate=self;
    
    NSMutableArray *objects;
    
    
    if ([imageList count]==0) {
        
        // imageList=[[NSMutableArray alloc]init];
        objects =[[NSMutableArray alloc]init];
        
        
    } else {
        
        objects =[[NSMutableArray alloc]initWithArray:imageList];
        
    }
    
    if ([objects count]==0) {
        
        self.embededScrollView.contentSize=CGSizeMake(topView.frame.size.width, topView.frame.size.height);
        
        viewForSingle=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, topView.frame.size.width, topView.frame.size.height)];
        
        NSLog(@"Thumbnail %@",thumbnailImage);
       
        if (thumbnailImage !=NULL) {
            
            [[SDImageCache sharedImageCache] queryDiskCacheForKey:thumbnailImage done:^(UIImage *image, SDImageCacheType cacheType) {
                
                if (image) {
                
                    [viewForSingle setImage:image];
                    viewForSingle.clipsToBounds=TRUE;
                    [self.embededScrollView addSubview:viewForSingle];
                    
                }else{
                    
                    [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnailImage]]] forKey:thumbnailImage];
                    
                    [viewForSingle sd_setImageWithURL:[NSURL URLWithString:thumbnailImage] placeholderImage:[UIImage imageNamed:@"no_product.png"]];
                    
                    viewForSingle.clipsToBounds=TRUE;
                    [self.embededScrollView addSubview:viewForSingle];
                    
                }
                
            }];
            
            
            //viewForSingle.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnailImage]]];
            //[self.imageList addObject:thumbnailImage];
        } else {
           
            viewForSingle.image=[UIImage imageNamed:@"noproduct.png"];
            viewForSingle.contentMode=UIViewContentModeCenter;
            
            self.imageList=[[NSMutableArray alloc]init];
            
            [self.embededScrollView addSubview:viewForSingle];
            
        }
      
        
    }else{
        
        self.embededScrollView.contentSize=CGSizeMake([objects count]*topView.frame.size.width, topView.frame.size.height);
        
    }
    
    int i=0;
    
    for (NSString *imageName in objects) {
    
        UIImageView *view=[[UIImageView alloc]initWithFrame:CGRectMake(i* topView.frame.size.width, topView.frame.origin.y, topView.frame.size.width, topView.frame.size.height)];
        
        NSURL *url = [NSURL URLWithString:imageName];
        
        if (url && url.scheme && url.host)
        {
            [[SDImageCache sharedImageCache] queryDiskCacheForKey:[url absoluteString] done:^(UIImage *image, SDImageCacheType cacheType) {
                
                if (image) {
                    [view setImage:image];
                }else{
                    
                    [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:url]] forKey:[url absoluteString]];
                    [view sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"no_product.png"]];
                }
                
            }];
            
 
        }else{
            
            [view setImage:[UIImage imageNamed:imageName]];
            
        }
        

        
        view.tag=i;
        
        view.clipsToBounds=TRUE;
        
        [self.embededScrollView addSubview:view];
        i++;
        
        
        
    }
    
    topView.frame = CGRectOffset(topView.frame, 0, 0);
    topHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, topView.frame.size.width, topViewHeight)];
    [topHeaderView addSubview:self.embededScrollView];
    
    return topHeaderView;

}



#pragma mark - Page Scroll Deleagte

-(void)itemClicked:(int)value{
    
    [self.embededScrollView setContentOffset:CGPointMake(320*(value-1), 0) animated:YES];
    
}


#pragma mark - table View Functions

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    switch (indexPath.row) {
        case 0:
            
            return 55;
        
            break;
        case 1:
            
            return 55;
            break;

        case 2:
            
            return 60;
            break;
 
        case 3:
            
            return 80;
            break;
        
        case 4:
        
            return 50;
            break;
       
        case 5:
            
            return applicationLabelSize.size.height+50;
            break;
       
        case 6:
            
            if ([related_thumbnails count]>0) {
                
                return 160;
            }else{
                
                return 55;
            }
            
            break;
       
        case 7:
            
            return 55;
            break;
            
        default:
            
            return 0;
            break;
   
    }
    
}


#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([related_thumbnails count]==0) {
      
        return 7;
    
    } else {
    
        return 8;
    
    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
           
        case 0:
            
            return [self getProductTitleCell:tableView WithIndex:indexPath];
            break;
       
        case 1:
            
            return [self getActionButtonCell:tableView  WithIndex:indexPath];
            break;
            
            
        case 2:
            
            return [self getSelectedProductCell:tableView WithIndex:indexPath];
            break;
        
        case 3:
            
            
            return [self getDescriptionCell:tableView WithIndex:indexPath];
            
            break;
        
        case 4:
            
            return [self getSpecificationCell:tableView WithIndex:indexPath];
            break;
            
        case 5:
            
            return [self getApplicationCell:tableView WithIndex:indexPath];
            break;
            
        case 6:
            
            if ([related_thumbnails count]>0) {
                
                return [self getRelatedProductCell:tableView WithIndex:indexPath];
                
            } else {
           
                return [self getContactSellerCell:tableView WithIndex:indexPath];
                
            }
            
            break;
            
        case 7:
            
            return [self getContactSellerCell:tableView WithIndex:indexPath];
            break;
        
        default:
            
            return nil;
            break;
    
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSObject *myValue=descriptionValue;
    
    switch (indexPath.row) {
        case 0:
            
            return;
            break;
           
        case 2:
            
            
            [self callSelectAttribute];
            break;

        case 3:
            
            
            if (myValue!=[NSNull null]) {
                
                DescriptionView *controller=[[DescriptionView alloc]initWithDescription:descriptionValue];
                self.base.navigationController.interactivePopGestureRecognizer.enabled=YES;
                self.base.navigationController.interactivePopGestureRecognizer.delegate=controller;
                
                [self.base.navigationController pushViewController:controller animated:YES];
                
            }
            
            break;
            
        case 4:
            
            if ((NSObject*)specsArray!=[NSNull null]) {
                
                SpecificationView *controller=[[SpecificationView alloc]initWithMetrixArray:specsArray withGroupedIds:groupedIds withGroupedThumbnails:groupedThumbnails];
                self.base.navigationController.interactivePopGestureRecognizer.enabled=YES;
                self.base.navigationController.interactivePopGestureRecognizer.delegate=controller;
                
                [self.base.navigationController pushViewController:controller animated:YES];
                
            }else{
                
                SCLAlertView *alert = [[SCLAlertView alloc] init];
                [alert showInfo:self.base title:@"Oops.." subTitle:@"Sorry,specification data unavailable" closeButtonTitle:@"Ok" duration:0.0f];
                
            }
            
            break;
            
        case 5:
            
            return;
            break;
            
            
        case 6:
            
            return;
            break;
            
        default:
            
            return;
            break;
    
    }
    
    
}

#pragma mark - Custom Cell Views

-(PageScrollViewCell*)getPageScrollCell{
    
    PageScrollViewCell *cell;
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"PageScrollViewCell" owner:self options:nil] objectAtIndex:0];
    
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    cell.delegate=self;
    [cell setUpACPScroll];
    
    cell.frame=CGRectMake(0,280, cell.frame.size.width, cell.frame.size.height);
    
    return cell;
}


-(RelatedProductsViewCell*)getRelatedProductCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    static NSString *identifier = @"RelatedProductsViewCell";
    RelatedProductsViewCell *cell =(RelatedProductsViewCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"RelatedProductsViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    cell.delegate=self;
    
    
    if ((NSObject*)related_ids !=[NSNull null]) {
        
        [cell setUpACPScrollWithRelatedProductsThumnails:related_thumbnails andIDs:related_ids];
        
    }
    
    return cell;
}


-(ProductTitleViewCell*)getProductTitleCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
   
    static NSString *identifier = @"ProductTitleViewCell";
    ProductTitleViewCell *cell =(ProductTitleViewCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"ProductTitleViewCell" owner:self options:nil] objectAtIndex:0];
    }
   
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    float rate=[self.price floatValue];
    float discount=[self.discountPrice floatValue];
    float discounted=rate - (rate*(discount/100));
    
    cell.mrpLabel.text=[NSString stringWithFormat:@"₹%.02f",rate];
    cell.priceLabel.text=[NSString stringWithFormat:@"₹%.02f",discounted];
    cell.priceLabel.textColor=[UIColor redColor];
    
    cell.discountinPercent.text=[NSString stringWithFormat:@"%.f %% off",discount];
    cell.discountinPercent.tintColor=[UIColor grayColor];
    
    
    
    if (![[NSString stringWithFormat:@"%@",self.stockLeft] isEqualToString:@"0"])
    {
        cell.inStockDetails.text=[NSString stringWithFormat:@"%@ %@ in stock",self.stockLeft!=nil?self.stockLeft:@"0",self.unitValue!=nil?self.unitValue:@"0"];
        cell.inStockDetails.textColor=[UIColor colorWithRed:48.0f/255.0f green:150.0f/255.0f blue:69.0f/255.0f alpha:1.0f];
        
    } else {
        
        cell.inStockDetails.text=[NSString stringWithFormat:@"Out of stock"];
        cell.inStockDetails.textColor=[UIColor redColor];
        
    }
    
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:cell.mrpLabel.text];
    
    [titleString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    [cell.mrpLabel  setAttributedText:titleString];
    
    [cell.mrpLabel sizeToFit];
    [cell.discountinPercent sizeToFit];
    [cell.priceLabel sizeToFit];
    [cell.inStockDetails sizeToFit];
    

    return cell;

}



-(ActionButtonsCell*)getActionButtonCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    
    static NSString *identifier = @"ActionButtonsCell";
    ActionButtonsCell *cell =(ActionButtonsCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"ActionButtonsCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    [cell.buttonCart setBackgroundImage:[UIImage imageNamed:@"addtocart.png"] forState:UIControlStateNormal];
    [cell.buttonWishList setBackgroundImage:[UIImage imageNamed:@"toWishlist.png"] forState:UIControlStateNormal];
    
    
    
    cell.buttonWishList.selected=isFav;
    cell.buttonCart.selected=isCart;
    cell.delegate=self;
    
    return cell;
    
}

-(GroupSelectionCell*)getSelectedProductCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    static NSString *identifier = @"GroupSelectionCell";
    GroupSelectionCell *cell =(GroupSelectionCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"GroupSelectionCell" owner:self options:nil] objectAtIndex:0];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([self.allGroupProducts count]>0) {
      
        
        Products *prod=(Products*)[self.allGroupProducts objectAtIndex:currentGroupOpen];
        
        NSLog(@"All attributes %@",[prod.attributes allObjects]);
        
        [cell.image3 setImageWithURL:[NSURL URLWithString:prod.thumbnail.p_id] placeholderImage:[UIImage animatedImageNamed:@"30.gif" duration:0.0f]];
        
        NSString *name1=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[prod.attributes allObjects] objectAtIndex:0]).name];
        
        NSString *name2=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[prod.attributes allObjects] objectAtIndex:1]).name];
        
        NSString *value1=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[prod.attributes allObjects] objectAtIndex:0]).attributeValue];
        
        NSString *value2=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[prod.attributes allObjects]objectAtIndex:1]).attributeValue];
        
        
        cell.selectedAttribute.text=[NSString stringWithFormat:@"%@:%@\n%@:%@",name1,value1,name2,value2];
        
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    
    return cell;
    
}


-(DescriptionCell*)getDescriptionCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    
    static NSString *identifier = @"DescriptionCell";
    DescriptionCell *cell =(DescriptionCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"DescriptionCell" owner:self options:nil] objectAtIndex:0];
    }
    
    NSObject *myvalue=descriptionValue;
    
    if (myvalue==[NSNull null]) {
       
        cell.rightNavigationButton.hidden=TRUE;
        cell.descriptionDetails.text=@"No Description";
        
    } else {
        
        cell.rightNavigationButton.hidden=FALSE;
        cell.descriptionDetails.text=[self flattenHTML:descriptionValue];
        
    }
    
    
    return cell;
    
}


-(SpecificationCell*)getSpecificationCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    
    static NSString *identifier = @"SpecificationCell";
    SpecificationCell *cell =(SpecificationCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"SpecificationCell" owner:self options:nil] objectAtIndex:0];
    }
    
    
    //cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        
        [cell setLayoutMargins:UIEdgeInsetsZero];
    
    }
    
    if ((NSObject*)specsArray==[NSNull null]) {
        
        cell.rightDetailsButton.hidden=TRUE;
        
    } else {
        
        cell.rightDetailsButton.hidden=FALSE;
        
    }
    
    return cell;
    
}


-(ApplicationCell*)getApplicationCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    
    static NSString *identifier = @"ApplicationCell";
    ApplicationCell *cell =(ApplicationCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"ApplicationCell" owner:self options:nil] objectAtIndex:0];
    }
    
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    
    NSString *applicationsString=@"";
    
    int i=1;
    for (NSString *string in applicationList) {
        
        applicationsString=[applicationsString stringByAppendingString:[NSString stringWithFormat:@"%d. %@\r",i,string]];
        i++;
    }

    
    cell.applicationValuesTextView.text=applicationsString;
    
    cell.mainView.frame=CGRectMake(cell.mainView.frame.origin.x, cell.mainView.frame.origin.y, cell.mainView.frame.size.width, applicationLabelSize.size.height+50);
    
    cell.frame=CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, applicationLabelSize.size.height+50);
    
    cell.applicationValuesTextView.frame =CGRectMake(5, cell.applicationValuesTextView.frame.origin.y, applicationLabelSize.size.width, applicationLabelSize.size.height);
    
 
    return cell;
    
}

-(ContactAdmin*)getContactSellerCell:(UITableView*)tableView WithIndex:(NSIndexPath*)indexPath{
    
    
    static NSString *identifier = @"ContactAdmin";
    ContactAdmin *cell =(ContactAdmin*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"ContactAdmin" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.delegate=self;
    return cell;
    
}



#pragma  mark - Related Product Delegate

-(void)relatedItemSelected:(NSString *)idValue{
    
    [self.base.navigationController pushViewController:[[ProductViewController alloc]initWithProductID:idValue] animated:YES];
    
}

#pragma mark - UIScrollView Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
    if (scrollView==self.localTableCopy) {
        
        ScrollDirection scrollDirection;
        
        if (lastContentOffset > scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionUp;
        else if (lastContentOffset < scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionDown;
        else
            scrollDirection=ScrollDirectionNone;
        
        
        lastContentOffset = scrollView.contentOffset.y;
        
        if (scrollDirection== ScrollDirectionNone) {
            
            [self.base.navigationController setNavigationBarHidden:FALSE animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];

        }else if (scrollDirection==ScrollDirectionDown){
            
            [self.base.navigationController setNavigationBarHidden:TRUE animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldHide" object:self];
            
        }else if (scrollDirection==ScrollDirectionUp){
            
            [self.base.navigationController setNavigationBarHidden:FALSE animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];
        }
        
    }else{
        
        currentImageOpen=ceilf((scrollView.contentOffset.x)/([[UIScreen mainScreen]bounds].size.width));
        
    }

}



#pragma mark - Navigaton Calls

-(void)callEnquiryView{
    
    self.MessageSubject.subjectField.text=[NSString stringWithFormat:@"Enquiry Of %@",self.productDictionary.name];
    [self.MessageSubject.messageField becomeFirstResponder];
     self.popup = [AFPopupView popupWithView:self.MessageSubject.view];
    [self.popup show];
    
}


-(void)callSelectAttribute{
    
    
    [self.selectAttribute.productImage setImageWithURL:[NSURL URLWithString:self.productDictionary.thumbnail.p_id] placeholderImage:[UIImage animatedImageNamed:@"30.gif" duration:0.0f]];
    
    self.selectAttribute.productName.text =self.productDictionary.name ;
    
    NSLog(@"Before sending %@",((Products*)[self.allGroupProducts lastObject]).description);
    
    self.selectAttribute.productDictionaries=self.allGroupProducts;
    self.selectAttribute.delegate=self;
    
    self.popup = [AFPopupView popupWithView:self.selectAttribute.view];
    
    [self.popup show];
    
}

-(void)hideSelectAttribute{
    
    [self.popup hide];
    
}

#pragma mark - functional Task


-(void)hide{
    
    [self.popup hide];
    
}

-(void)saveMessage{
   
    [self hide];
    
    self.MessageSubject.threadObj.linkedTo=LinkedTo_Product;
    self.MessageSubject.threadObj.linkedToRef=self.productDictionary.p_id;
    
            [self performSelectorInBackground:@selector(sendMessageToServerInBackground) withObject:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
    
}



-(void)sendMessageToServerInBackground{
    
    [MessagesCall_Response callApiForMessageCreation:self.MessageSubject.threadObj];
    
}


-(NSString *)flattenHTML:(NSString *)html {
    
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:html];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        
        [theScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}



#pragma mark - title cell delegate

-(void)addedToWishList:(id)sender{
    
    
    RNThemeImageButton *button=(RNThemeImageButton*)sender;
    
    if ((id)self.productDictionary.p_id !=[NSNull null]) {
        
            
        if (button.selected) {
            
            button.selected=FALSE;
            
            
            [self performSelectorInBackground:@selector(removeFromWishListinBG:) withObject:button];
            
        } else {
            
            button.selected=TRUE;
            
            
            [self performSelectorInBackground:@selector(addToWishListinBG:) withObject:button];
            
            
        }

        
    } else {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self.base title:@"Sorry" subTitle:@"Your product can not be added to cart,Please try later" closeButtonTitle:@"Ok" duration:0.0f];
        
    }
    
}


-(void)addToWishListinBG:(UIButton*)button{
    
    
    if ([ProductCall_Response callApiForRemovingProductWithIDToWishlist:self.productDictionary.p_id]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            Products *product=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id == %@",self.productDictionary.p_id]] lastObject];
            product.isFavourite=[NSNumber numberWithBool:TRUE];
            [GlobalCall saveContext];
            
        });
        
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            Products *product=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id == %@",self.productDictionary.p_id]] lastObject];
            product.isFavourite=[NSNumber numberWithBool:FALSE];
            [GlobalCall saveContext];
            
        });
        
    }
    
}

-(void)removeFromWishListinBG:(UIButton*)button{
    
    
    NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",self.productDictionary.p_id]]];
    Products *prodNow=[arrayOfFetch lastObject];
    
    if ([ProductCall_Response callApiForRemovingProductWithIDToWishlist:self.productDictionary.p_id]) {
        
        prodNow.isFavourite=[NSNumber numberWithBool:FALSE];
       [GlobalCall saveContext];
        
    }else{
        
        prodNow.isFavourite=[NSNumber numberWithBool:TRUE];
        [GlobalCall saveContext];
    
    }
    
    
}


-(void)addedToCartForCheckOut:(id)sender{
    
    
    if (![self.productDictionary.isInCart boolValue]) {
    
        NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",self.productDictionary.p_id]]];
        Products *prodNow=[arrayOfFetch lastObject];
        
        prodNow.isInCart=[NSNumber numberWithBool:TRUE];
        prodNow.cartQty=[NSNumber numberWithInt:1];
        [GlobalCall saveContext];
        
    }
    
    
    LeveyTabBarController *controller=[[LeveyTabBarController alloc]init];
    controller=((LeveyTabBarController*)self.base.view.window.rootViewController);
    [controller setSelectedIndex:3];
    
}


-(void)addedToCart:(id)sender{
   
    
    RNThemeImageButton *button=(RNThemeImageButton*)sender;
    
    if ((id)self.productDictionary.p_id !=[NSNull null]) {
        
        button.selected=!button.selected;
        
        if (button.selected) {
            
            NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",self.productDictionary.p_id]]];
            
            Products *prodNow=[arrayOfFetch lastObject];
            prodNow.isInCart=[NSNumber numberWithBool:TRUE];
            prodNow.cartQty=[NSNumber numberWithInt:1];
            
            [GlobalCall saveContext];
            

            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showSuccess:self.base title:@"Successful" subTitle:@"Product added to cart" closeButtonTitle:@"Ok" duration:0.0f];
            
            
            
        } else {
            
            NSArray *arrayOfFetch=[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",self.productDictionary.p_id]]];
            
            Products *prodNow=[arrayOfFetch lastObject];
            prodNow.isInCart=[NSNumber numberWithBool:FALSE];
            prodNow.cartQty=[NSNumber numberWithInt:0];
            
            [GlobalCall saveContext];
           
            
            button.selected=TRUE;
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showInfo:self.base title:@"Info" subTitle:@"Product is already in cart" closeButtonTitle:@"Ok" duration:0.0f];
            
        }
        
        
    }else{
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self.base title:@"Sorry" subTitle:@"Your product can not be added to wishlist,Please try later" closeButtonTitle:@"Ok" duration:0.0f];
        
    }


}



-(void)dismissPopup{
    
    [field resignFirstResponder];
    
    if ([field.text intValue]) {
        
        if ([field.text intValue] != 0) {
            
            
            //NSArray *qty=[[NSArray alloc]initWithObjects:[NSNumber numberWithInt:[field.text intValue]], nil];
            NSString *string=[NSString stringWithFormat:@"%@",field.text];
            
//            [AppProductDefaults saveCartList:[[((ProductViewController*)self.base).theProductDictionary valueForKey:JSON_RES_AppProducts_GroupedProducts] objectAtIndex:currentGroupOpen] withQuantity:string];
            
        } else {
            
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showNotice:self title:@"Invalid Quantity" subTitle:@"Please add   quantity more than zero" closeButtonTitle:@"Ok" duration:0.0f];
            
        }
        
    } else {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showNotice:self title:@"Invalid Quantity" subTitle:@"Please add quantity value." closeButtonTitle:@"Ok" duration:0.0f];
        
    }
    
}


#pragma mark - delegate of contact cell

-(void)openMessageEditor{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        User *user=[[[GlobalCall getDelegate]fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
        
        
        if ([[AppSettingDefaults getRegistrationType] isEqualToString:User_Registration_Call_Type_Query] && user.password== nil) {
            
            AuthChoiceController *cont=[[AuthChoiceController alloc]initWithTheView:self.base.view];
            
            UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:cont];
            navigation.navigationBarHidden=TRUE;
            
            [self.base.view.window.rootViewController presentViewController:navigation animated:YES completion:nil];
            
        }else{
            
            [self callEnquiryView];
            
        }
        
    });
    
    
}





#pragma mark - image transition code

-(void)tapped:(UIGestureRecognizer*)gesture{
    
    photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:nyListPhotos];
    photosViewController.delegate = self;
    [self.base presentViewController:photosViewController animated:YES completion:nil];
    
    
}

- (NSMutableArray*)newTestPhotos {
    
    NSMutableArray *array=[NSMutableArray new];
    
    if ([self.imageList count]>0) {
        
        
        int i=0;
        for (UIImageView *view in [self.embededScrollView subviews]) {
            
            NYTExamplePhoto *photo = [[NYTExamplePhoto alloc] init];
            
            photo.image =(UIImage*)view.image;
            
            photo.placeholderImage = [UIImage animatedImageNamed:@"30" duration:0.0f];
            photo.attributedCaptionTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d",i+1] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
            photo.attributedCaptionSummary = [[NSAttributedString alloc] initWithString:self.productDictionary.name attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
            // photo.attributedCaptionCredit = [[NSAttributedString alloc] initWithString:@"credit" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
            
            [array addObject:photo];
            
            i++;
        }
        
    } else if(viewForSingle.image) {
        
        NYTExamplePhoto *photo = [[NYTExamplePhoto alloc] init];
        
        photo.image =viewForSingle.image;
        photo.placeholderImage = [UIImage animatedImageNamed:@"30" duration:0.0f];
        photo.attributedCaptionTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d",1] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        photo.attributedCaptionSummary = [[NSAttributedString alloc] initWithString:self.productDictionary.name!=nil?self.productDictionary.name:@"" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        
        // photo.attributedCaptionCredit = [[NSAttributedString alloc] initWithString:@"credit" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
        
        [array addObject:photo];
        
        
    }
    
    
    return array;
}



#pragma mark - NYTPhotosViewControllerDelegate

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController referenceViewForPhoto:(id <NYTPhoto>)photo {
    
    
    return self.embededScrollView;
    
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController loadingViewForPhoto:(id <NYTPhoto>)photo {
    
    return nil;
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController captionViewForPhoto:(id <NYTPhoto>)photo {
    
    return nil;
}

- (NSDictionary *)photosViewController:(NYTPhotosViewController *)photosViewController overlayTitleTextAttributesForPhoto:(id <NYTPhoto>)photo {
    
    return nil;
    
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController didDisplayPhoto:(id <NYTPhoto>)photo atIndex:(NSUInteger)photoIndex {
    NSLog(@"Did Display Photo: %@ identifier: %lu", photo, (unsigned long)photoIndex);
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController actionCompletedWithActivityType:(NSString *)activityType {
    NSLog(@"Action Completed With Activity Type: %@", activityType);
}

- (void)photosViewControllerDidDismiss:(NYTPhotosViewController *)photosViewController {
    NSLog(@"Did Dismiss Photo Viewer: %@", photosViewController);
}


@end
