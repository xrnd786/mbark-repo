//
//  ProductListViewHelper.h
//  P1
//
//  Created by Xiphi Tech on 14/10/2014.
//
//

#import <UIKit/UIKit.h>


@interface GroupedProductViewHelper : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIView *localView;
@property (strong,nonatomic) UIViewController *base;


@property (strong,nonatomic) NSMutableArray *imageList;
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *price;
@property (strong,nonatomic) NSString *discountPrice;
@property (strong,nonatomic) NSArray *applicationList;
@property (strong,nonatomic) NSString *thumbnailImage;

@property (strong,nonatomic) NSString *stockLeft;
@property (strong,nonatomic) NSString *unitValue;
@property (strong,nonatomic) NSString *descriptionValue;
@property (strong,nonatomic) NSMutableArray *specsArray;

@property (strong,nonatomic) NSMutableArray *related_thumbnails;
@property (strong,nonatomic) NSMutableArray *related_ids;

@property (strong,nonatomic) NSMutableArray *groupedThumbnails;
@property (strong,nonatomic) NSMutableArray *groupedIds;

@property (strong,nonatomic) NSMutableArray *allGroupProducts;

@property (strong,nonatomic) Products *productDictionary;


-(id)initWithGroupedProductsTable:(UITableView*)table  withMainView:(UIView*)view  withController:(UIViewController*)controller withProductDictionary:(NSDictionary*)dictionary withProductObj:(Products*)product;
    
-(void)callEnquiryView;
-(void)reloadTheViews;


-(void)clearData;
-(void)hide;
-(void)saveMessage;

-(void)updateAllData;

    
@end
