//
//  GroupSelectionCell.h
//  mBark
//
//  Created by Xiphi Tech on 07/03/2015.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeLabel.h"


@interface GroupSelectionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *imageSelectedView;
@property (weak, nonatomic) IBOutlet UIImageView *image3;
@property (weak, nonatomic) IBOutlet RNThemeLabel *selectedAttribute;

@end
