//
//  addNewMessageSubject.m
//  mBark
//
//  Created by Xiphi Tech on 23/12/2014.
//
//

#import "SelectAttribute.h"
#import "selectAttributeCell.h"

#import "UIImage+animatedGIF.h"
#import "UIImageView+WebCache.h"
#import "ProductThumbnail.h"
#import "AppProductDefaults.h"

#import "AFPopupView.h"
#import "ProductAttributes.h"


@interface SelectAttribute ()<UIScrollViewDelegate,UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    int currentSelection;
}
@property (strong,nonatomic) NSMutableArray *selectedAttribute;
@property (strong,nonatomic) AFPopupView *popup;


@end

@implementation SelectAttribute

@synthesize productImage,productDictionaries,productName,selectedAttribute,popup,delegate,pictureSelectionCollection,variantsName,selectedIndex;

-(id)init{
    
    self = [super init];
    
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"SelectAttribute" owner:self options:nil];
        
        self.selectedAttribute=[[NSMutableArray alloc]init];
        //currentSelection=0;
        
        
    }
    
    return self;
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self.pictureSelectionCollection registerNib:[UINib nibWithNibName:@"selectAttributeCell" bundle:nil]   forCellWithReuseIdentifier: @"selectAttributeCell"];
    
    
    
}



- (IBAction)ExitPopUp:(id)sender {
    
    [self.delegate nowHideWithSelection:self.selectedAttribute andCurrentIndex:-1];
    
}

- (IBAction)ExitWithSelection:(id)sender {
    
    [self.delegate nowHideWithSelection:self.selectedAttribute andCurrentIndex:currentSelection];
    
}




#pragma mark - Collection View


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
  
    return [self.productDictionaries count];

}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //selectAttributeCell *cell;
    selectAttributeCell *cell= (selectAttributeCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"selectAttributeCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"selectAttributeCell" owner:self options:nil] objectAtIndex:0];
        
    }
    
    [cell.productImage setImageWithURL:[NSURL URLWithString:((Products*)[self.productDictionaries objectAtIndex:indexPath.row]).thumbnail.p_id] placeholderImage:[UIImage animatedImageNamed:@"30.gif" duration:0.0f]];
    
   
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-frame.png"]];
    
    if (indexPath.row == [self.selectedIndex integerValue]) {
        
        [self collectionView:collectionView didSelectItemAtIndexPath:indexPath];
        
    }
    
    return cell;

}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    [self.productImage setImageWithURL:[NSURL URLWithString:((Products*)[self.productDictionaries objectAtIndex:indexPath.row]).thumbnail.p_id] placeholderImage:[UIImage animatedImageNamed:@"30.gif" duration:0.0f]];
   
    self.productName.text=((Products*)[self.productDictionaries objectAtIndex:indexPath.row]).name;
    
    NSString *name1=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[((Products*)[self.productDictionaries objectAtIndex:indexPath.row]).attributes allObjects] objectAtIndex:0]).name];
    
    NSString *name2=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[((Products*)[self.productDictionaries objectAtIndex:indexPath.row]).attributes allObjects] objectAtIndex:1]).name];
    
    NSString *value1=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[((Products*)[self.productDictionaries objectAtIndex:indexPath.row]).attributes allObjects] objectAtIndex:0]).attributeValue];
    
    NSString *value2=[NSString stringWithFormat:@"%@",((ProductAttributes*)[[((Products*)[self.productDictionaries objectAtIndex:indexPath.row]).attributes allObjects] objectAtIndex:1]).attributeValue];
    
    self.variantsName.text=[NSString stringWithFormat:@"%@:%@\n%@:%@",name1,value1,name2,value2];
    
    
    currentSelection=(int)indexPath.row;
    [self.selectedAttribute removeAllObjects];
    [self.selectedAttribute addObject:[self.productDictionaries objectAtIndex:indexPath.row]];
    
    
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    currentSelection=-1;
    self.selectedAttribute =[[NSMutableArray alloc]init];
    
    
    
}


@end
