//
//  addNewMessageSubject.h
//  mBark
//
//  Created by Xiphi Tech on 23/12/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeTextView.h"
#import "RNThemeTextField.h"
#import "RNThemeLabel.h"

@protocol hideSelection <NSObject>

-(void)nowHideWithSelection:(NSArray*)selectionArray andCurrentIndex:(int)index;


@end

@interface SelectAttribute : UIViewController

@property (weak, nonatomic)  id<hideSelection> delegate;

@property (weak, nonatomic)  NSArray *productDictionaries;

@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UICollectionView *pictureSelectionCollection;
@property (weak, nonatomic) IBOutlet UILabel *variantsName;
@property (strong,nonatomic)  NSNumber *selectedIndex;

- (IBAction)ExitPopUp:(id)sender;
- (IBAction)ExitWithSelection:(id)sender;


@end
