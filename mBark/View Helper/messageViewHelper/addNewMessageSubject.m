//
//  addNewMessageSubject.m
//  mBark
//
//  Created by Xiphi Tech on 23/12/2014.
//
//

#import "addNewMessageSubject.h"

#import "Messages.h"
#import "MessagesDetails.h"

@interface addNewMessageSubject ()<UIScrollViewDelegate,UITextViewDelegate>


@end

@implementation addNewMessageSubject

@synthesize threadObj,messageEntityObj,subjectField,messageField;

-(id)init{
    
    self = [super init];
    if (self) {
        
        
        if ([GlobalCall isIpad]) {
            
            [[NSBundle mainBundle] loadNibNamed:@"addNewMessageSubject" owner:self options:nil];
            
        }else{
            
            [[NSBundle mainBundle] loadNibNamed:@"addNewMessageSubject" owner:self options:nil];
            
        }
        
       // [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)]];
        
        [self.subjectField becomeFirstResponder];
        self.messageField.delegate=self;
        
       
        
        
    }
    
    return self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

- (IBAction)hideNow:(id)sender {
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"HideAFPopup" object:nil];

}

-(void)tapped:(UITapGestureRecognizer*)tap{
    
    if ( [self.subjectField isKindOfClass:[UIResponder class]] == YES && [(UIResponder*)self.subjectField canResignFirstResponder] == YES )
        [self.subjectField resignFirstResponder];
    
    
    
}

- (IBAction)sendButtonClicked:(id)sender {
    
        [SVProgressHUD show];
        
        [self send];
    
}




#pragma mark - textview delegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
    style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    textView.inputAccessoryView = keyboardDoneButtonView;

    return YES;
    
}



-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    if ([self.messageField.text isEqualToString:@"Enter your text message here.."])
    {
        self.messageField.text=@"";
        
    }
    
}

-(void)textViewDidChange:(UITextView *)textView{
    
    
    
}


-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if([self.messageField.text isEqualToString:@""]){
        
         self.messageField.text=@"Enter your text message here..";
        
    }else{
        
        [textView endEditing:YES];

    }
        
}

//    
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    return YES;
//}


#pragma mark - send button 

- (void)doneClicked:(id)sender
{

    [self.view endEditing:YES];

}


-(void)send{
   
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"Messages" inManagedObjectContext:[GlobalCall getContext]];
    
    self.threadObj=[[Messages alloc]initWithEntity:entity insertIntoManagedObjectContext:[GlobalCall getContext]];
    
    
    NSEntityDescription *entityDeatail=[NSEntityDescription entityForName:@"MessagesDetails" inManagedObjectContext:[GlobalCall getContext]];
    
    self.messageEntityObj=[[MessagesDetails alloc]initWithEntity:entityDeatail insertIntoManagedObjectContext:[GlobalCall getContext]];
    
    
    self.threadObj.subject=self.subjectField.text;
    
    NSMutableArray *messageArray=[[NSMutableArray alloc]init];
    
    
    self.messageEntityObj.direction=@"FromAppUser";
    self.messageEntityObj.status=@"Sent";
    self.messageEntityObj.body=self.messageField.text;
    
    [messageArray addObject:self.messageEntityObj];
    
    [self.threadObj addMessagesDetailsObject:self.messageEntityObj];
    
    NSString *subject=self.subjectField.text;
    NSString *message=self.messageField.text;
    
    
    if ([message isEqualToString:@"Enter your text message here.."] || [subject isEqualToString:@""] || subject == nil || message == nil || [message isEqualToString:@""])
    {
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            SCLAlertView *view=[[SCLAlertView alloc]init];
            [view showWarning:self title:@"Message necessary" subTitle:@"Please add message." closeButtonTitle:@"Ok" duration:0.0f];
            
        });
        
    }else{
        
       
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SaveNewMessage" object:nil];

    }
    
    
    
    
}


@end
