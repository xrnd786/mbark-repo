//
//  messageView_helper.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "messageView_helper.h"

#import "MessgaeViewCell.h"
#import "ChatViewController.h"

#import "ThemeManager.h"
#import "RNThemeTextField.h"
#import "AFPopupView.h"
#import "addNewMessageSubject.h"
#import "MessagesCall_Response.h"
#import "MLPAccessoryBadge.h"

@interface messageView_helper ()<UISearchControllerDelegate,UISearchBarDelegate>
{
    UISearchBar *searchBar;
    NSMutableArray *currentBadgeCountArray;
}

@property (strong,nonatomic) NSMutableArray *messagesIDArray;
@property (strong,nonatomic) NSArray *unmodifiedMessagesArray;


@property (nonatomic, strong) AFPopupView *popup;
@property (strong,nonatomic) addNewMessageSubject *MessageSubject;
@property (strong,nonatomic) UIViewController *localCopyOfController;
@property (strong,nonatomic) UIView *localSorryCopy;
@property (strong,nonatomic) NSArray *tempMainMessageArray;
@property (strong,nonatomic) UIImage *meImage;
@property (strong,nonatomic) UIImage *adminImage;


@end

@implementation messageView_helper

@synthesize delegate,messagesArray;
@synthesize popup=_popup;
@synthesize MessageSubject=_MessageSubject;
@synthesize localTableCopy=_localTableCopy;
@synthesize localCopyOfController=_localCopyOfController;
@synthesize localSorryCopy=_localSorryCopy;
@synthesize tempMainMessageArray;
@synthesize messagesIDArray=_messagesIDArray;
@synthesize unmodifiedMessagesArray=_unmodifiedMessagesArray;


-(id)initWithTable:(UITableView*)tableView withSorryView:(UIView*)sorryView withSelfView:(UIViewController*)controller withThreadArray:(NSArray*)messages{
    
    self = [super init];
    if (self) {
        
        self.MessageSubject=[[addNewMessageSubject alloc]init];
        currentBadgeCountArray=[NSMutableArray new];
        
        self.localSorryCopy=sorryView;
        self.localTableCopy=tableView;
        self.localCopyOfController=controller;
        
        
        tableView.delegate=self;
        tableView.dataSource=self;
        [tableView setShowsVerticalScrollIndicator:NO];
        [tableView setBackgroundColor:[UIColor whiteColor]];
        
        tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        tableView.separatorColor=[UIColor colorWithRed:200.0f/255.0f green:200.0f/255.0f blue:200.0f/255.0f alpha:1.0f];
        
        if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [tableView setLayoutMargins:UIEdgeInsetsZero];
        }
       
        self.messagesArray=[[NSMutableArray alloc]initWithArray:messages];
        self.unmodifiedMessagesArray=[[NSArray alloc]initWithArray:messages];
        
        if ([self.messagesArray count]==0) {
            
            self.localSorryCopy.hidden=FALSE;
            self.localTableCopy.hidden=TRUE;
            
        } else {
            
            self.localSorryCopy.hidden=TRUE;
            self.localTableCopy.hidden=FALSE;
        
        }
     
        
        NSString *imagePath =[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.png",Local_User_Name]];
        
        self.meImage=[UIImage imageWithContentsOfFile:imagePath]!=nil?[UIImage imageWithContentsOfFile:imagePath]:[UIImage imageNamed:@"me.png"];
        
        imagePath =[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.png",Local_Logo_Name]];
        self.adminImage=[UIImage imageWithContentsOfFile:imagePath]!=nil?[UIImage imageWithContentsOfFile:imagePath]:[UIImage imageNamed:@"username.png"];
        
        if ([self.messagesArray count]!=0) {
           
            tableView.tableFooterView = ({
                
                UIView *viewBase=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 90)];;
                
                RNThemeLabel *label=[[RNThemeLabel alloc]initWithFrame:viewBase.frame];
                label.text=@"No more messages";
                label.textAlignment=NSTextAlignmentCenter;
                label.font=[UIFont italicSystemFontOfSize:9.0f];
                label.textColorKey=@"secondaryColor";
                [label applyTheme];
                
                [viewBase addSubview:label];
                viewBase;
                
            });
            
        }
        
        _localCopyOfController.navigationController.interactivePopGestureRecognizer.enabled = YES;
        
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 41, [[UIScreen mainScreen]bounds].size.width, 40)];
        searchBar.delegate=self;
        
        UISearchController *searchController = [[UISearchController alloc] init];
        searchController.delegate = self;
        [searchController setView:searchBar];
        
        _localTableCopy.tableHeaderView = searchBar;
        _localTableCopy.contentOffset = CGPointMake(0, CGRectGetHeight(searchBar.frame));
        
    }
    
    return self;
    
}


#pragma mark - tap 

-(void)tapped:(UIGestureRecognizer*)gesture{
    
    [searchBar resignFirstResponder];

}


#pragma mark UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return [self.messagesArray count];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!tableView.isEditing) {
      
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    }
    
    [self.delegate didSelectIndex:(int)indexPath.row withNewArray:self.messagesArray];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;

}


#pragma mark - UITable Data Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString* cellIdentifier;
    
    if ([GlobalCall isIpad]) {
        
        cellIdentifier=@"MessgaeViewCell";
        
    } else {
        
        cellIdentifier=@"MessgaeViewCell";
        
        
    }
    
    MessgaeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = (MessgaeViewCell*) [nib objectAtIndex:0];
        
        assert([cellIdentifier isEqualToString:cell.reuseIdentifier]);
    }
    
    Messages *threadObj=(Messages*)[self.messagesArray objectAtIndex:indexPath.row];
    MessagesDetails *lastDetails=[[threadObj.messagesDetails allObjects] lastObject];
    
    cell.messageTitle.text=threadObj.subject;
    cell.messgaeBody.text=lastDetails.body;
    
    NSDateFormatter *dateformat=[[NSDateFormatter alloc]init];
    [dateformat setDateFormat:@"dd-MM-yyyy"];
    
    cell.dateValue.text=[dateformat stringFromDate:lastDetails.createdOn];
    
    if ([lastDetails.direction isEqualToString:Message_Direction_ToAppUser]) {
        
        cell.fromLabel.text=FROM_ADMIN_PLACEHOLDER;
        cell.messageIcon.image=self.adminImage;
        
        if ([lastDetails.status isEqualToString:Message_STATUS_Pending]) {
        
            cell.messageTitle.textColor=[UIColor blackColor];
            cell.messageTitle.font=[UIFont fontWithName:[[ThemeManager sharedManager]fontForKey:@"generalFont"].fontName size:19.0f];
        
        }else{
            
            cell.messageTitle.textColorKey=@"secondaryColor";
            [cell.messageTitle applyTheme];
            cell.messageTitle.font=[UIFont fontWithName:[[ThemeManager sharedManager]fontForKey:@"generalFont"].fontName size:17.0f];
        }
        
    }else{
        
        cell.fromLabel.text=FROM_ME_PLACEHOLDER;
        
        
        cell.messageIcon.image=self.meImage;
        
        cell.messageTitle.textColorKey=@"secondaryColor";
        [cell.messageTitle applyTheme];
        cell.messageTitle.font=[UIFont fontWithName:[[ThemeManager sharedManager]fontForKey:@"generalFont"].fontName size:19.0f];
        
    }
    
    cell.messageIcon.layer.cornerRadius=20;
    cell.messageIcon.layer.masksToBounds=YES;
    cell.messageIcon.clipsToBounds = YES;
    
    
    MLPAccessoryBadge *accessoryBadge = [MLPAccessoryBadge new];
    [accessoryBadge.textLabel setShadowOffset:CGSizeZero];
    [accessoryBadge setCornerRadius:0];
    [accessoryBadge setHighlightAlpha:0];
    [accessoryBadge setShadowAlpha:0];
    [accessoryBadge setBackgroundColor:[[ThemeManager sharedManager]colorForKey:@"notification"]];
    [accessoryBadge setGradientAlpha:0];

    
    int currentBadgeCount=0;
    NSArray *detailsArray=[NSArray new];
    detailsArray=[threadObj.messagesDetails allObjects];
    
    for (MessagesDetails *entity in detailsArray) {
        if ([entity.direction isEqualToString:Message_Direction_ToAppUser]) {
            if ([entity.status isEqualToString:Message_STATUS_Pending]) {
                currentBadgeCount++;
            }
        }
    }
    
    if (currentBadgeCount !=0) {
        
        NSString *badgeValue=[NSString stringWithFormat:@"%d",currentBadgeCount];
        [accessoryBadge setText:badgeValue];
        [cell setAccessoryView:accessoryBadge];
        
    }else{
        
        [accessoryBadge removeFromSuperview];
        [cell setAccessoryView:nil];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        
    }

    return cell;
    
}

#pragma mark - Date formatter

- (NSString *)curentDateStringFromUTCDate:(NSString *)utcDateTimeInLine withFormat:(NSString *)dateFormat {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [formatter dateFromString:utcDateTimeInLine];
    
    
    NSDateFormatter *formatterFinal = [[NSDateFormatter alloc]init];
    [formatterFinal setDateFormat:dateFormat];
    
    NSString *convertedString = [formatterFinal stringFromDate:date];
    
    return convertedString;
}

#pragma mark - add New subject value

-(void)addClicked{
   
    
    _popup = [AFPopupView popupWithView:self.MessageSubject.view];
    [_popup show];
    
}


-(void)hide {
    
    [_popup hide];
    [self reloadTheViewsWithArray:self.messagesArray];
   
}

-(void)reloadTheViewsWithArray:(NSArray*)array{

    self.messagesArray=[[NSMutableArray alloc]initWithArray:array];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([self.messagesArray count]!=0) {
            
            self.localTableCopy.hidden=FALSE;
            self.localSorryCopy.hidden=TRUE;
            
            
        } else {
            
            self.localTableCopy.hidden=TRUE;
            self.localSorryCopy.hidden=FALSE;
        }
    
        
       [self.localTableCopy reloadData];

    });
    
}

#pragma mark - functional Task

-(void)saveMessage{
    
   
    
    [self hide];
    
    self.MessageSubject.threadObj.linkedTo=LinkedTo_General;
    self.MessageSubject.threadObj.linkedToRef=@"";
    
    
    [self performSelectorInBackground:@selector(sendMessageToServerInBackground) withObject:nil];
            
    dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
    });
    
    
}

-(void)sendMessageToServerInBackground{
    
    [MessagesCall_Response callApiForMessageCreation:self.MessageSubject.threadObj];
    
}


#pragma mark - Search Task

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    //searchText=[searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
   
    if ([searchText isEqualToString:@""]) {
        
        [self cancelSearch];
    
    }else{
        
        [self searchWithText:searchText];
        
    }

}

-(void)searchWithText:(NSString*)textToSearch{
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.subject contains[c] %@) OR (SELF.lastMessage.body contains[c] %@)",textToSearch,textToSearch];
    // if you need case sensitive search avoid '[c]' in the predicate
    
    NSArray *results = [[NSArray alloc]initWithArray:[self.messagesArray filteredArrayUsingPredicate:predicate]];
    
    NSMutableArray *messagesResultArray=[[NSMutableArray alloc]initWithArray:results];
    
    [self updateTableWithSearchResult:messagesResultArray];
    
}


-(void)updateTableWithSearchResult:(NSArray*)results{
    
    self.tempMainMessageArray=[[NSArray alloc]initWithArray:self.messagesArray];
    self.messagesArray=[[NSMutableArray alloc]initWithArray:results];
    [self.localTableCopy reloadData];
    
}


-(void)cancelSearch{
    
    self.messagesArray=[[NSMutableArray alloc]initWithArray:self.unmodifiedMessagesArray];
    [self.localTableCopy reloadData];
    [self tapped:[UIGestureRecognizer new]];
    
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1{
    
    [self searchWithText:searchBar1.text];
    
}


- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    
    
}



@end
