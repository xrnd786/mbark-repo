//
//  MessgaeViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 25/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "TDBadgedCell.h"


@interface MessgaeViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RNThemeLabel *messageTitle;
@property (weak, nonatomic) IBOutlet RNThemeLabel *messgaeBody;
@property (weak, nonatomic) IBOutlet UILabel *dateValue;
@property (weak, nonatomic) IBOutlet RNThemeLabel *fromLabel;
@property (weak, nonatomic) IBOutlet UIImageView *messageIcon;

@end
