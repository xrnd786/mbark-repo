//
//  messageView_helper.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

@protocol  MessageSelected_Protocol <NSObject>

-(void)didSelectIndex:(int)value withNewArray:(NSArray*)resultMessageArray;

@end

@interface messageView_helper : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (strong,nonatomic) id<MessageSelected_Protocol> delegate;

-(id)initWithTable:(UITableView*)tableView withSorryView:(UIView*)sorryView withSelfView:(UIViewController*)controller withThreadArray:(NSArray*)messages;


@property (strong,nonatomic) NSMutableArray *messagesArray;
@property (strong,nonatomic) UITableView *localTableCopy;


-(void)addClicked;

-(void)hide;
-(void)saveMessage;
-(void)reloadTheViewsWithArray:(NSArray*)array;


-(void)searchWithText:(NSString*)textToSearch;
-(void)cancelSearch;

@end
