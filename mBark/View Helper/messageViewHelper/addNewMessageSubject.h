//
//  addNewMessageSubject.h
//  mBark
//
//  Created by Xiphi Tech on 23/12/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeTextView.h"
#import "RNThemeTextField.h"
#import "RNThemeLabel.h"

typedef enum : NSUInteger {
    Product,
    General,
    CartQuery,
} LinkedToType;

#define LinkedTo_Product @"Product"
#define LinkedTo_General @"General"
#define LinkedTo_Cart @"Cart"

#import "Message.h"
#import "MessagesDetails.h"

@interface addNewMessageSubject : UIViewController

- (IBAction)hideNow:(id)sender;

@property (strong, nonatomic)  Messages *threadObj;
@property (strong, nonatomic)  MessagesDetails *messageEntityObj;

@property (weak, nonatomic) IBOutlet UITextField *subjectField;
@property (weak, nonatomic) IBOutlet UITextView *messageField;


- (IBAction)sendButtonClicked:(id)sender;

@end
