//
//  HomeNavigation.h
//  P1
//
//  Created by Xiphi Tech on 09/10/2014.
//
//

#import <UIKit/UIKit.h>



@interface HomeNavigation : UIView


@property (nonatomic, retain) UIViewController *baseView;

+ (id)sharedManager:(UIViewController*)myView;

- (id)initWithView:(UIViewController*)myView;

//PUBLIC (AVAILABLE TO HOME)
-(void)tableSettingsCell:(NSIndexPath*)indexPath;

-(void)tableOneList;
-(void)openSideMenu;


-(void)openProfileView;
-(void)openMessageView;
-(void)openWishListView;
-(void)openAboutView;
-(void)openTermsOfUseView;
-(void)openSentCartView;

-(void)openProductList:(NSString *)categoryName;
-(void)openProductListWithList:(NSArray *)productList;
-(void)openProduct:(Products*)product;
-(void)showBottomAndTop;


@end
