//
//  CategoryViewController.m
//  mBark
//
//  Created by Xiphi Tech on 30/06/2015.
//
//

#import "CategoryViewController.h"

#import "UIColor+HexString.h"
#import "UIImageView+WebCache.h"

#import "AppUIDefaults.h"

#import "ProductCategory.h"
#import "CategoryImage.h"

@interface CategoryViewController ()

@end

@implementation CategoryViewController

@synthesize dataArray=_dataArray;

-(id)init{
 
    self=[super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle]loadNibNamed:@"CategoryViewController" owner:self options:nil] lastObject];
        
    }
    
    return self;
}

- (IBAction)seeAllAction:(id)sender {

    [self.delegate seeAllCategory];
}

- (void)setUpACPScroll {
    
    _scrollMenu=[[ACPScrollMenu alloc]initWithFrame:CGRectMake(0, 0, self.baseView.frame.size.width, self.baseView.frame.size.height)];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_dataArray count]; i++)
    {
        
        NSLog(@"Category image Value %@",((CategoryImage*)((ProductCategory*)[_dataArray objectAtIndex:i]).image).image);

        NSString *imgName =((CategoryImage*)((ProductCategory*)[_dataArray objectAtIndex:i]).image).image;
        NSString *imgSelectedName =((CategoryImage*)((ProductCategory*)[_dataArray objectAtIndex:i]).image).image;
        
        ACPItem *item=[ACPItem new];
        item=[[ACPItem alloc] initACPItem:[UIImage imageNamed:@"bg.png"]   iconImage:imgName label:((ProductCategory*)[_dataArray objectAtIndex:i]).name andAction: ^(ACPItem *item) {
                    NSLog(@"Block called! %d", i);
        }];
       
        
        item.iconImageNew.tag=i+10;
        [item setHighlightedBackground:nil iconHighlighted:[UIImage imageNamed:imgSelectedName] textColorHighlighted:[UIColor colorWithHexString:[AppUIDefaults getPrimaryColor]]];
        
        [array addObject:item];
   
    }
    
    
    
    for (UIView *view in [_scrollMenu subviews]) {
        
        [view removeFromSuperview];
        
    }
    
    [_scrollMenu setUpACPScrollMenu:array];
    
    _scrollMenu.delegate = self;
    
    [self.baseView addSubview:_scrollMenu];
    
    for(ProductCategory *category in _dataArray) {
        
        NSString *imgName =((CategoryImage*)category.image).image;
        
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:imgName done:^(UIImage *image, SDImageCacheType cacheType) {
            
            int indexValue=(int)[[array valueForKeyPath:@"iconImageName"]indexOfObject:imgName];
            ACPItem *item=(ACPItem*)[_scrollMenu.scrollView viewWithTag:(indexValue+10)];
            
            if (image) {
                ((UIImageView*)[item.iconImageNew viewWithTag:(indexValue+10)]).image=image;
            }else{
                [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgName]]] forKey:imgName];
                [item.iconImageNew sd_setImageWithURL:[NSURL URLWithString:imgName] placeholderImage:[UIImage imageNamed:@"no_product.png"]];
            }
        }];
        
    }

}


-(void)scrollMenu:(ACPScrollMenu *)menu didSelectIndex:(NSInteger)selectedIndex{
    
    [_delegate categoryItemSelected:(int)selectedIndex];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
