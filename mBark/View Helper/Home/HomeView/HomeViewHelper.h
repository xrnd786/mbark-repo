//
//  MGSpotyViewController.h
//  MGSpotyView
//
//  Created by Matteo Gobbi on 25/06/2014.
//  Copyright (c) 2014 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define WEB @"Web"
#define PRODUCT @"Product"
#define CATEGORY @"Category"

extern CGFloat const kMGOffsetEffects;
extern CGFloat const kMGOffsetBlurEffect;

typedef enum : NSUInteger {
    TABLETYPENEWPRODUCT,
    TABLETYPEFEATURED,
    TABLETYPENONE,
} TABLETYPE;

typedef enum : NSUInteger {
    BANNERTYPEWEB,
    BANNERTYPEPRODUCT,
    BANNERTYPECATEGORY,
    BANNERTYPENOTHING,
} BANNERTYPE;



@protocol home_helper_Protocol <NSObject>

@required
-(void)productCellTouchedWith:(Products*)product;

@end



@interface HomeViewHelper : UIView <UIScrollViewDelegate>

@property (strong,nonatomic) id<home_helper_Protocol> delegate;


//Initialize for views
-(id)initWithTable:(TABLETYPE)type withMainView:(UIView*)view viewWithController:(UIViewController*)controller viewWithFeaturedCollectionView:(UICollectionView*)collection;


//UI Controls
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIImageView *mainImageView;

//Update data
@property (strong,nonatomic) NSArray *bannerImageList;
@property (strong,nonatomic) NSArray *bannerType;
@property (strong,nonatomic) NSArray *bannerNavigateTo;

@property (strong,nonatomic) NSArray *categoryList;
@property (strong,nonatomic) NSArray *productArray;




//reloadviews
-(void)updateTableHeader;
-(void)updateCategories;
-(void)updateFeatured;

- (void)scrollOnTimer;


@end
