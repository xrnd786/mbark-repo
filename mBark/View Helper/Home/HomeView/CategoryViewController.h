//
//  CategoryViewController.h
//  mBark
//
//  Created by Xiphi Tech on 30/06/2015.
//
//

#import <UIKit/UIKit.h>
#import "ACPScrollMenu.h"
#import "RNThemeButton.h"


@protocol CategoryItemProtocol <NSObject>

-(void)categoryItemSelected:(int)number;
-(void)seeAllCategory;


@end

@interface CategoryViewController : UIViewController<ACPScrollDelegate>


@property (strong,nonatomic) id<CategoryItemProtocol> delegate;

@property (strong, nonatomic)  ACPScrollMenu *scrollMenu;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak,nonatomic) NSArray *dataArray;
@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@property (weak, nonatomic) IBOutlet RNThemeButton *seeAllButton;

- (IBAction)seeAllAction:(id)sender;

-(void)setUpACPScroll;


@end
