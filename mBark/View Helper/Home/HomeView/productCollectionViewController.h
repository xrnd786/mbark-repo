//
//  productCollectionViewController.h
//  mBark
//
//  Created by Xiphi Tech on 01/07/2015.
//
//

#import <UIKit/UIKit.h>
#import "ACPFullScrollMenu.h"

#import "RNThemeButton.h"


@protocol CollectionItemProtocol <NSObject>

-(void)collectionItemSelected:(int)number;
-(void)seeAllCollectionProducts;

@end

@interface productCollectionViewController : UIViewController

@property (strong,nonatomic) id<CollectionItemProtocol> delegate;

@property (strong, nonatomic)  ACPFullScrollMenu *scrollMenu;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (strong,nonatomic) NSArray *dataArray;
@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@property (weak, nonatomic) IBOutlet RNThemeButton *seeAllButton;
@property (strong, nonatomic)  NSNumber *collectionTag;

- (IBAction)seeAllAction:(id)sender;

-(void)setUpACPScroll;



@end
