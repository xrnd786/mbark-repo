//
//  MGSpotyViewController.m
//  MGSpotyView
//
//  Created by Matteo Gobbi on 25/06/2014.
//  Copyright (c) 2014 Matteo Gobbi. All rights reserved.

#import <stdio.h>

#import "HomeViewHelper.h"

//*****TILE CELL*****//
#import "UITableGridViewCell.h"
#import "UIImageButton.h"
#import "UIColor+HexString.h"
#import "HomeFeatured.h"
#import "HomeFeatureddCell.h"
#import "HomeFeatureddCell_right.h"


//*****THEME VIEW*****//
#import "RNThemeLabel.h"
#import "RNThemeButton.h"
#import "RNThemeImageButton.h"

#import "AppUIDefaults.h"

#import "Home_DataHelper.h"
#import "AppProductDefaults.h"
#import "HomeNavigation.h"
#import "LeveyTabBarController.h"


#import "SwipeView.h"
#import "UIImage+animatedGIF.h"
#import "UIImageView+WebCache.h"

#import "In_AppWebView.h"
#import "CategoryMainListViewController.h"
#import "ProductCategory.h"

#import "CategoryViewController.h"
#import "productCollectionViewController.h"

#define kImageWidth  150
#define kImageHeight  150



@interface HomeViewHelper()<CategoryItemProtocol,featuredProductSelected,SwipeViewDataSource,SwipeViewDelegate,CollectionItemProtocol>
{
    RNThemeImageButton *toTop;
    BOOL hasTop;
    
    TABLETYPE MainType;
    UIPageControl *pageControl;
    SwipeView *_swipeView;
    
    CGFloat lastContentOffset;
    
    UIScrollView *scroll;
    
    CategoryViewController *categoryList;
    productCollectionViewController *FeaturedList;
    
    UIView *holder;
    CGFloat scrollViewHeight ;
}

@property (strong,nonatomic) NSMutableArray *imageList1;
@property (strong,nonatomic) NSMutableArray *titleList1;
@property (strong,nonatomic) NSMutableArray *priceList1;
@property (strong,nonatomic) NSMutableArray *discountList1;
@property (strong,nonatomic) NSMutableArray *type1;

@property (strong,nonatomic) NSMutableArray *imageList2;
@property (strong,nonatomic) NSMutableArray *titleList2;
@property (strong,nonatomic) NSMutableArray *priceList2;
@property (strong,nonatomic) NSMutableArray *discountList2;
@property (strong,nonatomic) NSMutableArray *type2;

@property (strong,nonatomic) NSMutableArray *colorList;


@property (strong,nonatomic) HomeFeatured *featured;

@property (strong,nonatomic) HomeNavigation *navigation_helper;

@property (strong,nonatomic) Home_DataHelper *data;

@property (strong,nonatomic) UIViewController *localCopyController;

@property (nonatomic, assign) CGFloat lastContentOffset;


@end


CGFloat const kMGOffsetEffects = 40.0;
CGFloat const kMGOffsetBlurEffect = 2.0;

@implementation HomeViewHelper

@synthesize imageList1=_imageList1;
@synthesize titleList1=_titleList1;
@synthesize priceList1=_priceList1;
@synthesize discountList1=_discountList1;
@synthesize type1=_type1;


@synthesize imageList2=_imageList2;
@synthesize titleList2=_titleList2;
@synthesize priceList2=_priceList2;
@synthesize discountList2=_discountList2;
@synthesize type2=_type2;

@synthesize colorList=_colorList;

@synthesize featured=_featured;
@synthesize tableView=_tableView;

@synthesize delegate=_delegate;
@synthesize data=_data;

@synthesize bannerImageList=_bannerImageList;
@synthesize bannerType=_bannerType;
@synthesize bannerNavigateTo=_bannerNavigateTo;

@synthesize productArray=_productArray;
@synthesize categoryList=_categoryList;


@synthesize navigation_helper=_navigation_helper;
@synthesize localCopyController;

-(id)initWithTable:(TABLETYPE)type withMainView:(UIView*)view viewWithController:(UIViewController*)controller viewWithFeaturedCollectionView:(UICollectionView*)collection{
    
    self = [super init];
    if (self) {
        
        
        scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], [GlobalCall getMyHeight])];
        scroll.userInteractionEnabled=TRUE;
        
        holder=[[UIView alloc]initWithFrame:CGRectMake(0, 0,  320, [GlobalCall isIpad]?400:250)];
        holder.backgroundColor=AppBarColor;
        
        
        categoryList=[[CategoryViewController alloc]init];
        categoryList.categoryName.text=@"Categories";
        CGRect rect=categoryList.view.frame;
        [categoryList.view setFrame:CGRectMake(0, holder.frame.size.height, rect.size.width, rect.size.height)];
        categoryList.delegate=self;
        
        FeaturedList=[[productCollectionViewController alloc]init];
        FeaturedList.categoryName.text=@"Featured Products";
        CGRect rectCategory=FeaturedList.view.frame;
        [FeaturedList.view setFrame:CGRectMake(0, holder.frame.size.height+ categoryList.view.frame.size.height, rectCategory.size.width, rectCategory.size.height)];
        FeaturedList.delegate=self;
        
        scroll.delegate=self;
        scrollViewHeight =0.0;
        
        [scroll addSubview:categoryList.view];
        [scroll addSubview:FeaturedList.view];
        
        [scroll setContentSize:CGSizeMake([GlobalCall getMyWidth], holder.frame.size.height+categoryList.view.frame.size.height+FeaturedList.view.frame.size.height+110.0f)];
        
        [controller.view addSubview:scroll];
        
        _navigation_helper=[[HomeNavigation alloc]initWithView:controller];
        MainType=type;
        
        
        localCopyController=[[UIViewController alloc]init];
        localCopyController=controller;
        
        _featured=[HomeFeatured new];
        _featured.collectionView=collection;
        
        _featured.collectionView.delegate=_featured;
        _featured.collectionView.dataSource=_featured;
        _featured.delegate=self;
        
        [_featured.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([HomeFeatureddCell class]) bundle:nil]  forCellWithReuseIdentifier:NSStringFromClass([HomeFeatureddCell class])];
        [_featured.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([HomeFeatureddCell_right class]) bundle:nil]  forCellWithReuseIdentifier:NSStringFromClass([HomeFeatureddCell_right class])];
        _featured.collectionView.backgroundColor=AppBarColor;
        
        _productArray=[[NSMutableArray alloc]init];
        _data=[[Home_DataHelper alloc]init];
        
        
        _tableView.hidden=TRUE;
    }
    
    return self;
    
}



-(void)updateTableHeader
{
    
      CGFloat myWidth=[GlobalCall getMyWidth];
        
        holder=[[UIView alloc]initWithFrame:CGRectMake(0, 0,  myWidth, [GlobalCall isIpad]?400:250)];
        holder.backgroundColor=AppBarColor;
        
        pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake((myWidth/2)-(myWidth/2), [GlobalCall isIpad]?370:230, myWidth, 20)];
        
        pageControl.pageIndicatorTintColor = [[ThemeManager sharedManager]colorForKey:@"secondaryColor"];
        pageControl.currentPageIndicatorTintColor = [[ThemeManager sharedManager]colorForKey:@"primaryColor"];
    
        [pageControl setNumberOfPages: [_bannerImageList count]] ;
        [pageControl setCurrentPage: 0] ;
        [pageControl setDefersCurrentPageDisplay: YES] ;
        [pageControl setNeedsDisplay];
        
        _swipeView=[[SwipeView alloc]initWithFrame:CGRectMake(0, 0,holder.frame.size.width, [GlobalCall isIpad]?400:250)];
        _swipeView.delegate=self;
        _swipeView.dataSource=self;
        _swipeView.wrapEnabled=TRUE;
        _swipeView.alignment = SwipeViewAlignmentCenter;
        _swipeView.pagingEnabled = YES;
        _swipeView.itemsPerPage = 1;
        _swipeView.truncateFinalPage = YES;
        _swipeView.backgroundColor=AppBarColor;
        
        [holder addSubview:_swipeView];
        [holder addSubview:pageControl];
    
        [holder setNeedsDisplay];
    
        [holder removeFromSuperview];
        [scroll addSubview:holder];
}


- (void)scrollOnTimer
{
    [_swipeView scrollByNumberOfItems:1 duration:0.5];
}



#pragma mark - swipe delegate

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return [_bannerImageList count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], [GlobalCall isIpad]?400:250)];
   
    NSString *urlString=[_bannerImageList objectAtIndex:index];
    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:urlString done:^(UIImage *image, SDImageCacheType cacheType) {
        
        if (image) {
            
            [imageView setImage:image];
            
            
        }else{
            
            [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]] forKey:urlString];
            [imageView sd_setImageWithURL:[NSURL URLWithString:urlString]];
           
        }
    }];
    
    imageView.tag=index;
    
    UITapGestureRecognizer *tapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bannerClicked:)];
    [imageView addGestureRecognizer:tapped];
    
    return imageView;
}

-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    
    [pageControl setCurrentPage:_swipeView.currentItemIndex];
    [pageControl setNeedsDisplay];

}

-(void)bannerClicked:(UITapGestureRecognizer*)recongnizer{
    
    int tagValue=(int)(((UIImageView*)[recongnizer view]).tag);
    
    
    BANNERTYPE type;
    
    if ([[_bannerType objectAtIndex:tagValue] isEqualToString:WEB]) {
        type=BANNERTYPEWEB;
     }else if([[_bannerType objectAtIndex:tagValue] isEqualToString:PRODUCT]){
        type=BANNERTYPEPRODUCT;
    }else if([[_bannerType objectAtIndex:tagValue] isEqualToString:CATEGORY]){
        type=BANNERTYPECATEGORY;
    }else{
        type=BANNERTYPENOTHING;
    }
    
    [self pushDescideWithType:type withNavigateToValue:[_bannerNavigateTo objectAtIndex:tagValue]];
    
    
}

-(void)pushDescideWithType:(BANNERTYPE)type withNavigateToValue:(NSString*)navigatePlace{
    
    
    switch (type) {
            
        case BANNERTYPEWEB:
            
            [self.localCopyController.navigationController pushViewController:[[In_AppWebView alloc]initWithURL:navigatePlace] animated:YES];
            break;
        
        case BANNERTYPEPRODUCT:
            
            break;
        
        case BANNERTYPECATEGORY:
            
            break;
            
        default:
            break;
    }
    
    
}


-(void)updateCategories
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [categoryList setDataArray:_categoryList];
        [categoryList setUpACPScroll];
        [categoryList.view setNeedsDisplay];
        
    });
    
}


-(void)updateFeatured
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [FeaturedList setDataArray:_productArray];
        [FeaturedList setUpACPScroll];
        [FeaturedList.view setNeedsDisplay];
        
    });
    
}


-(void)updateFeaturedProducts
{
   
    [self initializeTableWithFeatured];
    [scroll addSubview:_featured.collectionView];
    
   
    
}


#pragma mark - intialize with Data

-(void)initializeTableOfType:(TABLETYPE)type{
    
    switch (type) {
        case TABLETYPENEWPRODUCT:
            
            break;
        case TABLETYPEFEATURED:
            
            [self initializeTableWithFeatured];
            break;
            
        default:
            break;
    }
    
}


-(void)initializeTableWithFeatured{
    
    _featured.imageList=[_productArray valueForKeyPath:@"thumbnail.p_id"] ;
    _featured.productName=[_productArray valueForKeyPath:@"name"];
    _featured.Id=[_productArray valueForKeyPath:@"p_id"];
    _featured.productPrice=[_productArray valueForKeyPath:@"rate"];
    _featured.productdiscountedPrice=[_productArray valueForKeyPath:@"discount"];
    _featured.type=[_productArray valueForKeyPath:@"type"];
   
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setItemSize:CGSizeMake(155, 206.0f)];
    [_featured.collectionView setCollectionViewLayout:layout];

    [_featured.collectionView setFrame:CGRectMake(0, holder.frame.size.height+ categoryList.view.frame.size.height, [GlobalCall getMyWidth], _featured.collectionView.contentSize.height)];
    
    
    _featured.collectionView.delegate=_featured;
    _featured.collectionView.dataSource=_featured;
    
    [_featured.collectionView reloadData];
   
    
   dispatch_async(dispatch_get_main_queue(), ^{
   
       [_featured.collectionView setFrame:CGRectMake(0, holder.frame.size.height+ categoryList.view.frame.size.height, [GlobalCall getMyWidth], _featured.collectionView.contentSize.height)];
       
       scrollViewHeight=0.0;
       
       for (UIView* view in scroll.subviews)
       {
           NSLog(@"View Frame size %@",NSStringFromCGSize(view.frame.size));
           scrollViewHeight += view.frame.size.height;
       }
       
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           [scroll setContentSize:(CGSizeMake(320, scrollViewHeight))];
           [scroll setNeedsDisplay];
       });
       
       [_featured.collectionView setNeedsDisplay];
       [_featured.collectionView setNeedsDisplay];
       
    
    });
    
    
    
}

#pragma mark - UIScrollView Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView==self.tableView) {
        
        ScrollDirection scrollDirection;
        
        if (lastContentOffset > scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionUp;
        else if (lastContentOffset < scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionDown;
        else
            scrollDirection=ScrollDirectionNone;
        
        
        lastContentOffset = scrollView.contentOffset.y;
        
        if (scrollDirection== ScrollDirectionNone) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];
            
        }else if (scrollDirection==ScrollDirectionDown){
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldHide" object:self];
            
        }else if (scrollDirection==ScrollDirectionUp){
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];
            
        }
        
    }
    
    
}



-(void)productSelectedAtIndex:(int)productIndex{
    
    [_delegate productCellTouchedWith:[_productArray objectAtIndex:productIndex]];
    
}


-(void)imageItemClick:(UIButton *)button{
    
    UIImageButton *superOfButton=(UIImageButton*)[button superview];
    Products *productObject;
    
    if (superOfButton.column==0) {
        
        productObject=[_productArray objectAtIndex:(superOfButton.row-1)*2];
        
    } else {
        
        productObject=[_productArray objectAtIndex:((superOfButton.row-1)*2)+1];
    
    }
    
    [_delegate productCellTouchedWith:productObject];
   
}


#pragma mark - category Delegate

-(void)categoryItemSelected:(int)number{
    
    NSArray *categories=[[GlobalCall getDelegate]fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:nil];
    
   [_navigation_helper openProductList:((ProductCategory*)[categories objectAtIndex:number]).name];
    
}

-(void)seeAllCategory{
    
    [self.navigation_helper showBottomAndTop];
    
    [self.localCopyController.navigationController pushViewController:[[CategoryMainListViewController alloc]init] animated:YES];
    
}

-(void)collectionItemSelected:(int)number{
    
    if (number==10) {
      
        [_navigation_helper openProductListWithList:_productArray];
        
    }else{
        
        [_navigation_helper openProduct:(Products*)[_productArray objectAtIndex:number]];
    
    }
    
}


-(void)seeAllCollectionProducts{
    
    [_navigation_helper openProductListWithList:_productArray];
    
}

@end
