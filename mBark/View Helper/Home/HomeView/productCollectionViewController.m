//
//  productCollectionViewController.m
//  mBark
//
//  Created by Xiphi Tech on 01/07/2015.
//
//

#import "productCollectionViewController.h"
#import "UIColor+HexString.h"
#import "UIImageView+WebCache.h"

#import "AppUIDefaults.h"

#import "Products.h"
#import "ProductImages.h"
#import "ProductThumbnail.h"

#import "ACPItemFull.h"
#import "ACPItemLast.h"

@interface productCollectionViewController ()<ACPFullScrollDelegate>

@end

@implementation productCollectionViewController

@synthesize dataArray=_dataArray;

-(id)init{
    
    self=[super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle]loadNibNamed:@"productCollectionViewController" owner:self options:nil] lastObject];
        
    }
    
    return self;
}

- (IBAction)seeAllAction:(id)sender {

    [self.delegate seeAllCollectionProducts];
}

- (void)setUpACPScroll {
    
    _scrollMenu=[[ACPFullScrollMenu alloc]initWithFrame:CGRectMake(0, 0, self.baseView.frame.size.width, self.baseView.frame.size.height)];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    int count=0;
    
    if ([_dataArray count]>10 ) {
        count=10;
    }else{
        count=(int)[_dataArray count];
    }
    
    for (int i = 0; i < count; i++)
    {
        
        NSLog(@"Category image Value %@",((Products*)[_dataArray objectAtIndex:i]).thumbnail.p_id);
        
        
        NSString *imgName =((Products*)[_dataArray objectAtIndex:i]).thumbnail.p_id;
        NSString *imgSelectedName =((Products*)[_dataArray objectAtIndex:i]).thumbnail.p_id;
        
        
        ACPItemFull *item =[[ACPItemFull alloc] initACPItem:[UIImage imageNamed:@"bg.png"]   iconImage:imgName label:((Products*)[_dataArray objectAtIndex:i]).name
                                          andAction: ^(ACPItemFull *item) {
                                              
                                              NSLog(@"Block called! %d", i);
                                              
                                          }];
        item.iconImageNew.tag=10+i;
        
        [item setHighlightedBackground:nil iconHighlighted:[UIImage imageNamed:imgSelectedName] textColorHighlighted:[UIColor colorWithHexString:[AppUIDefaults getPrimaryColor]]];
        [array addObject:item];
        
    }
    
    if ([_dataArray count]>10 ) {
       
        ACPItemLast *item =[[ACPItemLast alloc] initACPItemLabel:@"Load More..." andAction: ^(ACPItemLast *item) {
                    NSLog(@"Block called!");
        }];
        [item setHighlightedBackgroundTextColorHighlighted:[UIColor colorWithHexString:[AppUIDefaults getPrimaryColor]]];
        [array addObject:item];
        
    }
    
    for (UIView *view in [_scrollMenu subviews]) {
        [view removeFromSuperview];
    }
    
    [_scrollMenu setUpACPScrollMenu:array];
    _scrollMenu.delegate = self;
    [self.baseView addSubview:_scrollMenu];
    
    
    int acpItemCount=0;
    
    if ([_dataArray count]>10 ) {
        acpItemCount=10;
        [array removeLastObject];
    }else{
        acpItemCount=(int)[_dataArray count];
    }

    
    for(int Value=0;Value<acpItemCount;Value++) {
        
        Products *prod=(Products*)[_dataArray objectAtIndex:Value];
        
        NSString *imgName =((ProductThumbnail*)prod.thumbnail).p_id;
        
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:imgName done:^(UIImage *image, SDImageCacheType cacheType) {
            
            int indexValue=(int)[[array valueForKeyPath:@"iconImageName"]indexOfObject:imgName];
            ACPItemFull *item=(ACPItemFull*)[_scrollMenu.scrollView viewWithTag:(indexValue+10)];
            
            if (image) {
                ((UIImageView*)[item.iconImageNew viewWithTag:(indexValue+10)]).image=image;
            }else{
                [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgName]]] forKey:imgName];
                [item.iconImageNew sd_setImageWithURL:[NSURL URLWithString:imgName] placeholderImage:[UIImage imageNamed:@"no_product.png"]];
            }
        
        }];
        
    }

    
}


-(void)scrollMenu:(ACPFullScrollMenu *)menu didSelectIndex:(NSInteger)selectedIndex{
    
    [_delegate collectionItemSelected:(int)selectedIndex];
    
}

@end
