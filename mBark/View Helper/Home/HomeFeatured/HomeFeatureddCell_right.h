//
//  HomeFeatureddCell.h
//  mBark
//
//  Created by Xiphi Tech on 06/04/2015.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeImageButton.h"


@interface HomeFeatureddCell_right : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet RNThemeLabel *productName;
@property (weak, nonatomic) IBOutlet RNThemeLabel *productPrice;
@property (weak, nonatomic) IBOutlet RNThemeLabel *discounted;
@property (weak, nonatomic) IBOutlet UIView *mainView;



@end
