
//
//  HomeFeatured.m
//  mBark
//
//  Created by Xiphi Tech on 06/04/2015.
//
//

#import "HomeFeatured.h"
#import "HomeFeatureddCell.h"
#import "HomeFeatureddCell_right.h"

#import "UIImage+animatedGIF.h"
#import "UIImageView+WebCache.h"

#import "AppProductDefaults.h"
#import "KRLCollectionViewGridLayout.h"


@interface HomeFeatured ()<UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation HomeFeatured

static NSString * const reuseIdentifier_left = @"HomeFeatureddCell";
static NSString * const reuseIdentifier_right = @"HomeFeatureddCell_right";

@synthesize imageList=_imageList;
@synthesize productdiscountedPrice=_productdiscountedPrice;
@synthesize Id=_Id;
@synthesize productName=_productName;
@synthesize productPrice=_productPrice;
@synthesize type=_type;
@synthesize isDiscounted=_isDiscounted;
@synthesize delegate=_delegate;



- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}



#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    NSLog(@"Featured List Values %@",_productPrice);
    
    return [_productPrice count];

}

- (CGSize)collectionViewContentSize
{
    NSInteger itemCount = [self.collectionView numberOfItemsInSection:0];
    NSInteger pages = ceil(itemCount / 16.0);
    
    return CGSizeMake(320 * pages, self.collectionView.frame.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row%2 !=0) {
        
       return  [self getRightCell:collectionView withIndexPath:indexPath];
        
    }else{
        
       return [self getLeftCell:collectionView withIndexPath:indexPath];
        
    }
    
}


-(HomeFeatureddCell*)getLeftCell:(UICollectionView*)collectionView withIndexPath:(NSIndexPath*)indexPath{
    
    HomeFeatureddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier_left forIndexPath:indexPath];
    
    NSString *imageString=[_imageList objectAtIndex:indexPath.row];
    float rate=[[_productPrice objectAtIndex:indexPath.row] floatValue];
    float discount=[[_productdiscountedPrice objectAtIndex:indexPath.row] floatValue];
    float discounted=rate - (rate*(discount/100));
    NSString *type=[_type objectAtIndex:indexPath.row];
    
    
    [cell.productImage setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageString]]]];
    cell.productName.text=[_productName objectAtIndex:indexPath.row];
    
    
    if ([type isEqualToString:@"Grouped"]) {
        
        
        if (rate >0) {
            
            cell.productPrice.hidden=TRUE;
            cell.discounted.text=[NSString stringWithFormat:@"Starts from ₹%.01f",rate];
            
            
        } else {
            
            cell.productPrice.hidden=TRUE;
            cell.discounted.text=[NSString stringWithFormat:@"Starts from ₹%.01f",rate];
            
        }
        
        
    } else {
        
        if (rate>0) {
            
            
            cell.discounted.text=[NSString stringWithFormat:@"₹%.01f",discounted];
            cell.productPrice.text=[NSString stringWithFormat:@"₹%.01f",rate];
            
            
        } else {
            
            cell.productPrice.hidden=TRUE;
            cell.discounted.text=[NSString stringWithFormat:@"₹%.01f",discounted];
            
        }
        
        
    }
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:cell.productPrice.text];
    [titleString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    [cell.productPrice  setAttributedText:titleString];
    
    cell.mainView.layer.borderColor=(__bridge CGColorRef)([UIColor blackColor]);
    cell.mainView.layer.borderWidth=0.5f;
    
    
    return cell;
    
}

-(HomeFeatureddCell_right*)getRightCell:(UICollectionView*)collectionView withIndexPath:(NSIndexPath*)indexPath{
    
    HomeFeatureddCell_right *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier_right forIndexPath:indexPath];
    
    NSString *imageString=[_imageList objectAtIndex:indexPath.row];
    float rate=[[_productPrice objectAtIndex:indexPath.row] floatValue];
    float discount=[[_productdiscountedPrice objectAtIndex:indexPath.row] floatValue];
    float discounted=rate - (rate*(discount/100));
    NSString *type=[_type objectAtIndex:indexPath.row];
    
    
    [cell.productImage setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageString]]]];
    cell.productName.text=[_productName objectAtIndex:indexPath.row];
    
    
    if ([type isEqualToString:@"Grouped"]) {
        
        
        if (rate >0) {
            
            cell.productPrice.hidden=TRUE;
            cell.discounted.text=[NSString stringWithFormat:@"Starts from ₹%.01f",rate];
            
            
        } else {
            
            cell.productPrice.hidden=TRUE;
            cell.discounted.text=[NSString stringWithFormat:@"Starts from ₹%.01f",rate];
            
        }
        
        
    } else {
        
        if (rate>0) {
            
            
            cell.discounted.text=[NSString stringWithFormat:@"₹%.01f",discounted];
            cell.productPrice.text=[NSString stringWithFormat:@"₹%.01f",rate];
            
            
        } else {
            
            cell.productPrice.hidden=TRUE;
            cell.discounted.text=[NSString stringWithFormat:@"₹%.01f",discounted];
            
        }
        
        
    }
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:cell.productPrice.text];
    [titleString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    [cell.productPrice  setAttributedText:titleString];
    cell.mainView.layer.borderColor=(__bridge CGColorRef)([UIColor blackColor]);
    cell.mainView.layer.borderWidth=0.5f;
    
    
    return cell;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    CGFloat availableWidthForCells = CGRectGetWidth(self.collectionView.frame) - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing * (2 - 1);
    CGFloat cellWidth = availableWidthForCells / 2;
    flowLayout.itemSize = CGSizeMake(cellWidth,cellWidth*(1/0.75));
    
    NSLog(@"Item size is %@", NSStringFromCGSize(flowLayout.itemSize));
    
    return flowLayout.itemSize;

}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [_delegate productSelectedAtIndex:(int)indexPath.row];
    
}



@end
