//
//  HomeFeatured.h
//  mBark
//
//  Created by Xiphi Tech on 06/04/2015.
//
//

#import <UIKit/UIKit.h>

@protocol  featuredProductSelected <NSObject>

-(void)productSelectedAtIndex:(int)productIndex;

@end

@interface HomeFeatured : UICollectionViewController

@property (strong,nonatomic) id<featuredProductSelected>  delegate;


@property (strong,nonatomic) NSArray *imageList;
@property (strong,nonatomic) NSArray *productName;
@property (strong,nonatomic) NSArray *Id;
@property (strong,nonatomic) NSArray *productPrice;
@property (strong,nonatomic) NSArray *type;
@property (strong,nonatomic) NSArray *isDiscounted;
@property (strong,nonatomic) NSArray *productdiscountedPrice;


@end
