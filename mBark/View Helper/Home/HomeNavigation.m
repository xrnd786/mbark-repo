 //
//  HomeNavigation.m
//  P1
//
//  Created by Xiphi Tech on 09/10/2014.
//
//

#import "HomeNavigation.h"

//*****PRESENTATION CONTORLLERS*****//
#import "SignIn.h"
#import "Search.h"
#import "ProfileViewController.h"
#import "MessageViewController.h"
#import "WishListViewController.h"
#import "AboutViewController.h"
#import "TermsOfUseViewController.h"
#import "AuthChoiceController.h"

#import "ProductViewController.h"
#import "ProductListController.h"

#import "REFrostedViewController.h"

#import "User.h"
#import "CartListViewController.h"
#import "GroupedProductViewController.h"

#import "HomeCustomNavBarController.h"

@implementation HomeNavigation


@synthesize baseView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (id)sharedManager:(UIViewController*)myView {
    
    static HomeNavigation *sharedMyManager = nil;
    
    @synchronized(self) {
        
    if (sharedMyManager == nil)
         
        sharedMyManager = [[self alloc] initWithView:myView];
        
    }
    
    return sharedMyManager;
}


- (id)initWithView:(UIViewController*)myView
{
    self = [super init];
    
    if (self) {
        
        self.baseView=myView;
        // Initialization code
        
    }
    return self;
    
}

#pragma mark - helper Function
-(void)tableSettingsCell:(NSIndexPath*)indexPath{
    
    switch (indexPath.section) {
        case 0:
            
            if (indexPath.row==0) {
                
                [self openSignInView];
                
            } else {
                
            }
            
            break;
        case 1:
            
            break;
            
        default:
            break;
    
    }
    
}


#pragma mark - navigation functions

-(void)showBottomAndTop{
    
    [GlobalCall showBottomBar];
    self.baseView.navigationController.navigationBarHidden=FALSE;
}



-(void)openSignInView{
    
    SignIn *controller=[[SignIn alloc]init];
    [self.baseView.view.window.rootViewController presentViewController:controller animated:YES completion:nil];
}


-(void)tableOneList{

    ProductListController *controller=[[ProductListController alloc]init];
    
    controller.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    
    self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self showBottomAndTop];
    
    [self.baseView.navigationController pushViewController:controller animated:YES];
    
}

-(void)openProduct:(Products*)product{
    
    
    [self showBottomAndTop];
    
    if ([product.type isEqualToString:@"Grouped"]) {
       
        
        GroupedProductViewController *controller=[GlobalCall groupedProductViewController];
        controller.productID=product.p_id;
        
        @try {
            
            [self.baseView.navigationController pushViewController:controller animated:NO];
            
        } @catch (NSException * ex) {
            
            [self.baseView.navigationController popToViewController:controller animated:YES];
            
            
        }@finally{
            
        }
        
    } else {
        
        
        ProductViewController *controller=[GlobalCall productViewController];
        controller.productID=product.p_id;
        
        @try {
            
            [self.baseView.navigationController pushViewController:controller animated:NO];
            
        } @catch (NSException * ex) {
        
            [self.baseView.navigationController popToViewController:controller animated:YES];
            
            
        }@finally{
            
        }

       
    }
    
}


-(void)openProductList:(NSString *)categoryName{
    
    ProductListController *controller=[[ProductListController alloc]initWithCategoryName:categoryName];
    
    self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self showBottomAndTop];
    [self.baseView.navigationController pushViewController:controller animated:YES];
    
}

-(void)openProductListWithList:(NSArray *)productList{
    
    ProductListController *controller=[[ProductListController alloc]initWithList:productList];
    
    self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self showBottomAndTop];
    [self.baseView.navigationController pushViewController:controller animated:YES];
    
}

-(void)openSideMenu{

    [self.baseView.frostedViewController presentMenuViewController];

}


-(void)openProfileView{

    NSArray *newarray=[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:@"u_id" withPredicate:nil];
    
    NSString *password=((User*)[newarray lastObject]).password;
    
    NSLog(@"new array %@",newarray);
    
    
    if (password == nil) {
        
        AuthChoiceController *controller=[[AuthChoiceController alloc]initWithTheView:[self.baseView.frostedViewController contentViewController].view];
        UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:controller];
        navigation.navigationBarHidden=TRUE;
        [self.baseView.view.window.rootViewController presentViewController: navigation animated:YES completion:nil];
        
    } else {
        
        ProfileViewController *controller = [[ProfileViewController alloc]init];
        self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
        self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
        
        [self showBottomAndTop];
        [self.baseView.navigationController pushViewController:controller animated:YES];

    }
    
    
}


-(void)openMessageView{
    
    MessageViewController *controller = [[MessageViewController alloc]init];
    
    self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self showBottomAndTop];
    [self.baseView.navigationController pushViewController:controller animated:YES];
    
}

-(void)openWishListView{
    
    WishListViewController *controller = [[WishListViewController alloc]init];
    
    self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self showBottomAndTop];
    [self.baseView.navigationController pushViewController:controller animated:YES];
    
}

-(void)openAboutView{
    
    AboutViewController *controller = [[AboutViewController alloc]init];
    //self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
    //self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self showBottomAndTop];
    [self.baseView.navigationController pushViewController:controller animated:YES];
    
    
}

-(void)openTermsOfUseView{
    
    TermsOfUseViewController *controller = [[TermsOfUseViewController alloc]init];
    
    [self showBottomAndTop];
    [self.baseView.navigationController pushViewController:controller animated:YES];
    

    
}

-(void)openSentCartView{
    
    CartListViewController *controller = [[CartListViewController alloc]initForPostedCarts];
    self.baseView.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.baseView.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self showBottomAndTop];
    [self.baseView.navigationController pushViewController:controller animated:YES];

}


@end
