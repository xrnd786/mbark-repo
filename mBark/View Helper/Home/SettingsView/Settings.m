//
//  Settings.m
//  P1
//
//  Created by Xiphi Tech on 07/10/2014.
//
//

#import "Settings.h"
#import "SignIn.h"



@interface Settings ()
@end

@implementation Settings

@synthesize delegate=_delegate;


-(id)initWithTable:(UITableView*)table{
    
    self = [super init];
    if (self) {
        
        table.delegate=self;
        table.dataSource=self;
        
        
        
        // Initialization code
    }
    return self;
    
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - table View Functions


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return ([GlobalCall findBounds].size.width*10.0f)/100;
    
}



#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 2;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int sectionRowCount=0;
    
    switch (section) {
        case 0:
            
            sectionRowCount=2;
            break;
        case 1:
            
            sectionRowCount=1;
            break;
        
            
        default:
            return sectionRowCount;
            break;
    }
    
    return sectionRowCount;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        
    }
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    switch (indexPath.section) {
        case 0:
            
            if (indexPath.row == 0) {
                
                cell.textLabel.text = @"Profile";
                
            } else {
                
                cell.textLabel.text = @"Messages";
                
            }
            break;
        case 1:
            
            cell.textLabel.text = @"Settings";
            
            break;
            
        default:
            break;
    }
    
    
    return cell;
    
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    if ([_delegate respondsToSelector:@selector(tableSettingsCell:)])
        [_delegate tableSettingsCell:indexPath];


    
   
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
