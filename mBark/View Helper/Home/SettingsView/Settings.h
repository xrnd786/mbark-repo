//
//  Settings.h
//  P1
//
//  Created by Xiphi Tech on 07/10/2014.
//
//

#import <UIKit/UIKit.h>


@protocol SettignsDelegate<NSObject>

-(void)tableSettingsCell:(NSIndexPath*)indexPath;

@end

@interface Settings : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    __unsafe_unretained id <SettignsDelegate>delegate;
    
}

@property (nonatomic,assign)  id <SettignsDelegate> delegate;

-(id)initWithTable:(UITableView*)table;


@end
