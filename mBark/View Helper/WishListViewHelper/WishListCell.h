//
//  ProductLitsCell.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeLabel.h"
#import "RNThemeImageButton.h"

@interface WishListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet RNThemeLabel *productTitle;
@property (weak, nonatomic) IBOutlet RNThemeLabel *mrpPrice;
@property (weak, nonatomic) IBOutlet RNThemeLabel *discountedPrice;

@end
