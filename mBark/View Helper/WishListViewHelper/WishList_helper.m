//
//  WishList_helper.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "WishList_helper.h"

#import "ProductListCell.h"

#import "UIImageView+WebCache.h"
#import "WishListViewController.h"
#import "ProductViewController.h"
#import "ProductThumbnail.h"

@interface WishList_helper ()
{
    CGFloat lastContentOffset;
}

@property (strong,nonatomic) NSArray *productMasterArray;


@end

@implementation WishList_helper

@synthesize localTableCopy,localTopBar,productMasterArray,baseView;

-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view   andProductArray:(NSArray*)array withController:(UIViewController*)controller{
    
    self = [super init];
    
    if (self) {
        
        productMasterArray=[[NSArray alloc]initWithArray:array];
        
        
        [table setShowsVerticalScrollIndicator:NO];
        
        if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
            [table setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
            [table setLayoutMargins:UIEdgeInsetsZero];
        }
        
       
        table.delegate=self;
        table.dataSource=self;
        [table setBackgroundColor:[UIColor whiteColor]];
        
        
        table.tableFooterView = ({
            
            UIView *viewBase=[[UIView alloc]initWithFrame:CGRectMake(0, 0, table.frame.size.width, 90)];;
            
            RNThemeLabel *label=[[RNThemeLabel alloc]initWithFrame:viewBase.frame];
            label.text=@"No more products";
            label.textAlignment=NSTextAlignmentCenter;
            label.font=[UIFont italicSystemFontOfSize:9.0f];
            label.textColorKey=@"secondaryColor";
            [label applyTheme];
            
            [viewBase addSubview:label];
            viewBase;
            
        });
        
        localTableCopy=table;
        
        if ([productMasterArray count]==0) {
            
            table.hidden=TRUE;
            
        } else {
            
            table.hidden=FALSE;
            
        }
        
        self.baseView=[[UIViewController alloc]init];
        self.baseView =controller;
        
    }
    
    return self;
}





#pragma mark - table View Functions

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;

}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
    
}


#pragma mark - UIScrollView Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
    
}



#pragma mark- table Data Functions



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSLog(@"Image Count %lu",(unsigned long)[productMasterArray count]);
    return [productMasterArray count];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"WishListCell";
    ProductListCell *cell =(ProductListCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"WishListCell" owner:self options:nil] objectAtIndex:0];
    }
    
    Products *product=[productMasterArray objectAtIndex:indexPath.row];
    
    cell.productImage.layer.shadowOffset = CGSizeMake(-5, 5);
    cell.productImage.layer.shadowRadius = 3;
    cell.productImage.layer.shadowOpacity = 0.3;
    [cell.productImage setImageWithURL:[NSURL URLWithString:product.thumbnail.p_id]];
    cell.productTitle.text=product.name;
    
    
    float rate=[product.rate floatValue];
    float discount=[product.discount floatValue];
    float discounted=rate - (rate*(discount/100));
    
    cell.mrpPrice.text=[NSString stringWithFormat:@"₹%.02f",rate];
    cell.discountedPrice.text=[NSString stringWithFormat:@"₹%.02f",discounted];
    cell.discountedPrice.textColor=[UIColor redColor];
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:cell.mrpPrice.text];
    
    [titleString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    [cell.mrpPrice  setAttributedText:titleString];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ProductViewController *controller=[GlobalCall productViewController];
    controller.productID=((Products*)[self.productMasterArray objectAtIndex:indexPath.row]).p_id;
    
    @try {
        
        [self.baseView.navigationController pushViewController:controller animated:NO];
        
    } @catch (NSException * ex) {
        
        [self.baseView.navigationController popToViewController:controller animated:YES];
        
    }@finally{}

    [self.baseView.navigationController pushViewController:controller animated:YES];
    
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray *array=[[NSMutableArray alloc]initWithArray:productMasterArray];
    
    Products *prod=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id = %@",((Products*)[array objectAtIndex:indexPath.row]).p_id]] lastObject];
    NSLog(@"Product ID %@ and is favourite %@",prod.p_id,prod.isFavourite);
    
    prod.isFavourite=[NSNumber numberWithBool:FALSE];
    [GlobalCall saveContext];
    
    [array removeObjectAtIndex:indexPath.row];
    productMasterArray = [[NSMutableArray alloc]initWithArray:array];
    [localTableCopy reloadData];
    
}


@end
