//
//  ProductLitsCell.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "WishListCell.h"

@implementation WishListCell

@synthesize productImage,productTitle,mrpPrice,discountedPrice;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)FavouriteToggle:(id)sender {
    
    UIButton *button=(UIButton*)sender;
    
    if (button.selected) {
        
        button.selected=FALSE;
        
    } else {
        
        button.selected=TRUE;
    
    }
    
}
@end
