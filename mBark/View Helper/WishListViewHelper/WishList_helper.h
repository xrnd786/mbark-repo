//
//  WishList_helper.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface WishList_helper : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (strong,nonatomic) UITableView *localTableCopy;
@property (strong,nonatomic) UIView *localTopBar;
@property (strong,nonatomic) UIViewController *baseView;


-(id)initWithTable:(UITableView*)table  withMainView:(UIView*)view andProductArray:(NSArray*)array withController:(UIViewController*)controller;



@end
