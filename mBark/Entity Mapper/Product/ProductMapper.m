//
//  ProductMapper.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "ProductMapper.h"

#import "AppProductDefaults.h"
#import "Attribute.h"


@implementation ProductMapper


-(Product*)mapThisServerDictionaryToProduct:(NSDictionary*)productDictionary{
    
   // NSLog(@"Print My Dictionary %@",[productDictionary description]);
    
    Product *myProduct=[Product new];
    
    myProduct.Applications=[productDictionary valueForKey:JSON_RES_AppProducts_Applications];
    
    myProduct.Attributes=[productDictionary valueForKey:JSON_RES_AppProducts_Attributes];
    
    
    if ([productDictionary valueForKey:JSON_RES_AppProducts_Attributes] !=[NSNull null]) {
        
        NSArray *iteratingArray=[[NSArray alloc]initWithArray:(NSArray*)[productDictionary valueForKey:JSON_RES_AppProducts_Attributes] ];
        
        NSMutableArray *attributesList=[[NSMutableArray alloc]init];
        
        for (NSDictionary *attribute in iteratingArray) {
           
            DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass: [Attribute class]];
            Attribute *attributeObj = [parser parseDictionary:attribute];
            [attributesList addObject:attributeObj];
        
        }
        
        myProduct.Attributes=[[NSArray alloc]initWithArray:(NSArray*)attributesList];
        
        
    }else{
     
        myProduct.Attributes=[[NSArray alloc]init];
    }
    
   
    
    myProduct.AvailableUnits=[productDictionary valueForKey:JSON_RES_AppProducts_AvailableUnits];
    myProduct.Category=[productDictionary valueForKey:JSON_RES_AppProducts_Category];
    
    myProduct.Description=[productDictionary valueForKey:JSON_RES_AppProducts_Description];
    myProduct.Discontinued=[productDictionary valueForKey:JSON_RES_AppProducts_Discontinued];
    myProduct.Discount=[productDictionary valueForKey:JSON_RES_AppProducts_Discount];
    myProduct.Featured=[productDictionary valueForKey:JSON_RES_AppProducts_Featured];
    
    myProduct.GroupedProducts=(((NSObject*)([productDictionary valueForKey:JSON_RES_AppProducts_GroupedProducts])==[NSNull null])?[[NSArray alloc]init]:[[NSArray alloc]initWithArray:(NSArray*)[productDictionary valueForKey:JSON_RES_AppProducts_GroupedProducts]]);
    
    myProduct.Id=[productDictionary valueForKey:JSON_RES_AppProducts_Id];
    myProduct.Images=[productDictionary valueForKey:JSON_RES_AppProducts_Images];
    myProduct.ImageUrls=[productDictionary valueForKey:JSON_RES_AppProducts_ImageUrls];
    myProduct.MeasurementUnit=[productDictionary valueForKey:JSON_RES_AppProducts_MeasurementUnit];
    myProduct.Name=[productDictionary valueForKey:JSON_RES_AppProducts_Name];
    myProduct.OutOfStock=[productDictionary valueForKey:JSON_RES_AppProducts_OutOfStock];
    myProduct.Rate=[productDictionary valueForKey:JSON_RES_AppProducts_Rate];
    myProduct.RelatedProducts=[productDictionary valueForKey:JSON_RES_AppProducts_RelatedProducts];
    
    myProduct.RelatedThumbnails=[productDictionary valueForKey:JSON_RES_AppProducts_RelatedProducts_Thumbnail];
    myProduct.ShippedInDays=[productDictionary valueForKey:JSON_RES_AppProducts_ShippedInDays];
    myProduct.SKU=[productDictionary valueForKey:JSON_RES_AppProducts_SKU];
    
    myProduct.Tags=[productDictionary valueForKey:JSON_RES_AppProducts_Tags];
    myProduct.Thumbnail=[[productDictionary valueForKey:JSON_RES_AppProducts_Thumbnail] valueForKey:JSON_RES_AppProducts_Thumbnail_Image];
    myProduct.Type=[productDictionary valueForKey:JSON_RES_AppProducts_Type];
    
    
    return myProduct;
    
}
@end
