//
//  ProductMapper.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>

#import "Product.h"

@interface ProductMapper : NSEntityMapping

-(Product*)mapThisServerDictionaryToProduct:(NSDictionary*)productDictionary;


@end
