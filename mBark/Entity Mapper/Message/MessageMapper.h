//
//  ProductMapper.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>

#import "MessageEntity.h"
#import "MessageThread.h"


@interface MessageMapper : NSEntityMapping

-(MessageThread*)mapThisMessageThreadDictionaryToMessageThreadObj:(NSDictionary*)mtDictionary withSubject:(NSString*)subject;

-(MessageEntity*)mapThisMessageDictionaryToMessageObj:(NSDictionary*)messageThreadDictionary;


@end
