//
//  ProductMapper.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "MessageMapper.h"
#import "MessagesCall_Response.h"

@implementation MessageMapper

-(MessageThread*)mapThisMessageThreadDictionaryToMessageThreadObj:(NSDictionary*)mtDictionary withSubject:(NSString*)subject{

    MessageThread *message=[MessageThread new];
    
    message.subject=subject;
    
    message.threadID=[mtDictionary valueForKey:JSON_RES_Messages_ID];
    
    NSArray *arrayOfmessages=[[NSArray alloc]initWithArray:[mtDictionary valueForKey:JSON_RES_Messages_Chat]];
    
    
    NSMutableArray *newMessagesArray=[[NSMutableArray alloc]init];
    
    NSLog(@"Array Count %d",[arrayOfmessages count]);
    
    for (NSDictionary *dictionary in arrayOfmessages) {
       
        
        NSLog(@"Dictionary of messages %@",dictionary);
        
        MessageEntity *message=[MessageEntity new];
        message=(MessageEntity*)[self mapThisMessageDictionaryToMessageObj:dictionary];
        [newMessagesArray addObject:message];
    
    }
    message.messages=[[NSMutableArray alloc]initWithArray:newMessagesArray];
    
    
    message.lastMessage=([arrayOfmessages count]>0)?[newMessagesArray objectAtIndex:[arrayOfmessages count]-1]:[MessageEntity new];
   
    message.linkedTo=[mtDictionary valueForKey:JSON_Body_LinkedTo];
    
    message.linkReference=[mtDictionary valueForKey:JSON_Body_LinkedToRef];
    
    return message;
    
    
}

-(MessageEntity*)mapThisMessageDictionaryToMessageObj:(NSDictionary*)dictionary {
    
    MessageEntity *message=[MessageEntity new];
   
    NSDictionary *messageThreadDictionary=[[NSDictionary alloc]initWithDictionary:dictionary];
    
    NSLog(@"Dictionary of messages %@",messageThreadDictionary);
    
    
    message.status=[messageThreadDictionary valueForKey:JSON_RES_MESSAGE_Status];
    
    message.body=[messageThreadDictionary valueForKey:JSON_RES_MESSAGE_Body];
    message.direction=[messageThreadDictionary valueForKey:JSON_RES_MESSAGE_Direction];
   
    message.date=[self curentDateStringFromUTCDate:[NSString stringWithFormat:@"%@",[messageThreadDictionary valueForKey:JSON_RES_MESSAGE_Date]] withFormat:@"dd-MM-yyyy"];
    
    return message;
    
    
}


#pragma mark - Date formatter

- (NSDate *)curentDateStringFromUTCDate:(NSString *)utcDateTimeInLine withFormat:(NSString *)dateFormat {
    
    NSLog(@"UTC Date Time  : %@",utcDateTimeInLine);
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [formatter dateFromString:utcDateTimeInLine];
    
    NSLog(@"Date : %@",date);
    
    return date;
    /*
    NSDateFormatter *formatterFinal = [[NSDateFormatter alloc]init];
    [formatterFinal setDateFormat:dateFormat];
    
    NSString *convertedString = [formatterFinal stringFromDate:date];
    
    return convertedString;*/
}


-(NSString*)getTimeFromUTCDate:(NSString*)utcDateTime withFormat:(NSString*)timeFormat{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    //The Z at the end of your string represents Zulu which is UTC
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:utcDateTime];
    NSLog(@"original time: %@", newTime);
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:timeFormat];
    
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    NSLog(@"%@", finalTime);
    
    return finalTime;
    
}
@end
