//
//  ProductMapper.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "StoreMapper.h"

#import "AppStoresDefaults.h"
#import "Store.h"


@implementation StoreMapper


-(Store*)mapThisServerDictionaryToStore:(NSDictionary*)storeDictionary{
    
    Store *myStore=[Store new];
    
    myStore.StoreID=[storeDictionary valueForKey:JSON_RES_Store_Id];
    myStore.Name=[storeDictionary valueForKey:JSON_RES_Store_Name];
    
    myStore.Contact=[storeDictionary valueForKey:JSON_RES_Store_Contact];
    myStore.Email=[storeDictionary valueForKey:JSON_RES_Store_Email];
    
    myStore.Address=[storeDictionary valueForKey:JSON_RES_Store_Address];
    myStore.City=[storeDictionary valueForKey:JSON_RES_Store_City];
    myStore.Pincode=[storeDictionary valueForKey:JSON_RES_Store_Pincode];
    myStore.Country=[storeDictionary valueForKey:JSON_RES_Store_Country];
    
    
    MyUserLocation *location=[MyUserLocation new];
    location.Latitude=[NSString stringWithFormat:@"%f",[[[storeDictionary valueForKey:JSON_RES_Store_Location] valueForKey:JSON_RES_Store_Latitude] floatValue]];
    location.Longitude=[NSString stringWithFormat:@"%f", [[[storeDictionary valueForKey:JSON_RES_Store_Location] valueForKey:JSON_RES_Store_Longitude] floatValue]];
    
    myStore.Location=location;
    
    return myStore;
    
}

@end
