//
//  ProductMapper.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>
#import "Store.h"

@interface StoreMapper : NSEntityMapping

-(Store*)mapThisServerDictionaryToStore:(NSDictionary*)storeDictionary;


@end
