//
//  ProfileViewController.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import "ProfileViewController.h"

#import "RegisterViewController.h"
#import "ChangePassword.h"
#import "HomeCustomNavBarController.h"
#import "UIView+REFrostedViewController.h"

#import "MyTableViewController.h"
#import "MyTopViewController.h"

#import "UIImageView+Letters.h"
#import "RNThemeButton.h"
#import "AppUserDefaults.h"

#import "Products.h"
#import "Messages.h"
#import "MessagesDetails.h"


@interface ProfileViewController ()<EditCalled,MyTableNavigateDelegate>
{
    MyTableViewController * tableViewController;
    MyTopViewController * topViewController;
}

@end

@implementation ProfileViewController



-(id)init{
    
    self=[super init];
    
    if (self) {
        
       self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProfileViewController" owner:self options:nil] objectAtIndex:0];
        
       [self initialiseCustomViews];
        
    }
    
    return self;
    
}


-(void)initialiseCustomViews{
    
     tableViewController = [MyTableViewController new];
     topViewController   = [[MyTopViewController alloc]initWithName:[tableViewController.arrayOfCaptured objectAtIndex:0]];
    
    [topViewController setImageNow];
    topViewController.delegate=self;
   
    tableViewController.delegate=self;
    
    UITableViewController *myTable=tableViewController;
    UIViewController *myView=topViewController;
   
    [self setupWithTopViewController:myView height:250.0f tableViewController:myTable];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self performSelectorInBackground:@selector(updateData) withObject:nil];
    
}

-(void)updateData{
    
    [tableViewController reloadMyData];
    
    NSString *name=[tableViewController.arrayOfCaptured objectAtIndex:0];
    
    if (![name isEqualToString:topViewController.name]) {
        
        [topViewController updateViewWithName:(NSMutableString*)name];
        
    }
}

#pragma table logout


-(void)logOut{
    
    [self backClicked];
    
    [self wipeOutData];
    [[NSFileManager defaultManager] removeItemAtPath:[NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.png",Local_User_Name]] error:nil];
    
}

-(void)changePassword{
    
    
    ChangePassword *controller=[[ChangePassword alloc]init];
    UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:controller];
    navigation.navigationBarHidden=TRUE;
    [self.view.window.rootViewController presentViewController:navigation animated:YES completion:nil];
    
}

#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"edit" withController:self withTitle:@"My Profile"];
    
}




-(void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)filterClicked{
    
    [self callEdit];
    
}

- (void)callEdit{
    
    RegisterViewController *controller=[[RegisterViewController alloc]initWithTheBgImage:[self.view re_screenshot] inEditMode:TRUE];
    UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:controller];
    navigation.navigationBarHidden=TRUE;
    [self.view.window.rootViewController presentViewController: navigation animated:YES completion:nil];
    
}




////////////////////////////////////////////////////////////////////////
#pragma mark - TapGesture Delegate
////////////////////////////////////////////////////////////////////////

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    CGPoint tapPoint = [touch locationInView:self.view];;
    
    if (([self.topViewController.view hitTest:tapPoint withEvent:nil])) {
        
        MyTopViewController * mtvc = (MyTopViewController *)self.topViewController;
        
        // check for tap into button 1
        tapPoint = [touch locationInView:mtvc.editButton];
        if ([mtvc.editButton hitTest:tapPoint withEvent:nil]) {
            [mtvc.editButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            return YES;
        }
        
        
        
    }
    
    return NO;
}



- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;

}


////////////////////////////////////////////////////////////////////////
#pragma mark - Public
////////////////////////////////////////////////////////////////////////

- (void)willChangeHeightOfTopViewControllerFromHeight:(CGFloat)oldHeight toHeight:(CGFloat)newHeight {
    
    MyTopViewController * topViewController1 = (MyTopViewController *)self.topViewController;
    [topViewController1 willChangeHeightFromHeight:oldHeight toHeight:newHeight];
    
    float r = (self.topViewControllerStandartHeight * 1.5f) / newHeight;
    [self.tableViewController.view setAlpha:r*r*r*r*r*r];
    
}



#pragma mark - functinalCall
-(void)wipeOutData{
    
    NSArray *products=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:nil];
    
    for (Products *productsObj in products) {
        
        productsObj.isInCart=[NSNumber numberWithBool:FALSE];
        productsObj.isFavourite=[NSNumber numberWithBool:FALSE];
        
        [GlobalCall saveContext];
    
    }
    
   
    NSArray *messages=[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:nil withPredicate:nil];
    
    
    //error handling goes here
    for (NSManagedObject * message in messages) {
        [[GlobalCall getContext] deleteObject:message];
    }
    
    [GlobalCall saveContext]
    ;
}

@end
