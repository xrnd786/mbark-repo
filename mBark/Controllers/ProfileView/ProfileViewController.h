//
//  ProfileViewController.h
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import <UIKit/UIKit.h>

#import "M6ParallaxController.h"

@interface ProfileViewController : M6ParallaxController<UIGestureRecognizerDelegate>


@end
