//
//  CategoryMainListViewController.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "CategoryMainListViewController.h"

#import "CategoryListView_Helper.h"
#import "ProductListController.h"

#import "HomeCustomNavBarController.h"

@interface CategoryMainListViewController ()<categoryClicked>
{
    BOOL isApplication;
    
}
@property (strong,nonatomic) CategoryListView_Helper *helper;

@end

@implementation CategoryMainListViewController

@synthesize helper;

-(id)init{
    
    self = [super init];
    if (self) {
        
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"CategoryMainListViewController" owner:self options:nil] objectAtIndex:0];
        
        isApplication=FALSE;
        [self loadCustomViews];
        
        
    }
    return self;
    
    
}


-(id)initWithApplicationList{
    
    self = [super init];
    if (self) {
        
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"CategoryMainListViewController" owner:self options:nil] objectAtIndex:0];
        
        isApplication=TRUE;
        
        [SVProgressHUD show];
        [self loadApplicationCustomViews];
        
        
    }
    return self;
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
   

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
}



#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    NSString *titleString;
    
    if (isApplication) {
        
        titleString=@"Applications";
   
    }else{
        
        titleString=@"Categories";
    
    }
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:titleString];
    
}


#pragma mark - loadView Functions

-(void)loadCustomViews{
    
    helper=[[CategoryListView_Helper alloc]initWithView:self.view];
    helper.delegate=self;
}


-(void)loadApplicationCustomViews{
    
    helper=[[CategoryListView_Helper alloc]initWithViewForApplications:self.view];
    helper.delegate=self;
    
}


- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma  mark - Category delegate

-(void)categoryClickedWithCategoryName:(NSString*)categoryName{
    
    ProductListController *controller=[[ProductListController alloc]initWithCategoryName:categoryName];
    self.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.navigationController.interactivePopGestureRecognizer.delegate=controller;
    [self.navigationController pushViewController:controller animated:YES];
    
    
}


-(void)applicationClickedWithApplicationName:(NSString*)applicationName andProductsIds:(NSString*)Id{
    
    ProductListController *controller=[[ProductListController alloc]initWithApplicationId:Id andApplicationName:applicationName];
    self.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.navigationController.interactivePopGestureRecognizer.delegate=controller;
    [self.navigationController pushViewController:controller animated:YES];
    
}


@end
