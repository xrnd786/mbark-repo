//
//  InternetConnection.h
//  mBark
//
//  Created by Xiphi Tech on 02/04/2015.
//
//

#import <UIKit/UIKit.h>

@protocol RetryConnecting <NSObject>

-(void)retryLoadingData;

@end

@interface InternetConnection : UIViewController

@property (strong,nonatomic) id<RetryConnecting> delegate;


- (IBAction)retryConnecting:(id)sender;

@end
