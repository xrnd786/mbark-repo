//
//  InternetConnection.m
//  mBark
//
//  Created by Xiphi Tech on 02/04/2015.
//
//

#import "InternetConnection.h"

@interface InternetConnection ()

@end

@implementation InternetConnection

@synthesize delegate=_delegate;

-(id)init{
    
    self = [super init];
    if (self) {
        
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"InternetConnection" owner:self options:nil] objectAtIndex:0];
        
        
    }
    return self;
    
}


- (IBAction)retryConnecting:(id)sender {
    
    [_delegate retryLoadingData];
    
}
@end
