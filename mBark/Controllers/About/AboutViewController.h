//
//  AboutViewController.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *detailsWebView;
@property (weak, nonatomic) IBOutlet UIImageView *bannerImgae;

@end
