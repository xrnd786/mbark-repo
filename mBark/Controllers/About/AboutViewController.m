//
//  AboutViewController.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "AboutViewController.h"

#import "AboutData.h"
#import "UIImageView+WebCache.h"
#import "In_AppWebView.h"
#import "UIImage+animatedGIF.h"

#import "HomeCustomNavBarController.h"

#define stringBefore  @"<img src=\"data:image/png;base64,"
#define stringAfter  @"\" />"

@interface AboutViewController ()<UIWebViewDelegate,UIGestureRecognizerDelegate>

@property (strong,nonatomic) AboutData *data_helper;
@property (strong,nonatomic) In_AppWebView *webView;

@end

@implementation AboutViewController

@synthesize detailsWebView=_detailsWebView;
@synthesize data_helper=_data_helper;
@synthesize bannerImgae=_bannerImgae;


-(id)init{
    
    self = [super init];
    
    if (self) {
       
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"AboutViewController" owner:self options:nil] objectAtIndex:0];
        
    }
    return self;
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
   
    self.navigationController.navigationBar.backgroundColor=AppBarColor;
    _data_helper=[[AboutData alloc]init];
    
   
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [SVProgressHUD show];
    [self performSelectorInBackground:@selector(loadData) withObject:nil];
    
}



#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@"About us"];
    
}

#pragma mark - Load data

-(void)loadData{
    
    NSString *dataString=[[_data_helper getAboutBanner]stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSString *aboutDeatils=[_data_helper getAbout];
    
    
    if (dataString != nil) {
    
       
        NSString *final=[[[stringBefore stringByAppendingString:dataString] stringByAppendingString:stringAfter] stringByAppendingString:aboutDeatils];
       
        
        NSLog(@"Final html data %@",final);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_detailsWebView loadHTMLString:final baseURL:nil];
            [SVProgressHUD dismiss];
        
        });
        
    } else {
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
           [SVProgressHUD dismiss];
        
        });
        
    }
    
    
}


#pragma mark - NIB Calls


- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)facebookFanPage:(id)sender {
    
    
    [self.navigationController pushViewController:[[In_AppWebView alloc]initWithURL:[_data_helper getFacebook]] animated:YES];
    
}

- (void)googleFanPage:(id)sender {
     [self.navigationController pushViewController:[[In_AppWebView alloc]initWithURL:[_data_helper getGooglePlus]] animated:YES];
}

- (void)twitterFanPage:(id)sender {
     [self.navigationController pushViewController:[[In_AppWebView alloc]initWithURL:[_data_helper getTwitter]] animated:YES];
}

- (void)websiteFanPage:(id)sender {
     //[self.navigationController pushViewController:[[In_AppWebView alloc]initWithURL:[_data_helper getBannerURL]] animated:YES];
}

#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}



@end
