//
//  MenuViewCell.h
//  mBark
//
//  Created by Xiphi Tech on 19/11/2014.
//
//

#import <UIKit/UIKit.h>

//*****THEME CONTROLS*****//
#import "RNThemeLabel.h"


@interface MenuViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet RNThemeLabel *optionTitle;

@end
