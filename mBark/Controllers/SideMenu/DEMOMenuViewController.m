//
//  DEMOMenuViewController.m
//  REFrostedViewControllerExample
//
//  Created by Roman Efimov on 9/18/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOMenuViewController.h"
#import "TDBadgedCell.h"

//*****THEME CONTROLS*****//
#import "RNThemeLabel.h"

//*****Custom Cell*****//
#import "MenuViewCell.h"

//*****FROSTED BACKGROUND*****//
#import "UIImage+ImageEffects.h"

#import "Home.h"
#import "MessageViewController.h"
#import "WishListViewController.h"
#import "AboutViewController.h"

#import "Menu_DataHelper.h"
#import "UIImageView+WebCache.h"
#import "ThemeManager.h"

#import "AppUIDefaults.h"
#import "Messages.h"
#import "MessagesCall_Response.h"

@interface DEMOMenuViewController()
{
   int currentBadgeCount;
}
@property (strong,nonatomic) NSMutableArray *listOptions;
@property (strong,nonatomic) Menu_DataHelper *menuHelper;
@property (strong,nonatomic) NSArray *messagesArray;


@end

@implementation DEMOMenuViewController

@synthesize listOptions,delegate,menuHelper;

-(id)init{
    
    self=[super init];
    if (self) {
        
        currentBadgeCount=0;
        [self performSelectorInBackground:@selector(callForBadgeCountInBG) withObject:nil];
        
    }
    
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];

    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self performSelectorInBackground:@selector(callForBadgeCountInBG) withObject:nil];
    
    self.menuHelper=[[Menu_DataHelper alloc]init];
    
    self.tableView.separatorColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = ({
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, [GlobalCall getMyHeight]/3)];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, [GlobalCall getMyWidth]/3,  [GlobalCall getMyWidth]/3)];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        NSData *pngData = [[NSData alloc] initWithBase64EncodedString:([self.menuHelper getTenantLogo] ==nil)?@"":[self.menuHelper getTenantLogo] options:1];
        [imageView setImage:[UIImage imageWithData:pngData]];
        imageView.contentMode=UIViewContentModeScaleToFill;
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = 50.0;
        imageView.layer.borderColor = [UIColor clearColor].CGColor;
        imageView.layer.borderWidth = 0.2f;
        imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        imageView.layer.shouldRasterize = YES;
        imageView.clipsToBounds = YES;
        
        RNThemeLabel *label = [[RNThemeLabel alloc] initWithFrame:CGRectMake(0, [GlobalCall getMyHeight]/4, [GlobalCall getMyWidth], 50)];
        label.text = [self.menuHelper getOrganization];
        label.fontKey=@"headerFont";
        label.textColorKey=@"secondaryColor";
        label.backgroundColor = [UIColor clearColor];
        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        label.textAlignment=NSTextAlignmentCenter;
        [label applyTheme];
        
        [view addSubview:imageView];
        [view addSubview:label];
        
        
        view;
        
    });
    
    [self initializeArrayForTableItems];
    
}

#pragma mark - backgroundCall

-(void)callForBadgeCountInBG{

    NSArray *messages=[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:@"createdOn" withPredicate:[NSPredicate predicateWithFormat:@"isdeleted==0"]];
    
    __block int badgeCount=0;
    
    
    for (Messages *thread in messages) {
        
        Messages *returnThread=[MessagesCall_Response callApiForGetMessageThread:thread.m_id];
        
        MessagesDetails *details=((MessagesDetails*)[[returnThread.messagesDetails allObjects] lastObject]);
        
        if ([details.direction isEqualToString:Message_Direction_ToAppUser]) {
            
            if ([details.status isEqualToString:Message_STATUS_Pending]) {
                
                badgeCount++;
                
            }
        }
        
    }
    
    currentBadgeCount=badgeCount;
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [self.tableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:[NSIndexPath indexPathForRow:1 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
    
        
    });
    
    //**********************Banner images**************////////////
    dispatch_group_t groupMessages = dispatch_group_create();
    
    dispatch_group_async(groupMessages,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             
                             badgeCount=0;
                             
             for (Messages *thread in messages) {
                 
                     Messages *returnThread=[MessagesCall_Response callApiForGetMessageThread:thread.m_id];
                 
                     MessagesDetails *details=((MessagesDetails*)[[returnThread.messagesDetails allObjects] lastObject]);
                 
                         if ([details.direction isEqualToString:Message_Direction_ToAppUser]) {
                             
                                 if ([details.status isEqualToString:Message_STATUS_Pending]) {
                                     
                                     badgeCount++;
                                     
                                 }
                           }
                                 
             }
                             
                             
     });
    
    
    dispatch_group_notify(groupMessages,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
        
        currentBadgeCount=badgeCount;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           [self.tableView reloadRowsAtIndexPaths:[[NSArray alloc]initWithObjects:[NSIndexPath indexPathForRow:1 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
            
            
        });
    });
    
    
}

#pragma mark -
#pragma mark Initializer Delegate


-(void)initializeArrayForTableItems{
    
    listOptions=[[NSMutableArray alloc]init];
    
    [listOptions addObject:@"Profile"];
    [listOptions addObject:@"Messages"];
    [listOptions addObject:@"Wishlist"];
    [listOptions addObject:@"Purchase history"];
    [listOptions addObject:@"About us"];
    [listOptions addObject:@"Terms of user"];
    
}


#pragma mark - table View Functions


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [GlobalCall getRowHeight];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    return nil;
}



#pragma mark- table Data Functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [listOptions count];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    if (indexPath.row==1) {
        
        return [self getBadgeCellView:tableView withNSIndexPath:indexPath];
        
    } else {
        
        return [self getCell:tableView withIndexPath:indexPath];
        
    }
    
    
}

-(MenuViewCell*)getCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath{
    
    static NSString *cellIdentifier = @"MenuViewCell";
    
    
    MenuViewCell *cell = (MenuViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        
        cell =[[[NSBundle mainBundle]loadNibNamed:@"MenuViewCell" owner:self options:nil] objectAtIndex:0];
        
        
    }
    
    cell.optionTitle.text=listOptions[indexPath.row];
            
    
    return cell;
    
}


-(TDBadgedCell*)getBadgeCellView:(UITableView*)tableView withNSIndexPath:(NSIndexPath*)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    
    TDBadgedCell *cell = [[TDBadgedCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] ;
    
    
    RNThemeLabel *titleLabel=[[RNThemeLabel alloc]initWithFrame:CGRectMake(8, 3, 200, 38)];
    titleLabel.fontKey=@"generalFont";
    titleLabel.textColorKey=@"secondaryColor";
    titleLabel.text=listOptions[indexPath.row];
    [titleLabel applyTheme];
    
    [cell.contentView addSubview:titleLabel];
    
    cell.badgeColor =[[ThemeManager sharedManager]colorForKey:@"notification"];
    cell.badge.radius = 0.5f;
    
    if (currentBadgeCount!=0) {
        
        cell.badgeString=[NSString stringWithFormat:@"%d",currentBadgeCount];
        NSLog(@"Badge string %@",cell.badgeString);
        
    }
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate indexTouched:(int)indexPath.row];
   
}







@end
