//
//  ChatViewController.h
//  mBark
//
//  Created by Xiphi Tech on 25/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "SOMessagingViewController.h"
#import "RNThemeImageButton.h"
#import "RNThemeLabel.h"

#import "Messages.h"

@interface ChatViewController : SOMessagingViewController<UIGestureRecognizerDelegate>

-(id)initWithThreadObj:(Messages*)threadObj ;


@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet RNThemeImageButton *closeNow;
- (IBAction)actionCloseNow:(id)sender;
@property (weak, nonatomic) IBOutlet RNThemeLabel *headingText;


@end
