//
//  ChatViewController.m
//  mBark
//
//  Created by Xiphi Tech on 25/11/2014.
//
//

#import "ChatViewController.h"
#import "ChatView_helper.h"
#import "HomeCustomNavBarController.h"

#import "Messages.h"
#import "MessagesDetails.h"

#import "MessagesCall_Response.h"

@interface ChatViewController ()

@property (strong,nonatomic) ChatView_helper *helper;
@property (strong,nonatomic) Messages *threadMain;

@end


@implementation ChatViewController

@synthesize helper=_helper;
@synthesize baseView=_baseView;

-(id)initWithThreadObj:(Messages*)threadObj{
    
    self = [super init];
    
    if (self) {
        
         self.view=[[[NSBundle mainBundle] loadNibNamed:@"ChatViewController" owner:self options:nil] objectAtIndex:0];
        
        self.threadMain=threadObj;
        self.headingText.text=threadObj.subject;
        
        [self loadCustomViews];
        
        [self performSelectorInBackground:@selector(getData) withObject:nil];
        [self performSelectorInBackground:@selector(updateReadStatus) withObject:nil];
        
    }
    
    return self;
    
}




-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [_helper refreshMessages];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getData{
    
    self.threadMain=[MessagesCall_Response callApiForGetMessageThread:self.threadMain.m_id];
   
    dispatch_async(dispatch_get_main_queue(), ^{
       
        _helper.threadMain=self.threadMain;
        [_helper loadData];
        
    });
    
}


-(void)updateReadStatus{
    
    [MessagesCall_Response callApiToMarkRead:self.threadMain.m_id];
}



#pragma mark - loadView Functions

-(void)loadCustomViews{
    
    _helper=[[ChatView_helper alloc]initWithBaseView:self.view andMainController:self withThreadObj:self.threadMain];
    
}



- (IBAction)actionCloseNow:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [_helper DoDismissTask];
        
    }];
    
}
@end
