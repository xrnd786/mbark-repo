//
//  MasterTab.m
//  mBark
//
//  Created by Xiphi Tech on 21/04/2015.
//
//

#import "MasterTab.h"
#import "MasterTab_helper.h"

@interface MasterTab ()

@property (strong,nonatomic) MasterTab_helper *helper;

@end

@implementation MasterTab

@synthesize helper,myTabBar;

-(id)init{
    
    self=[super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle]loadNibNamed:@"MasterTab" owner:self options:nil] lastObject];
        
        
    }
    
    return self;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    helper=[MasterTab_helper new];
    helper=[helper initWithMyTabBar:self.myTabBar];
    self.navigationController.navigationBarHidden=FALSE;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
