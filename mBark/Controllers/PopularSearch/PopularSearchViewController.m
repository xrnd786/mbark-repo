//
//  PopularSearchViewController.m
//  mBark
//
//  Created by Xiphi Tech on 28/04/2015.
//
//

#import "PopularSearchViewController.h"
#import "PopularSearch_helper.h"

#import "HomeCustomNavBarController.h"

#import "SearchCall_Response.h"
#import "AppSearchDefaults.h"

#import "ProductListController.h"


@interface PopularSearchViewController ()<PopularSearch_helperDelegate>

@property (strong,nonatomic) PopularSearch_helper *helper;

@property (strong,nonatomic) NSMutableArray *searchList;


@end

@implementation PopularSearchViewController

@synthesize searchList,helper,popSearchTable,noItemPlaceholder;


-(id)init
{
    
    self = [super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"PopularSearchViewController" owner:self options:nil] objectAtIndex:0];
        
        
        searchList=[NSMutableArray new];
        helper=[[PopularSearch_helper alloc]initWithTable:popSearchTable andWithController:self];
        helper.delegate=self;
        
        
        [SVProgressHUD show];
        [self performSelectorInBackground:@selector(callPopSearchList) withObject:nil];
        
    }
    
    return self;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@"Popular search"];
    
}


-(void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark -  Background calls

-(void)callPopSearchList{
    
    searchList=(NSMutableArray*)[SearchCall_Response callApiForPopularSearchList];
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        if ([searchList count]==0) {
            
            popSearchTable.hidden=TRUE;
            noItemPlaceholder.hidden=false;
            
            
        }else{
            
            popSearchTable.hidden=FALSE;
            noItemPlaceholder.hidden=TRUE;
            
            helper.searchList=searchList;
            [helper refreshTableWithNewData];
            
        }
        
        [SVProgressHUD dismiss];
        
    });
    
}

#pragma mark - Popular helper delegate

-(void)didSelectRowAtIndex:(int)indexValue{
    
    NSString *searchedText=[searchList objectAtIndex:indexValue];
    
    [AppSearchDefaults saveSearchText:searchedText];
    
   [self.navigationController pushViewController:[[ProductListController alloc]initWithSearchTextForProductList:searchedText] animated:YES];
   
}

@end
