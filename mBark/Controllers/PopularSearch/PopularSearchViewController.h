//
//  PopularSearchViewController.h
//  mBark
//
//  Created by Xiphi Tech on 28/04/2015.
//
//

#import <UIKit/UIKit.h>

@interface PopularSearchViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *popSearchTable;
@property (weak, nonatomic) IBOutlet UIView *noItemPlaceholder;

@end
