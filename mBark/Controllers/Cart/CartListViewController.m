//
//  WishListViewController.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "CartListViewController.h"
#import "CartList_helper.h"

#import <Appirater/Appirater.h>

#import "CartList_DataHelper.h"
#import "ProductCall_Response.h"
#import "ProductThumbnail.h"
#import "HomeCustomNavBarController.h"
#import "CartMaster.h"
#import "Products.h"

@interface CartListViewController ()<callSendCart>
{
    BOOL isCart;
    
}
@property (strong,nonatomic) CartList_helper *helper;
@property (strong,nonatomic) CartList_DataHelper *dataHelper;
@property (strong,nonatomic) NSMutableArray *cartArray;
@property (weak,nonatomic) NSString *Id;

@end

@implementation CartListViewController

@synthesize helper,sorryView,dataHelper,cartArray,Id;

-(id)init{
    
    self = [super init];
    if (self) {
        
        isCart=FALSE;
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"CartListViewController" owner:self options:nil] objectAtIndex:0];
        
    }
    
    return self;
    
}

-(id)initForPostedCarts{
    
    self = [super init];
    if (self) {
        
        isCart=TRUE;
        
    
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"CartListViewController" owner:self options:nil] objectAtIndex:0];
        
        cartArray=[[NSMutableArray alloc]initWithArray:[[GlobalCall getDelegate]fetchStatementWithEntity:[CartMaster class] withSortDescriptor:@"createdOn" withPredicate:nil]];
        
        NSLog(@"Cart array posted %@",cartArray);
        
        [self loadCustomViewsForSentCartWithArray:cartArray];
        [self.cartListTable reloadData];
        
    }
    
    return self;
    
}



-(id)initWithCartProducts:(NSString*)cartIdLocal andProductName:(NSString*)cartName{
    
    self = [super init];
    if (self) {
        
       self.view=[[[NSBundle mainBundle] loadNibNamed:@"CartListViewController" owner:self options:nil] objectAtIndex:0];
            
        
        if ((NSObject*)cartIdLocal !=[NSNull null]) {
            
            Id=cartIdLocal;
            
        }
        
        [self loadCustomViewsWithArray:cartArray];
        
        
    }
    
    return self;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];
    
    if (isCart) {
        
        //cartArray=[[NSMutableArray alloc]initWithArray:[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"isInCart == %d",1]]];
        
        [self loadCustomViewsForSentCartWithArray:cartArray];
        [self.cartListTable reloadData];
        
    } else {
        
        cartArray=[[NSMutableArray alloc]initWithArray:[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"isInCart == %d",1]]];
        

        [self loadCustomViewsWithArray:cartArray];
        [self.cartListTable reloadData];
        
        self.navigationItem.leftBarButtonItem=nil;
        
    }
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTable:) name:APINotification_Cart_Sent_Saved object:nil];
   
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self ];
    
   // [self showBottomAndTop];
    
}

-(void)showBottomAndTop{
    
    self.navigationController.navigationBarHidden=FALSE;
    [GlobalCall showBottomBar];
    
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}



-(void)loadCustomViewsWithCartId:(NSString*)Id{
    
    
    helper=[[CartList_helper alloc]initWithCartProductsTable:self.cartListTable withMainView:self.view andCartId:Id withController:self];
    helper.delegate=self;
    
}


#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@"My Purchase"];
    
    [self.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
    
}



#pragma mark - loadView Functions

-(void)loadCustomViewsWithArray:(NSArray*)array{
    
    if ([array count]==0) {
        
        sorryView.hidden=FALSE;
        self.cartListTable.hidden=TRUE;
        
    } else {
        
        sorryView.hidden=TRUE;
        self.cartListTable.hidden=FALSE;
        
        helper=[[CartList_helper alloc]initWithTable:self.cartListTable withMainView:self.view andProductArray:array withController:self];
        helper.delegate=self;
    
    }
    
    
}

-(void)loadCustomViewsForSentCartWithArray:(NSArray*)array{
    
    if ([array count]==0) {
        
        sorryView.hidden=FALSE;
        
    } else {
        
        sorryView.hidden=TRUE;
        
    }
    
    
    
    helper=[[CartList_helper alloc]initWithTableForCart:self.cartListTable withMainView:self.view  andProductArray:array withController:self];
    
    
}



-(NSArray*)loadData{
    
    dataHelper=[[CartList_DataHelper alloc]init];
    return [dataHelper getCartListArray];

}

#pragma mark - Nib Events

- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)send:(NSArray*)cartMaster{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD show];
    
    });
    
    [self performSelectorInBackground:@selector(callInBgWithCartMaster:) withObject:cartMaster];
    
    
}


-(void)callInBgWithCartMaster:(NSArray*)cartMaster{
    
    NSMutableArray *myCartArray=[[NSMutableArray alloc]init];
    NSMutableArray *productsArray=[[NSMutableArray alloc]init];
    
    for (Products *dic in cartMaster) {
        
        [myCartArray addObject:[[NSDictionary alloc]initWithObjectsAndKeys:dic.p_id,@"ProductId",dic.cartQty,@"Quantity", nil]];
        [productsArray addObject:[[NSDictionary alloc]initWithObjectsAndKeys:dic.name,@"Name",dic.thumbnail.p_id,@"Image",dic.cartQty,@"Quantity", nil]];
        
    }
    
    [ProductCall_Response callApiForSendCart:myCartArray saveArray:productsArray];
    
}


#pragma mark - Notification fucntions

-(void)updateTable:(NSNotificationCenter*)notification{
   
    cartArray=(NSMutableArray*)[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"isInCart == true"]];
    [self loadCustomViewsWithArray:cartArray];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD dismiss];
        
    });
    
    
    [Appirater userDidSignificantEvent:YES];
    
}

#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

@end
