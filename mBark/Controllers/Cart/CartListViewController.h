//
//  WishListViewController.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeImageButton.h"

@interface CartListViewController : UIViewController<UIGestureRecognizerDelegate>

-(id)initForPostedCarts;
-(id)initWithCartProducts:(NSString*)cartIdLocal andProductName:(NSString*)cartName;

-(void)send:(NSArray*)cartMaster;


@property (weak, nonatomic) IBOutlet UITableView *cartListTable;
@property (weak, nonatomic) IBOutlet UIView *sorryView;

@end