//
//  MainViewController.m
//  mBark
//
//  Created by Xiphi Tech on 16/04/2015.
//
//

#import "MainViewController.h"
#import "ABCIntroView.h"

#import "LeveyTabBarController.h"
#import "Home.h"
#import "Search.h"
#import "StoreLocatorViewController.h"
#import "CartListViewController.h"

#import "MasterTab.h"

@interface MainViewController ()<ABCIntroViewDelegate,UINavigationControllerDelegate>

@property (strong,nonatomic) ABCIntroView *introView;


@end



@implementation MainViewController

@synthesize introView;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=TRUE;
    
    self.introView = [[ABCIntroView alloc] initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], [GlobalCall getMyHeight])];
    self.introView.delegate = self;
    self.introView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:self.introView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)onDoneButtonPressed{
    
    
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.introView.alpha = 0;
    } completion:^(BOOL finished) {
        
        [self loadBase];
        [self.introView removeFromSuperview];
        
        
    }];
    
}


-(void)loadBase{
    
    NSMutableDictionary *homeImages = [NSMutableDictionary dictionaryWithCapacity:3];
    [homeImages setObject:[UIImage imageNamed:@"home.png"] forKey:@"Default"];
    [homeImages setObject:[UIImage imageNamed:@"home.png"] forKey:@"Highlighted"];
    [homeImages setObject:[UIImage imageNamed:@"home.png"] forKey:@"Selected"];
    
    
    NSMutableDictionary *searchImages = [NSMutableDictionary dictionaryWithCapacity:3];
    [searchImages setObject:[UIImage imageNamed:@"search.png"] forKey:@"Default"];
    [searchImages setObject:[UIImage imageNamed:@"search.png"] forKey:@"Highlighted"];
    [searchImages setObject:[UIImage imageNamed:@"search.png"] forKey:@"Selected"];
    
    
    NSMutableDictionary *selectionImages = [NSMutableDictionary dictionaryWithCapacity:3];
    [selectionImages setObject:[UIImage imageNamed:@"myBag.png"] forKey:@"Default"];
    [selectionImages setObject:[UIImage imageNamed:@"myBag.png"] forKey:@"Highlighted"];
    [selectionImages setObject:[UIImage imageNamed:@"myBag.png"] forKey:@"Selected"];
    
    
    NSMutableDictionary *locator = [NSMutableDictionary dictionaryWithCapacity:3];
    [locator setObject:[UIImage imageNamed:@"store.png"] forKey:@"Default"];
    [locator setObject:[UIImage imageNamed:@"store.png"] forKey:@"Highlighted"];
    [locator setObject:[UIImage imageNamed:@"store.png"] forKey:@"Selected"];
    
    
    NSMutableArray *labels = [[NSMutableArray alloc]initWithObjects:@"Home",@"Search",@"Stores",@"  Cart", nil];
    
    NSArray *imagesList = [NSArray arrayWithObjects:homeImages,searchImages,locator,selectionImages, nil];
    
    
    
    Home *home = [[Home alloc] init];
    
    
    HomeCustomNavBarController *homeNavigator=[[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"menu" rightImageKey:@"notification"withController:home withTitle:@""];
    [homeNavigator pushViewController:home animated:YES];
    homeNavigator.delegate=(id<UINavigationControllerDelegate>)self;
    
    
    DEMOMenuViewController *menuController = [[DEMOMenuViewController alloc] init];
    menuController.delegate=(id<selectFromLeft>)home;
    
    
    REFrostedViewController *frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:homeNavigator menuViewController:menuController];
    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleLight;
    frostedViewController.liveBlur = YES;
    frostedViewController.delegate = (id<REFrostedViewControllerDelegate>)home;
    
    HomeCustomNavBarController *navigateFrosted = [[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"menu" rightImageKey:@"notification"withController:frostedViewController withTitle:@""];
    [navigateFrosted pushViewController:frostedViewController animated:YES];
    navigateFrosted.navigationBarHidden=TRUE;
    
    
    Search *search=[Search new];
    CartListViewController *selected=[CartListViewController new];
    StoreLocatorViewController *store=[StoreLocatorViewController new];
    
    
    HomeCustomNavBarController *navigateSearch = [[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"back" rightImageKey:@"" withController:search withTitle:@""];
    navigateSearch.navigationBarHidden=TRUE;
    [navigateSearch pushViewController:search animated:YES];
    navigateSearch.delegate=self;
    
    
    HomeCustomNavBarController *navigateCart = [[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"back" rightImageKey:@"" withController:selected withTitle:@""];
    [navigateCart pushViewController:selected animated:YES];
    navigateCart.navigationBarHidden=FALSE;
    navigateCart.delegate=self;
    
    HomeCustomNavBarController *navigateStore = [[HomeCustomNavBarController alloc]initWithTitle:@"Stores" WithController:store];
    [navigateStore pushViewController:store animated:YES];
    navigateStore.navigationBarHidden=FALSE;
    navigateStore.delegate=self;
    
    NSArray *controllersList = [NSArray arrayWithObjects:navigateFrosted,navigateSearch,navigateStore,navigateCart,nil];
    
    
    LeveyTabBarController *controller=[[LeveyTabBarController alloc]initWithImageArray:imagesList labelsArray:labels withViewController:controllersList];
    [controller setViewControllers:(NSMutableArray*)controllersList];
    controller.delegate = (id<LeveyTabBarControllerDelegate>)search;
    
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], 20)];
    view.backgroundColor=[[ThemeManager sharedManager]colorForKey:@"primaryColor"];
    [controller.view addSubview:view];
    
    self.view.window.rootViewController=controller;
    
    //[[MasterTab alloc]init];
    
}



#pragma mark - delegate navigationController
- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)ViewController animated:(BOOL)animated
{
    
    
    if ([ViewController respondsToSelector:@selector(willAppearIn:)])
        [ViewController performSelector:@selector(willAppearIn:) withObject:navController];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
