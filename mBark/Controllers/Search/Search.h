//
//  Search.h
//  P1
//
//  Created by Xiphi Tech on 01/10/2014.
//
//

#import <UIKit/UIKit.h>

//*****VIEW AND DATA HELPER*****//
#import "SearchTable.h"


//*****CUSTOM VIEW*****//
#import "SearchTableView.h"

#import "LeveyTabBarController.h"

@interface Search : UIViewController<UISearchBarDelegate,searchRowSelected,UISearchDisplayDelegate,LeveyTabBarControllerDelegate>
{
    SearchTable *searchResultHelper;
}

@property (weak, nonatomic) IBOutlet UIView *topBar;

@property (nonatomic,strong) NSMutableArray *filteredArray;

@property (weak, nonatomic) IBOutlet SearchTableView *searchResultTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *mySearchBar;

@end
