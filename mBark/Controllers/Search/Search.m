//
//  Search.m
//  P1
//
//  Created by Xiphi Tech on 01/10/2014.
//
//

#import "Search.h"


//*****NAVIGATION HELPER*****//
#import "SearchNavigation.h"
#import "Search_DataHelper.h"
#import "SearchTable.h"

#import "ProductListController.h"
#import "HomeCustomNavBarController.h"
#import "AppSearchDefaults.h"

@interface Search ()<LeveyTabBarControllerDelegate>
{
    SearchNavigation *helper;

}

@property (strong,nonatomic) Search_DataHelper *dataHelper;


@end


@implementation Search


@synthesize searchResultTableView,mySearchBar,dataHelper,filteredArray,topBar;


-(id)init{
    
    self = [super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"Search" owner:self options:nil] objectAtIndex:0];
        
        [self loadCustomView];
        self.mySearchBar.delegate=self;
        
        self.dataHelper=[[Search_DataHelper alloc]init];
        self.filteredArray=[[NSMutableArray alloc]init];
  
        [self.topBar setBackgroundColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
        
    }
    
    return self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=TRUE;

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self.mySearchBar becomeFirstResponder];
    
    searchResultHelper.lastSearched =[[NSMutableArray alloc]initWithArray:[AppSearchDefaults getSearhcedTexts]];
    [searchResultTableView reloadData];
    
    for (UIView *subView in self.mySearchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                //set font color here
                searchBarTextField.textColor = [UIColor blackColor];
                
                break;
            }
        }
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
}



#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@""];
    
    [self.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
    
    [self.topBar setBackgroundColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
    
}

#pragma mark- Search Delegate Methods


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    if ( [searchBar isKindOfClass:[UIResponder class]] == YES && [(UIResponder*)searchBar canResignFirstResponder] == YES )
        
        [searchBar resignFirstResponder];
    
    
    searchBar.text=@"";
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        searchResultHelper.isSearch=[NSNumber numberWithBool:FALSE];
        [searchResultHelper.localTableCopy reloadData];
        
    });
    
    LeveyTabBarController *controller=[[LeveyTabBarController alloc]init];
    controller=((LeveyTabBarController*)self.view.window.rootViewController);
    
    [controller setSelectedIndex:0];
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    [self performSelectorInBackground:@selector(callForPrompts:) withObject:searchText];
    
}




#pragma mark - backgroud tasks

-(void)callForPrompts:(NSString*)searchText{
    
    if ([searchText isEqualToString:@""]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            searchResultHelper.isSearch=[NSNumber numberWithBool:FALSE];
            [searchResultHelper.localTableCopy reloadData];
            
        });
        
    }else{
        
        NSArray *array=[[NSArray alloc]initWithArray:[self.dataHelper loadPromptListForSearchText:searchText]];
        
        NSLog(@"%@",array);
        
        
            dispatch_async(dispatch_get_main_queue(), ^{
                
                searchResultHelper.isSearch=[NSNumber numberWithBool:TRUE];
                [searchResultHelper updateSearchTableWithContent:array withSearchText:searchText];
                
            });
        
    }
    
    
}


#pragma mark - Custom Views

-(void)loadCustomView{
    
    searchResultHelper=[[SearchTable alloc]initWithTable:self.searchResultTableView andMainView:self];
    searchResultHelper.delegate=self;
    helper=[SearchNavigation sharedManager:self];
    
}


#pragma mark - DelegateCalls

-(void)rowSelected:(int)value{
    
    self.navigationController.navigationBarHidden=FALSE;
    
    
    switch (value) {
        case 0:
            
            [helper openCategoryList];
            break;
            
        case 1:
            
            [helper openPopularList];
            break;
        case 2:
            
            [helper openApplicationList];
            break;
            
        default:
            break;
    }
    
    
}



-(void)searchRowSelected:(NSString*)value{
    
    
    ProductListController *controller=[[ProductListController alloc]initWithSearchTextForProductList:value];
    
    
    self.navigationController.navigationBarHidden=FALSE;
    self.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.navigationController.interactivePopGestureRecognizer.delegate=controller;
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

@end
