//
//  ChangePassword.m
//  mBark
//
//  Created by Xiphi Tech on 20/04/2015.
//
//

#import "ChangePassword.h"
#import "UserLoginCall_Response.h"
#import "AppUserDefaults.h"
#import "User.h"


@interface ChangePassword ()<UITextFieldDelegate>
{
    BOOL validated;
    
}
@end

@implementation ChangePassword

@synthesize oldPassword,password,repeatPassword,topView;


-(id)init{
    
    self=[super init];
    
    if (self) {
        
       self.view=[[[NSBundle mainBundle] loadNibNamed:@"ChangePassword" owner:self options:nil] objectAtIndex:0];
            
        UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
        [self.view addGestureRecognizer:gesture];
        
        self.topView.backgroundColor=[[ThemeManager sharedManager]colorForKey:@"primaryColor"];
        
    }
    
    return self;
    
}

-(void)dismissKeyboard{
    
    [oldPassword resignFirstResponder];
    [password resignFirstResponder];
    [repeatPassword resignFirstResponder];
    
    
}

- (IBAction)changePassword:(id)sender {

    
    SCLAlertView *view=[[SCLAlertView alloc]init];
    
    if ([oldPassword.text isEqualToString:@""]) {
        
        [view showWarning:self title:@"Old password necessary" subTitle:@"Please enter old password" closeButtonTitle:@"Ok" duration:0.0f];
        validated=FALSE;
        
    }else if ([password.text isEqualToString:@""]){
        [view showWarning:self title:@"New password necessary" subTitle:@"Please enter new password" closeButtonTitle:@"Ok" duration:0.0f];
        validated=FALSE;
        
        
    }else if ([repeatPassword.text isEqualToString:@""]){
        
        [view showWarning:self title:@"New password necessary" subTitle:@"Please enter new password" closeButtonTitle:@"Ok" duration:0.0f];
        validated=FALSE;
        
    }else{
        
        validated=TRUE;
    
    }
    
    if (validated) {
    
        
        [SVProgressHUD show];
        [self performSelectorInBackground:@selector(changePasswordOnWeb) withObject:nil];

        
    }else{
        
        
        
    }
    
}

- (IBAction)closeNow:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
       
        
    }];
    
}

#pragma mark - web call


-(void)changePasswordOnWeb{
    
    
    [UserLoginCall_Response callApiForUserLoginChange:oldPassword.text newPassword:password.text action:^(bool answer) {
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            SCLAlertView *view=[[SCLAlertView alloc]init];
            
            [SVProgressHUD dismiss];
            
            if (answer) {
                
                [view showSuccess:self title:@"Successful" subTitle:@"Password updated" closeButtonTitle:@"Ok" duration:0.0f];
                
                User *userObj=[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
                userObj.password=password.text;
                [[GlobalCall getDelegate] saveContext];
                
                User *userObjNew =[[[GlobalCall getDelegate] fetchStatementWithEntity:[User class] withSortDescriptor:nil withPredicate:nil] lastObject];
                NSLog(@"New password is %@",userObjNew.password);
                
                [self closeNow:nil];
                
            } else {
                [view showError:self title:@"Incorrect data" subTitle:@"Please type old password correctly" closeButtonTitle:@"Ok" duration:0.0f];
            }
            
            
        });
        
    }];
    
}


#pragma mark - textfield deleagte

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    SCLAlertView *view=[[SCLAlertView alloc]init];
    
    
    if (textField == repeatPassword) {
        
        if ((![repeatPassword.text isEqualToString:@""]) && (![password.text isEqualToString:@""])) {
            
            if (![repeatPassword.text isEqualToString:password.text]) {
                
                [view showWarning:self title:@"No match" subTitle:@"Please repeat the new password correctly" closeButtonTitle:@"Ok" duration:0.0f];
                validated=FALSE;
                repeatPassword.text=@"";
                
            }else{
                validated=TRUE;
                
            }
            
        }
        
    } else if(textField == password){
        
        if ((![repeatPassword.text isEqualToString:@""]) && (![password.text isEqualToString:@""])) {
            
            if (![password.text isEqualToString:repeatPassword.text]) {
                
                [view showWarning:self title:@"No match" subTitle:@"Oops, please type correct password" closeButtonTitle:@"Ok" duration:0.0f];
                validated=FALSE;
                password.text=@"";
                
            }else{
               
                validated=TRUE;
                
            }
            
        }
        
    }
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self changePassword:textField];
    return TRUE;
    
}

@end
