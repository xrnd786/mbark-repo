//
//  ChangePassword.h
//  mBark
//
//  Created by Xiphi Tech on 20/04/2015.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeTextField.h"


@interface ChangePassword : UIViewController
@property (weak, nonatomic) IBOutlet RNThemeTextField *oldPassword;
@property (weak, nonatomic) IBOutlet RNThemeTextField *password;
@property (weak, nonatomic) IBOutlet RNThemeTextField *repeatPassword;
- (IBAction)changePassword:(id)sender;
- (IBAction)closeNow:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *topView;

@end
