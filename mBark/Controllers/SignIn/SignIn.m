//
//  SignIn.m
//  P1
//
//  Created by Xiphi Tech on 30/09/2014.
//
//

#import "SignIn.h"

#import "RegisterViewController.h"
#import "SigninViewHelper.h"
#import "UserLoginCall_Response.h"

@interface SignIn ()

@property (strong,nonatomic) SigninViewHelper *helper;

@end

@implementation SignIn


@synthesize bgView,helper,txtPass,topView,txtUsername;


-(id)initWithTheBgImage:(UIImage*)newSizedImage{
    
    self = [super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"SignIn" owner:self options:nil] objectAtIndex:0];
        self.bgView.backgroundColor=[UIColor colorWithPatternImage:newSizedImage];
        self.topView.backgroundColor=[[ThemeManager sharedManager]colorForKey:@"primaryColor"];
        
        UITapGestureRecognizer *tapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
        
        [self.bgView addGestureRecognizer:tapped];
        
    }
    return self;
    
}


-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SendWithSigedIn" object:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - XIB EVENTS


- (IBAction)back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)signIn:(id)sender {

    [SVProgressHUD show];
    [self performSelectorInBackground:@selector(send) withObject:nil];
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [SVProgressHUD show];
    [self performSelectorInBackground:@selector(send) withObject:nil];
    return YES;
}


#pragma mark - Functions
-(void)send{
    
    
    if ([UserLoginCall_Response callApiForUserLoginAuthenticaton:txtPass.text withUsername:txtUsername.text])
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        });
        
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            if ([txtPass isKindOfClass:[UIResponder class]] == YES && [(UIResponder*)txtPass canResignFirstResponder] == YES )
            {
                
                    [txtPass resignFirstResponder];
                
                
            }
        
            SCLAlertView *view=[[SCLAlertView alloc]init];
            [view showError:self title:@"Error" subTitle:@"Please enter correct password" closeButtonTitle:@"Ok" duration:0.0f];
            txtPass.text=@"";
            
        });
    
    }
  
    
    
}

-(void)tapped:(UIGestureRecognizer*)gesture{
    
    if ( [txtPass isKindOfClass:[UIResponder class]] == YES && [(UIResponder*)txtPass canResignFirstResponder] == YES )
        [txtPass resignFirstResponder];
   
    
}

@end
