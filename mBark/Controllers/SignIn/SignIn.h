//
//  SignIn.h
//  P1
//
//  Created by Xiphi Tech on 30/09/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeTextField.h"

@interface SignIn : UIViewController

-(id)initWithTheBgImage:(UIImage*)newSizedImage;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet RNThemeTextField *txtPass;
@property (weak, nonatomic) IBOutlet RNThemeTextField *txtUsername;

- (IBAction)back:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)signIn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *topView;

@end
