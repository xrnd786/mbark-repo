//
//  WishListViewController.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "WishListViewController.h"

#import "WishList_helper.h"
#import "WishList_DataHelper.h"

#import "HomeCustomNavBarController.h"


@interface WishListViewController ()

@property (strong,nonatomic) WishList_helper *helper;
@property (strong,nonatomic) WishList_DataHelper *dataHelper;

@end

@implementation WishListViewController

@synthesize helper,sorryView,dataHelper;

-(id)init{
    
    self = [super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"WishListViewController" owner:self options:nil] objectAtIndex:0];
        
    }
    
    return self;
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self loadCustomViewsWithArray:[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"isFavourite == True"]]];
    
}


#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@"My Wishlist"];
    
}


#pragma mark - loadView Functions

-(void)loadCustomViewsWithArray:(NSArray*)array{
    
    if ([array count]==0) {
        
        sorryView.hidden=FALSE;
        
    } else {
        
        sorryView.hidden=TRUE;
        
    }
    
    helper=[[WishList_helper alloc]initWithTable:self.wishListTable withMainView:self.view  andProductArray:array withController:self];
    
}

-(NSArray*)loadData{
    
    dataHelper=[[WishList_DataHelper alloc]init];
    return [dataHelper getWishListArray];

}

#pragma mark - Nib Events

- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

@end
