//
//  WishListViewController.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface WishListViewController : UIViewController<UIGestureRecognizerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *wishListTable;
@property (weak, nonatomic) IBOutlet UIView *sorryView;



@end
