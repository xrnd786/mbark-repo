//
//  AuthChoiceController.h
//  mBark
//
//  Created by Xiphi Tech on 19/12/2014.
//
//

#import <UIKit/UIKit.h>

@interface AuthChoiceController : UIViewController

-(id)initWithTheView:(UIView*)mainView;
- (IBAction)closeNow:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
- (IBAction)facebookLoginClicked:(id)sender;
- (IBAction)signUpClicked:(id)sender;
- (IBAction)loginClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *topView;

@end
