//
//  AuthChoiceController.m
//  mBark
//
//  Created by Xiphi Tech on 19/12/2014.
//
//

#import "AuthChoiceController.h"

#import "UIImage+REFrostedViewController.h"
#import "UIView+REFrostedViewController.h"
#import "RECommonFunctions.h"
#import "SignIn.h"
#import "RegisterViewController.h"
#import "UIImage+Color.h"


@interface AuthChoiceController ()

@property (weak, readwrite, nonatomic) UIColor *blurTintColor; // Used only when live blur is off
@property (assign, readwrite, nonatomic) CGFloat blurRadius; // Used only when live blur is off
@property (assign, readwrite, nonatomic) CGFloat blurSaturationDeltaFactor; // Used only when live blur is off

@property (strong, readwrite, nonatomic) UIView *viewForImage;

@end

@implementation AuthChoiceController

@synthesize blurRadius=_blurRadius;
@synthesize blurTintColor=_blurTintColor;
@synthesize blurSaturationDeltaFactor=_blurSaturationDeltaFactor;
@synthesize viewForImage=_viewForImage;
@synthesize backgroundView=_backgroundView;
@synthesize topView;
UIImage* newSizedImage;


-(id)initWithTheView:(UIView*)mainView{
    
    self = [super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"AuthChoiceController" owner:self options:nil] objectAtIndex:0];
        
        self.viewForImage=mainView;
        [self commonInit];
        newSizedImage=nil;
    }
    
    
    self.topView.backgroundColor=[[ThemeManager sharedManager]colorForKey:@"primaryColor"];
    
    return self;
    
}



- (void)commonInit
{

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    self.wantsFullScreenLayout = YES;
#pragma clang diagnostic pop
    
    _blurTintColor = REUIKitIsFlatMode() ? nil : [UIColor colorWithWhite:1 alpha:0.3f];
    _blurSaturationDeltaFactor = 1.8f;
    _blurRadius = 50.0f;
   
}


-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SendWithSigedIn" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (newSizedImage!=nil) {
        
        // newSizedImage=nil;
        //self.backgroundView.backgroundColor=[UIColor whiteColor];
        
    }else{
        
        newSizedImage=[UIImage imageWithImage:[[self.viewForImage re_screenshot] re_applyBlurWithRadius:self.blurRadius tintColor:self.blurTintColor saturationDeltaFactor:self.blurSaturationDeltaFactor maskImage:nil] scaledToSize:CGSizeMake(320, [UIScreen mainScreen].bounds.size.height)];
        
        self.backgroundView.backgroundColor=[UIColor colorWithPatternImage:newSizedImage];
    }
    
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}


- (IBAction)closeNow:(id)sender {
    
    
    [SVProgressHUD show];
    
    [self dismissViewControllerAnimated:YES completion:^{

        [SVProgressHUD dismiss];

    }];
    
}

- (IBAction)facebookLoginClicked:(id)sender {
    
    
}

- (IBAction)signUpClicked:(id)sender {

   
    [self.navigationController pushViewController:[[RegisterViewController alloc]initWithTheBgImage:newSizedImage inEditMode:FALSE] animated:YES];
    
}

- (IBAction)loginClicked:(id)sender {

    
    [self.navigationController pushViewController:[[SignIn alloc]initWithTheBgImage:newSizedImage] animated:YES];
    
}
@end
