//
//  MessageViewController.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <UIKit/UIKit.h>


@interface MessageViewController : UIViewController<UIGestureRecognizerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *messgeTableView;
@property (weak, nonatomic) IBOutlet UIView *sorryView;


@end
