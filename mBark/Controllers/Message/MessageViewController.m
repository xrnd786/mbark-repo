//
//  MessageViewController.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "MessageViewController.h"

//******Helper Class******//
#import "messageView_helper.h"


//******Navigation******//
#import "ChatViewController.h"

#import "AppMessagesDefaults.h"
#import "AppSettingDefaults.h"
#import "AuthChoiceController.h"
#import "AppUserDefaults.h"
#import "MessagesCall_Response.h"

#import "HomeCustomNavBarController.h"

@interface MessageViewController ()<MessageSelected_Protocol>

@property (strong,nonatomic) messageView_helper *helper;
@property (strong,nonatomic) ChatViewController *chatViewController;
@property (strong,nonatomic) NSArray *messages;

@end

@implementation MessageViewController

@synthesize helper;
@synthesize sorryView;
@synthesize messages;

-(id)init{
    
    self = [super init];
    if (self) {
        
        
       self.view=[[[NSBundle mainBundle] loadNibNamed:@"MessageViewController" owner:self options:nil] objectAtIndex:0];
            
       [self performSelectorInBackground:@selector(loadFromLocalOrWebInBackground) withObject:nil];

        
    }
   
    return self;
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [SVProgressHUD show];
    [self loadNotifications];

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if ([messages count]==0) {
    
        messages=[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:@"createdOn" withPredicate:[NSPredicate predicateWithFormat:@"isdeleted==0"]];
        
    }
    
    
    helper.messagesArray=[[NSMutableArray alloc]initWithArray:messages];
    [helper.localTableCopy reloadData];
    [self.messgeTableView setNeedsDisplay];

    [SVProgressHUD dismiss];
    
}

-(void)viewWillDisappear:(BOOL)animated{
   
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ReloadViews" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SendWithSigedIn" object:nil];
    
    
}

- (void)didReceiveMemoryWarning {
  
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - loadView Functions

-(void)loadNotifications{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ReloadViews" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self  name:@"SendWithSigedIn" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hide) name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveMessage) name:@"SaveNewMessage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadViews) name:@"ReloadViews" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(callNewMessage:) name:@"SendWithSigedIn" object:nil];
    
}

-(void)loadCutomView{
    
    helper=[[messageView_helper alloc]initWithTable:self.messgeTableView withSorryView:self.sorryView withSelfView:self withThreadArray:(NSArray*)messages];
    helper.delegate=self;
    
}

#pragma mark - back calls
-(void)loadFromLocalOrWebInBackground{
    
    messages=[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:@"createdOn" withPredicate:nil];
    
    
    
     [self performSelectorInBackground:@selector(backgroundCalls) withObject:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self loadCutomView];
            
        });
    
}


-(void)backgroundCalls{
    
  
    //**********************Banner images**************////////////
    dispatch_group_t groupMessages = dispatch_group_create();
    
    dispatch_group_async(groupMessages,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             
                             for (Messages *thread in messages) {
                                 
                                 [MessagesCall_Response callApiForGetMessageThread:thread.m_id];
                                 
                             }
                             
                             
                         });
    
    
    dispatch_group_notify(groupMessages,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
       
        messages=[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:@"createdOn" withPredicate:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            helper.messagesArray=[[NSMutableArray alloc]initWithArray:messages];
            [helper.localTableCopy reloadData];
        });
    });
 

}

#pragma mark - helper Tasks

-(void)hide{
    
    [helper hide];
}

-(void)saveMessage{
    
    [helper saveMessage];
}

-(void)reloadViews{
    

    messages=[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:@"createdOn" withPredicate:[NSPredicate predicateWithFormat:nil]];
    
    
    [helper reloadTheViewsWithArray:messages];
    
}



#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"add" withController:self withTitle:@"Messages"];
    [self.navigationController.navigationBar.delegate positionForBar:self.navigationController.navigationBar];
    
}


#pragma mark - Nib Calls

- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];

}


- (void)filterClicked{
 
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([[AppSettingDefaults getRegistrationType] isEqualToString:User_Registration_Call_Type_Query] && [AppUserDefaults getUserPassword]==nil) {
            
            AuthChoiceController *cont=[[AuthChoiceController alloc]initWithTheView:self.view];
            
            UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:cont];
            navigation.navigationBarHidden=TRUE;
            
            [self.view.window.rootViewController presentViewController:navigation animated:YES completion:nil];
            
        }else{
            
            [helper addClicked];
        }
        
    });
    
}

#pragma mark - Message Helper Delegate Call

-(void)didSelectIndex:(int)value withNewArray:(NSArray*)resultMessageArray{
    
    self.messages=[[NSArray alloc]initWithArray:resultMessageArray];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    
    self.chatViewController= [[ChatViewController alloc] initWithThreadObj:[self.messages objectAtIndex:value]];
        
    [self.view.window.rootViewController presentViewController:self.chatViewController animated:YES completion:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
                       
    }];
    
}


#pragma mark - chat box delegate

-(void)viewDismissed{
    
    [self.chatViewController dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma  mark - Notification call

-(void)callNewMessage:(NSNotification*)notification{
    
     if ([AppUserDefaults getUserPassword]!=nil) {
      
          [helper addClicked];
    
     }
    
}


#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}


@end
