//
//  ProductListController.h
//  P1
//
//  Created by Xiphi Tech on 10/10/2014.
//
//


#import <UIKit/UIKit.h>

#import "RNThemeImageButton.h"
#import "RNThemeLabel.h"

typedef enum : NSUInteger {
    CategoryProductList,
    ApplicationProductList,
    CartProductList,
    SearchProductList,
    FeaturedProductList
} ProductType;

@interface ProductListController : UIViewController<UIGestureRecognizerDelegate>

-(id)initWithCategoryName:(NSString*)categoryName;
-(id)initWithList:(NSArray*)productList;

-(id)initWithCartProducts:(NSString*)cartIdLocal andProductName:(NSString*)cartName;

-(id)initWithApplicationId:(NSString*)applicationId andApplicationName:(NSString*)applicationName;

-(id)initWithSearchTextForProductList:(NSString*)search;



@property (weak, nonatomic) IBOutlet UITableView *productTable;

@property (weak, nonatomic) IBOutlet UIView *sorryView;

@end
