//
//  ProductListController.m
//  P1
//
//  Created by Xiphi Tech on 10/10/2014.
//
//

#import "ProductListController.h"

#import "ProductListViewHelper.h"
#import "FilterViewController.h"

#import "ProductList_DataHelper.h"
#import "Application_DataHelper.h"
#import "Search_DataHelper.h"
#import "HomeCustomNavBarController.h"

@interface ProductListController ()
{
    int productTypeForNow;
    BOOL isFiltered;
}

@property (strong,nonatomic) ProductListViewHelper *helper;

@property (strong,nonatomic) ProductList_DataHelper *dataHelper;
@property (strong,nonatomic) Application_DataHelper *applicationDataHelper;
@property (strong,nonatomic) Search_DataHelper *searchDataHelper;
@property (strong,nonatomic) FilterViewController *myFilterView;

@property (weak,nonatomic) NSString *category;
@property (weak,nonatomic) NSString *Id;
@property (weak,nonatomic) NSString *searchText;

@property (strong,nonatomic) NSMutableArray *productList;


@end

@implementation ProductListController


@synthesize helper,productTable,dataHelper,category,sorryView,Id,applicationDataHelper,searchDataHelper,searchText,myFilterView,productList;


-(id)initWithCategoryName:(NSString*)categoryName{
    
    self = [super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProductListController" owner:self options:nil] objectAtIndex:0];
        
        productTypeForNow=CategoryProductList;
        
        category=categoryName;
        isFiltered=FALSE;
       
    }
    
    return self;
    
}

-(id)initWithList:(NSArray*)productList{
    
    self = [super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProductListController" owner:self options:nil] objectAtIndex:0];
        productTypeForNow=FeaturedProductList;
        category=@"Featured Products";
        self.productList=productList;
        isFiltered=FALSE;
        
    }
    
    return self;

}


-(id)initWithCartProducts:(NSString*)cartIdLocal andProductName:(NSString*)cartName{
    
    self = [super init];
    if (self) {
        
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProductListController" owner:self options:nil] objectAtIndex:0];
            
        
        category=cartName;
        
        if ((NSObject*)cartIdLocal !=[NSNull null]) {
            
            Id=cartIdLocal;
            
        }
       
        productTypeForNow=CartProductList;
        isFiltered=FALSE;
        
    }
    
    return self;
    
}

-(id)initWithApplicationId:(NSString*)applicationId andApplicationName:(NSString*)applicationName{
    
    self = [super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProductListController" owner:self options:nil] objectAtIndex:0];
            
        
        
        if ((NSObject*)applicationId !=[NSNull null]) {
            
            Id=applicationId;
            
        }
        
        category=applicationName;
        
        productTypeForNow=ApplicationProductList;
        isFiltered=FALSE;
        
    }
    
    return self;
    
}

-(id)initWithSearchTextForProductList:(NSString*)search
{

   self = [super init];

   if (self) {
    
       
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProductListController" owner:self options:nil] objectAtIndex:0];
           
       
       category=search;
       productTypeForNow=SearchProductList;
       self.searchText=search;
       isFiltered=FALSE;
       
   }

    return self;

}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FiltersApplied" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FiltersCleared" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callFilters:) name:@"FiltersApplied" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callCleared:) name:@"FiltersCleared" object:nil];
    
    [self productIsOfType];
    
}

-(void)viewWillDisappear:(BOOL)animated{
   
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FiltersApplied" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FiltersCleared" object:nil];

}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}


#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"filter" withController:self withTitle:category];
   
}


#pragma mark - loadView Functions



-(void)productIsOfType{
    
    switch (productTypeForNow) {
        
        case CategoryProductList:
            
            [self loadCustomViewsWithArray:[self loadDataWithCategory]];
            break;
        
        case CartProductList:
            
            [self loadCustomViewsWithCartId:Id];
           
            break;
        
        case ApplicationProductList:
            
            [self loadCustomViewsWithApplicationId:Id];
            break;
        
        case SearchProductList:
            
            
            [self loadCustomViewsWithSearchText:self.searchText];
            break;
            
            
        case FeaturedProductList:
            
            [self loadCustomViewsWithArray:self.productList];
            break;
            
        default:
            break;
    }
    
    
       
}

-(void)loadCustomViewsWithArray:(NSArray*)array{
    
    self.productList=[NSMutableArray new];
    self.productList=(NSMutableArray*)array;
    
    NSLog(@"Prodct list count %lu",(unsigned long)[self.productList count]);
    
    if ([array count]==0) {
        
        sorryView.hidden=FALSE;
        
    } else {
        
        sorryView.hidden=TRUE;
        helper=[[ProductListViewHelper alloc]initWithTable:self.productTable withMainView:self.view andProductArray:array withController:self];
        
    }
    
    self.navigationItem.title=category;

}



-(void)loadCustomViewsWithCartId:(NSString*)CartId{
   
    sorryView.hidden=TRUE;
    helper=[[ProductListViewHelper alloc]initWithCartProductsTable:self.productTable withMainView:self.view  andCartId:CartId withController:self];
    
    self.navigationController.title=category;
    
}

-(void)loadCustomViewsWithApplicationId:(NSString*)ApplicationId{
    
    sorryView.hidden=TRUE;
    helper=[[ProductListViewHelper alloc]initWithApplicationProductsTable:self.productTable withMainView:self.view andApplicationId:ApplicationId withController:self];
    self.navigationController.title=category;
    
}

-(void)loadCustomViewsWithSearchText:(NSString*)searchText{
    
    sorryView.hidden=TRUE;
    helper=[[ProductListViewHelper alloc]initWithSearchProductsTable:self.productTable withMainView:self.view withController:self withSearchText:self.searchText];
    self.navigationController.title=category;
    
}


-(NSArray*)loadDataWithCategory{
    
    dataHelper=[[ProductList_DataHelper alloc]init];
    return [dataHelper getProductArray:category];

}

-(NSArray*)loadDataWithApplicationId{
    
    applicationDataHelper=[[Application_DataHelper alloc]init];
    return [applicationDataHelper loadApplicationsProductsWithId:self.Id];
    
}




#pragma mark - NIB EVENTS

- (void)backClicked{
    
    [SVProgressHUD dismiss];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)filterClicked{
    
    if (isFiltered) {
        
        NSMutableArray *newFinalFilters=[[NSMutableArray alloc]initWithArray:self.myFilterView.finalFilters];
        
        self.myFilterView =[[FilterViewController alloc]initWithFilterApplied:newFinalFilters andCategoryName:category];
        [self.view.window.rootViewController presentViewController:self.myFilterView animated:YES completion:nil];
        
    } else {
        
        self.myFilterView=[[FilterViewController alloc]initWithCategoryName:category];
        [self.view.window.rootViewController presentViewController:self.myFilterView animated:YES completion:nil];
    }
    
    
}

-(void)callFilters:(NSNotification*)notification{
    
    isFiltered=TRUE;
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    
    
    NSArray *array=[[NSArray alloc]initWithArray:notification.object];
    NSLog(@"Array %@",array);
 
    
    if ([array count]==0) {
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.productTable.hidden=TRUE;
            sorryView.hidden=FALSE;
            [SVProgressHUD dismiss];
            [self.productTable setNeedsDisplay];
            [self.view setNeedsDisplay];
            
        });
        
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            sorryView.hidden=TRUE;
            self.productTable.hidden=false;
          
            helper=[[ProductListViewHelper alloc]initWithTable:self.productTable withMainView:self.view andProductArray:array withController:self];
            self.navigationController.title=category;
            
            [self.productTable reloadData];
            [SVProgressHUD dismiss];
        
            [self.productTable setNeedsDisplay];
            [self.view setNeedsDisplay];
                
        });
        
    
    }
    
    
}



-(void)callCleared:(NSNotification*)notification{

    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD show];
        
    });
    
    [self productIsOfType];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.productTable reloadData];
        [SVProgressHUD dismiss];
        
    });

}

#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

@end
