//
//  FilterViewController.m
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import "FilterViewController.h"

#import "Filter_Helper.h"
#import "VerticalScrollTabBarView.h"

#import "ProductList_DataHelper.h"

#import "ProductCategory.h"
#import "ProductCall_Response.h"
#import "FilterObject.h"
#import "Attribute.h"

@interface FilterViewController ()<FiltersApplied>{
    VerticalScrollTabBarView *tabView;
    BOOL isCategoryFilter;
    int currentTabIndex;
}

@property (strong,nonatomic) Filter_Helper *helper;
@property (strong,nonatomic) NSMutableArray *titleList;
@property (strong,nonatomic) NSMutableArray *valuesList;
@property (strong,nonatomic) NSMutableArray *boundsList;
@property (strong,nonatomic) NSMutableArray *filterControls;
@property (strong,nonatomic) NSMutableArray *attributeNames;
@property (strong,nonatomic) NSMutableArray *parameterNames;

@property (strong,nonatomic) ProductList_DataHelper *Producthelper;

@end

@implementation FilterViewController

@synthesize baseView,helper,leftTabView,titleList,valuesList,boundsList,Producthelper,filterControls,attributeNames,parameterNames,finalArray,localCategoryName,finalFilters;

-(id)initWithCategoryName:(NSString*)categoryName{
    
    self = [super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"FilterViewController" owner:self options:nil] objectAtIndex:0];
            
        self.localCategoryName=categoryName;
        [self loadCustomViews:categoryName];
        
        self.finalFilters=[[NSMutableArray alloc]init];
        
    }
    
    return self;
}


-(id)initWithFilterApplied:(NSArray*)filtersApplied andCategoryName:(NSString*)categoryName{
    
    self = [super init];
    if (self) {
        
        self.localCategoryName=categoryName;
        self.finalFilters=[[NSMutableArray alloc]initWithArray:filtersApplied];
        
        [self loadCustomViews:categoryName];
        
         self.view=[[[NSBundle mainBundle] loadNibNamed:@"FilterViewController" owner:self options:nil] objectAtIndex:0];
        
    }
    
    return self;
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    tabView.selectedTabIndex=[helper.currentTabSelected integerValue];
     NSLog(@"Selected index %ld",(long)tabView.selectedTabIndex);
    
    
}

- (void)didReceiveMemoryWarning {
  
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}


#pragma mark - Helper

-(void)loadCustomViews:(NSString*)category{
    
    [SVProgressHUD show];
    [self performSelectorInBackground:@selector(loadData:) withObject:category];
    
}

-(void)loadData:(NSString*)category{
    
    
    titleList=[[NSMutableArray alloc]init];
    valuesList=[[NSMutableArray alloc]init];
    boundsList=[[NSMutableArray alloc]init];
    
    filterControls=[[NSMutableArray alloc]init];
    attributeNames=[[NSMutableArray alloc]init];
    parameterNames=[[NSMutableArray alloc]init];
    
    Producthelper =[[ProductList_DataHelper alloc]init];
    NSMutableArray *bigData=[NSMutableArray new];
    
    //NSMutableArray *tempBigData=[[NSMutableArray alloc]initWithArray:[AppCategoryDefaults getCategoriesFilterForCategory:[AppCategoryDefaults getCategoryIDFromName:category]]];
    NSMutableArray *tempBigData=[[NSMutableArray alloc]init];
                                 //initWithArray:[[GlobalCall getDelegate]fetchStatementWithEntity:[Filter class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"productCategory.name == %@",self.localCategoryName]]];
    
    
    if ([tempBigData count]!=0) {
        
        
        for (FilterObject *filter in tempBigData) {
            
            //NSLog(@"Object values %@, %@, %@, %@ %@",filter.A,filter.attributeId,filter.attributeType,filter.name,filter.productCategory.name);
            
        }
        
        bigData=tempBigData;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
           
            [Producthelper getCategoryAttributeFor:category];
        
        });
        
    }else{
        
        bigData =[[NSMutableArray alloc]initWithArray:[Producthelper getCategoryAttributeFor:category]];
        
    }
    
    
    NSMutableArray *childrenBigArray=(NSMutableArray*)[((ProductCategory*)[(NSMutableArray*)[[GlobalCall getDelegate]fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"name == %@",self.localCategoryName]]lastObject]).children allObjects];
    NSMutableArray *children=[[NSMutableArray alloc]init];
    
    
    for (NSArray *array in childrenBigArray) {
        
        [children addObject:[array objectAtIndex:1]];
        
    }
    
    if ([children count]!=0) {
        
        //isCategoryFilter=TRUE;
        [titleList addObject:@"Category"];
        [attributeNames addObject:@"Category"];
        [filterControls addObject:@"Checkbox"];
        [parameterNames addObject:@""];
        
        NSSortDescriptor* sortOrder;
        NSArray *valueListNew=[[NSArray alloc]initWithArray:(NSArray*)children];
        sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending: YES];
        valueListNew=[valueListNew sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
       
        [valuesList addObject:valueListNew];
        [boundsList addObject:[NSArray arrayWithObjects:@"", nil]];
        
    }else{
        
        for (FilterObject *attribute in bigData) {
            
            [titleList addObject:attribute.AttributeName];
            [filterControls addObject:attribute.Type];
            [parameterNames addObject:attribute.Values];
            
            if ([attribute.AttributeName isEqualToString:@"Rate"]) {
                
                [attributeNames addObject:@""];
                
            } else {
                
                [attributeNames addObject:@"Attributes"];
                
            }
            
            NSArray *valueListNew=[[NSArray alloc]initWithArray:attribute.Values];
            NSSortDescriptor* sortOrder;
            
            if ([attribute.AttributeName isEqualToString:@"Rate"]) {
                
                sortOrder = [NSSortDescriptor sortDescriptorWithKey:nil ascending: YES];
                NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
                [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                
                NSArray *sArray = [valueListNew sortedArrayUsingComparator:^(id a, id b) {
                    
                    NSNumber *rank1 = [numFormatter numberFromString:a];
                    NSNumber *rank2 = [numFormatter numberFromString:b];
                    return (NSComparisonResult)[rank1 compare:rank2];
                    
                    //return [[numFormatter numberFromString:a] compare:[numFormatter numberFromStrings:b]];
                }];
               
                NSLog(@"%@",sArray);
                
                valueListNew=[NSArray arrayWithArray:sArray];
                
                
                if ([valueListNew count]>=2) {
                    
                    [boundsList addObject:[[NSArray alloc]initWithObjects:[valueListNew objectAtIndex:0],[valueListNew objectAtIndex:([valueListNew count]-1)], nil]];
                    
                } else if([valueListNew count]==1){
                    
                    [boundsList addObject:[NSArray arrayWithObjects:@"0",[valueListNew objectAtIndex:1], nil]];
                    
                }else{
                    
                    [boundsList addObject:[NSArray arrayWithObjects:@"", nil]];
                }
                
                
            } else {
                
                NSSortDescriptor* sortOrder;
                
                sortOrder = [NSSortDescriptor sortDescriptorWithKey:nil ascending: YES];
                NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
                [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                
                NSArray *sArray = [valueListNew sortedArrayUsingComparator:^(id a, id b) {
                    return [[numFormatter numberFromString:a] compare:[numFormatter numberFromString:b]];
                }];
                
                NSLog(@"%@",sArray);
                
                valueListNew=[NSArray arrayWithArray:sArray];
                
                [boundsList addObject:[NSArray arrayWithObjects:@"", nil]];
                
            }
            
            [valuesList addObject:valueListNew];
            
            
        }

   
    }
    
    if([bigData count]!=0){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            helper=[[Filter_Helper alloc]initWithView:self.view andTabView:tabView withLeftTabView:(UIView*)self.leftTabView :self.baseView withAttributeTitleArray:titleList withAttributeValuesList:valuesList withBoundsArray:boundsList typeOfFilterControl:filterControls attributeName:attributeNames parameter:self.parameterNames];
            
            helper.delegate=self;
            [helper updateFinalFilters:self.finalFilters withTabIndex:currentTabIndex];
            helper.localCopyOfTabView.selectedTabIndex=0;
            [SVProgressHUD dismiss];
            
        });
        
    }
    
    
}


- (IBAction)closeView:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
       
        tabView.selectedTabIndex=[helper.currentTabSelected integerValue];
        NSLog(@"My variable %ld",(long)[helper.currentTabSelected integerValue]);
        
    }];
    
}

- (IBAction)clearFilter:(id)sender {
    
    [helper clear];
}

- (IBAction)applyFilter:(id)sender {
    
    [helper apply];
    
}

-(void)filterApplied{
    
    self.finalArray =[[NSArray alloc]init];
    self.finalFilters=[NSMutableArray new];
    
    int i=0;
    for (NSString *value in helper.filterValueCheckedArray) {
        
        
        if ([[helper.filterOnCheckedArray objectAtIndex:i] isEqualToString:@"Attributes"] || [[helper.filterOnCheckedArray objectAtIndex:i] isEqualToString:@"Rate"]) {
            
            [self.finalFilters addObject:[[NSDictionary alloc]initWithObjectsAndKeys:value,@"Value",[helper.filterOnCheckedArray objectAtIndex:i],@"FilterOn",[helper.filterComparerArray objectAtIndex:i],@"Comparer",[helper.filterPrameterCheckedArray objectAtIndex:i],@"Parameter", nil]];
            
        }else{
            
            
            
        }
        
        i++;
    }
    
    
    [self.finalFilters addObject:[[NSDictionary alloc]initWithObjectsAndKeys:self.localCategoryName,@"Value",@"Category",@"FilterOn",@"Equals",@"Comparer",@"",@"Parameter", nil]];
    
    NSLog(@"final filters %@",finalFilters);
    
    __block NSMutableArray *newAnswerData=[NSMutableArray new];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
       
       newAnswerData =(NSMutableArray*)[ProductCall_Response callApiForProductsWithFilter:self.finalFilters];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"FiltersApplied" object:newAnswerData userInfo:nil];
 
    });
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(void)filterCleared{
    
    
     [[NSNotificationCenter defaultCenter]postNotificationName:@"FiltersCleared" object:nil userInfo:nil];
    
}

@end
