//
//  FilterViewController.h
//  mBark
//
//  Created by Xiphi Tech on 21/11/2014.
//
//

#import <UIKit/UIKit.h>


@interface FilterViewController : UIViewController


-(id)initWithCategoryName:(NSString*)categoryName;
-(id)initWithFilterApplied:(NSArray*)filtersApplied andCategoryName:(NSString*)categoryName;


@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UIView *leftTabView;
@property (strong, nonatomic) NSArray *finalArray;
@property (strong, nonatomic) NSMutableArray *finalFilters;
@property (strong,nonatomic) NSString *localCategoryName;

- (IBAction)closeView:(id)sender;
- (IBAction)clearFilter:(id)sender;
- (IBAction)applyFilter:(id)sender;

@end
