//
//  RegisterViewController.m
//  P1
//
//  Created by Xiphi Tech on 09/10/2014.
//
//

#import "RegisterViewController.h"

#import "RegistrationHelper.h"
#import "UIImage+REFrostedViewController.h"
#import "UIView+REFrostedViewController.h"
#import "RECommonFunctions.h"
#import "UIImage+Color.h"
#import "RNThemeButton.h"

#define prompt_for_norule @"Hi, Enter your password to retreive your communication."


@interface RegisterViewController ()

@property (strong,nonatomic) RegistrationHelper *helper;

@property (weak, readwrite, nonatomic) UIColor *blurTintColor; // Used only when live blur is off
@property (assign, readwrite, nonatomic) CGFloat blurRadius; // Used only when live blur is off
@property (assign, readwrite, nonatomic) CGFloat blurSaturationDeltaFactor; // Used only when live blur is off


@end

@implementation RegisterViewController

@synthesize helper,scrollViewCon,bgView,btnBack,topView;

-(id)initWithTheBgImage:(UIImage*)bgImage inEditMode:(BOOL)editMode{
    
    self = [super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"RegisterViewController" owner:self options:nil] objectAtIndex:0];
        
        [self commonInit];
        
        
        self.topView.backgroundColor=[[ThemeManager sharedManager]colorForKey:@"primaryColor"];
        
        self.bgView.backgroundColor=[UIColor colorWithPatternImage:[bgImage re_applyBlurWithRadius:self.blurRadius tintColor:self.blurTintColor saturationDeltaFactor:self.blurSaturationDeltaFactor maskImage:nil]];
        self.helper=[[RegistrationHelper alloc]initWithScroller:self.scrollViewCon withMainSelf:self inEditMode:editMode];
        
        if (editMode) {
            
            self.btnBack.hidden=TRUE;
            
        }
        
    }
    
    return self;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SendWithSigedIn" object:nil];

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)commonInit
{
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    self.wantsFullScreenLayout = YES;
#pragma clang diagnostic pop
    
    _blurTintColor = REUIKitIsFlatMode() ? nil : [UIColor colorWithWhite:1 alpha:0.3f];
    _blurSaturationDeltaFactor = 1.8f;
    _blurRadius = 50.0f;
    
}



#pragma mark - NIB EVENTS

- (IBAction)goBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dismissView:(id)sender {
    
    [SVProgressHUD show];
    [self dismissViewControllerAnimated:YES completion:^{
        [SVProgressHUD dismiss];
    }];

}
@end
