//
//  RegisterViewController.h
//  P1
//
//  Created by Xiphi Tech on 09/10/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeImageButton.h"

@interface RegisterViewController : UIViewController


-(id)initWithTheBgImage:(UIImage*)bgImage inEditMode:(BOOL)editMode;

- (IBAction)goBack:(id)sender;
- (IBAction)dismissView:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewCon;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet RNThemeImageButton *btnBack;
@property (weak, nonatomic) IBOutlet UIView *topView;

@end
