//
//  ProfileViewController.m
//  mBark
//
//  Created by Xiphi Tech on 01/12/2014.
//
//

#import "TermsOfUseViewController.h"

#import "HomeCustomNavBarController.h"

@interface TermsOfUseViewController ()<UIGestureRecognizerDelegate>

@end

@implementation TermsOfUseViewController


-(id)init{
    
    self=[super init];
    
    if (self) {
        
         self.view=[[[NSBundle mainBundle] loadNibNamed:@"TermsOfUseViewController" owner:self options:nil] objectAtIndex:0];
            
        [self initialiseCustomViews];
        
    }
    
    return self;
    
}


-(void)initialiseCustomViews{
    
  //  _helper=[[ProfileViewHelper alloc]initWithMainView:self.baseView withController:self];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@"Terms of use"];
    
}


-(void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

@end
