//
//  StoreLocatorViewController.m
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import "StoreLocatorViewController.h"

#import "StoreLocatorHelper.h"
#import "HomeCustomNavBarController.h"
#import "StoreCall_Response.h"
#import "AppStoresDefaults.h"

@interface StoreLocatorViewController ()

@property (strong,nonatomic) StoreLocatorHelper *helper;
@property (strong,nonatomic) NSArray *allStores;

@end

@implementation StoreLocatorViewController

@synthesize baseView=_baseView;
@synthesize helper=_helper;

-(id)init{
    
    self=[super init];
    
    if (self) {
       
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"StoreLocatorViewController" owner:self options:nil] objectAtIndex:0] ;
        
        [self initialiseCustomViews];
        
    }
    
    return self;
    
}


#pragma mark - view appear

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [GlobalCall showBottomBar];
    
    
    [self performSelectorInBackground:@selector(getAllStores) withObject:nil];
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
}


#pragma mark - background call

-(void)getAllStores{
    
    NSArray *arrayOfStores=[[NSArray alloc]initWithArray:[AppStoresDefaults getAllStores]];
    
    if ([arrayOfStores count]>0) {
        
        self.allStores=[[NSArray alloc]initWithArray:arrayOfStores];
        [_helper initialiseCustomViewsWithStoresArray:self.allStores];
        
        arrayOfStores=[[NSArray alloc]initWithArray:[StoreCall_Response callApiForStoreDetails]];
         self.allStores=[[NSArray alloc]initWithArray:arrayOfStores];
        [_helper initialiseCustomViewsWithStoresArray:self.allStores];
        
        
    } else {
        
        arrayOfStores=[[NSArray alloc]initWithArray:[StoreCall_Response callApiForStoreDetails]];
        self.allStores=[[NSArray alloc]initWithArray:arrayOfStores];
        [_helper initialiseCustomViewsWithStoresArray:self.allStores];
        
    }
    
    
}

#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
   
    id view=[((HomeCustomNavBarController*)self.navigationController) initWithTitle:@"Our stores" WithController:self];
    [self.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
    
}



-(void)initialiseCustomViews{
    
    _helper=[[StoreLocatorHelper alloc]initWithMainView:self.baseView withController:self];
    
}



@end
