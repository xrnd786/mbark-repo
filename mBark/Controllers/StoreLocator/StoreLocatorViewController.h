//
//  StoreLocatorViewController.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#import <UIKit/UIKit.h>

#import "LeveyTabBarController.h"

@interface StoreLocatorViewController : UIViewController<LeveyTabBarControllerDelegate>


@property (weak, nonatomic) IBOutlet UIView *baseView;

@end
