//
//  PostedCarts.m
//  mBark
//
//  Created by Xiphi Tech on 30/03/2015.
//
//

#import "PostedCarts.h"
#import "HomeCustomNavBarController.h"
#import "PostedCartHelper.h"

#import "CartMaster.h"

@interface PostedCarts ()
{
    CartMaster *cartMaster;
}
@property (strong,nonatomic) PostedCartHelper *helper;



@end

@implementation PostedCarts

@synthesize sorryView,helper;


-(id)initWithCartDictionary:(CartMaster*)cartMasterObj{
    
    self = [super init];
    if (self) {
        
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"PostedCarts" owner:self options:nil] objectAtIndex:0];
            
        
        
        cartMaster=cartMasterObj;
        [self loadCustomViewsForSentCart];

        
       [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hide) name:@"HideAFPopup" object:nil];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveMessage) name:@"SaveNewMessage" object:nil];
        
        

        
        
    }
    
    return self;
    
}



-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
   
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SendWithSigedIn" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ReloadViews" object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
    

}



#pragma mark - save and hide popup

-(void)hide{
    
    [self.helper hide];
}


-(void)saveMessage{
    
    [self.helper saveMessage];
    
}

#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:cartMaster.c_id];

}



#pragma mark - loadView Functions



-(void)loadCustomViewsForSentCart
{
    helper=[[PostedCartHelper alloc]initWithTable:self.cartList withMainView:self.view withCartObj:cartMaster withController:self];
    
}



#pragma mark - Nib Events

- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}




#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}



/*
 
 {
 AppUserId = 5518ed5ef78aac03bc03397a;
 CartCreatedOn = "0001-01-01T00:00:00";
 CartNumber = "#P-1003-Q1415";
 CreatedBy = "";
 Details =     (
 {
 Price = 0;
 ProductId = 54d9d0aef78aac15d8ef54f3;
 Quantity = 1;
 },
 {
 Price = 0;
 ProductId = 54d9cfbdf78aac15d8ef54f2;
 Quantity = 1;
 }
 );
 Id = 5518f6bcf78aac03bc0339ab;
 IsDeleted = 0;
 ModifiedBy = "<null>";
 ModifiedOn = "0001-01-01T00:00:00";
 Notes = Notes;
 Products =     (
 {
 Image = "https://precitech.mbark.in/Content/Users/43281313-bcce-4dd7-b693-0320eab6608b/Images/checking-weigh1.png";
 Name = "Inline Weighing Analytics";
 Quantity = 1;
 },
 {
 Image = "https://precitech.mbark.in/Content/Users/43281313-bcce-4dd7-b693-0320eab6608b/Images/checking-weigh3.png";
 Name = "Check Weighing ";
 Quantity = 1;
 }
 );
 Status = Received;
 TenantId = "43281313-bcce-4dd7-b693-0320eab6608b";
 Terms = "<null>";
 }
 
 */

@end
