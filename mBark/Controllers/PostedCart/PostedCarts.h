//
//  PostedCarts.h
//  mBark
//
//  Created by Xiphi Tech on 30/03/2015.
//
//

#import <UIKit/UIKit.h>

@interface PostedCarts : UIViewController


@property (weak, nonatomic) IBOutlet UITableView *cartList;
@property (weak, nonatomic) IBOutlet UIView *sorryView;

-(id)initWithCartDictionary:(NSDictionary*)dictionary;
    
@end
