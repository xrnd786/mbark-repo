//
//  In-AppWebView.m
//  mBark
//
//  Created by Xiphi Tech on 05/12/2014.
//
//

#import "In_AppWebView.h"
#import "HomeCustomNavBarController.h"

@interface In_AppWebView ()<UIWebViewDelegate>

@property (strong,nonatomic) NSString *urlString;

@end

@implementation In_AppWebView

@synthesize urlString,urlView;


-(id)initWithURL:(NSString*)URL{
    
    self = [super init];
    
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"In_AppWebView" owner:self options:nil] objectAtIndex:0];
        
        self.urlString=URL;
    
    }
    return self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self openUrl:self.urlString];
    
}

#pragma mark - navigation delegate

-(void)willAppearIn:(UINavigationController *)navigationController
{
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"" withController:self withTitle:@""];
    
}


#pragma mark - Web view

-(void)openUrl:(NSString *)ref
{
   
    NSURL *URL = [NSURL URLWithString:ref];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:URL  cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    self.urlView.delegate = self ;
    [self.urlView loadRequest:requestObj];
    
}

#pragma mark - Navigation


- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Webview deleagte

@end
