//
//  In-AppWebView.h
//  mBark
//
//  Created by Xiphi Tech on 05/12/2014.
//
//

#import <UIKit/UIKit.h>

@interface In_AppWebView : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *urlView;


-(id)initWithURL:(NSString*)URL;
@end
