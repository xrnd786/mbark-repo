//
//  ProductViewController.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "ProductViewController.h"
#import "AppSettingDefaults.h"

#import "SingleProduct_DataHelper.h"
#import "RNThemeTextField.h"
#import "AuthChoiceController.h"
#import "HomeCustomNavBarController.h"
#import "InternetConnection.h"
#import "Products.h"
#import "ProductThumbnail.h"

@interface ProductViewController ()<RetryConnecting>
{
    RNThemeTextField *field;
    InternetConnection *noNetwork;
    BOOL isFav;
    BOOL isInCart;
    
}
@property (strong,nonatomic) Products *productDictionary;
@property (strong,nonatomic) ProductViewHelper *helper;

@property (strong,nonatomic) SingleProduct_DataHelper *productHelper;

@end

@implementation ProductViewController

@synthesize helper=_helper;
@synthesize productTable=_productTable;
@synthesize productDictionary=_productDictionary;
@synthesize productHelper=_productHelper;
@synthesize productID=_productID;


-(id)init{
    
    self=[super init];
    
    if (self) {
     
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProductViewController" owner:self options:nil] objectAtIndex:0];
            
        
        [self loadCustomViews];
        
        //[GlobalCall hideBottomBar];
        
    }
    
    return self;
}


-(id)initWithProductID:(NSString*)productID{
    
    self = [super init];
    if (self) {
        
         self.view=[[[NSBundle mainBundle] loadNibNamed:@"ProductViewController" owner:self options:nil] objectAtIndex:0];
            
        
        _productID=productID;
        _productHelper=[[SingleProduct_DataHelper alloc]init];
    
        
        [self performSelectorInBackground:@selector(loadFromLocalOrWebInBackground) withObject:nil];
        
    }
    
    return self;
    
}

-(void)loadFromLocalOrWebInBackground{
   
    
    _productID=self.productID;
    
    NSArray *arrayOfProd=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",_productID]]] ;
    
    if ([arrayOfProd count]>0) {
        
        _productDictionary=[arrayOfProd lastObject];
        isFav=[_productDictionary.isFavourite boolValue];
        isInCart=[_productDictionary.isFavourite boolValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [((HomeCustomNavBarController*)self.navigationController) addTitleNowInController:self withTitle:_productDictionary.name];
            [self loadCustomData];
            
        });
        
    }else{
        
        [self loadBasedOnConnection];
        
    }

}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SendWithSigedIn" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ReloadViews" object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
    

    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.backgroundColor=AppBarColor;
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SendWithSigedIn" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ReloadViews" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(callNewMessage:) name:@"SendWithSigedIn" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadViews) name:@"ReloadViews" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hide) name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveMessage) name:@"SaveNewMessage" object:nil];
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    NSLog(@"Product ID %@",self.productID);
    [self performSelectorInBackground:@selector(loadFromLocalOrWebInBackground) withObject:nil];
    [self.navigationController setNavigationBarHidden:FALSE animated:YES];
    
    
    /*if (_productDictionary == nil) {
        [self loadBasedOnConnection];
    }*/
    
    
}


-(void)viewDidDisappear:(BOOL)animated{
    
    [self performSelectorInBackground:@selector(clearData) withObject:nil];
    
}

-(void)loadBasedOnConnection{
    
    if ([GlobalCall isConnected]) {
        
        [SVProgressHUD show];
        [self performSelectorInBackground:@selector(backgroundCalls) withObject:nil];
        
        
    } else {
        
        noNetwork=[[InternetConnection alloc]init];
        noNetwork.delegate=self;
        
        [self.view addSubview:noNetwork.view];
        _productTable.hidden=TRUE;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
        });
    }
    
    
}


#pragma mark - internet connection view delegate

-(void)retryLoadingData{
    
    if ([GlobalCall isConnected]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD show];
            
        });
        
        [noNetwork.view removeFromSuperview];
        [self performSelectorInBackground:@selector(backgroundCalls) withObject:nil];
        
        noNetwork=nil;
        
    } else {
       
        noNetwork=[[InternetConnection alloc]init];
        noNetwork.delegate=self;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view addSubview:noNetwork.view];
            _productTable.hidden=TRUE;
            
        });
        
        
    }

    
}


#pragma mark - save and hide popup

-(void)hide{
    
    [self.helper hide];
}


-(void)saveMessage{
    
    [self.helper saveMessage];
    
}



-(void)backgroundCalls{
   
    
    _productDictionary=[self.productHelper getProductDictionary:_productID];
    
    
    if (_productDictionary) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [((HomeCustomNavBarController*)self.navigationController) addTitleNowInController:self withTitle:_productDictionary.name];
            
            [self loadCustomData];
            [SVProgressHUD dismiss];
            
            
        });

        
    }else{
        
        noNetwork=[[InternetConnection alloc]init];
        noNetwork.delegate=self;
        [self.view addSubview:noNetwork.view];
        _productTable.hidden=TRUE;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
        });

        
    }
    
    
}



-(void)reloadViews{
    
    [_helper reloadTheViews];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - loadView Functions

-(void)loadCustomViews{
    
    
    _productTable.hidden=FALSE;
    _helper=[[ProductViewHelper alloc]initWithTable:_productTable withMainView:self.view  :self WithDictionary:_productDictionary withIsFavourite:isFav withIsAdded:isInCart];
    
     NSLog(@"Table Cell %@",NSStringFromCGRect([_productTable rectForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]));
    
}


-(void)loadCustomData{
    
    _helper.name=_productDictionary.name;
    _helper.descriptionValue=_productDictionary.productDescription;
    _helper.price=[_productDictionary.rate stringValue];
    _helper.discountPrice=[_productDictionary.discount stringValue];
    //_helper.applicationList=_productDictionary.applications;
    _helper.specsArray=(NSMutableArray*)[_productDictionary.attributes allObjects];
    _helper.stockLeft=[_productDictionary.availableUnits stringValue];
    _helper.imageList=(NSMutableArray*)[_productDictionary.images allObjects];
    _helper.thumbnailImage=_productDictionary.thumbnail.p_id;
    _helper.unitValue=_productDictionary.measurementUnit;
    _helper.productDictionary=_productDictionary;
    
    [_helper updateAllData];
 
}

-(void)clearData{
    
    
    
        self.navigationItem.title=@"";
        _helper.imageList=[NSMutableArray new];
        _helper.price=@"0";
        _helper.descriptionValue=@"";
        _helper.name=@"";
        _helper.stockLeft=@"0";
        _helper.thumbnailImage=@"";
        _helper.unitValue=@"";
    
        [_helper clearData];
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
        [self.navigationController.navigationBar setNeedsDisplay];
        [self.view setNeedsDisplay];
        
    });
    
}

#pragma mark - navigation delegate


-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"share" withController:self withTitle:@""];
     self.navigationController.navigationBar.backgroundColor=AppBarColor;
    
}

#pragma mark- All Nib Calls

- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)filterClicked{
    
    NSLog(@"Images %@",_productDictionary.thumbnail.p_id);
    
    UIImage *imageNew=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_productDictionary.thumbnail.p_id]]];
    
    if (!imageNew) return;
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[imageNew] applicationActivities:nil];
    
  //  NSMutableArray *activityList=[[NSMutableArray alloc]initWithObjects:uiactivit, nil];
    
    [self presentViewController:activityViewController animated:YES completion:nil];

}

#pragma  mark - Notification call

-(void)callNewMessage:(NSNotification*)notification{
    
    /*if ([AppUserDefaults getUserPassword]!=nil) {
        
        [_helper callEnquiryView];
        
    }*/
    
}


#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

@end
