//
//  ProductViewController.h
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeImageButton.h"
#import "Products.h"
#import "ProductViewHelper.h"


@interface ProductViewController : UIViewController<UIGestureRecognizerDelegate>


-(id)initWithProductDictionary:(Products*)dictionary;
-(id)initWithProductID:(NSString*)productID;
-(id)initWithProductObject:(Products*)productObj;


@property (weak, nonatomic) IBOutlet UITableView *productTable;

@property (strong,nonatomic) NSString *productID;



@end
