//
//  ProductViewController.h
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import <UIKit/UIKit.h>
#import "RNThemeImageButton.h"


@interface GroupedProductViewController : UIViewController<UIGestureRecognizerDelegate>


-(id)initWithProductDictionary:(NSDictionary*)dictionary;
-(id)initWithProductID:(NSString*)productID;

@property (weak, nonatomic) IBOutlet UITableView *productTable;
@property (strong,nonatomic) NSString *productID;



@end
