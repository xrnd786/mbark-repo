//
//  ProductViewController.m
//  mBark
//
//  Created by Xiphi Tech on 22/11/2014.
//
//

#import "GroupedProductViewController.h"

#import "AppProductDefaults.h"
#import "AppUserDefaults.h"
#import "AppSettingDefaults.h"

#import "GroupedProductViewHelper.h"
#import "GroupedProduct_DataHelper.h"

#import "RNThemeTextField.h"
#import "AuthChoiceController.h"
#import "DXPopover.h"
#import "HomeCustomNavBarController.h"

#import "InternetConnection.h"
#import "Products.h"
#import "ProductThumbnail.h"

@interface GroupedProductViewController ()<RetryConnecting>
{
    RNThemeTextField *field;
    InternetConnection *noNetwork;
    NSArray *arrayOfProducts;
    
}
@property (strong,nonatomic) GroupedProductViewHelper *helper;
@property (strong,nonatomic) Products *productDictionary;
@property (strong,nonatomic) GroupedProduct_DataHelper *dataHelper;
@property (strong,nonatomic) NSDictionary *theProductDictionary;

@end

@implementation GroupedProductViewController

@synthesize helper=_helper;
@synthesize productTable=_productTable;
@synthesize productDictionary=_productDictionary;
@synthesize productID=_productID;
@synthesize dataHelper=_dataHelper;
@synthesize theProductDictionary=_theProductDictionary;


-(id)init{
    
    self=[super init];
    
    if (self) {
        
        
        [self loadMyView];
        [self loadCustomViews];

        
        
    }
    
    return self;
}


-(id)initWithProductID:(NSString*)productID{
    
    self = [super init];
    
    if (self) {
        
        self.productID=productID;
        
        [self loadMyView];
        
        
    }
    
    return self;
    
}


-(void)loadMyView{
    
    
    self.view=[[[NSBundle mainBundle] loadNibNamed:@"GroupedProductViewController" owner:self options:nil] objectAtIndex:0];
    
    self.dataHelper=[[GroupedProduct_DataHelper alloc]init];
    
}



-(void)loadFromLocalOrWebInBackground{
    
    _productID=self.productID;
    _productDictionary=[[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"p_id=='%@'",_productID]]] lastObject];
    
    
    NSLog(@"Product Grouped Array %@",_productDictionary.grouped);
    
    if ([[_productDictionary.grouped allObjects]count]>0) {
        
        arrayOfProducts=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id IN %@",[_productDictionary.grouped allObjects]]] ;
        
        if ([arrayOfProducts count]>0) {
            
            _productDictionary=[arrayOfProducts lastObject];
            
            dispatch_async(dispatch_get_main_queue(), ^{
              
                [((HomeCustomNavBarController*)self.navigationController) addTitleNowInController:self withTitle:_productDictionary.name];
                [self loadCustomData];
            
            });
            
        }else{
            
            [self loadBasedOnConnection];
            
        }

        
    }else{
        
        [self loadBasedOnConnection];
        
    }
    
    
}




-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.backgroundColor=AppBarColor;
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SendWithSigedIn" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ReloadViews" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(callNewMessage:) name:@"SendWithSigedIn" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadViews) name:@"ReloadViews" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hide) name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveMessage) name:@"SaveNewMessage" object:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SendWithSigedIn" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ReloadViews" object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"HideAFPopup" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveNewMessage" object:nil];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
   
    NSLog(@"Product ID %@",self.productID);
    
    [self performSelectorInBackground:@selector(loadFromLocalOrWebInBackground) withObject:nil];
    
}


-(void)viewDidDisappear:(BOOL)animated{
    
    [self performSelectorInBackground:@selector(clearData) withObject:nil];
    
}



-(void)clearData{
    
    self.navigationItem.title=@"";
    _helper.imageList=[NSMutableArray new];
    _helper.price=@"0";
    _helper.descriptionValue=@"";
    _helper.name=@"";
    _helper.stockLeft=@"0";
    _helper.thumbnailImage=@"";
    _helper.unitValue=@"";
    
    [_helper clearData];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.navigationController.navigationBar setNeedsDisplay];
        [self.view setNeedsDisplay];
        
    });
    
}


-(void)loadBasedOnConnection{
    
    if ([GlobalCall isConnected]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD show];
        });
        
        [self backgroundSeverCallForProd:_productID];
        
    } else {
        
        noNetwork=[[InternetConnection alloc]init];
        noNetwork.delegate=self;
        
        [self.view addSubview:noNetwork.view];
        _productTable.hidden=TRUE;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
    }
    
}

#pragma mark - save and hide popup

-(void)hide{
    
    [_helper hide];
}


-(void)saveMessage{
    
    [_helper saveMessage];
    
}



#pragma mark - internet connection view delegate

-(void)retryLoadingData{
    
    if ([GlobalCall isConnected]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD show];
            
        });
        
        [noNetwork.view removeFromSuperview];
        [self performSelectorInBackground:@selector(backgroundSeverCallForProd:) withObject:_productID];
        
        noNetwork=nil;
        
    } else {
        
        noNetwork=[[InternetConnection alloc]init];
        noNetwork.delegate=self;
        [self.view addSubview:noNetwork.view];
        _productTable.hidden=TRUE;
        
    }
    
    
}



#pragma mark - background calls

-(void)backgroundSeverCallForProd:(NSString*)ID{
    
    _productDictionary=[self.dataHelper getGroupedProductDictionary:_productID];
    
    if ([[_productDictionary.grouped allObjects] count]>0) {
        
        NSLog(@"Grouped Objects %@",[[_productDictionary.grouped allObjects] objectAtIndex:0]);
        
        arrayOfProducts=[[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"p_id IN %@",[_productDictionary.grouped allObjects]]] ;
        
        if ([arrayOfProducts count]>0) {
            
            _productDictionary=[arrayOfProducts lastObject];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [((HomeCustomNavBarController*)self.navigationController) addTitleNowInController:self withTitle:_productDictionary.name];
                [self loadCustomData];
                [SVProgressHUD dismiss];
                
            });
            
        }else{
            
            
            
        }
    
        
    }else{
        
        noNetwork=[[InternetConnection alloc]init];
        noNetwork.delegate=self;
        [self.view addSubview:noNetwork.view];
        _productTable.hidden=TRUE;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
        });
        
        
    }

    
    
}

-(void)reloadViews{
    
    [_helper reloadTheViews];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - loadView Functions

-(void)loadCustomViews{
    
    _helper=[[GroupedProductViewHelper alloc]initWithGroupedProductsTable:_productTable withMainView:self.view  withController:self  withProductDictionary:_theProductDictionary withProductObj:_productDictionary];
    _productTable.hidden=FALSE;
    
}



-(void)loadCustomData{
    
    _helper.name=_productDictionary.name;
    _helper.descriptionValue=_productDictionary.productDescription;
    _helper.price=[_productDictionary.rate stringValue];
    _helper.discountPrice=[_productDictionary.discount stringValue];
    //_helper.applicationList=_productDictionary.applications;
    _helper.specsArray=(NSMutableArray*)[_productDictionary.attributes allObjects];
    _helper.allGroupProducts=[[NSMutableArray alloc]initWithArray:arrayOfProducts];
    
    NSLog(@"Specs Array %@ and\nAll grouped Array %@",_helper.specsArray,_helper.allGroupProducts);
    NSLog(@"Product dictionary %@",_productDictionary.thumbnail.p_id);
    
    _helper.stockLeft=[_productDictionary.availableUnits stringValue];
    _helper.imageList=(NSMutableArray*)[_productDictionary.images allObjects];
    _helper.thumbnailImage=_productDictionary.thumbnail.p_id;
    _helper.unitValue=_productDictionary.measurementUnit;
    _helper.productDictionary=_productDictionary;
    
    [_helper updateAllData];
    
}


#pragma mark - navigation delegate


-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    [((HomeCustomNavBarController*)self.navigationController) initForProductListWithWithLeftImageKey:@"back" andFilterKey:@"share" withController:self withTitle:@""];
    
}


#pragma mark- All Nib Calls


- (void)backClicked{
    
    [self.navigationController popViewControllerAnimated:YES];

}


-(void)filterClicked{
    
    NSLog(@"Images %@",_productDictionary.thumbnail.p_id);
    
    UIImage *imageNew=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_productDictionary.thumbnail.p_id]]];
    
    if (!imageNew) return;
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[imageNew] applicationActivities:nil];
    
    [self presentViewController:activityViewController animated:YES completion:nil];

}


 
- (void)enquireClicked:(UIButton*)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([[AppSettingDefaults getRegistrationType] isEqualToString:User_Registration_Call_Type_Query] && [AppUserDefaults getUserPassword]==nil) {
            
            AuthChoiceController *cont=[[AuthChoiceController alloc]initWithTheView:self.view];
            
            UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:cont];
            navigation.navigationBarHidden=TRUE;
            
            [self.view.window.rootViewController presentViewController:navigation animated:YES completion:nil];
            
        }else{
            
            [_helper callEnquiryView];

        }
        
    });
    
}


#pragma  mark - Notification call

-(void)callNewMessage:(NSNotification*)notification{
    
    if ([AppUserDefaults getUserPassword]!=nil) {
        
        [_helper callEnquiryView];
        
    }
    
}


#pragma mark - Gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}
@end
