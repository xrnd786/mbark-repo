//
//  Home.h
//  P1
//
//  Created by Xiphi Tech on 30/09/2014.
//
//

#import <UIKit/UIKit.h>


//*****VIEW AND DATA HELPER*****//


//*****THEME CONTROLS*****//
#import "RNThemeImageButton.h"

#import "REFrostedViewController.h"
#import "DEMOMenuViewController.h"
#import "HomeCustomNavBarController.h"

@interface Home : UIViewController<REFrostedViewControllerDelegate,selectFromLeft,UINavigationBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *HotSellingTable;

@property (weak, nonatomic) IBOutlet UIScrollView *homeScroller;
@property (weak, nonatomic) IBOutlet UICollectionView *featuredCollection;



@end
