//
//  Home.m
//  P1
//
//  Created by Xiphi Tech on 30/09/2014.
//
//

#import "Home.h"

//*****NAVIGATION HELPER*****//
#import "HomeNavigation.h"
#import "HomeViewHelper.h"
#import "Home_DataHelper.h"

#import "UIImageView+WebCache.h"

#import "TenantCall_Response.h"
#import "AppTenantDefaults.h"


@interface Home ()<home_helper_Protocol,UINavigationControllerDelegate>
{
    
    HomeNavigation *helper;
    UIImageView *logoData;
    NSTimer *timer;
    
}
@property (strong,nonatomic) HomeViewHelper *homeHelper;
@property (strong,nonatomic) Home_DataHelper *homeData_helper;
@property (strong,nonatomic) UIImageView *tenantLogo;

@end

@implementation Home

@synthesize HotSellingTable=_HotSellingTable;
@synthesize homeScroller=_homeScroller;
@synthesize homeHelper=_homeHelper;
@synthesize homeData_helper=_homeData_helper;
@synthesize featuredCollection=_featuredCollection;


-(id)init{
    
    self = [super init];
    if (self) {
        
        self.view=[[[NSBundle mainBundle] loadNibNamed:@"Home" owner:self options:nil] objectAtIndex:0];
        
        
        //initializing
        helper=[HomeNavigation sharedManager:self];
        _homeData_helper=[[Home_DataHelper alloc]init];
      
        
        [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
        [self loadCustomViews];
        
        [GlobalCall removeOberverWithNotifier:APINotification_Home_Done handlerClass:self withObject:nil];
        [GlobalCall addObserverWithNotifier:APINotification_Home_Done andSelector:@selector(NowReloadData:) handlerClass:self withObject:nil];
 
        [self.navigationController setViewControllers:@[self] animated:NO];
        
}
    return self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    NSString *imagePath=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.png",Local_Logo_Name]];
    
   self.tenantLogo=[[UIImageView alloc]initWithFrame:CGRectMake(([GlobalCall getMyWidth]/2)-20,0 ,40 , 40)];
    [self.tenantLogo setContentMode:UIViewContentModeScaleAspectFit];
    
    self.tenantLogo.image=[UIImage imageWithContentsOfFile:imagePath];
    
    CGRect frameForLogo=CGRectMake((([UIScreen mainScreen].bounds.size.width)/2)-(self.tenantLogo.frame.size.width/2),5 ,self.tenantLogo.frame.size.width ,self.tenantLogo.frame.size.height);
    [self.tenantLogo setFrame:frameForLogo];
    
    self.navigationItem.titleView=self.tenantLogo!=nil?self.tenantLogo:nil;
    [self.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
    
   
    if ([_homeHelper.productArray count]>0) {
        
    } else {
       
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD show];
            
        });

        
        [self performSelectorInBackground:@selector(backgroundCalls) withObject:nil];
        

    }
    
    timer=[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    [timer fire];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    [self stopNow];
    [GlobalCall removeOberverWithNotifier:APINotification_Home_Done handlerClass:self withObject:nil];
    
}

-(void)NowReloadData:(NSNotification*)object{
    
    [self performSelectorInBackground:@selector(getImageData) withObject:nil];
    [self performSelectorInBackground:@selector(getCategoryData) withObject:nil];
    [self performSelectorInBackground:@selector(getFeaturedProducts) withObject:nil];
    
}

#pragma mark - timer calls

-(void)onTimer{
    
    [_homeHelper scrollOnTimer];
    
}

-(void)stopNow{
    
    [timer invalidate];
    
}


-(void)backgroundCalls{
    
    [self getLogo];
    [self getImageData];
    [self getCategoryData];
    [self getFeaturedProducts];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });

    
}

#pragma mark - navigation delegate

-(void)menuClicked{
    
    [helper openSideMenu];
    
}

-(void)notificationClicked{}


#pragma mark -
#pragma mark Gesture recognizer

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    [self.view endEditing:YES];
    
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController panGestureRecognized:sender];

}



#pragma mark - loadView Functions

-(void)loadCustomViews{
    
    _homeHelper=[[HomeViewHelper alloc]initWithTable:TABLETYPEFEATURED withMainView:self.view viewWithController:self viewWithFeaturedCollectionView:_featuredCollection];
    _homeHelper.delegate=self;
    
}

-(void)willAppearIn:(UINavigationController *)navigationController
{
    
    id new=[((HomeCustomNavBarController*)(self.navigationController)) initForHomeWithLeftImageKey:@"menu" rightImageKey:@"notification" withController:self withTitle:@""];
    
    [self.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
    
}


#pragma mark - fetch data on threads


-(void)getLogo{
    
    //**********************Banner images**************////////////
    dispatch_group_t groupLogo = dispatch_group_create();
    
    dispatch_group_async(groupLogo,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             
                             // block1
                             NSLog(@"Logo image fetch start");
                            
                             NSData *data=[[NSData alloc]init];
                             data=[TenantCall_Response callApiForTenantLogo];
                             
                             NSString *res = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                             
                             [AppTenantDefaults saveTenantLogo:res];
                             
                             NSString *imageData=[_homeData_helper getTenantLogo];
                             NSData *pngData = [[NSData alloc] initWithBase64EncodedString:(imageData==nil)?@"":imageData options:1];
                             
                             logoData = [[UIImageView alloc] initWithImage: [UIImage imageWithData:pngData]];
                             logoData.contentMode=UIViewContentModeScaleAspectFit;
                             
                             NSLog(@"Logo image fetch end");
                             
                         });
    
    
    dispatch_group_notify(groupLogo,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
        
        NSLog(@"UPDATING Logo IMGAE");
        [self updateLogo];
        
    });
    
    
}

-(void)updateLogo{
    
    [self.tenantLogo setImage:logoData.image];
    
    CGRect frameForLogo=CGRectMake((([UIScreen mainScreen].bounds.size.width)/2)-(self.tenantLogo.frame.size.width/2),5 ,self.tenantLogo.frame.size.width ,self.tenantLogo.frame.size.height);
    [self.tenantLogo setFrame:frameForLogo];
   
    NSError *error;
    NSString  *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",[Local_Logo_Name stringByAppendingString:@".png"]]];
    [UIImagePNGRepresentation(self.tenantLogo.image) writeToFile:pngPath atomically:YES];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.navigationItem.titleView =self.tenantLogo;
        
    });
    
}


-(void)getImageData{
    
    
    //**********************Banner images**************////////////
    dispatch_group_t groupBanners = dispatch_group_create();
    
    dispatch_group_async(groupBanners,dispatch_get_global_queue
        (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
        
        // block1
        NSLog(@"Banner image fetch start");
        
        _homeHelper.bannerImageList=[_homeData_helper getAllSliderImages];
        _homeHelper.bannerType=[_homeData_helper getAllSliderNavigationTypes];
        _homeHelper.bannerNavigateTo=[_homeData_helper getAllSliderNavigateTo];
        
        NSLog(@"Banner image fetch end");
            
    });
    
    
    dispatch_group_notify(groupBanners,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
        
        NSLog(@"UPDATING BANNER IMGAES");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
             [_homeHelper updateTableHeader];
            
        });
        
    });
    
    
}


-(void)getCategoryData{
    
    
    //**********************Categories**************////////////
    dispatch_group_t groupCategory = dispatch_group_create();
    
    dispatch_group_async(groupCategory,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             
                             // block1
                             NSLog(@"Category fetch start");
                             _homeHelper.categoryList=[_homeData_helper getAllCategories];
                             NSLog(@"CATEGORY FETCH ENDED");
                             
    });
    
    
    dispatch_group_notify(groupCategory,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
        
        NSLog(@"CATEGORY VIEW UPDATE START");
        
        dispatch_async(dispatch_get_main_queue(), ^{
         
            [_homeHelper updateCategories];
           
         });
        
        NSLog(@"CATEGORY VIEW UPDATE ENDS");
        
    });
    
    
}

-(void)getFeaturedProducts{
    
    
    //**********************Featured**************////////////
    dispatch_group_t groupFeatured = dispatch_group_create();
    
    dispatch_group_async(groupFeatured,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             
                             // block1
                             NSLog(@"FEATURED fetch start");
                             _homeHelper.productArray=[_homeData_helper getAllFeaturedProducts];
                             NSLog(@"FEATURED FETCH ENDED");
                             
                         });
    
    
    dispatch_group_notify(groupFeatured,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
        
        NSLog(@"FEATURED VIEW UPDATE START");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_homeHelper updateFeatured];
            
        });
        
        NSLog(@"FEATURED VIEW UPDATE ENDS");
        
    });
    
    
}

#pragma mark - Delegate Calls of Home Helper


-(void)productCellTouchedWith:(Products *)product{

    [helper openProduct:product];
    
}


#pragma mark - Delegate Calls of Left Menu

-(void)indexTouched:(int)value{
    
    [self.frostedViewController hideMenuViewController];

    switch (value) {
        case 0:
            
            [helper openProfileView];
            break;
        
        case 1:
            
            [helper openMessageView];
            break;
        
        case 2:
            
            [helper openWishListView];
            break;
        case 3:
            
            [helper openSentCartView];
            break;
            

        case 4:
            
            [helper openAboutView];
            break;
            
        case 5:
            
            [helper openTermsOfUseView];
            break;
            
        default:
            break;
            
    }
    
   
}


@end
