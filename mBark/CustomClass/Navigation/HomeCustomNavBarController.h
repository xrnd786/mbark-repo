//
//  CustomNavBarController.h
//  mBark
//
//  Created by Xiphi Tech on 25/03/2015.
//
//

#import <UIKit/UIKit.h>

#import "RNThemeImageButton.h"

@interface HomeCustomNavBarController : UINavigationController<UINavigationControllerDelegate>

-(id)initForHomeWithLeftImageKey:(NSString*)leftImageKey rightImageKey:(NSString*)rightImageKey withController:(UIViewController*)controller withTitle:(NSString*)title;
-(void)initForProductListWithWithLeftImageKey:(NSString*)leftImageKey andFilterKey:(NSString*)rightImageKey withController:(UIViewController*)controller withTitle:(NSString*)title;
-(id)initWithTitle:(NSString*)title WithController:(UIViewController*)controller;


-(void)addTitleNowInController:(UIViewController*)controller withTitle:(NSString*)titleNew;

-(void)setBadgeForControllerLeftItem:(UIViewController*)viewcontroller;


@property (strong,nonatomic) RNThemeImageButton *leftButton;
@property (strong,nonatomic) RNThemeImageButton *rightButton;


@end
