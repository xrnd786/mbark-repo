//
//  CustomNavBarController.m
//  mBark
//
//  Created by Xiphi Tech on 25/03/2015.
//
//

#import "HomeCustomNavBarController.h"

#import "HomeCustomNavigationBar.h"
#import "UIBarButtonItem+Badge.h"

#import "RNThemeImageButton.h"
#import "RNThemeLabel.h"

#import "ThemeManager.h"
#import "Home.h"

#import "AppUIDefaults.h"

@interface HomeCustomNavBarController ()<UIGestureRecognizerDelegate,UINavigationBarDelegate,UIBarPositioningDelegate>

@end

@implementation HomeCustomNavBarController

@synthesize leftButton,rightButton;

-(id)initForHomeWithLeftImageKey:(NSString*)leftImageKey rightImageKey:(NSString*)rightImageKey withController:(UIViewController*)controller withTitle:(NSString*)titleNew{
    
    self=[super init];
    
    if (self) {
        
        
        
        //******LEFT BUTTON***********//
       if (![leftImageKey isEqualToString:@""]) {
           
            leftButton = [RNThemeImageButton buttonWithType:UIButtonTypeCustom];
            leftButton.frame = CGRectMake(0, 0, 40, 40);
            
           leftButton.backgroundColorKey=@"lightColor";
           leftButton.highlightedColorKey=@"secondaryColor";
           leftButton.backgroundImageKey=leftImageKey;
            [leftButton applyTheme];
            
            [leftButton addTarget:controller action:@selector(menuClicked) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
            controller.navigationItem.leftBarButtonItem = navLeftButton;
            
            
        }
        
        
        //******RIGHT BUTTON***********//
        if (![rightImageKey isEqualToString:@""]) {
            
            rightButton = [RNThemeImageButton buttonWithType:UIButtonTypeCustom];
            rightButton.frame = CGRectMake([GlobalCall getMyWidth]-40, 0, 40, 40);
            
            rightButton.backgroundColorKey=@"lightColor";
            rightButton.highlightedColorKey=@"secondaryColor";
            rightButton.backgroundImageKey=rightImageKey;
            [rightButton applyTheme];
            
            [rightButton addTarget:controller action:@selector(notificationClicked) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *navRightButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
            controller.navigationItem.rightBarButtonItem = navRightButton;
            
        }
        
    //controller.navigationController.navigationBar.delegate=self;
        [controller.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
        controller.navigationController.navigationBar.opaque = YES;
        controller.navigationController.navigationBar.translucent = NO;

        
        if (![titleNew isEqualToString:@""]) {
            CGFloat tentWidth=([GlobalCall getMyWidth]-150.0f);
            
            UIView *view=[[UIView alloc]initWithFrame:CGRectMake((([GlobalCall getMyWidth]/2)-(tentWidth/2)), 0, tentWidth, 40)];
            
            RNThemeLabel *title=[[RNThemeLabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 40)];
            title.textAlignment=NSTextAlignmentCenter;
            title.text=titleNew;
            title.textColorKey=@"lightColor";
            title.fontKey=@"headerFont";
            [title applyTheme];
            [view addSubview:title];
            controller.navigationItem.titleView=view;
            
        }
        
    }
    
    
    return self;

}

-(void)initForProductListWithWithLeftImageKey:(NSString*)leftImageKey andFilterKey:(NSString*)rightImageKey withController:(UIViewController*)controller withTitle:(NSString*)titleNew{

   
        if (![leftImageKey isEqualToString:@""]) {
           
            leftButton = [RNThemeImageButton buttonWithType:UIButtonTypeCustom];
            leftButton.frame = CGRectMake(0, 0, 40, 40);
            
            leftButton.backgroundColorKey=@"lightColor";
            leftButton.highlightedColorKey=@"secondaryColor";
            leftButton.backgroundImageKey=leftImageKey;
            [leftButton applyTheme];
            
            
            [leftButton addTarget:controller action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
            
            
            UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
            
            controller.navigationItem.leftBarButtonItem = navLeftButton;
            controller.navigationItem.leftBarButtonItem.badgeBGColor=[UIColor clearColor];
            
            
            
        }
    
      if (![rightImageKey isEqualToString:@""]) {
        
        rightButton = [RNThemeImageButton buttonWithType:UIButtonTypeCustom];
        rightButton.frame = CGRectMake([GlobalCall getMyWidth]-40, 0, 40, 40);
        
        rightButton.backgroundColorKey=@"lightColor";
        rightButton.highlightedColorKey=@"secondaryColor";
        rightButton.backgroundImageKey=rightImageKey;
       [rightButton applyTheme];
        
          [rightButton addTarget:controller action:@selector(filterClicked) forControlEvents:UIControlEventTouchUpInside];
          
          
          
        UIBarButtonItem *navRightButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        controller.navigationItem.rightBarButtonItem = navRightButton;
          
      }
    
    [controller.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
    controller.navigationController.navigationBar.opaque = YES;
    controller.navigationController.navigationBar.translucent = NO;

    
    if (![titleNew isEqualToString:@""]) {
        CGFloat tentWidth=([GlobalCall getMyWidth]-150.0f);
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake((([GlobalCall getMyWidth]/2)-(tentWidth/2)), 0, tentWidth, 40)];
        
        RNThemeLabel *title=[[RNThemeLabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 40)];
        title.textAlignment=NSTextAlignmentCenter;
        title.text=titleNew;
        title.textColorKey=@"lightColor";
        title.fontKey=@"headerFont";
        [title applyTheme];
        [view addSubview:title];
        controller.navigationItem.titleView=view;
        
    }
    
}




-(id)initWithTitle:(NSString*)titleNew WithController:(UIViewController*)controller{
    
    self=[super init];
    
    if (self) {
        
        if (![titleNew isEqualToString:@""]) {
            CGFloat tentWidth=([GlobalCall getMyWidth]-150.0f);
            
            UIView *view=[[UIView alloc]initWithFrame:CGRectMake((([GlobalCall getMyWidth]/2)-(tentWidth/2)), 0, tentWidth, 40)];
            
            RNThemeLabel *title=[[RNThemeLabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 40)];
            title.textAlignment=NSTextAlignmentCenter;
            title.text=titleNew;
            title.textColorKey=@"lightColor";
            title.fontKey=@"headerFont";
            [title applyTheme];
            [view addSubview:title];
            controller.navigationItem.titleView=view;
            
        }
        
        [controller.navigationController.navigationBar setBarTintColor:[[ThemeManager sharedManager]colorForKey:@"primaryColor"]];
        controller.navigationController.navigationBar.opaque = YES;
        controller.navigationController.navigationBar.translucent = NO;

    }
    
    return self;
}


-(void)setBadgeForControllerLeftItem:(UIViewController*)viewController{
    
    Home *controller=(Home*)viewController;
    
    controller.navigationItem.leftBarButtonItem.badgeValue = @"1";
    controller.navigationItem.leftBarButtonItem.badgeBGColor = [[ThemeManager sharedManager]colorForKey:@"notification"];
    controller.navigationItem.leftBarButtonItem.badgeOriginX=1;
    controller.navigationItem.leftBarButtonItem.badgeOriginY=1;
    
}



- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar {
    
    return UIBarPositionTopAttached;

}

-(void)addTitleNowInController:(UIViewController*)controller withTitle:(NSString*)titleNew{
    
    
    if (![titleNew isEqualToString:@""]) {
        CGFloat tentWidth=([GlobalCall getMyWidth]-150.0f);
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake((([GlobalCall getMyWidth]/2)-(tentWidth/2)), 0, tentWidth, 40)];
        
        RNThemeLabel *title=[[RNThemeLabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 40)];
        title.textAlignment=NSTextAlignmentCenter;
        title.text=titleNew;
        title.textColorKey=@"lightColor";
        title.fontKey=@"headerFont";
        [title applyTheme];
        [view addSubview:title];
        controller.navigationItem.titleView=view;
        
    }
}

@end
