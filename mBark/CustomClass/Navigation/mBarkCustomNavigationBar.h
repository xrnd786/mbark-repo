//
//  mBarkCustomNavigationBar.h
//  mBark
//
//  Created by Xiphi Tech on 24/03/2015.
//
//

#import <UIKit/UIKit.h>
#import "UINavigationController+Persistence.h"

@interface mBarkCustomNavigationBar : UINavigationBar

@end
