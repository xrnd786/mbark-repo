//
//  ConnectionToast.h
//  mBark
//
//  Created by Xiphi Tech on 22/04/2015.
//
//

#import <Foundation/Foundation.h>

@interface MessageToast : NSObject


-(void)showToastWithTitle:(NSString*)title withSubtitle:(NSString*)subtitle;

@end
