//
//  ConnectionToast.m
//  mBark
//
//  Created by Xiphi Tech on 22/04/2015.
//
//

#import "MessageToast.h"
#import "CRToast.h"

@implementation MessageToast

-(void)showToastWithTitle:(NSString*)title withSubtitle:(NSString*)subtitle{
 
    [CRToastManager showNotificationWithOptions:[self optionsWithTitleText:title andSubtitle:subtitle]
                                 apperanceBlock:^(void) {
                                     NSLog(@"Appeared");
                                 }
                                completionBlock:^(void) {
                                    NSLog(@"Completed");
    }];

}


- (NSDictionary*)optionsWithTitleText:(NSString*)title andSubtitle:(NSString*)subTitle{
   
    NSMutableDictionary *options = [
                        @{kCRToastNotificationTypeKey
                                  :  @(CRToastTypeNavigationBar),
                        kCRToastNotificationPresentationTypeKey
                                  : @(CRToastPresentationTypeCover),
                        kCRToastUnderStatusBarKey
                                  : @(FALSE),
                        kCRToastTextKey
                                  : title,
                        kCRToastTextAlignmentKey
                                  : @(NSTextAlignmentLeft),
                        kCRToastTimeIntervalKey
                                  : @(2.0f),
                        kCRToastAnimationInTypeKey
                                  : @(CRToastAnimationTypeLinear),
                        kCRToastAnimationOutTypeKey
                                 : @(CRToastAnimationTypeLinear),
                        kCRToastAnimationInDirectionKey
                                 : @(0),
                        kCRToastAnimationOutDirectionKey
                                 : @(0)} mutableCopy];
    
      options[kCRToastImageKey] = [UIImage imageNamed:@"message.png"];
      options[kCRToastSubtitleTextKey] = subTitle;
      options[kCRToastSubtitleTextAlignmentKey] = @(NSTextAlignmentLeft);
    
    options[kCRToastInteractionRespondersKey] = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTap
            automaticallyDismiss:YES
            block:^(CRToastInteractionType interactionType){
                NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
            }]];
    
    return [NSDictionary dictionaryWithDictionary:options];

}


@end
