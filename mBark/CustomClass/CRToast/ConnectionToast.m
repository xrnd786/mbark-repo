//
//  ConnectionToast.m
//  mBark
//
//  Created by Xiphi Tech on 22/04/2015.
//
//

#import "ConnectionToast.h"
#import "CRToast.h"

@implementation ConnectionToast

-(void)showToastWithText:(NSString*)Text{
    
    [CRToastManager showNotificationWithOptions:[self optionsWithText:Text]
                                 apperanceBlock:^(void) {
                                     NSLog(@"Appeared");
                                 }
                                completionBlock:^(void) {
                                    NSLog(@"Completed");
    }];

}

-(void)showToast{
    
    [CRToastManager showNotificationWithOptions:[self optionsWithText:APP_NO_INTERNET]
                                 apperanceBlock:^(void) {
                                     NSLog(@"Appeared");
                                 }
                                completionBlock:^(void) {
                                    NSLog(@"Completed");
                                }];
    
}





-(void)showToastForUnauthorised{
    
    [CRToastManager showNotificationWithOptions:[self optionsWithText:APP_UNAUTHORIZED_USER]
                                 apperanceBlock:^(void) {
                                     NSLog(@"Appeared");
                                 }
                                completionBlock:^(void) {
                                    NSLog(@"Completed");
                                }];
    
}


- (NSDictionary*)optionsWithText:(NSString*)text{
   
    NSMutableDictionary *options = [
                        @{kCRToastNotificationTypeKey
                                  :  @(CRToastTypeStatusBar),
                        kCRToastNotificationPresentationTypeKey
                                  : @(CRToastPresentationTypeCover),
                        kCRToastUnderStatusBarKey
                                  : @(FALSE),
                        kCRToastTextKey
                                  : text,
                        kCRToastTextAlignmentKey
                                  : @(NSTextAlignmentCenter),
                        kCRToastTimeIntervalKey
                                  : @(2.0f),
                        kCRToastAnimationInTypeKey
                                  : @(CRToastAnimationTypeLinear),
                        kCRToastAnimationOutTypeKey
                                 : @(CRToastAnimationTypeLinear),
                        kCRToastAnimationInDirectionKey
                                 : @(0),
                        kCRToastAnimationOutDirectionKey
                                 : @(0)} mutableCopy];
    
      options[kCRToastImageKey] = [UIImage imageNamed:@"alert_icon.png"];
      options[kCRToastSubtitleTextKey] = @"";
      options[kCRToastSubtitleTextAlignmentKey] = @(NSTextAlignmentCenter);
    
    options[kCRToastInteractionRespondersKey] = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTap
            automaticallyDismiss:YES
        block:^(CRToastInteractionType interactionType){
        NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                     }]];
    
    return [NSDictionary dictionaryWithDictionary:options];
}


@end
