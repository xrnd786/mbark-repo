//
//  ConnectionToast.h
//  mBark
//
//  Created by Xiphi Tech on 22/04/2015.
//
//

#import <Foundation/Foundation.h>

@interface ConnectionToast : NSObject

-(void)showToastWithText:(NSString*)Text;

-(void)showToast;
-(void)showToastForUnauthorised;

@end
