//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "WishList_DataHelper.h"

#import "AppProductDefaults.h"

@interface WishList_DataHelper()



@end


@implementation WishList_DataHelper


-(NSArray*)getWishListArray{
    
    return  [AppProductDefaults getProductsArrayForWishList];
    
}



@end
