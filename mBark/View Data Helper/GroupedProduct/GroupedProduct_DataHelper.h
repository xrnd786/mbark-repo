//
//  Home_DataHelper.h
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import <Foundation/Foundation.h>
#import "ProductCall_Response.h"


@interface GroupedProduct_DataHelper : NSObject


-(Products*)getGroupedProductDictionary:(NSString*)productID;

@end
