//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "GroupedProduct_DataHelper.h"


@interface GroupedProduct_DataHelper()

@end


@implementation GroupedProduct_DataHelper



-(Products*)getGroupedProductDictionary:(NSString*)productID{
    
    return [ProductCall_Response callApiForGroupedProductWithID:productID];
    
}







@end
