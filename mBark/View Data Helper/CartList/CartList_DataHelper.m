//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "CartList_DataHelper.h"


@interface CartList_DataHelper()



@end


@implementation CartList_DataHelper


-(NSArray*)getCartListArray{
    
    return [[GlobalCall getDelegate]fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"IsInCart == true"]]];
    
    
}



@end
