//
//  Application_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 16/03/2015.
//
//

#import "Search_DataHelper.h"
#import "SearchCall_Response.h"

@implementation Search_DataHelper


-(NSArray*)loadPromptListForSearchText:(NSString*)searchText{
    
   return [SearchCall_Response callApiForSearchListWithText:searchText];
    
}


-(NSArray*)loadProductsListForSearch:(NSString*)finalSearchText{
    
    return [SearchCall_Response callApiForProductListWithSearchText:finalSearchText];
    
}

@end
