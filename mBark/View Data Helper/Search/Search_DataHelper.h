//
//  Application_DataHelper.h
//  mBark
//
//  Created by Xiphi Tech on 16/03/2015.
//
//

#import <Foundation/Foundation.h>
#import "SearchCall_Response.h"

@interface Search_DataHelper : NSObject

-(NSArray*)loadPromptListForSearchText:(NSString*)searchText;
-(NSArray*)loadProductsListForSearch:(NSString*)finalSearchText;


@end
