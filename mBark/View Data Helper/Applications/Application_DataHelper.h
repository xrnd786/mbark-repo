//
//  Application_DataHelper.h
//  mBark
//
//  Created by Xiphi Tech on 16/03/2015.
//
//

#import <Foundation/Foundation.h>
#import "ApplicationCall_Response.h"

@interface Application_DataHelper : NSObject

-(NSArray*)loadApplicationsList;
-(NSArray*)loadApplicationsProductsWithId:(NSString*)Id;


@end
