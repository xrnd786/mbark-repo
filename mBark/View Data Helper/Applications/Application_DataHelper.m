//
//  Application_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 16/03/2015.
//
//

#import "Application_DataHelper.h"

@implementation Application_DataHelper


-(NSArray*)loadApplicationsList{
    
   return [ApplicationCall_Response callApiForApplicationList];
    
}


-(NSArray*)loadApplicationsProductsWithId:(NSString*)Id{
    
    return [ApplicationCall_Response callApiForProductListWithId:Id];
    
}


@end
