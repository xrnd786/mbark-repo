//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "Category_DataHelper.h"
#import "ProductCategory.h"

@interface Category_DataHelper()


@end


@implementation Category_DataHelper


-(NSArray*)getAllCategories{
    
    return [[GlobalCall getDelegate]fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:nil];
    
}

@end
