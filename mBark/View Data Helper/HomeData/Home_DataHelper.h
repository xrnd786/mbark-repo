//
//  Home_DataHelper.h
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import <Foundation/Foundation.h>

@interface Home_DataHelper : NSObject


-(NSString*)getTenantLogo;
-(NSArray*)getAllSliderImages;
-(NSArray*)getAllSliderNavigateTo;
-(NSArray*)getAllSliderNavigationTypes;

-(NSArray*)getAllCategories;
-(NSArray*)getAllFeaturedProducts;

@end
