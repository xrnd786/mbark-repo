//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "Home_DataHelper.h"

#import "AppTenantDefaults.h"
#import "AppSlidersDefaults.h"
#import "AppProductDefaults.h"
#import "ProductCall_Response.h"

#import "ProductCategory.h"
#import "Products.h"

@interface Home_DataHelper()

@property (strong,nonatomic) AppTenantDefaults *tenant;
@property (strong,nonatomic) NSArray *products;

@end


@implementation Home_DataHelper


@synthesize tenant,products;

-(NSString*)getTenantLogo{
    
    return [AppTenantDefaults getLogoURL];
    
}

-(NSArray*)getAllSliderImages{
    
    return [AppSlidersDefaults getSlidersImage];

}

-(NSArray*)getAllSliderNavigateTo{

    return [AppSlidersDefaults getSlidersImagesNavigateTo];
 
}

-(NSArray*)getAllSliderNavigationTypes{
    
    return [AppSlidersDefaults getSlidersImagesTypes];

}


-(NSArray*)getAllCategories{
    
    
    NSArray *objArray=[[NSArray alloc]initWithArray:[[GlobalCall getDelegate] fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:nil]];
    return objArray;
    
  
    
}



-(NSArray*)getAllFeaturedProducts{
    
    NSArray *objArray=[[NSArray alloc]initWithArray:[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"isFeatured==1"]]];
    
    NSLog(@"Array of price List %@",[objArray valueForKeyPath:@"rate"]);
    NSLog(@"Array of Name List %@",[objArray valueForKeyPath:@"name"]);
    return objArray;
    
}



@end
