//
//  AboutData.m
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import "AboutData.h"

#import "AppTenantDefaults.h"
#import "TenantCall_Response.h"

@interface AboutData()

@property (strong,nonatomic) AppTenantDefaults *tenant;

@end

@implementation AboutData

@synthesize tenant;

-(NSString*)getAboutBanner{
    
    return [TenantCall_Response callApiForTenantBanner];
    
}

-(NSString*)getAbout{
    
     return [AppTenantDefaults getAbout];

}


-(NSString*)getFacebook{
    
    return [AppTenantDefaults getFacebook];
    
}


-(NSString*)getTwitter{
    
   return  [AppTenantDefaults getTwitter];
    
    
}

-(NSString*)getGooglePlus{
    
   return  [AppTenantDefaults getGooglePlus];
    
}

-(NSString*)getMySite{
    
  return   [AppTenantDefaults getAppStore];
    
}



@end
