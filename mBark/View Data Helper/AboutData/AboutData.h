//
//  AboutData.h
//  mBark
//
//  Created by Xiphi Tech on 20/11/2014.
//
//

#import <Foundation/Foundation.h>

@interface AboutData : NSObject

@property (strong,nonatomic) NSString *aboutTitle;
@property (strong,nonatomic) NSString *aboutDetails;

-(NSString*)getAboutBanner;
-(NSString*)getAbout;
-(NSString*)getFacebook;
-(NSString*)getTwitter;
-(NSString*)getGooglePlus;
-(NSString*)getMySite;


@end
