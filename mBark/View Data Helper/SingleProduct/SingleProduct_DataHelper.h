//
//  Home_DataHelper.h
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import <Foundation/Foundation.h>
#import "ProductCall_Response.h"
#import "CartMaster.h"
#import "Products.h"


@interface SingleProduct_DataHelper : NSObject


-(Products*)getProductDictionary:(NSString*)productID;
-(CartMaster*)getCartDictionary:(NSString*)cartID;

@end
