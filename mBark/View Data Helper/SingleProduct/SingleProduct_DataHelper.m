//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "SingleProduct_DataHelper.h"

@interface SingleProduct_DataHelper()

@end


@implementation SingleProduct_DataHelper



-(Products*)getProductDictionary:(NSString*)productID{
    
    return [ProductCall_Response callApiForProductWithID:productID];
    
}



-(CartMaster*)getCartDictionary:(NSString*)cartID{
    
    return [[[GlobalCall getDelegate]fetchStatementWithEntity:[CartMaster class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"c_id == %@",cartID]]] lastObject];
    
}




@end
