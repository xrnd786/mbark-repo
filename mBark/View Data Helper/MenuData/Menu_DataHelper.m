//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "Menu_DataHelper.h"

#import "AppTenantDefaults.h"

@interface Menu_DataHelper()

@property (strong,nonatomic) AppTenantDefaults *tenant;

@end


@implementation Menu_DataHelper


@synthesize tenant;

-(NSString*)getTenantLogo{
    
    return [AppTenantDefaults getLogoURL];
    
}
-(NSString*)getOrganization{
    
    return [AppTenantDefaults getOrganization];
    
}



@end
