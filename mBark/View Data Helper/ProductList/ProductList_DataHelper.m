//
//  Home_DataHelper.m
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import "ProductList_DataHelper.h"

#import "AppProductDefaults.h"
#import "AppCategoryDefaults.h"

#import "CategoryCall_Response.h"
#import "Products.h"


@implementation ProductList_DataHelper


-(NSArray*)getProductArray:(NSString*)categoryName{
    
    return [[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:[NSPredicate predicateWithFormat:@"productCategory == %@",categoryName]];
    
}




-(NSArray*)getCategoryAttributeFor:(NSString*)categoryName{
    
    return [CategoryCall_Response callApiForCategoryFilter:categoryName];
    
}



@end
