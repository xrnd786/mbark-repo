//
//  Home_DataHelper.h
//  mBark
//
//  Created by Xiphi Tech on 17/11/2014.
//
//

#import <Foundation/Foundation.h>

@interface ProductList_DataHelper : NSObject


-(NSArray*)getProductArray:(NSString*)categoryName;
-(NSArray*)getCategoryAttributeFor:(NSString*)categoryName;


@end
