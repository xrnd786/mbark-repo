//
//  Product.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>


@interface MyUserLocation : NSEntityDescription


@property (strong,nonatomic) NSString *Latitude;
@property (strong,nonatomic) NSString *Longitude;


@end
