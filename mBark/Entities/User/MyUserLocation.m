//
//  Product.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "MyUserLocation.h"

@implementation MyUserLocation


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.Latitude forKey:@"Latitude"];
    [encoder encodeObject:self.Longitude forKey:@"Longitude"];
    
}

- (MyUserLocation*)initWithCoder:(NSCoder *)decoder {
    
    self = [super initWithCoder:decoder];
    
    if (self) {
        
        self.Latitude =[decoder decodeObjectForKey:@"Latitude"];
        self.Longitude =[decoder decodeObjectForKey:@"Longitude"];
        
    }
    
    return self;
}


@end
