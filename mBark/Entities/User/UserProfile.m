//
//  Product.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "UserProfile.h"

@implementation UserProfile


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.UserID forKey:@"UserID"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.Firstname forKey:@"Firstname"];
    [encoder encodeObject:self.Lastname forKey:@"Lastname"];
    [encoder encodeObject:self.Organization forKey:@"Organization"];
    [encoder encodeObject:self.Contact forKey:@"Contact"];
    [encoder encodeObject:self.AddressLine1 forKey:@"AddressLine1"];
    [encoder encodeObject:self.AddressLine2 forKey:@"AddressLine2"];
    [encoder encodeObject:self.Country forKey:@"Country"];
    [encoder encodeObject:self.State forKey:@"State"];
    
    [encoder encodeObject:self.Location forKey:@"Location"];
    [encoder encodeObject:self.Facebook forKey:@"Facebook"];
    [encoder encodeObject:self.Twitter forKey:@"Twitter"];
    [encoder encodeObject:self.Email forKey:@"Email"];
    [encoder encodeObject:self.Gender forKey:@"Gender"];
    [encoder encodeObject:self.City forKey:@"City"];
    [encoder encodeObject:self.Pincode forKey:@"Pincode"];
    [encoder encodeObject:self.Password forKey:@"Password"];
    
   
}

- (UserProfile*)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
       
        
        self.UserID =[decoder decodeObjectForKey:@"UserID"];
        self.username =[decoder decodeObjectForKey:@"username"];
        self.Firstname =[decoder decodeObjectForKey:@"Firstname"];
        self.Lastname =[decoder decodeObjectForKey:@"Lastname"];
        self.Organization  =[decoder decodeObjectForKey:@"Organization"];
        self.Contact  =[decoder decodeObjectForKey:@"Contact"];
        self.AddressLine1  =[decoder decodeObjectForKey:@"AddressLine1"];
        self.AddressLine2  =[decoder decodeObjectForKey:@"AddressLine2"];
        self.Country  =[decoder decodeObjectForKey:@"Country"];
        self.State  =[decoder decodeObjectForKey:@"State"];
        
        self.Location.Longitude  =[decoder decodeObjectForKey:@"Location"];
        self.Location.Latitude  =[decoder decodeObjectForKey:@"Latitude"];
        
        self.Facebook  =[decoder decodeObjectForKey:@"Facebook"];
        self.Twitter  =[decoder decodeObjectForKey:@"Twitter"];
        self.Email  =[decoder decodeObjectForKey:@"Email"];
        self.Gender  =[decoder decodeObjectForKey:@"Gender"];
        self.City  =[decoder decodeObjectForKey:@"City"];
        self.Pincode  =[decoder decodeObjectForKey:@"Pincode"];
        self.Password  =[decoder decodeObjectForKey:@"Password"];
        
    }
    
    return self;
}


@end
