//
//  Product.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>
#import "MyUserLocation.h"

@interface UserProfile : NSEntityDescription


@property (strong,nonatomic) NSString *UserID;
@property (strong,nonatomic) NSString *username;
@property (strong,nonatomic) NSString *Firstname;
@property (strong,nonatomic) NSString *Lastname;
@property (strong,nonatomic) NSString *Organization;
@property (strong,nonatomic) NSString *Contact;
@property (strong,nonatomic) NSString *AddressLine1;
@property (strong,nonatomic) NSString *AddressLine2;
@property (strong,nonatomic) NSString *Country;
@property (strong,nonatomic) NSString *State;

@property (strong,nonatomic) NSString *Facebook;
@property (strong,nonatomic) NSString *Twitter;
@property (strong,nonatomic) NSString *Email;
@property (strong,nonatomic) NSString *Gender;
@property (strong,nonatomic) NSString *City;
@property (strong,nonatomic) NSNumber *Pincode;
@property (strong,nonatomic) NSString *Password;


@property (strong,nonatomic) MyUserLocation *Location;



@end
