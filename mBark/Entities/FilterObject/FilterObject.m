//
//  FilterObject.m
//  mBark
//
//  Created by Xiphi Tech on 02/05/2015.
//
//

#import "FilterObject.h"

@implementation FilterObject



- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.Name forKey:@"Name"];
    [encoder encodeObject:self.AttributeId forKey:@"AttributeId"];
    [encoder encodeObject:self.AttributeName forKey:@"AttributeName"];
    [encoder encodeObject:self.AttributeType forKey:@"AttributeType"];
    [encoder encodeObject:self.Type forKey:@"Type"];
    [encoder encodeObject:self.Values forKey:@"Values"];
    
}

- (FilterObject*)initWithCoder:(NSCoder *)decoder {
    
    self = [super initWithCoder:decoder];
   
    if (self) {
        
        self.Name  =[decoder decodeObjectForKey:@"Name"];
        self.AttributeId  =[decoder decodeObjectForKey:@"AttributeId"];
        self.AttributeName  =[decoder decodeObjectForKey:@"AttributeName"];
        self.AttributeType  =[decoder decodeObjectForKey:@"AttributeType"];
        self.Type  =[decoder decodeObjectForKey:@"Type"];
        self.Values  =[decoder decodeObjectForKey:@"Values"];
        
    }
    
    return self;
}



@end
