//
//  FilterObject.h
//  mBark
//
//  Created by Xiphi Tech on 02/05/2015.
//
//

#import <Foundation/Foundation.h>

@interface FilterObject : NSEntityDescription


@property (strong,nonatomic) NSString *Name;
@property (strong,nonatomic) NSString *AttributeId;
@property (strong,nonatomic) NSString *AttributeName;
@property (strong,nonatomic) NSString *AttributeType;
@property (strong,nonatomic) NSString *Type;
@property (strong,nonatomic) NSArray *Values;


           
@end

