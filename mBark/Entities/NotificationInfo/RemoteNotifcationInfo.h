//
//  RemoteNotifcationInfo.h
//  mBark
//
//  Created by Xiphi Tech on 02/05/2015.
//
//

#import <Foundation/Foundation.h>

@interface RemoteNotifcationInfo : NSEntityDescription



@property (strong,nonatomic) NSDictionary *alert;
@property (strong,nonatomic) NSString *badge;
@property (strong,nonatomic) NSString *category;
@property (strong,nonatomic) NSString *sound;


@end
