//
//  Product.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "Thumbnail.h"

@implementation Thumbnail


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.Image forKey:@"Image"];
   
}

- (Thumbnail*)initWithCoder:(NSCoder *)decoder {
   
    self = [super initWithCoder:decoder];
    if (self) {
       
        self.Image =[decoder decodeObjectForKey:@"Image"];
       

    }
    return self;
}


@end
