//
//  Product.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "Product.h"

@implementation Product


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.Id forKey:@"Id"];
    [encoder encodeObject:self.SKU forKey:@"SKU"];
    [encoder encodeObject:self.Name forKey:@"Name"];
    [encoder encodeObject:self.Type forKey:@"Type"];
    [encoder encodeObject:self.Description forKey:@"Description"];
    [encoder encodeObject:self.Category forKey:@"Category"];
    [encoder encodeObject:self.AvailableUnits forKey:@"AvailableUnits"];
    [encoder encodeObject:self.MeasurementUnit forKey:@"MeasurementUnit"];
    [encoder encodeObject:self.Rate forKey:@"Rate"];
    [encoder encodeObject:self.Discount forKey:@"Discount"];
    [encoder encodeObject:self.Discontinued forKey:@"Discontinued"];
    [encoder encodeObject:self.OutOfStock forKey:@"OutOfStock"];
    [encoder encodeObject:self.ShippedInDays forKey:@"ShippedInDays"];
    [encoder encodeObject:self.Featured forKey:@"Featured"];
    [encoder encodeObject:self.Tags forKey:@"Tags"];
    [encoder encodeObject:self.Applications forKey:@"Applications"];
    [encoder encodeObject:self.Images forKey:@"Images"];
    [encoder encodeObject:self.ImageUrls forKey:@"ImageUrls"];
    [encoder encodeObject:self.Thumbnail forKey:@"Thumbnail"];
    [encoder encodeObject:self.GroupedProducts forKey:@"GroupedProducts"];
    [encoder encodeObject:self.RelatedProducts forKey:@"RelatedProducts"];
    [encoder encodeObject:self.RelatedThumbnails forKey:@"RelatedThumbnails"];
    [encoder encodeObject:self.Attributes forKey:@"Attributes"];
    
}

- (Product*)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.Id =[decoder decodeObjectForKey:@"Id"];
        self.SKU =[decoder decodeObjectForKey:@"SKU"];
        self.Name =[decoder decodeObjectForKey:@"Name"];
        self.Type =[decoder decodeObjectForKey:@"Type"];
        self.Description =[decoder decodeObjectForKey:@"Description"];
        self.Category =[decoder decodeObjectForKey:@"Category"];
        self.AvailableUnits =[decoder decodeObjectForKey:@"AvailableUnits"];
        self.MeasurementUnit =[decoder decodeObjectForKey:@"MeasurementUnit"];
        self.Rate =[decoder decodeObjectForKey:@"Rate"];
        self.Discount =[decoder decodeObjectForKey:@"Discount"];
        self.Discontinued =[decoder decodeObjectForKey:@"Discontinued"];
        self.OutOfStock =[decoder decodeObjectForKey:@"OutOfStock"];
        self.ShippedInDays =[decoder decodeObjectForKey:@"ShippedInDays"];
        self.Featured =[decoder decodeObjectForKey:@"Featured"];
        self.Tags =[decoder decodeObjectForKey:@"Tags"];
        self.Applications =[decoder decodeObjectForKey:@"Applications"];
        self.Images =[decoder decodeObjectForKey:@"Images"];
        self.ImageUrls =[decoder decodeObjectForKey:@"ImageUrls"];
        self.Thumbnail =[decoder decodeObjectForKey:@"Thumbnail"];
        self.GroupedProducts =[decoder decodeObjectForKey:@"GroupedProducts"];
        self.RelatedProducts =[decoder decodeObjectForKey:@"RelatedProducts"];
        self.RelatedThumbnails =[decoder decodeObjectForKey:@"RelatedThumbnails"];
        self.Attributes =[decoder decodeObjectForKey:@"Attributes"];

    }
    return self;
}


@end
