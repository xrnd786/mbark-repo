//
//  Product.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>
#import "Attribute.h"
#import "Thumbnail.h"

@interface Product : NSEntityDescription


@property (strong,nonatomic) NSString *Id;
@property (strong,nonatomic) NSString *SKU;
@property (strong,nonatomic) NSString *Name;
@property (strong,nonatomic) NSString *Type;
@property (strong,nonatomic) NSString *Description;
@property (strong,nonatomic) NSString *Category;
@property (strong,nonatomic) NSString *AvailableUnits;
@property (strong,nonatomic) NSString *MeasurementUnit;
@property (strong,nonatomic) NSString *Rate;
@property (strong,nonatomic) NSString *Discount;
@property (strong,nonatomic) NSString *Discontinued;
@property (strong,nonatomic) NSString *OutOfStock;
@property (strong,nonatomic) NSString *ShippedInDays;
@property (strong,nonatomic) NSString *Featured;
@property (strong,nonatomic) NSString *Tags;

@property (strong,nonatomic) NSArray *Applications;

@property (strong,nonatomic) NSString *Images;

@property (strong,nonatomic) NSArray *ImageUrls;

@property (strong,nonatomic) Thumbnail *Thumbnail;

@property (strong,nonatomic) NSArray *GroupedProducts;

@property (strong,nonatomic) NSArray *RelatedProducts;

@property (strong,nonatomic) NSArray *RelatedThumbnails;

@property (strong,nonatomic) NSArray *Attributes;




@end
