//
//  Attribute.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "MessageThread.h"

@implementation MessageThread



- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.threadID forKey:@"threadID"];
    [encoder encodeObject:self.subject forKey:@"subject"];
    [encoder encodeObject:self.messages forKey:@"messages"];
    [encoder encodeObject:self.lastMessage forKey:@"lastMessage"];
    [encoder encodeObject:self.linkedTo forKey:@"linkedTo"];
    [encoder encodeObject:self.linkReference forKey:@"linkReference"];
    
    
}

- (MessageThread*)initWithCoder:(NSCoder *)decoder {
    
    self = [super initWithCoder:decoder];
    if (self) {
        
        self.threadID  =[decoder decodeObjectForKey:@"threadID"];
        self.subject  =[decoder decodeObjectForKey:@"subject"];
        self.messages  =[decoder decodeObjectForKey:@"messages"];
        self.lastMessage  =[decoder decodeObjectForKey:@"lastMessage"];
        self.linkedTo  =[decoder decodeObjectForKey:@"linkedTo"];
        self.linkReference  =[decoder decodeObjectForKey:@"linkReference"];
        
    }
    
    return self;
}



@end
