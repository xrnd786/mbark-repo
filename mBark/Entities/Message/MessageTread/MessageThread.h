//
//  Attribute.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>
#import "MessageEntity.h"


@interface MessageThread : NSEntityDescription


@property (strong,nonatomic) NSString *threadID;
@property (strong,nonatomic) NSString *subject;
@property (strong,nonatomic) NSArray *messages;
@property (strong,nonatomic) MessageEntity *lastMessage;
@property (strong,nonatomic) NSString *linkedTo;
@property (strong,nonatomic) NSString *linkReference;


@end
