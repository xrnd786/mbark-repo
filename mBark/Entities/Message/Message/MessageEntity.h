//
//  Attribute.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>


@interface MessageEntity : NSEntityDescription

@property (strong,nonatomic) NSString *status;
@property (strong,nonatomic) NSString *body;
@property (strong,nonatomic) NSDate *date;
@property (strong,nonatomic) NSString *direction;


@end
