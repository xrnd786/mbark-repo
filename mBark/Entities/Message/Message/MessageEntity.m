//
//  Attribute.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "MessageEntity.h"

@implementation MessageEntity




- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.status forKey:@"status"];
    [encoder encodeObject:self.body forKey:@"body"];
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.direction forKey:@"direction"];
    
    
}

- (MessageEntity*)initWithCoder:(NSCoder *)decoder {
    
    self = [super initWithCoder:decoder];
    if (self) {
        
        
        self.status =[decoder decodeObjectForKey:@"status"];
        self.body =[decoder decodeObjectForKey:@"body"];
        self.date =[decoder decodeObjectForKey:@"date"];
        self.direction =[decoder decodeObjectForKey:@"direction"];
        
    }
    
    return self;
}



@end
