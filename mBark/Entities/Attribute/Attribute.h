//
//  Attribute.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>

@interface Attribute : NSEntityDescription


@property (strong,nonatomic) NSString *Id;
@property (strong,nonatomic) NSString *Type;
@property (strong,nonatomic) NSString *Name;
@property (strong,nonatomic) NSString *Group;
@property (strong,nonatomic) NSString *AttributeValue;



@end
