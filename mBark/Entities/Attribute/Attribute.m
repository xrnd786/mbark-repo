//
//  Attribute.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "Attribute.h"

@implementation Attribute


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.Id forKey:@"Id"];
    [encoder encodeObject:self.Type forKey:@"Type"];
    [encoder encodeObject:self.Name forKey:@"Name"];
    [encoder encodeObject:self.Group forKey:@"Group"];
    [encoder encodeObject:self.AttributeValue forKey:@"AttributeValue"];
}

- (Attribute*)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self)
    {
        self.Id =[decoder decodeObjectForKey:@"Id"];
        self.Group =[decoder decodeObjectForKey:@"Group"];
        self.Name =[decoder decodeObjectForKey:@"Name"];
        self.Type =[decoder decodeObjectForKey:@"Type"];
        self.AttributeValue =[decoder decodeObjectForKey:@"AttributeValue"];
    }
    
    return self;
    
}


@end
