//
//  Product.m
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import "Store.h"

@implementation Store


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.StoreID forKey:@"StoreID"];
    [encoder encodeObject:self.Name forKey:@"Name"];
    
    [encoder encodeObject:self.Contact forKey:@"Contact"];
    [encoder encodeObject:self.Email forKey:@"Email"];
    
    [encoder encodeObject:self.Address forKey:@"Address"];
    [encoder encodeObject:self.City forKey:@"City"];
    [encoder encodeObject:self.Pincode forKey:@"Pincode"];
    
    [encoder encodeObject:self.State forKey:@"State"];
    [encoder encodeObject:self.Country forKey:@"Country"];
    [encoder encodeObject:self.Location forKey:@"Location"];
    
   
} 

- (Store*)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
       
        
        self.StoreID   =[decoder decodeObjectForKey:@"StoreID"];
        self.Name   =[decoder decodeObjectForKey:@"Name"];
        
        self.Contact   =[decoder decodeObjectForKey:@"Contact"];
        self.Email   =[decoder decodeObjectForKey:@"Email"];
        
        self.Address   =[decoder decodeObjectForKey:@"Address"];
        self.City   =[decoder decodeObjectForKey:@"City"];
        self.Pincode   =[decoder decodeObjectForKey:@"Pincode"];
        
        self.State   =[decoder decodeObjectForKey:@"State"];
        self.Country  =[decoder decodeObjectForKey:@"Country"];
        self.Location =[decoder decodeObjectForKey:@"Location"];
        
    }
    return self;
}

@end
