//
//  Product.h
//  mBark
//
//  Created by Xiphi Tech on 18/03/2015.
//
//

#import <CoreData/CoreData.h>

#import "Store.h"
#import "MyUserLocation.h"

@interface Store : NSEntityDescription


@property (strong,nonatomic) NSString *StoreID;
@property (strong,nonatomic) NSString *Name;

@property (strong,nonatomic) NSString *Contact;
@property (strong,nonatomic) NSString *Email;

@property (strong,nonatomic) NSString *Address;
@property (strong,nonatomic) NSString *City;
@property (strong,nonatomic) NSString *Pincode;
@property (strong,nonatomic) NSString *State;
@property (strong,nonatomic) NSString *Country;

@property (strong,nonatomic) MyUserLocation *Location;

@end
