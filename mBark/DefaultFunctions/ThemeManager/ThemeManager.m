//
//  RNThemeManager.m
//  DT Kit
//
//  Created by Ryan Nystrom on 12/2/12.
//  Copyright (c) 2012 Ryan Nystrom. All rights reserved.
//

#import "ThemeManager.h"
#import "UIColor+HexString.h"

NSString * const ThemeManagerDidChangeThemes = @"ThemeManagerDidChangeThemes";

@interface ThemeManager ()

@property (nonatomic, strong, readwrite) NSDictionary *styles;
@property (nonatomic, strong, readwrite) NSString *currentThemeName;

@end

@implementation ThemeManager

#pragma mark - Singleton

+ (ThemeManager *)sharedManager {
    static ThemeManager *_sharedTheme = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedTheme = [[self alloc] init];
    });
    
    return _sharedTheme;
}

- (id)init {
    if (self = [super init]) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *themeName = [defaults objectForKey:@"com.whoisryannystrom.ThemeManager.defaulttheme"];
       
        if (! themeName) {
           
            themeName = @"Default";
        
        }
        
        [self changeTheme:themeName];
    }
    return self;
}


#pragma mark - Setters

- (void)setStyles:(NSDictionary *)styles {
   
    BOOL isFirst = _styles == nil;
    _styles = styles;
  
    if (! isFirst) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ThemeManagerDidChangeThemes object:nil];
    
    }

}

- (void)setCurrentThemeName:(NSString *)currentThemeName {
    _currentThemeName = currentThemeName;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:currentThemeName forKey:@"com.whoisryannystrom.ThemeManager.defaulttheme"];
    [defaults synchronize];

}

#pragma mark - Actions

- (void)changeTheme:(NSString *)themeName {
    
    
    if ([themeName isEqualToString:self.currentThemeName]) {
        return;
    }
    
    self.currentThemeName = themeName;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:[[NSString stringWithFormat:@"%@",self.currentThemeName]stringByAppendingString:@".plist"]];
    
    //NSString *path = [[NSBundle mainBundle] pathForResource:self.currentThemeName ofType:@"plist"];
    
    NSDictionary *styles = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    // if our theme inherits from another, merge
    if (styles[@"INHERITED_THEME"] != nil) {
        styles = [self inheritedThemeWithParentTheme:styles[@"INHERITED_THEME"] childTheme:styles];
    }
    self.styles = styles;
    
}


- (NSDictionary *)inheritedThemeWithParentTheme:(NSString *)parentThemeName childTheme:(NSDictionary *)childTheme {
   
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:[[NSString stringWithFormat:@"%@",self.currentThemeName]stringByAppendingString:@".plist"]];
   
    //NSString *path = [[NSBundle mainBundle] pathForResource:parentThemeName ofType:@"plist"];
    NSMutableDictionary *parent = [[NSDictionary dictionaryWithContentsOfFile:plistPath] mutableCopy];
    
    //merge child into parent overriding parent values
    [childTheme enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
        parent[key] = obj;
    }];
    
    return parent;
}

#pragma mark - Constants

#pragma mark - Fonts

- (UIFont *)fontForKey:(NSString*)key {
   
    
    NSString *sizeKey = [key stringByAppendingString:@"Size"];
    
    NSString *fontName = self.styles[key];
    NSString *size = self.styles[sizeKey];
    
    //NSLog(@"Font Name and Size %@ %@",fontName,size);
    
    
//    while (self.styles[fontName]) {
//        fontName = self.styles[fontName];
//    }
    
//    while (self.styles[size]) {
    
        if ([GlobalCall isIpad]) {
            
            size =[NSString stringWithFormat:@"%f",[size floatValue]+3.0f];
            
        }
    
//    }

    if (fontName && size) {
        return [UIFont fontWithName:fontName size:size.floatValue];
    }
    
    return nil;
}

#pragma mark - Colors

- (UIColor *)colorForKey:(NSString *)key {
    NSString *hexString = self.styles[key];
    
   // NSLog(@"hex Value is %@",hexString);
    
    /*
    while (self.styles[hexString]) {
        hexString = self.styles[hexString];
    }
    */
    
    if (hexString) {
       
        return [UIColor colorWithHexString:hexString];
    
    }
    
    return nil;
}

#pragma mark - Images

- (UIImage *)imageForKey:(NSString *)key {
    NSString *imageName = self.styles[key];
    
    if (imageName) {
        
        return [UIImage imageNamed:imageName];
    
    }
    
    //while (self.styles[imageName]) {
      //  imageName = self.styles[imageName];
    //}
    
    return nil;
}

@end
