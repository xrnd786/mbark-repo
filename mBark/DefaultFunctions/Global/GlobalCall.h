//
//  GlobalCall.h
//  P1
//
//  Created by Xiphi Tech on 01/10/2014.
//
//

#import <UIKit/UIKit.h>
#import "XP1AppDelegate.h"
#import "ProductViewController.h"
#import "GroupedProductViewController.h"

#define AppBarColor  [UIColor colorWithRed:(245.0/255.0) green:(245.0/255) blue:(245.0/255) alpha:1.0]

#define SeperatorColor [UIColor lightGrayColor]

#define APP_NO_INTERNET @"Sorry No Internet Connection.Please try with 3G/Wifi again"
#define APP_TIME_OUT @"It is taking longer than usual,Please try again after sometime."
#define APP_UNAUTHORIZED_USER @"Unauthorized User !"


typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


@interface GlobalCall : UIView

+(BOOL)isIpad;
+(CGRect)findBounds;

+(void)hideBottomBar;
+(void)showBottomBar;

+(void)addObserverWithNotifier:(NSString*)notifierString andSelector:(SEL)handleResponse handlerClass:(id)className withObject:(id)object;
+(void)removeOberverWithNotifier:(NSString*)notifierString handlerClass:(id)className withObject:(id)object;

+(CGFloat)getMyWidth;
+(CGFloat)getMyHeight;
+(CGFloat)getRowHeight;



+(BOOL)isConnected;

+(NSManagedObjectContext*)getContext;
+(XP1AppDelegate*)getDelegate;

+(void)saveContext;

+(ProductViewController*)productViewController;
+(GroupedProductViewController*)groupedProductViewController;


@end
