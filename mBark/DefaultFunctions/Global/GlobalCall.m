//
//  GlobalCall.m
//  P1
//
//  Created by Xiphi Tech on 01/10/2014.
//
//

#import "GlobalCall.h"
#import "XP1AppDelegate.h"

@interface GlobalCall ()
{
}

@end


@implementation GlobalCall

static ProductViewController *productSharedView;
static GroupedProductViewController *groupedProductSharedView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
   
    if (self) {
        // Initialization code
    }
    
    return self;

}

+(BOOL)isIpad{
    
    BOOL answer;
    
    if (UIUserInterfaceIdiomPad==UI_USER_INTERFACE_IDIOM()) {
        answer=TRUE;
    } else {
        answer=FALSE;
    }
    
    return answer;
    
}

+(CGRect)findBounds{
    
    return [[UIScreen mainScreen] bounds];
    
}

+(CGFloat)getMyWidth{
    
    return [[UIScreen mainScreen] bounds].size.width;
    
}


+(CGFloat)getMyHeight{
    
    return [[UIScreen mainScreen] bounds].size.height;
    
}


+(CGFloat)getRowHeight{
    
    if ([self isIpad]) {
        
        return 80.0f;
        
    } else {
        
        return 40.0f;
        
    }
    
}


+(void)hideBottomBar{

    /*
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldHide" object:self];
    */
    
}


+(void)showBottomBar{
 
    /*
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomBarShouldUnhide" object:self];
    */
    
}

#pragma mark - notification

+(void)addObserverWithNotifier:(NSString*)notifierString andSelector:(SEL)handleResponse handlerClass:(id)className withObject:(id)object{
    
     [[NSNotificationCenter defaultCenter] addObserver:className selector:handleResponse name:notifierString  object:object];
    
}


+(void)removeOberverWithNotifier:(NSString*)notifierString handlerClass:(id)className withObject:(id)object{
    
    [[NSNotificationCenter defaultCenter] removeObserver:className name:notifierString object:object];
    
}


+(BOOL)isConnected{
    
   return  [[XP1AppDelegate new] connected];
    
}

+(XP1AppDelegate*)getDelegate{
    
    return  ((XP1AppDelegate*)[[UIApplication sharedApplication]delegate]);
    
}


+(NSManagedObjectContext*)getContext{
    
    return  ((XP1AppDelegate*)[[UIApplication sharedApplication]delegate]).managedObjectContext;
    
}



+(void)saveContext{
    
    return  [((XP1AppDelegate*)[[UIApplication sharedApplication]delegate]) saveContext];
    
}


+(ProductViewController*)productViewController{
    
    if (productSharedView) {
    
        return productSharedView;
        
    }else{
        
        productSharedView=[[ProductViewController alloc]init];
        return productSharedView;
    }
    
    
}



+(GroupedProductViewController*)groupedProductViewController{
    
    if (groupedProductSharedView) {
        
        return groupedProductSharedView;
        
    }else{
        
        groupedProductSharedView=[[GroupedProductViewController alloc]init];
        return groupedProductSharedView;
    }
    
    
}

@end
