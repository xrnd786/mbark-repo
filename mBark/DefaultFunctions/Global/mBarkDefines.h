//
//  mBarkDefines.h
//  mBark
//
//  Created by Xiphi Tech on 27/11/2014.
//
//

#ifndef mBark_mBarkDefines_h
#define mBark_mBarkDefines_h

#define METERS_PER_MILE 1609.344

//Images stored locally with names

#define Local_User_Name @"Me_userprofile"
#define Local_Logo_Name @"company_logo"
#define Local_Banner_Name @"company_banner"


//notifictions names
#define APINotification_UserIdentification @"APINotification_UserIdentifier"

//Tenant
#define APINotification_Tenant @"APINotification_Tenant"
#define APINotification_Tenant_Logo @"APINotification_Tenant_Logo"
#define APINotification_Tenant_Banner @"APINotification_Tenant_Banner"
#define APINotification_Tenant_Slider @"APINotification_Tenant_Slider"
#define APINotification_Tenant_Saved @"APINotification_Tenant_Saved"

//Attributes
#define APINotification_Attributes_Get @"APINotification_Attributes_Get"
#define APINotification_Attributes_Saved @"APINotification_Attributes_Saved"


//Category
#define APINotification_Categories_Get @"APINotification_Categories_Get"
#define APINotification_Categories_Saved @"APINotification_Categories_Saved"
#define APINotification_Categories_Filters @"APINotification_Categories_Filters"
#define APINotification_Categories_Filters_Received @"APINotification_Categories_Filters_Received"


//Products
#define APINotification_Products_Get @"APINotification_Products_Get"
#define APINotification_Product_GetSingle @"APINotification_Products_GetSingle"
#define APINotification_Product_Saved @"APINotification_Products_Saved"
#define APINotification_Product_Filters @"APINotification_Products_Filters"
#define APINotification_Product_FeaturedGet @"APINotification_Products_FeaturedGet"


//Users
#define APINotification_User_Registration @"APINotification_User_Registration"
#define APINotification_User_Registration_Done @"APINotification_User_Registration_Done"

#define APINotification_User_SignIn @"APINotification_User_SignIn"
#define APINotification_User_Details @"APINotification_User_Details"

//User Login
#define APINotification_UserLogin_Created @"APINotification_UserLogIn_Created"
#define APINotification_UserLogin_Login @"APINotification_UserLogIn_Created"
#define APINotification_UserLogin_Update @"APINotification_UserLogIn_Update"
#define APINotification_UserLogin_ChangeLogin @"APINotification_UserLogIn_ChangeLogin"


//Message
#define APINotification_Messages_CreateThread @"APINotification_Messages_CreateThread"
#define APINotification_Messages_Get @"APINotification_Messages_Get"
#define APINotification_Messages_Post @"APINotification_Messages_Post"
#define APINotification_Messages_Reload @"APINotification_Messages_Reload"


//Error
#define APINotification_Server_Error @"APINotification_Server_Error"
#define APINotification_FirstTime_Server_Error @"APINotification_FirstTime_Server_Error"


//Cart
#define APINotification_Cart_Send @"APINotification_Cart_Send"
#define APINotification_Cart_Sent_Saved @"APINotification_Cart_Sent_Saved"

//User Registration Call
#define User_Registration_Call_Type_Query @"Query"
#define User_Registration_Call_Type_Start @"Start"
#define User_Registration_Call_Type_Never @"Nevers"


//User Store Call
#define APINotification_Store_Get @"APINotification_Store_Get"


//Server call complete
#define  APINotification_Home_Done @"APINotification_Home_Done"


//COLOR DEFINES
#define MBARK_GREEN [UIColor colorWithRed:(39.0/255.0) green:(185.0/255) blue:(154.0/255) alpha:1.0]
#define MBARK_YELLOW [UIColor colorWithRed:(240.0/255.0) green:(196.0/255) blue:(25.0/255) alpha:1.0]


//NOtification strings
#define MBARK_NOCONNECTION_PROMPT_TEXT @"Please check your internet connection"



//REQUEST TYPES
#define POST @"POST"
#define PUT @"PUT"
#define GET @"GET"
#define DELETE @"DELETE"


#endif
