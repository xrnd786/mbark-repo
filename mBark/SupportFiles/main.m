 //
//  main.m
//  P1
//
//  Created by Xiphi Tech on 29/09/2014.
//
//

#import <UIKit/UIKit.h>

#import "XP1AppDelegate.h"

int main(int argc, char * argv[])
{

    @autoreleasepool {
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XP1AppDelegate class]));
    
    }
   
    
}