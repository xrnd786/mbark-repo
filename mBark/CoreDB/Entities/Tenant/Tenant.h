//
//  Tenant.h
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Tenant : NSManagedObject

@property (nonatomic, retain) NSString * organization;
@property (nonatomic, retain) id banner;
@property (nonatomic, retain) id logo;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * helpline;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) id location;
@property (nonatomic, retain) NSString * facebook;
@property (nonatomic, retain) NSString * twitter;
@property (nonatomic, retain) NSString * googlePlus;
@property (nonatomic, retain) NSString * about;
@property (nonatomic, retain) NSString * appStoreLink;
@property (nonatomic, retain) NSString * playStoreLink;
@property (nonatomic, retain) NSString * cNAME;

@end
