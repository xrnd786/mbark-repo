//
//  Tenant.m
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import "Tenant.h"


@implementation Tenant

@dynamic organization;
@dynamic banner;
@dynamic logo;
@dynamic email;
@dynamic helpline;
@dynamic address;
@dynamic location;
@dynamic facebook;
@dynamic twitter;
@dynamic googlePlus;
@dynamic about;
@dynamic appStoreLink;
@dynamic playStoreLink;
@dynamic cNAME;

@end
