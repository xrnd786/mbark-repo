//
//  Applications.h
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Applications : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) id products;
@property (nonatomic, retain) NSString * a_id;

@end
