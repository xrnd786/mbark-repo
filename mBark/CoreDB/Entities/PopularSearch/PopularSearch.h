//
//  PopularSearch.h
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PopularSearch : NSManagedObject

@property (nonatomic, retain) id popularSearch;

@end
