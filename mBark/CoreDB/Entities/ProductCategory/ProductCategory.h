//
//  ProductCategory.h
//  
//
//  Created by Xiphi Tech on 23/06/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CategoryImage, Filter, ProductCategory, Products;

@interface ProductCategory : NSManagedObject

@property (nonatomic, retain) NSString * categoryDescription;
@property (nonatomic, retain) NSNumber * isParent;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * pcat_id;
@property (nonatomic, retain) NSNumber * sequence;
@property (nonatomic, retain) NSSet *children;
@property (nonatomic, retain) NSSet *filter;
@property (nonatomic, retain) CategoryImage *image;
@property (nonatomic, retain) Products *product;
@end

@interface ProductCategory (CoreDataGeneratedAccessors)

- (void)addChildrenObject:(ProductCategory *)value;
- (void)removeChildrenObject:(ProductCategory *)value;
- (void)addChildren:(NSSet *)values;
- (void)removeChildren:(NSSet *)values;

- (void)addFilterObject:(Filter *)value;
- (void)removeFilterObject:(Filter *)value;
- (void)addFilter:(NSSet *)values;
- (void)removeFilter:(NSSet *)values;

@end
