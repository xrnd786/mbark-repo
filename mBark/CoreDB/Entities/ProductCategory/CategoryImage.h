//
//  CategoryImage.h
//  
//
//  Created by Xiphi Tech on 14/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProductCategory;

@interface CategoryImage : NSManagedObject

@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) ProductCategory *productCategory;

@end
