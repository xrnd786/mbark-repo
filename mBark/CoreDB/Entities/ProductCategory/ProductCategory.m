//
//  ProductCategory.m
//  
//
//  Created by Xiphi Tech on 23/06/2015.
//
//

#import "ProductCategory.h"
#import "CategoryImage.h"
#import "Filter.h"
#import "ProductCategory.h"
#import "Products.h"


@implementation ProductCategory

@dynamic categoryDescription;
@dynamic isParent;
@dynamic name;
@dynamic pcat_id;
@dynamic sequence;
@dynamic children;
@dynamic filter;
@dynamic image;
@dynamic product;

@end
