//
//  CartMaster.m
//  
//
//  Created by Xiphi Tech on 28/05/2015.
//
//

#import "CartMaster.h"
#import "Cart.h"


@implementation CartMaster

@dynamic c_id;
@dynamic createdOn;
@dynamic notes;
@dynamic status;
@dynamic products;
@dynamic details;

@end
