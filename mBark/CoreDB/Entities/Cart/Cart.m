//
//  Cart.m
//  
//
//  Created by Xiphi Tech on 22/05/2015.
//
//

#import "Cart.h"
#import "CartMaster.h"


@implementation Cart

@dynamic productId;
@dynamic quantity;
@dynamic price;
@dynamic cart_id;

@end
