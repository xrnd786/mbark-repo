//
//  Cart.h
//  
//
//  Created by Xiphi Tech on 22/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CartMaster;

@interface Cart : NSManagedObject

@property (nonatomic, retain) NSString * productId;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) CartMaster *cart_id;

@end
