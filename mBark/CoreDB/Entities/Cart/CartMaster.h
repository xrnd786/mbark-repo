//
//  CartMaster.h
//  
//
//  Created by Xiphi Tech on 28/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cart;

@interface CartMaster : NSManagedObject

@property (nonatomic, retain) NSString * c_id;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) id products;
@property (nonatomic, retain) NSSet *details;
@end

@interface CartMaster (CoreDataGeneratedAccessors)

- (void)addDetailsObject:(Cart *)value;
- (void)removeDetailsObject:(Cart *)value;
- (void)addDetails:(NSSet *)values;
- (void)removeDetails:(NSSet *)values;

@end
