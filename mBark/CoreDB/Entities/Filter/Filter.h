//
//  Filter.h
//  
//
//  Created by Xiphi Tech on 23/06/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProductCategory;

@interface Filter : NSManagedObject

@property (nonatomic, retain) NSString * attributeId;
@property (nonatomic, retain) NSString * attributeName;
@property (nonatomic, retain) NSString * attributeType;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) id values;
@property (nonatomic, retain) ProductCategory *productCategory;

@end
