//
//  Filter.m
//  
//
//  Created by Xiphi Tech on 23/06/2015.
//
//

#import "Filter.h"
#import "ProductCategory.h"


@implementation Filter

@dynamic attributeId;
@dynamic attributeName;
@dynamic attributeType;
@dynamic name;
@dynamic type;
@dynamic values;
@dynamic productCategory;

@end
