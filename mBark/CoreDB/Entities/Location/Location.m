//
//  Location.m
//  
//
//  Created by Xiphi Tech on 11/05/2015.
//
//

#import "Location.h"


@implementation Location

//@synthesize latitude=_latitude;
//@synthesize longitude=_longitude;

@dynamic  latitude,longitude;



- (void)setLatitude:(NSString *)newName
{
    //[self willChangeValueForKey:@"latitude"];
    [self setPrimitiveValue:newName forKey:@"latitude"];
    [self didChangeValueForKey:@"latitude"];
}


-(void)setLongitude:(NSString *)longitude1{
    
    //[self willChangeValueForKey:@"longitude"];
    [self setPrimitiveValue:longitude1 forKey:@"longitude"];
    [self didChangeValueForKey:@"longitude"];
    
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.longitude forKey:@"longitude"];
    [encoder encodeObject:self.latitude forKey:@"latitude"];

}


- (Location*)initWithCoder:(NSCoder *)decoder {
   
    self = [super init];
   
    if (self) {
        
        
        self.longitude =[decoder decodeObjectForKey:@"longitude"];
        self.latitude =[decoder decodeObjectForKey:@"latitude"];
    
    }
    return self;

}


@end
