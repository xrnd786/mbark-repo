//
//  MessagesDetails.h
//  
//
//  Created by Xiphi Tech on 29/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Messages;

@interface MessagesDetails : NSManagedObject

@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * direction;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) Messages *messageThread;

@end
