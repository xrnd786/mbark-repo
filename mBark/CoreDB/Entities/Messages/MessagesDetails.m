//
//  MessagesDetails.m
//  
//
//  Created by Xiphi Tech on 29/05/2015.
//
//

#import "MessagesDetails.h"
#import "Messages.h"


@implementation MessagesDetails

@dynamic status;
@dynamic body;
@dynamic direction;
@dynamic createdOn;
@dynamic messageThread;

@end
