//
//  Messages.m
//  
//
//  Created by Xiphi Tech on 29/05/2015.
//
//

#import "Messages.h"
#import "MessagesDetails.h"


@implementation Messages

@dynamic appUserId;
@dynamic createdBy;
@dynamic deleted;
@dynamic linkedTo;
@dynamic linkedToRef;
@dynamic m_id;
@dynamic modifiedBy;
@dynamic modifiedOn;
@dynamic tenantId;
@dynamic subject;
@dynamic createdOn;
@dynamic messagesDetails;

@end
