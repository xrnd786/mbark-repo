//
//  Messages.h
//  
//
//  Created by Xiphi Tech on 29/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MessagesDetails;

@interface Messages : NSManagedObject

@property (nonatomic, retain) NSString * appUserId;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSNumber * isdeleted;
@property (nonatomic, retain) NSString * linkedTo;
@property (nonatomic, retain) NSString* linkedToRef;
@property (nonatomic, retain) NSString * m_id;
@property (nonatomic, retain) NSString * modifiedBy;
@property (nonatomic, retain) NSString * modifiedOn;
@property (nonatomic, retain) NSString * tenantId;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSSet *messagesDetails;
@end

@interface Messages (CoreDataGeneratedAccessors)

- (void)addMessagesDetailsObject:(MessagesDetails *)value;
- (void)removeMessagesDetailsObject:(MessagesDetails *)value;
- (void)addMessagesDetails:(NSSet *)values;
- (void)removeMessagesDetails:(NSSet *)values;

@end
