//
//  ProductImages.m
//  
//
//  Created by Xiphi Tech on 14/05/2015.
//
//

#import "ProductImages.h"
#import "Products.h"


@implementation ProductImages

@dynamic image_id;
@dynamic image_url;

@end
