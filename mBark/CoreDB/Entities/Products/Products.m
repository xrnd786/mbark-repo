//
//  Products.m
//  
//
//  Created by Xiphi Tech on 22/06/2015.
//
//

#import "Products.h"
#import "ProductAttributes.h"
#import "ProductDocument.h"
#import "ProductImages.h"
#import "ProductThumbnail.h"
#import "Products.h"


@implementation Products

@dynamic applications;
@dynamic availableUnits;
@dynamic cartQty;
@dynamic discount;
@dynamic grouped;
@dynamic isDiscontinued;
@dynamic isFavourite;
@dynamic isFeatured;
@dynamic isInCart;
@dynamic isOutOfStock;
@dynamic measurementUnit;
@dynamic name;
@dynamic p_id;
@dynamic productCategory;
@dynamic productDescription;
@dynamic rate;
@dynamic shippedInDays;
@dynamic sKU;
@dynamic tags;
@dynamic type;
@dynamic attributes;
@dynamic document;
@dynamic images;
@dynamic related;
@dynamic thumbnail;

@end
