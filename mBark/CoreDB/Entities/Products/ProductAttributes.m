//
//  ProductAttributes.m
//  
//
//  Created by Xiphi Tech on 14/05/2015.
//
//

#import "ProductAttributes.h"
#import "Products.h"


@implementation ProductAttributes

@dynamic att_id;
@dynamic type;
@dynamic group;
@dynamic name;
@dynamic attributeValue;
@dynamic product_id;

@end
