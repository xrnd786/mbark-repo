//
//  ProductAttributes.h
//  
//
//  Created by Xiphi Tech on 14/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Products;

@interface ProductAttributes : NSManagedObject

@property (nonatomic, retain) NSString * att_id;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * group;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * attributeValue;
@property (nonatomic, retain) Products *product_id;

@end
