//
//  Products.h
//  
//
//  Created by Xiphi Tech on 22/06/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define JSON_RES_AppProducts_SKU @"SKU"
#define JSON_RES_AppProducts_Name @"Name"
#define JSON_RES_AppProducts_Type @"Type"
#define JSON_RES_AppProducts_Description @"Description"
#define JSON_RES_AppProducts_Category @"Category"
#define JSON_RES_AppProducts_AvailableUnits @"AvailableUnits"
#define JSON_RES_AppProducts_MeasurementUnit @"MeasurementUnit"
#define JSON_RES_AppProducts_Rate @"Rate"
#define JSON_RES_AppProducts_Discount @"Discount"
#define JSON_RES_AppProducts_Discontinued @"Discontinued"
#define JSON_RES_AppProducts_OutOfStock @"OutOfStock"
#define JSON_RES_AppProducts_ShippedInDays @"ShippedInDays"
#define JSON_RES_AppProducts_Featured @"Featured"
#define JSON_RES_AppProducts_Tags @"Tags"
#define JSON_RES_AppProducts_Applications @"Applications"
#define JSON_RES_AppProducts_Images @"Images"
#define JSON_RES_AppProducts_ImageUrls @"ImageUrls"
#define JSON_RES_AppProducts_Thumbnail @"Thumbnail"
#define JSON_RES_AppProducts_Thumbnail_Image @"Image"
#define JSON_RES_AppProducts_Documents @"Documents"
#define JSON_RES_AppProducts_RelatedProducts @"RelatedProducts"
#define JSON_RES_AppProducts_RelatedProducts_Thumbnail @"RelatedThumbnails"

#define JSON_RES_AppProducts_Attributes @"Attributes"
#define JSON_RES_AppProducts_AttributeValue @"AttributeValue"

#define JSON_RES_AppProducts_Value @"Value"
#define JSON_RES_AppProducts_Id @"Id"
#define JSON_RES_AppProducts_Group @"Group"
#define JSON_RES_AppProducts_GroupedProducts @"GroupedProducts"

#define JSON_RES_AppAttribute_Name @"AttributeName"
#define JSON_RES_AppAttribute_Values @"Values"


@class ProductAttributes, ProductDocument, ProductImages, ProductThumbnail, Products;

@interface Products : NSManagedObject

@property (nonatomic, retain) id applications;
@property (nonatomic, retain) NSNumber * availableUnits;
@property (nonatomic, retain) NSNumber * cartQty;
@property (nonatomic, retain) NSNumber * discount;
@property (nonatomic, retain) id grouped;
@property (nonatomic, retain) NSNumber * isDiscontinued;
@property (nonatomic, retain) NSNumber * isFavourite;
@property (nonatomic, retain) NSNumber * isFeatured;
@property (nonatomic, retain) NSNumber * isInCart;
@property (nonatomic, retain) NSNumber * isOutOfStock;
@property (nonatomic, retain) NSString * measurementUnit;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * p_id;
@property (nonatomic, retain) NSString * productCategory;
@property (nonatomic, retain) NSString * productDescription;
@property (nonatomic, retain) NSNumber * rate;
@property (nonatomic, retain) NSNumber * shippedInDays;
@property (nonatomic, retain) NSString * sKU;
@property (nonatomic, retain) NSString * tags;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSSet *attributes;
@property (nonatomic, retain) NSSet *document;
@property (nonatomic, retain) NSSet *images;
@property (nonatomic, retain) NSSet *related;
@property (nonatomic, retain) ProductThumbnail *thumbnail;
@end

@interface Products (CoreDataGeneratedAccessors)

- (void)addAttributesObject:(ProductAttributes *)value;
- (void)removeAttributesObject:(ProductAttributes *)value;
- (void)addAttributes:(NSSet *)values;
- (void)removeAttributes:(NSSet *)values;

- (void)addDocumentObject:(ProductDocument *)value;
- (void)removeDocumentObject:(ProductDocument *)value;
- (void)addDocument:(NSSet *)values;
- (void)removeDocument:(NSSet *)values;

- (void)addImagesObject:(ProductImages *)value;
- (void)removeImagesObject:(ProductImages *)value;
- (void)addImages:(NSSet *)values;
- (void)removeImages:(NSSet *)values;

- (void)addRelatedObject:(Products *)value;
- (void)removeRelatedObject:(Products *)value;
- (void)addRelated:(NSSet *)values;
- (void)removeRelated:(NSSet *)values;

@end
