//
//  ProductDocument.h
//  
//
//  Created by Xiphi Tech on 14/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Products;

@interface ProductDocument : NSManagedObject

@property (nonatomic, retain) NSString * p_id;
@property (nonatomic, retain) Products *doc_url;

@end
