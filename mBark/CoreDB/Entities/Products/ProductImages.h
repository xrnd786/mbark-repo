//
//  ProductImages.h
//  
//
//  Created by Xiphi Tech on 14/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Products;

@interface ProductImages : NSManagedObject

@property (nonatomic, retain) NSString * image_id;
@property (nonatomic, retain) Products *image_url;

@end
