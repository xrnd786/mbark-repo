//
//  LastSearch.h
//  
//
//  Created by Xiphi Tech on 03/06/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LastSearch : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * searchedOn;

@end
