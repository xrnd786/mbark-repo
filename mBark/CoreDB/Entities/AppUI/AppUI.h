//
//  AppUI.h
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AppUI : NSManagedObject

@property (nonatomic, retain) NSString * primaryColor;
@property (nonatomic, retain) NSString * secondaryColor;
@property (nonatomic, retain) NSString * backgroundColor;
@property (nonatomic, retain) NSString * menuColor;
@property (nonatomic, retain) NSString * primaryTextColor;
@property (nonatomic, retain) NSString * secondaryTextColor;
@property (nonatomic, retain) id generalFont;
@property (nonatomic, retain) id headerFont;
@property (nonatomic, retain) id smallFont;
@property (nonatomic, retain) NSString * favouriteColor;

@end
