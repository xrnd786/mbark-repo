//
//  AppUI.m
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import "AppUI.h"


@implementation AppUI

@dynamic primaryColor;
@dynamic secondaryColor;
@dynamic backgroundColor;
@dynamic menuColor;
@dynamic primaryTextColor;
@dynamic secondaryTextColor;
@dynamic generalFont;
@dynamic headerFont;
@dynamic smallFont;
@dynamic favouriteColor;

@end
