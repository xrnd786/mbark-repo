//
//  AppSettings.m
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import "AppSettings.h"


@implementation AppSettings

@dynamic productDisplayName;
@dynamic categoriesDisplayName;
@dynamic applicationDisplayName;
@dynamic appModel;
@dynamic registrationType;
@dynamic registrationRules;
@dynamic productRules;
@dynamic userRules;
@dynamic slider;
@dynamic app_id;
@dynamic deleted;
@dynamic tenantId;
@dynamic createdBy;
@dynamic modifiedBy;
@dynamic modifiedOn;

@end
