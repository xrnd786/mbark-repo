//
//  AppSettings.h
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AppSettings : NSManagedObject

@property (nonatomic, retain) NSString * productDisplayName;
@property (nonatomic, retain) NSString * categoriesDisplayName;
@property (nonatomic, retain) NSString * applicationDisplayName;
@property (nonatomic, retain) NSString * appModel;
@property (nonatomic, retain) NSString * registrationType;
@property (nonatomic, retain) id registrationRules;
@property (nonatomic, retain) id productRules;
@property (nonatomic, retain) id userRules;
@property (nonatomic, retain) id slider;
@property (nonatomic, retain) NSString * app_id;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * tenantId;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * modifiedBy;
@property (nonatomic, retain) NSDate * modifiedOn;

@end
