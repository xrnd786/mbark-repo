//
//  AttributeValue.m
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import "AttributeValue.h"


@implementation AttributeValue

@dynamic name;
@dynamic type;
@dynamic group;
@dynamic groupable;
@dynamic att_id;
@dynamic deleted;
@dynamic tenantId;
@dynamic createdBy;
@dynamic modifiedBy;
@dynamic modifiedOn;

@end
