//
//  AttributeValue.h
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AttributeValue : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * group;
@property (nonatomic, retain) NSString * groupable;
@property (nonatomic, retain) NSString * att_id;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * tenantId;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * modifiedBy;
@property (nonatomic, retain) NSDate * modifiedOn;

@end
