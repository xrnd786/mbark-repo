//
//  User.m
//  
//
//  Created by Xiphi Tech on 11/05/2015.
//
//

#import "User.h"


@implementation User

@dynamic addressLine1;
@dynamic addressLine2;
@dynamic contact;
@dynamic country;
@dynamic email;
@dynamic facebook;
@dynamic firstName;
@dynamic lastName;
@dynamic location;
@dynamic organization;
@dynamic password;
@dynamic pincode;
@dynamic state;
@dynamic twitter;
@dynamic u_id;
@dynamic username;
@dynamic gender;

@end
