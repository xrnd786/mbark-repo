//
//  User.h
//  
//
//  Created by Xiphi Tech on 11/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Location.h"

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * addressLine1;
@property (nonatomic, retain) NSString * addressLine2;
@property (nonatomic, retain) NSString * contact;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * facebook;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) id location;
@property (nonatomic, retain) NSString * organization;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * pincode;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * twitter;
@property (nonatomic, retain) NSString * u_id;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * gender;

@end
