//
//  Wishlist.h
//  
//
//  Created by Xiphi Tech on 09/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Wishlist : NSManagedObject

@property (nonatomic, retain) NSString * productId;
@property (nonatomic, retain) NSNumber * quantity;

@end
