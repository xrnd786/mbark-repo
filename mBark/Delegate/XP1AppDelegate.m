//
//
//  XP1AppDelegate.m
//  P1
//
//  Created by Xiphi Tech on 29/09/2014.
//
//

#import "XP1AppDelegate.h"

//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

#import <Appirater/Appirater.h>

#import "AppSettingDefaults.h"
#import "AppUserDefaults.h"
#import "AppTenantDefaults.h"
#import "AppCategoryDefaults.h"
#import "AppUIDefaults.h"

#import "Messages.h"
#import "MessagesDetails.h"

#import "AuthChoiceController.h"

#import "IdentifyUser.h"
#import "TenantCall_Response.h"
#import "CategoryCall_Response.h"
#import "AttributeCall_Response.h"
#import "ProductCall_Response.h"
#import "MessagesCall_Response.h"

#import "Home.h"
#import "DEMOMenuViewController.h"

#import "Search.h"
#import "CartListViewController.h"
#import "StoreLocatorViewController.h"
#import "UIViewController+REFrostedViewController.h"
#import "LeveyTabBarController.h"

#import "HomeCustomNavBarController.h"
#import "HomeCustomNavigationBar.h"
#import "UIBarButtonItem+Badge.h"

#import "MainViewController.h"
#import "MasterTab.h"
#import "ChatViewController.h"

#import "MessageToast.h"

#import "ProductCategory.h"
#import "User.h"

#define kTabBarHeight 49.0f
#define kTopBarHeight 49.0f
#define kTabBariPadHeight 49.0f

#define kControlleriPadWidth 768.0f
#define kControlleriPhoneWidth 320.0f

#define MyAppID @"966464334"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define mBark_DataBase_sqlite @"mBark.sqlite"
#define mBark_DataBase_coredata @"mBark"

@interface XP1AppDelegate()<REFrostedViewControllerDelegate,UINavigationControllerDelegate>
{
    NSTimeInterval elapsedTime;

}

@property (strong,nonatomic) LeveyTabBarController *viewController;
@property (strong,nonatomic) Home *home;


@end

@implementation XP1AppDelegate

@synthesize viewController,home;
@synthesize managedObjectContext=_managedObjectContext;
@synthesize managedObjectModel=_managedObjectModel;
@synthesize persistentStoreCoordinator=_persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self configureAppirater];
    
    [application setApplicationIconBadgeNumber:0];
    
    [self reachabilitySetUp];
   // [Fabric with:@[CrashlyticsKit]];
    
    [GlobalCall removeOberverWithNotifier:APINotification_Tenant_Saved handlerClass:self withObject:nil];
    [GlobalCall addObserverWithNotifier:APINotification_Tenant_Saved andSelector:@selector(valueSaved) handlerClass:self withObject:nil];
    
    NSLog(@"Products list %@",[[[GlobalCall getDelegate] fetchStatementWithEntity:[Products class] withSortDescriptor:nil withPredicate:nil] valueForKeyPath:@"productCategory"]);
    
    self.viewController=[[LeveyTabBarController alloc]init];
    
    if ([AppTenantDefaults getToken] ==nil) {
    
        [AppTenantDefaults saveDeviceToken:@""];
        
        if(IS_OS_8_OR_LATER) {
           
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes: (UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert) categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            //register to receive notifications
            UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
        }
        
    }
    
   
    NSString *uId=((User*)[[self fetchStatementWithEntity:[User class] withSortDescriptor:@"u_id" withPredicate:nil] lastObject]).u_id;
    
    
    if (!uId) {
    
        [self performSelectorInBackground:@selector(saveDefaultUI) withObject:nil];
        
        MainViewController *main=[[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
        self.window.rootViewController=[[UINavigationController alloc]initWithRootViewController:main];
        [self performSelectorInBackground:@selector(callWebServices) withObject:nil];
        
    }else{
      
        //[self performSelectorInBackground:@selector(makeUsualCall) withObject:nil];
        
        [self loadNow];
       
    }
    
    
    [self.window makeKeyAndVisible];
    
    [Appirater appLaunched:YES];
    
    return YES;

}


-(void)reachabilitySetUp{
   
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    //hostReach = [Reachability reachabilityWithHostname: @"www.apple.com"];
    //[hostReach startNotifier];
    //[self updateInterfaceWithReachability: hostReach];
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    //wifiReach = [Reachability reachabilityForLocalWiFi];
    //[wifiReach startNotifier];
    //[self updateInterfaceWithReachability: wifiReach];
    
}


-(void)configureAppirater{
   
    [Appirater setAppId:MyAppID];
    [Appirater setDaysUntilPrompt:-1];
    [Appirater setUsesUntilPrompt:3];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:10];
    [Appirater setDebug:NO];
    
}

-(void)saveDefaultUI{
    
  [AppUIDefaults saveAppUIDetails:[[NSDictionary alloc]init]];

}

-(void)callWebServices{
    
   
    dispatch_queue_t Web_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
        dispatch_sync(Web_Call_queue, ^{
            
            
            //**********************USER IDENTIFICATION**************//
            NSLog(@"User Identification Started");
            [IdentifyUser addUserForStart];
            NSLog(@"User Identification Ended");
            
            [self makeUsualCall];
            
          
        });
    
}

-(void)makeUsualCall{
    //**********************TENANT CALL**************//
    NSLog(@"Tenant Call Started");
    [TenantCall_Response callApiForTenantProfile];
    NSLog(@"Tenant Call Ended");
    
    
    
    //**********************CATEGORY**************//
    dispatch_group_t groupCategory = dispatch_group_create();
    dispatch_group_async(groupCategory,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             NSLog(@"Category Call Started");
                             [CategoryCall_Response callApiForCategory];
                         });
    
    dispatch_group_notify(groupCategory,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
        
        NSLog(@"Category Call Ended");
        
        [[NSNotificationCenter defaultCenter]postNotificationName:APINotification_Home_Done object:nil userInfo:nil];
        
        
        //**********************PRODUCTS**************//
        dispatch_queue_t Prod_Call_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
        dispatch_sync(Prod_Call_queue, ^ {
            
           NSLog(@"Products Call Started");
           NSArray  *categories=[[NSArray alloc]initWithArray:[self fetchStatementWithEntity:[ProductCategory class] withSortDescriptor:nil withPredicate:nil]] ;
            
            for (ProductCategory *category in categories)
            {
                NSString *string=category.name;
                NSSet *children=category.children;
                
                int j=0;
                for (NSString *innerValues in children) {
                    
                    [ProductCall_Response callApiForProductsForCategory:innerValues];
                    j++;
                    
                }
                
                [ProductCall_Response callApiForProductsForCategory:string];
            }
            
            [GlobalCall removeOberverWithNotifier:APINotification_Server_Error handlerClass:self withObject:nil];
            [GlobalCall addObserverWithNotifier:APINotification_Server_Error andSelector:@selector(firstTimeServerError:) handlerClass:self withObject:nil];
            
        });
        
        
    });
    
    
    //**********************FEATURED**************//
    dispatch_group_t groupFeatured = dispatch_group_create();
    dispatch_group_async(groupFeatured,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             NSLog(@"Featured Call Started");
                             [ProductCall_Response callApiForProductsFeatured];
                         });
    dispatch_group_notify(groupFeatured,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
        NSLog(@"Featured Call Ended");
        [[NSNotificationCenter defaultCenter]postNotificationName:APINotification_Home_Done object:nil userInfo:nil];
    });
    
    
    
    //**********************ATTRIBUTE**************//
    /* dispatch_group_t groupAttribute = dispatch_group_create();
    dispatch_group_async(groupAttribute,dispatch_get_global_queue
                         (DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                             NSLog(@"Attribute Call Started");
                             [AttributeCall_Response callApiForAttributeList];
                         });*/
    
    
}


#pragma mark - NOTIFICATION RESPONSE

-(void)firstTimeServerError:(NSNotification*)notification{
    
    //[SVProgressHUD dismiss];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD dismiss];
        /*
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self.window.rootViewController title:@"Request failure" subTitle:@"Sorry, We are facing server error. Please try again later" closeButtonTitle:@"Ok" duration:0.0f];
        */
        
    });
    
}


-(void)valueSaved{
    
    NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"ProductSetup" ofType:@"plist"];
    NSLog(@"Print Path %@",plistPath);
   
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //[self loadNow];
            
        });
        
    }else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
           //self.window.rootViewController=[[DomainViewController alloc]init];
            
        });
        
    }
    
    
}



-(void)loadNow{
   
    [self loadControllers];
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [GlobalCall getMyWidth], 20)];
    view.backgroundColor=[[ThemeManager sharedManager]colorForKey:@"primaryColor"];
    [self.viewController.view addSubview:view];
    
    self.window.rootViewController =self.viewController;

   // [[MasterTab alloc]init];
}

-(void)loadControllers{
    
    
    self.home = [[Home alloc] init];
   
    HomeCustomNavBarController *homeNavigator=[[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"menu" rightImageKey:@"notification"withController:self.home withTitle:@""];
    [homeNavigator pushViewController:self.home animated:YES];
    homeNavigator.delegate=(id<UINavigationControllerDelegate>)self;
    
    
    DEMOMenuViewController *menuController = [[DEMOMenuViewController alloc] init];
    menuController.delegate=(id<selectFromLeft>)self.home;
    
    REFrostedViewController *frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:homeNavigator menuViewController:menuController];
    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleLight;
    frostedViewController.liveBlur = YES;
    frostedViewController.delegate = (id<REFrostedViewControllerDelegate>)self.home;
    
    HomeCustomNavBarController *navigateFrosted = [[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"menu" rightImageKey:@"notification"withController:frostedViewController withTitle:@""];
    [navigateFrosted pushViewController:frostedViewController animated:YES];
    navigateFrosted.navigationBarHidden=TRUE;
    
   
    Search *search=[Search new];
    CartListViewController *selected=[CartListViewController new];
    StoreLocatorViewController *store=[StoreLocatorViewController new];
    
    
    HomeCustomNavBarController *navigateSearch = [[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"back" rightImageKey:@"" withController:search withTitle:@""];
    navigateSearch.navigationBarHidden=TRUE;
    [navigateSearch pushViewController:search animated:YES];
    navigateSearch.delegate=self;
    
    
    HomeCustomNavBarController *navigateCart = [[HomeCustomNavBarController alloc]initForHomeWithLeftImageKey:@"back" rightImageKey:@"" withController:selected withTitle:@""];
    [navigateCart pushViewController:selected animated:YES];
    navigateCart.navigationBarHidden=FALSE;
    navigateCart.delegate=self;
    
    HomeCustomNavBarController *navigateStore = [[HomeCustomNavBarController alloc]initWithTitle:@"Stores" WithController:store];
    [navigateStore pushViewController:store animated:YES];
    navigateStore.navigationBarHidden=FALSE;
    navigateStore.delegate=self;
    
    
    NSArray *controllersList = [NSArray arrayWithObjects:navigateFrosted,navigateSearch,navigateStore,navigateCart,nil];
    
    NSMutableDictionary *homeImages = [NSMutableDictionary dictionaryWithCapacity:3];
    [homeImages setObject:[UIImage imageNamed:@"home.png"] forKey:@"Default"];
    [homeImages setObject:[UIImage imageNamed:@"home.png"] forKey:@"Highlighted"];
    [homeImages setObject:[UIImage imageNamed:@"home.png"] forKey:@"Selected"];
    
    
    NSMutableDictionary *searchImages = [NSMutableDictionary dictionaryWithCapacity:3];
    [searchImages setObject:[UIImage imageNamed:@"search.png"] forKey:@"Default"];
    [searchImages setObject:[UIImage imageNamed:@"search.png"] forKey:@"Highlighted"];
    [searchImages setObject:[UIImage imageNamed:@"search.png"] forKey:@"Selected"];
    
    
    NSMutableDictionary *selectionImages = [NSMutableDictionary dictionaryWithCapacity:3];
    [selectionImages setObject:[UIImage imageNamed:@"myBag.png"] forKey:@"Default"];
    [selectionImages setObject:[UIImage imageNamed:@"myBag.png"] forKey:@"Highlighted"];
    [selectionImages setObject:[UIImage imageNamed:@"myBag.png"] forKey:@"Selected"];
    
    
    NSMutableDictionary *locator = [NSMutableDictionary dictionaryWithCapacity:3];
    [locator setObject:[UIImage imageNamed:@"store.png"] forKey:@"Default"];
    [locator setObject:[UIImage imageNamed:@"store.png"] forKey:@"Highlighted"];
    [locator setObject:[UIImage imageNamed:@"store.png"] forKey:@"Selected"];
    
    
    NSMutableArray *labels = [[NSMutableArray alloc]initWithObjects:@"Home",@"Search",@"Stores",@"  Cart", nil];
    
    NSArray *imagesList = [NSArray arrayWithObjects:homeImages,searchImages,locator,selectionImages, nil];
    
    self.viewController=[[LeveyTabBarController alloc]initWithImageArray:imagesList labelsArray:labels withViewController:controllersList];
    self.viewController.delegate = (id<LeveyTabBarControllerDelegate>)search;
    
}



#pragma mark - delegate navigationController
- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)ViewController animated:(BOOL)animated
{
    
    
    if ([ViewController respondsToSelector:@selector(willAppearIn:)])
        [ViewController performSelector:@selector(willAppearIn:) withObject:navController];
    
}


#pragma mark - push notification

- (BOOL) pushNotificationOnOrOff
{
    if ([UIApplication instancesRespondToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        return ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]);
    } else {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        return (types & UIRemoteNotificationTypeAlert);
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application   didRegisterUserNotificationSettings:   (UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
    
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    
  //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }

}
#endif

- (void)application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    NSLog(@"App state Active%d",app.applicationState==UIApplicationStateActive);
    NSLog(@"App state Bg%d",app.applicationState==UIApplicationStateBackground);
    NSLog(@"Notifixation userinfo %@",userInfo);
    
    
    [app setApplicationIconBadgeNumber:0];
    
    if ([[[userInfo valueForKey:@"aps"] valueForKey:@"category"] isEqualToString:RemoteNotification_Message]) {
        
        
        NSArray *messages=[[GlobalCall getDelegate]fetchStatementWithEntity:[Messages class] withSortDescriptor:@"createdOn" withPredicate:[NSPredicate predicateWithFormat:@"isdeleted==0"]];
        
        
        for (Messages *thread in messages) {
            
            NSLog(@"Thread ID %@",thread.m_id);
            
            NSString *receivedThreadID=[[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"loc-args"] lastObject];
            
            NSLog(@"Received thread ID %@",receivedThreadID);
            
            
            if ([thread.m_id isEqualToString:receivedThreadID]) {
                
                Messages *message=[MessagesCall_Response callApiForGetMessageThread:thread.m_id];
                
                if ([app applicationState] == UIApplicationStateActive) {
                    
                    [[MessageToast new] showToastWithTitle:thread.subject   withSubtitle:((MessagesDetails*)[[message.messagesDetails allObjects] lastObject]).body];
                
                }else{
                    
                    ChatViewController *chatBox=[[ChatViewController alloc]initWithThreadObj:message];
                    [self.window.rootViewController presentViewController:chatBox animated:YES completion:^{
                        
                    }];
                    
                }
                
                completionHandler(UIBackgroundFetchResultNewData);
                
                
            }else{
                
                completionHandler(UIBackgroundFetchResultNoData);
                
            }
            
        }
        
    }
    
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //[MessagesCall_Response callApiForGetMessageThread: withSubject:];
    
    NSLog(@"Call fetch iCloud");
    completionHandler(UIBackgroundFetchResultNewData);
}




- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    NSString *deviceTokenString = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"device token is %@",deviceTokenString);
    [AppTenantDefaults saveDeviceToken:deviceTokenString];
    
    
}

-(void)applicationWillTerminate:(UIApplication *)application{
    
    [self saveContext];
    
}

#pragma mark - Reachability


- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    
    NetworkStatus remoteHostStatus = [curReach currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        
        NSLog(@"not reachable");
        
        SCLAlertView *viewHUD=[[SCLAlertView alloc]init];
        [viewHUD showError:self.window.rootViewController title:@"Network Connection" subTitle:@"Please check your internet connection" closeButtonTitle:@"Ok" duration:0.0f];
        
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        
        NSLog(@"wifi");
    
    }
    
    
    //else if (remoteHostStatus == ReachableViaCarrierDataNetwork) { NSLog(@"carrier");
    /*
    if(curReach == hostReach)
    {
        
        connectionRequiredHost= [curReach connectionRequired];
        
    }
    
    if(curReach == internetReach)
    {
        connectionRequiredinternetReach=[self configureTextField:curReach];
    }
    if(curReach == wifiReach)
    {
        connectionRequiredWifi=[self configureTextField:curReach];
        [self showViewWithReachability];
    }*/
    
}


-(void)showViewWithReachability{
    
    if (connectionRequiredHost || connectionRequiredinternetReach || connectionRequiredWifi) {
       
    }
    
}


- (BOOL) configureTextField:(Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=NO;
    //connectionRequired= [curReach connectionRequired];
    
    
    switch (netStatus)
    {
        case NotReachable:
        {
            connectionRequired=YES;
            //statusString = @"Access Not Available";
            //connectionRequired= NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
            connectionRequired=NO;
            //statusString = @"Reachable WWAN";
            break;
        }
        case ReachableViaWiFi:
        {
            connectionRequired=NO;
            //statusString= @"Reachable WiFi";
            break;
        }
        default:
        {
            return NO;
        }
    
    }
    
    return connectionRequired;
    
}


- (BOOL)connected
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus ;
    
}



#pragma mark- appdelegate

-(void)applicationWillEnterForeground:(UIApplication *)application{
    
    [Appirater appEnteredForeground:YES];
    
}


#pragma mark - core data functions

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        else
    
            NSLog( @"Data successfully inserted");
    
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"mBark" withExtension:@"mom" subdirectory:@"mBark.momd"];
    NSLog(@"Model url %@",modelURL);
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    //链接数据库，备数据存储
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:mBark_DataBase_sqlite];
    NSLog(@"storeURL%@",storeURL);
    
    NSError *error = nil;
    //持久化存储调度器由托管对象模型初始化
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    //设置数据存储方式为SQLITE
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}





#pragma mark - core data statement prepare

-(NSArray*)fetchStatementWithEntity:(Class)className withSortDescriptor:(NSString*)descriptor withPredicate:(NSPredicate*)predicate{
    
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc]init];
    NSEntityDescription *object=[NSEntityDescription entityForName:NSStringFromClass(className) inManagedObjectContext:[self managedObjectContext]];
    [fetch setEntity: object ];
    
    if (descriptor) {
        
        NSSortDescriptor *sort=[[NSSortDescriptor alloc]initWithKey:descriptor ascending:NO];
        NSLog(@"%@",sort);
        
        NSArray *sortarr=[[NSArray alloc]initWithObjects:sort, nil];
        [fetch setSortDescriptors:sortarr];
        
    }
    
    
    if (predicate) {
        
        [fetch setPredicate:predicate];
        
    }
    
    
    
    NSError *error;
    NSArray *fetchresult=[self.managedObjectContext executeFetchRequest:fetch error:&error];
    
    //NSLog(@"Fetch result for entity %@ is %@",NSStringFromClass(className),fetchresult);
    //User *returnedObject=[fetchresult lastObject];
    
    return fetchresult;
    
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    
    
    NSURL *url=[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    //NSLog(@"%@",url);
    //NSFileManager *file=[NSFileManager defaultManager];
    //NSLog(@"%@",file);
    
    NSLog(@"%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]);
    return url;
}



@end
