//
//  XP1AppDelegate.h
//  P1
//
//  Created by Xiphi Tech on 29/09/2014.
//
//

#import <UIKit/UIKit.h>


#define RemoteNotification_Message @"Message"

@class Reachability;

@interface XP1AppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability *internetReach;
    Reachability *hostReach;
    Reachability *wifiReach;
    
    BOOL connectionRequiredHost;
    BOOL connectionRequiredWifi;
    BOOL connectionRequiredinternetReach;
    
}

@property (strong, nonatomic) UIWindow *window;


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (BOOL)connected;
-(NSArray*)fetchStatementWithEntity:(Class)className withSortDescriptor:(NSString*)descriptor withPredicate:(NSPredicate*)predicate;


@end
